#!/usr/bin/env sh

FMT_WARNING="Please format the following files using 'flutter format':"
FMT_OUTPUT="$(flutter format -o none lib | head -n -1)"

CUPERTINO_WARNING="Please remove or change all occurrences of cupertino.dart in the followning files:"
CUPERTINO_OUTPUT="$(grep -R cupertino.dart lib | grep -v -e show)"

warning() {
    RED='\033[0;31m'
    NC='\033[0m'

    printf "%s\n\n%b%s%b\n" "$1" "$RED" "$2" "$NC"
}

test -z "$FMT_OUTPUT"       || warning "$FMT_WARNING"   "$FMT_OUTPUT"
test -z "$CUPERTINO_OUTPUT" || warning "$CUPERTINO_WARNING" "$CUPERTINO_OUTPUT"

# Return 1 on any warnings
test -z "$FMT_OUTPUT" && test -z "$CUPERTINO_OUTPUT" || exit 1

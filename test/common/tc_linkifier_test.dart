/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/foundation.dart';
import 'package:flutter_linkify/flutter_linkify.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:tc_frontend/common/widgets/tc_url_linkifier.dart';

/// Based on 'linkify' tests, see
/// https://github.com/Cretezy/linkify/blob/master/test/linkify_test.dart
void _expectListEqual(
  List<LinkifyElement> actual,
  List<LinkifyElement> expected,
) {
  expect(
    listEquals(
      actual,
      expected,
    ),
    true,
    reason: 'Expected $actual to be $expected',
  );
}

void t(String input, List<LinkifyElement> expected) => _expectListEqual(
    TCUrlLinkifier().parse(
      [TextElement(input)],
      LinkifyOptions(
        looseUrl: false,
        humanize: false,
        removeWww: false,
      ),
    ),
    expected);

void main() {
  group("TCUrlLinkifier tests", () {
    test("Match http:// URL", () {
      const url = 'http://eins.de';

      t(url, [
        UrlElement(url, url),
      ]);
    });

    test("Match https:// URL", () {
      const url = 'https://eins.de';

      t(url, [
        UrlElement(url, url),
      ]);
    });

    test("Match URL followed by blank", () {
      const url = 'https://eins.de';
      const blank = ' ';

      t('$url$blank', [
        UrlElement(url, url),
        TextElement(blank),
      ]);
    });

    test("Match URL followed by comma", () {
      const url = 'https://eins.de';
      const comma = ',';

      t('$url$comma', [
        UrlElement(url, url),
        TextElement(comma),
      ]);
    });

    test("Match URL followed by period", () {
      const url = 'https://eins.de';
      const period = '.';

      t('$url$period', [
        UrlElement(url, url),
        TextElement(period),
      ]);
    });

    test("Match URL followed by exclamation point", () {
      const url = 'https://eins.de';
      const bang = '!';

      t('$url$bang', [
        UrlElement(url, url),
        TextElement(bang),
      ]);
    });

    test("Match URL followed by question mark", () {
      const url = 'https://eins.de';
      const qm = '?';

      t('$url$qm', [
        UrlElement(url, url),
        TextElement(qm),
      ]);
    });

    test("Match multiple URLs separated with blanks", () {
      const url1 = 'https://eins.de';
      const url2 = 'https://zwei.com';
      const url3 = 'https://drei.xyz';

      t('$url1 $url2 $url3', [
        UrlElement(url1, url1),
        TextElement(' '),
        UrlElement(url2, url2),
        TextElement(' '),
        UrlElement(url3, url3),
      ]);
    });

    test("Match multiple URLs separated with commas", () {
      const url1 = 'https://eins.de';
      const word1 = ',';
      const url2 = 'https://zwei.com';
      const word2 = ',';
      const url3 = 'https://drei.xyz';

      t('$url1$word1$url2$word2$url3', [
        UrlElement(url1, url1),
        TextElement(word1),
        UrlElement(url2, url2),
        TextElement(word2),
        UrlElement(url3, url3),
      ]);
    });

    test("Match multiple URLs separated with text", () {
      const url1 = 'https://eins.de';
      const word1 = ' 123 ';
      const url2 = 'https://zwei.com';
      const word2 = ' abc ';
      const url3 = 'https://drei.xyz';

      t('$url1$word1$url2$word2$url3', [
        UrlElement(url1, url1),
        TextElement(word1),
        UrlElement(url2, url2),
        TextElement(word2),
        UrlElement(url3, url3),
      ]);
    });

    test("Match multiple URLs with no space between them", () {
      const url1 = 'https://eins.de';
      const url2 = 'https://zwei.com';
      const url3 = 'https://drei.xyz';

      t('$url1$url2$url3', [
        UrlElement(url1, url1),
        UrlElement(url2, url2),
        UrlElement(url3, url3),
      ]);
    });

    test("Do not match URL with misspelled protocol", () {
      const url = 'htps://test.de';

      t(url, [
        TextElement(url),
      ]);
    });

    test("Match 'www.test.com'", () {
      const input = 'www.test.com';

      t(input, [
        UrlElement(input, input),
      ]);
    });

    test("Only match 'www.test.com' in 'wwww.test.com'", () {
      const input = 'wwww.test.com';

      t(input, [
        TextElement('w'),
        UrlElement(input.substring(1), input.substring(1)),
      ]);
    });

    test("Do not match 'ww.test.com'", () {
      const input = 'ww.test.com';

      t(input, [
        TextElement(input),
      ]);
    });

    test("Do not match 'test.com'", () {
      const input = 'test.com';

      t(input, [
        TextElement(input),
      ]);
    });

    test("Match 'moodle.thm.de'", () {
      const input = 'moodle.thm.de';

      t(input, [
        UrlElement(input),
      ]);
    });

    test("Do not match empty string", () {
      const input = '';

      t(input, [
        TextElement(input),
      ]);
    });

    test("Do not match blank string", () {
      const input = '   ';

      t(input, [
        TextElement(input),
      ]);
    });

    test("Do not match '...'", () {
      const input = '...';

      t(input, [
        TextElement(input),
      ]);
    });

    test("Do not match URL without protocol followed by period", () {
      const url = 'eins.de';
      const period = '.';

      t('$url$period', [
        TextElement('$url$period'),
      ]);
    });

    test("Match URL without protocol starting with 'www' followed by period",
        () {
      const url = 'www.eins.de';
      const period = '.';

      t('$url$period', [
        UrlElement(url),
        TextElement(period),
      ]);
    });

    test("Match URL without protocol starting with 'www' surrounded by periods",
        () {
      const period = '.';
      const url = 'www.eins.de';

      t('$period$url$period', [
        TextElement(period),
        UrlElement(url),
        TextElement(period),
      ]);
    });

    test("Match URL following colon", () {
      const text = 'hey check this out:';
      const url = 'https://www.eins.de';

      t('$text$url', [
        TextElement(text),
        UrlElement(url),
      ]);
    });

    test("Match URL following question mark", () {
      const text = 'did you mean this?';
      const url = 'https://www.eins.de';

      t('$text$url', [
        TextElement(text),
        UrlElement(url),
      ]);
    });

    test("Do not match email address", () {
      const input = 'test@test.com';

      t(input, [
        TextElement(input),
      ]);
    });

    test("Do not match complex email address", () {
      const input = 'tiny.campus@sub.domain.tld';

      t(input, [
        TextElement(input),
      ]);
    });

    test("Do not match mailto: URI", () {
      const input = 'mailto:test@test.de';

      t(input, [
        TextElement(input),
      ]);
    });

    test("Do not match ftp:// URL", () {
      const input = 'ftp://test.de';

      t(input, [
        TextElement(input),
      ]);
    });

    test("Match emoji URL", () {
      const input = 'https://💩.la';

      t(input, [
        UrlElement(input, input),
      ]);
    });

    test("Do not match emoji URL without protocol", () {
      const input = '💩.la';

      t(input, [
        TextElement(input),
      ]);
    });

    test("Match URL with query parameters", () {
      const url = 'https://test.com?query=test&also=too';

      t(url, [
        UrlElement(url, url),
      ]);
    });

    test("Match URL with URL-encoded query parameters", () {
      const url = 'https://test.com?query=test&also=too%20much';

      t(url, [
        UrlElement(url, url),
      ]);
    });

    test("Match IPv4 address", () {
      const url = 'http://127.0.0.1';

      t(url, [
        UrlElement(url, url),
      ]);
    });

    test("Match IPv4 address with port", () {
      const url = 'http://127.0.0.1:8080';

      t(url, [
        UrlElement(url, url),
      ]);
    });

    test("Do not match IPv4 address without protocol", () {
      const url = '127.0.0.1';

      t(url, [
        TextElement(url),
      ]);
    });
  }, skip: true);
}

/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/foundation.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:tc_frontend/pages/home/model/module.dart';

void main() {
  group("ModuleModel constructor test", () {
    test("Does not allow id to be zero", () {
      expect(
        () => Module(
          id: 0,
          name: "Some",
          description: "invalid",
          pathToImage: "/module",
          onTapPath: "/model",
        ),
        kDebugMode ? throwsAssertionError : throwsArgumentError,
      );
    });
  });

  group("ModuleModel method test", () {
    test("Toggle works as intended", () {
      expect(
        Module(
          id: 1,
          name: "Some",
          description: "valid",
          pathToImage: "/module",
          onTapPath: "/model",
        ).toToggled(),
        equals(
          Module(
            id: 1,
            name: "Some",
            description: "valid",
            pathToImage: "/module",
            onTapPath: "/model",
            enabled: false,
          ),
        ),
      );
    });
  });
}

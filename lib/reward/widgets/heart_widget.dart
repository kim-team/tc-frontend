/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';

import '../model/likable.dart';

class HeartWidget extends StatelessWidget {
  final Likable likable;
  final double fontSize;
  final double iconSize;
  final VoidCallback? toggleLike;

  const HeartWidget({
    Key? key,
    required this.likable,
    this.fontSize = 17,
    this.iconSize = 22,
    this.toggleLike,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => Row(
        children: <Widget>[
          Text(
            '${likable.likes}',
            style: Theme.of(context)
                .textTheme
                .bodyText2
                ?.copyWith(color: Colors.grey),
          ),
          IconButton(
            padding: EdgeInsets.symmetric(vertical: 0, horizontal: 1),
            visualDensity: VisualDensity.compact,
            icon: Icon(
              likable.isLiked ? Icons.favorite : Icons.favorite_border,
              size: iconSize,
            ),
            color: likable.isLiked ? Colors.red : Colors.grey,
            onPressed: toggleLike,
          ),
        ],
      );
}

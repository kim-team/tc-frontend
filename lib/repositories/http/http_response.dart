/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

part of 'http_repository.dart';

/// Provides a simple way to check and match a
/// [Response.statusCode] to an [HttpException].
extension _HttpResponseStatus on Response {
  /// List of every [HttpException] directly dependent on [statusCode].
  void _checkResponseStatus() {
    final uri = request?.url;
    final message = "StatusCode: $statusCode Header: $headers Body: $body";
    switch (statusCode) {
      case 200:
      case 201:
      case 202:
      case 204:
        return;
      case 400:
        throw BadRequestException(message, uri);
      case 401:
        throw UnauthorizedException(message, uri);
      case 403:
        throw ForbiddenException(message, uri);
      case 404:
        throw NotFoundException(message, uri);
      case 405:
        throw MethodNotAllowedException(message, uri);
      case 429:
        throw TooManyRequestsException(message, uri);
      case 500:
        throw InternalServerErrorException(message, uri);
      case 501:
        throw NotImplementedException(message, uri);
      default:
        throw HttpRepoException(message, uri);
    }
  }
}

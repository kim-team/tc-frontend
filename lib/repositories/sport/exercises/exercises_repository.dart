/*
 * Copyright 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
import 'dart:async';
import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../common/assets_adapter.dart';
import '../../../common/constants/api_constants.dart';
import '../../../common/styled_print.dart';
import '../../../modules/sport/model/exercise.dart';
import '../../../modules/sport/model/sport_model.dart';
import '../../../modules/sport/model/sport_tab_model.dart';
import '../../http/http_repository.dart';
import 'exercises_repository_constants.dart';

const Duration _updateIntervallThreshold = Duration(seconds: 5);

// Maybe change Repo in such a way that the Exercise list is stored in ram
// and only use shared prefs as a fallback for that.
// This is due to potential performance issues concerning repeated
// deserialization of said exercises
// Also maybe change repo to make use of isolates for parsing
class ExerciseRepository {
  static final ExerciseRepository _singleton = ExerciseRepository._internal();

  factory ExerciseRepository() => _singleton;

  final HttpRepository _httpRepository;

  ExerciseRepository._internal() : _httpRepository = HttpRepository();

  final _prefs = SharedPreferences.getInstance;

  bool loadingComplete = false;

  List<Exercise> exerciseList = <Exercise>[];
  List<SportTabModel> sportTabList = <SportTabModel>[];

  late SportModel sportModel;

  Future<SportModel> getExercises() async {
    Future<String> payload;
    // check if there is already a cached instance of exercise list
    // if not, try fetching one from the server.getExercises
    // if that fails use local fallback file
    // SharedPreferences.getInstance().then((sp) {
    //   sp.remove(sportUpdateTimestampKey);
    // });
    if ((await _prefs()).containsKey(sportUpdateTimestampKey)) {
      var lastUpdate = DateTime.tryParse(
              (await _prefs()).getString(sportUpdateTimestampKey)!) ??
          DateTime.fromMillisecondsSinceEpoch(1);
      // if difference between last update and now is larger
      // than _updateIntervallThreshold try to fetch new list
      if (DateTime.now()
              .difference(lastUpdate)
              .compareTo(_updateIntervallThreshold) >=
          0) {
        sprint(
          "[EXERCISES] Preparing request to server",
          style: PrintStyle.sport,
          prefix: '[SPORT]',
        );
        payload = fetchDataFromServer();
      } else {
        sprint(
          "[EXERCISES] Load exercises from cache",
          style: PrintStyle.sport,
          prefix: '[SPORT]',
        );
        payload = fetchDataFromCache();
      }
    } else {
      /// [TODO] When does this condition trigger? Confusing
      sprint(
        "[EXERCISES] What is happening here?",
        style: PrintStyle.sport,
        prefix: '[SPORT]',
      );
      payload = fetchDataFromServer();
    }
    // parsing here:
    return SportModel.fromJson(jsonDecode(await payload));
  }

  Future<String> fetchDataFromServer() async {
    sprint(
      "[EXERCISES] Downloading data",
      style: PrintStyle.sport,
      prefix: '[SPORT]',
    );

    try {
      final payload = utf8.decode(
          (await _httpRepository.get(sportExercisesUrl, secured: false))
              .bodyBytes);
      sprint(
        "[EXERCISES] Download complete",
        style: PrintStyle.sport,
        prefix: '[SPORT]',
      );
      return await saveDataToCache(payload);
    } on Exception catch (e) {
      sprint(
        "[EXERCISES][fetchDataFromServer]"
        "Error while downloading data: ${e.toString()}",
        style: PrintStyle.sport,
        prefix: '[SPORT]',
      );
      return fetchDataFromCache();
    }
  }

  Future<String> fetchDataFromCache() => _prefs().then<String>((prefs) {
        // Use cache in case no connection could be established
        if (prefs.containsKey(sportExercisePrefsKey)) {
          return prefs.getString(sportExercisePrefsKey)!;
        } else {
          // in the event there hasn't been already a connection
          // established, use local fall back
          return fetchDataFromFallback();
        }
      });

  Future<String> fetchDataFromFallback() => rootBundle.loadString(
        AssetAdapter.exercisesJson(),
        cache: true,
      );

  Future<String> saveDataToCache(String payload) => _prefs().then<String>(
      (pr) => pr
              .setString(sportExercisePrefsKey, payload)
              .whenComplete(() => pr.setString(
                  sportUpdateTimestampKey, DateTime.now().toIso8601String()))
              .then((_) {
            sprint(
              "[EXERCISES] Cache saved",
              style: PrintStyle.sport,
              prefix: '[SPORT]',
            );
            return payload;
          }));

/*Future<DateTime> getTimestamp() => _prefs()
      .then<String>((prefs) => prefs.getString(sportUpdateTimestampKey) ?? "")
      .then<DateTime>(DateTime.tryParse);

  Future<List<Exercise>> getExercisesFromCache() => fetchDataFromCache()
      .then<List<Exercise>>((src) => (jsonDecode(src) as List<dynamic>)
          .map((e) => Exercise.fromJson(e))
          .toList());*/
}

// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cafeteria_settings.dart';

// **************************************************************************
// CopyWithGenerator
// **************************************************************************

extension CafeteriaSettingsCopyWith on CafeteriaSettings {
  CafeteriaSettings copyWith({
    List<Allergens>? allergens,
    List<DislikedIngredients>? dislikedIngredients,
    List<String>? favorites,
    CafeteriaFilter? filter,
    bool? hasDoneWizard,
    PriceClass? priceClass,
    bool? showTestApi,
  }) {
    return CafeteriaSettings(
      allergens: allergens ?? this.allergens,
      dislikedIngredients: dislikedIngredients ?? this.dislikedIngredients,
      favorites: favorites ?? this.favorites,
      filter: filter ?? this.filter,
      hasDoneWizard: hasDoneWizard ?? this.hasDoneWizard,
      priceClass: priceClass ?? this.priceClass,
      showTestApi: showTestApi ?? this.showTestApi,
    );
  }
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CafeteriaSettings _$CafeteriaSettingsFromJson(Map json) => CafeteriaSettings(
      hasDoneWizard: json['hasDoneWizard'] as bool? ?? false,
      priceClass:
          $enumDecodeNullable(_$PriceClassEnumMap, json['priceClass']) ??
              PriceClass.student,
      favorites: (json['favorites'] as List<dynamic>?)
              ?.map((e) => e as String)
              .toList() ??
          const [],
      filter: $enumDecodeNullable(_$CafeteriaFilterEnumMap, json['filter']) ??
          CafeteriaFilter.none,
      allergens: (json['allergens'] as List<dynamic>?)
              ?.map((e) => $enumDecode(_$AllergensEnumMap, e))
              .toList() ??
          const [],
      dislikedIngredients: (json['dislikedIngredients'] as List<dynamic>?)
              ?.map((e) => $enumDecode(_$DislikedIngredientsEnumMap, e))
              .toList() ??
          const [],
      showTestApi: json['showTestApi'] as bool? ?? false,
    );

Map<String, dynamic> _$CafeteriaSettingsToJson(CafeteriaSettings instance) =>
    <String, dynamic>{
      'hasDoneWizard': instance.hasDoneWizard,
      'priceClass': _$PriceClassEnumMap[instance.priceClass],
      'filter': _$CafeteriaFilterEnumMap[instance.filter],
      'favorites': instance.favorites,
      'allergens':
          instance.allergens.map((e) => _$AllergensEnumMap[e]).toList(),
      'showTestApi': instance.showTestApi,
      'dislikedIngredients': instance.dislikedIngredients
          .map((e) => _$DislikedIngredientsEnumMap[e])
          .toList(),
    };

const _$PriceClassEnumMap = {
  PriceClass.student: 'student',
  PriceClass.staff: 'staff',
  PriceClass.guest: 'guest',
};

const _$CafeteriaFilterEnumMap = {
  CafeteriaFilter.none: 'none',
  CafeteriaFilter.vegetarian: 'vegetarian',
  CafeteriaFilter.vegan: 'vegan',
};

const _$AllergensEnumMap = {
  Allergens.gluten: 'gluten',
  Allergens.crustacean: 'crustacean',
  Allergens.egg: 'egg',
  Allergens.fish: 'fish',
  Allergens.peanut: 'peanut',
  Allergens.soy: 'soy',
  Allergens.milk: 'milk',
  Allergens.nuts: 'nuts',
  Allergens.celery: 'celery',
  Allergens.mustard: 'mustard',
  Allergens.sesame: 'sesame',
  Allergens.sulfur: 'sulfur',
  Allergens.lupines: 'lupines',
  Allergens.cthulu: 'cthulu',
};

const _$DislikedIngredientsEnumMap = {
  DislikedIngredients.fish: 'fish',
  DislikedIngredients.poultry: 'poultry',
  DislikedIngredients.lamb: 'lamb',
  DislikedIngredients.beef: 'beef',
  DislikedIngredients.pork: 'pork',
  DislikedIngredients.alcohol: 'alcohol',
  DislikedIngredients.gelatin: 'gelatin',
  DislikedIngredients.spicy: 'spicy',
  DislikedIngredients.garlic: 'garlic',
};

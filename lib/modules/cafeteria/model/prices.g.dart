// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'prices.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Prices _$PricesFromJson(Map json) => Prices(
      json['staff'] as String? ?? "k.A.",
      json['guest'] as String? ?? "k.A.",
      json['student'] as String? ?? "k.A.",
    );

Map<String, dynamic> _$PricesToJson(Prices instance) => <String, dynamic>{
      'staff': instance.staff,
      'guest': instance.guest,
      'student': instance.student,
    };

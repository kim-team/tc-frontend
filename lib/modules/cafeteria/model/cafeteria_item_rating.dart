/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:copy_with_extension/copy_with_extension.dart';
import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'cafeteria_item_rating.g.dart';

/// A model for a rating for a [CafeteriaItem]
@CopyWith()
@JsonSerializable(anyMap: true, explicitToJson: true)
class CafeteriaItemRating extends Equatable {
  /// the average rating of an item
  final double avgRating;

  /// the amount of ratings for an item
  final int ratings;

  final int userRating;

  const CafeteriaItemRating(
      {required this.avgRating,
      required this.ratings,
      required this.userRating});

  const CafeteriaItemRating.initialData()
      : avgRating = 0.0,
        ratings = 0,
        userRating = -1;

  factory CafeteriaItemRating.fromJson(Map<String, dynamic> json) =>
      _$CafeteriaItemRatingFromJson(json);

  Map<String, dynamic> toJson() => _$CafeteriaItemRatingToJson(this);

  @override
  List<Object> get props => [avgRating, ratings, userRating];
}

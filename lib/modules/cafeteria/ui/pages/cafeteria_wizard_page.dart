/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/widgets/I18nText.dart';

import '../../../../common/constants/routing_constants.dart';
import '../../../../common/tc_theme.dart';
import '../../bloc/cafeteria_settings/cafeteria_settings_bloc.dart';
import '../../model/cafeteria_settings.dart';
import 'cafeteria_order_page.dart';
import 'wizard/cafeteria_wizard_allergen_page.dart';
import 'wizard/cafeteria_wizard_cafeteriaselect_page.dart';
import 'wizard/cafeteria_wizard_dislikes_page.dart';
import 'wizard/cafeteria_wizard_filter_page.dart';
import 'wizard/cafeteria_wizard_price_page.dart';

/// A page that holds a [TabBarView] for the different pages for
/// the setup Wizard
class CafeteriaWizardPage extends StatefulWidget {
  const CafeteriaWizardPage({Key? key}) : super(key: key);

  @override
  _CafeteriaWizardPageState createState() => _CafeteriaWizardPageState();
}

class _CafeteriaWizardPageState extends State<CafeteriaWizardPage>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;
  int _currentIndex = 0;

  @override
  void initState() {
    _tabController = TabController(
      initialIndex: _currentIndex,
      length: 6,
      vsync: this,
    );
    _tabController.addListener(setIndex);
    super.initState();
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        backgroundColor: CorporateColors.passiveBackgroundLight,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(160),
          child: Container(
            decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Colors.grey,
                  offset: Offset(0.0, 1.0), //(x,y)
                  blurRadius: 6.0,
                ),
              ],
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                IconButton(
                  icon: Icon(Icons.arrow_back_ios),
                  onPressed: () => Navigator.of(context).pop(context),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(10, 30, 10, 15),
                  child: Stack(
                    alignment: AlignmentDirectional.center,
                    children: [
                      SizedBox(
                        height: 75.0,
                        width: 75.0,
                        child: CircularProgressIndicator(
                          strokeWidth: 8.0,
                          backgroundColor: Color.fromRGBO(230, 230, 230, 1.0),
                          valueColor: AlwaysStoppedAnimation<Color>(
                              Color.fromRGBO(252, 90, 73, 1.0)),
                          value: (_currentIndex + 1) / 6,
                        ),
                      ),
                      Positioned(
                        child: SizedBox(
                          height: 75.0,
                          width: 75.0,
                          child: Center(
                            child: Text(
                              "${_currentIndex + 1} of 6",
                              style: TextStyle(
                                fontSize: 18,
                                color: CorporateColors.tinyCampusDarkBlue,
                              ),
                            ),
                          ),
                        ),
                      ),
                      Stack(
                        children: buildSeparators(6),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(15, 0, 0, 0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.fromLTRB(0, 15, 0, 0),
                        child: I18nText(
                          _getPageName(_currentIndex),
                          child: Text(
                            'Seitenname',
                            style: TextStyle(
                              fontSize: 22,
                              fontWeight: FontWeight.w600,
                              letterSpacing: -1.2,
                              color: CorporateColors.tinyCampusDarkBlue,
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(0, 15, 0, 0),
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            _currentIndex < 5
                                ? I18nText(
                                    'modules.cafeteria.wizard.next_up',
                                    child: Text(
                                      'Als nächstes: ',
                                      style: TextStyle(
                                          color: Colors.grey,
                                          letterSpacing: -0.5),
                                    ),
                                  )
                                : Container(
                                    height: 0,
                                  ),
                            I18nText(
                              _getPageName((_currentIndex + 1)),
                              child: Text(
                                'Seitenname',
                                style: TextStyle(color: Colors.grey),
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
        body: Stack(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(bottom: 0.0),
              child: TabBarView(
                controller: _tabController,
                children: <Widget>[
                  CafeteriaWizardCafeteriaSelectPage(),
                  CafeteriaOrderPage(),
                  CafeteriaWizardFilterPage(
                    settings: BlocProvider.of<CafeteriaSettingsBloc>(context)
                        .state
                        .settings,
                    onSettingsChanged: (_settings) => setState(
                      () {
                        BlocProvider.of<CafeteriaSettingsBloc>(context).add(
                          ChangeSettingsEvent(
                            settings: _settings,
                          ),
                        );
                      },
                    ),
                  ),
                  CafeteriaWizardAllergenPage(
                    settings: BlocProvider.of<CafeteriaSettingsBloc>(context)
                        .state
                        .settings,
                    onSettingsChanged: (_settings) => setState(
                      () {
                        BlocProvider.of<CafeteriaSettingsBloc>(context).add(
                          ChangeSettingsEvent(
                            settings: _settings,
                          ),
                        );
                      },
                    ),
                  ),
                  CafeteriaWizardDislikesPage(
                    settings: BlocProvider.of<CafeteriaSettingsBloc>(context)
                        .state
                        .settings,
                    onSettingsChanged: (_settings) => setState(
                      () {
                        BlocProvider.of<CafeteriaSettingsBloc>(context).add(
                          ChangeSettingsEvent(
                            settings: _settings,
                          ),
                        );
                      },
                    ),
                  ),
                  CafeteriaWizardPricePage(
                    settings: BlocProvider.of<CafeteriaSettingsBloc>(context)
                        .state
                        .settings,
                    onSettingsChanged: (_settings) => setState(
                      () {
                        BlocProvider.of<CafeteriaSettingsBloc>(context).add(
                          ChangeSettingsEvent(
                            settings: _settings,
                          ),
                        );
                      },
                    ),
                  ),
                ],
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 16.0,
                  vertical: 10.0,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    _tabController.index > 0
                        ? RaisedButton(
                            color: Colors.white,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(45)),
                            child: Padding(
                              padding: const EdgeInsets.all(16.0),
                              child: I18nText(
                                'modules.cafeteria.wizard.back',
                                child: Text(
                                  "Zurück",
                                  style: TextStyle(
                                    fontSize: 18,
                                    color: CorporateColors.tinyCampusDarkBlue,
                                  ),
                                ),
                              ),
                            ),
                            onPressed: () =>
                                setState(() => _tabController.index--),
                          )
                        : Container(
                            height: 0,
                          ),
                    RaisedButton(
                      color: CorporateColors.tinyCampusDarkBlue,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(45)),
                      child: Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: _tabController.index < _tabController.length - 1
                            ? I18nText(
                                'modules.cafeteria.wizard.next',
                                child: Text(
                                  "Weiter",
                                  style: TextStyle(
                                    fontSize: 18,
                                    color: Colors.white,
                                  ),
                                ),
                              )
                            : I18nText(
                                'modules.cafeteria.wizard.finalize',
                                child: Text(
                                  "Abschließen",
                                  style: TextStyle(
                                    fontSize: 18,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                      ),
                      onPressed: () => setState(
                        () {
                          if (_tabController.index <
                              _tabController.length - 1) {
                            _tabController.index++;
                          } else {
                            BlocProvider.of<CafeteriaSettingsBloc>(context).add(
                              ChangeSettingsEvent(
                                settings:
                                    BlocProvider.of<CafeteriaSettingsBloc>(
                                            context)
                                        .state
                                        .settings
                                        .copyWith(hasDoneWizard: true),
                              ),
                            );
                            Navigator.of(context).pushReplacementNamed(
                              cafeteriaOverviewRoute,
                            );
                          }
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      );

  /// A function that builds the radial separators for the progress indicator
  List<Widget> buildSeparators(int nbSeparators) {
    var sep = <Widget>[];
    for (var i = 0; i < nbSeparators; i++) {
      sep.add(
        Transform.rotate(
          angle: i * 1 / nbSeparators * 2 * pi,
          child: SizedBox(
            width: 3,
            height: 90,
            child: Column(
              children: <Widget>[
                Container(
                  width: 8,
                  height: 14,
                  color: Colors.white,
                )
              ],
            ),
          ),
        ),
      );
    }
    return sep;
  }

  /// A function that returns a [I18n] key for the individual wizard page names
  String _getPageName(int index) {
    switch (index) {
      case 0:
        return 'modules.cafeteria.wizard.choose_your_cafeteria';
      case 1:
        return 'modules.cafeteria.wizard.cafeteria_order';
      case 2:
        return 'modules.cafeteria.settings.diet';
      case 3:
        return 'modules.cafeteria.settings.allergens';
      case 4:
        return 'modules.cafeteria.wizard.avoid_ingredients';
      case 5:
        return 'modules.cafeteria.wizard.price_category';
      default:
        return 'modules.cafeteria.wizard.almost_done';
    }
  }

  void setIndex() {
    setState(() {
      _currentIndex = _tabController.index;
    });
  }
}

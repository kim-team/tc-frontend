/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../bloc/cafeteria_settings/cafeteria_settings_bloc.dart';
import 'cafeteria_order_page.dart';
import 'wizard/cafeteria_wizard_cafeteriaselect_page.dart';

/// A page that holds a [TabBarView] for [CafeteriaWizardCafeteriaSelectPage]
/// and [CafeteriaOrderPage]
class CafeteriaSelectionPage extends StatefulWidget {
  final int initIndex;

  const CafeteriaSelectionPage({
    Key? key,
    required this.initIndex,
  }) : super(key: key);

  @override
  _CafeteriaSelectionPageState createState() => _CafeteriaSelectionPageState();
}

class _CafeteriaSelectionPageState extends State<CafeteriaSelectionPage>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;

  @override
  void initState() {
    _tabController = TabController(
      initialIndex: widget.initIndex,
      length: 2,
      vsync: this,
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) =>
      BlocBuilder<CafeteriaSettingsBloc, CafeteriaSettingsState>(
        builder: (context, state) => Scaffold(
          appBar: AppBar(
            title: I18nText('modules.cafeteria.selection.title'),
            bottom: TabBar(
              tabs: [
                Tab(
                  text: FlutterI18n.translate(
                    context,
                    'modules.cafeteria.selection.selection',
                  ),
                ),
                BlocProvider.of<CafeteriaSettingsBloc>(context)
                            .state
                            .cafeteriaSelection
                            .length >
                        1
                    ? Tab(
                        text: FlutterI18n.translate(
                          context,
                          'modules.cafeteria.selection.order',
                        ),
                      )
                    : Tab(
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(0, 8, 0, 0),
                          child: Column(
                            children: <Widget>[
                              Text(
                                FlutterI18n.translate(
                                  context,
                                  'modules.cafeteria.selection.order',
                                ),
                                style: TextStyle(
                                  color: Colors.grey,
                                ),
                              ),
                              Text(
                                FlutterI18n.translate(
                                  context,
                                  'modules.cafeteria.selection'
                                  '.min_selection_hint',
                                ),
                                style: TextStyle(
                                  color: Colors.grey,
                                  fontSize: 12,
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
              ],
              controller: _tabController,
            ),
          ),
          body: TabBarView(
            controller: _tabController,
            children: <Widget>[
              CafeteriaWizardCafeteriaSelectPage(),
              CafeteriaOrderPage(),
            ],
          ),
        ),
      );

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }
}

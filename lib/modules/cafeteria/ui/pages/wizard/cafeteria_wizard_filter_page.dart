/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';

import '../../../model/cafeteria_settings.dart';
import '../../widgets/settings_widget.dart';
import '../../widgets/wizard_example_card.dart';

/// A wizard page that lets the user chose which diet options
/// are to be displayed e.g. "Vegan", "Vegetarian" and "None"
class CafeteriaWizardFilterPage extends StatefulWidget {
  final ValueChanged<CafeteriaSettings> onSettingsChanged;
  final CafeteriaSettings settings;

  const CafeteriaWizardFilterPage({
    Key? key,
    required this.onSettingsChanged,
    required this.settings,
  }) : super(key: key);

  @override
  _CafeteriaWizardFilterPageState createState() =>
      _CafeteriaWizardFilterPageState();
}

class _CafeteriaWizardFilterPageState extends State<CafeteriaWizardFilterPage> {
  @override
  Widget build(BuildContext context) => ListView(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 5,
              vertical: 40,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                ConstrainedBox(
                  child: InkWell(
                    splashColor: Color.fromRGBO(0, 0, 0, 0),
                    highlightColor: Color.fromRGBO(0, 0, 0, 0),
                    child: WizardExampleCard(
                      category: Category.vegan,
                      settings: widget.settings,
                      scaleDown: true,
                    ),
                    onTap: () => widget.onSettingsChanged(widget.settings
                        .copyWith(filter: CafeteriaFilter.vegan)),
                  ),
                  constraints: BoxConstraints(
                    maxWidth: MediaQuery.of(context).size.width / 3 - 5,
                    maxHeight: MediaQuery.of(context).size.width / 3 - 5,
                  ),
                ),
                ConstrainedBox(
                  child: InkWell(
                    splashColor: Color.fromRGBO(0, 0, 0, 0),
                    highlightColor: Color.fromRGBO(0, 0, 0, 0),
                    child: WizardExampleCard(
                      category: Category.vegetarian,
                      settings: widget.settings,
                      scaleDown: true,
                    ),
                    onTap: () => widget.onSettingsChanged(widget.settings
                        .copyWith(filter: CafeteriaFilter.vegetarian)),
                  ),
                  constraints: BoxConstraints(
                    maxWidth: MediaQuery.of(context).size.width / 3 - 5,
                    maxHeight: MediaQuery.of(context).size.width / 3 - 5,
                  ),
                ),
                ConstrainedBox(
                  child: InkWell(
                    splashColor: Color.fromRGBO(0, 0, 0, 0),
                    highlightColor: Color.fromRGBO(0, 0, 0, 0),
                    child: WizardExampleCard(
                      category: Category.none,
                      settings: widget.settings,
                      scaleDown: true,
                    ),
                    onTap: () => widget.onSettingsChanged(
                        widget.settings.copyWith(filter: CafeteriaFilter.none)),
                  ),
                  constraints: BoxConstraints(
                    maxWidth: MediaQuery.of(context).size.width / 3 - 5,
                    maxHeight: MediaQuery.of(context).size.width / 3 - 5,
                  ),
                ),
              ],
            ),
          ),
          SettingsWidget(
            settings: widget.settings,
            title: 'modules.cafeteria.settings.diet',
            type: SettingsType.filter,
            onSettingsChanged: (_settings) => widget
                .onSettingsChanged(CafeteriaSettings.fromSettings(_settings)),
            children: CafeteriaFilter.values.toList(),
          ),
          Container(
            height: 80.0,
          ),
        ],
      );
}

/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../../../../common/tc_theme.dart';
import '../../../bloc/cafeteria_bloc.dart';
import '../../../bloc/cafeteria_settings/cafeteria_settings_bloc.dart';
import '../../../model/cafeteria_settings.dart';
import '../../widgets/settings_widget.dart';

/// A wizard page that lets the user select which [Cafeteria]s are to be shown
/// in the [CafeteriaOverviewPage]
class CafeteriaWizardCafeteriaSelectPage extends StatefulWidget {
  const CafeteriaWizardCafeteriaSelectPage({Key? key}) : super(key: key);

  @override
  _CafeteriaWizardCafeteriaSelectPageState createState() =>
      _CafeteriaWizardCafeteriaSelectPageState();
}

class _CafeteriaWizardCafeteriaSelectPageState
    extends State<CafeteriaWizardCafeteriaSelectPage> {
  @override
  Widget build(BuildContext context) =>
      BlocBuilder<CafeteriaBloc, CafeteriaState>(
        builder: (context, state) => ListView(
          children: <Widget>[
            kDebugMode
                ? Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 5,
                      vertical: 15,
                    ),
                    child: Card(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                          horizontal: 0,
                          vertical: 15,
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 15),
                              child: I18nText(
                                'modules.cafeteria.wizard.test_data',
                                child: Text(
                                  'Test Data',
                                  style: TextStyle(
                                    fontSize: 18,
                                    fontWeight: FontWeight.w500,
                                    color: CorporateColors.tinyCampusDarkBlue,
                                  ),
                                ),
                              ),
                            ),
                            SwitchListTile(
                              value: BlocProvider.of<CafeteriaSettingsBloc>(
                                      context)
                                  .state
                                  .settings
                                  .showTestApi,
                              onChanged: (toggled) {
                                BlocProvider.of<CafeteriaSettingsBloc>(context)
                                    .add(SettingsChangedEvent(
                                        settings: BlocProvider.of<
                                                CafeteriaSettingsBloc>(context)
                                            .state
                                            .settings
                                            .copyWith(
                                              showTestApi: toggled,
                                            )));
                                BlocProvider.of<CafeteriaBloc>(context)
                                    .add(LoadingEvent(loadTest: toggled));
                              },
                              title: I18nText(
                                'modules.cafeteria.wizard.test_data_hint',
                                child: Text(
                                  'Show Test Data',
                                  style: TextStyle(
                                    fontSize: 16,
                                    color: CorporateColors.tinyCampusDarkBlue,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  )
                : Container(),
            SettingsWidget(
              settings: BlocProvider.of<CafeteriaSettingsBloc>(context)
                  .state
                  .settings,
              title: 'modules.cafeteria.settings.choose_cafeteria',
              type: SettingsType.cafeteria,
              onSettingsChanged: (_list) {
                setState(() {
                  BlocProvider.of<CafeteriaSettingsBloc>(context)
                      .add(SelectCafeteriaEvent(_list));
                });
              },
              children: state.cafeteriaList,
            ),
            Container(
              height: 80.0,
            ),
          ],
        ),
      );
}

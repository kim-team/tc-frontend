/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:assorted_layout_widgets/assorted_layout_widgets.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../../common/assets_adapter.dart';
import '../../../../common/map_functions.dart';
import '../../../../common/tc_markdown_stylesheet.dart';
import '../../../../common/tc_theme.dart';
import '../../../../common/widgets/tc_button.dart';
import '../../../organizer/ui/organizer/o_drawer/i18n_common.dart';
import '../../bloc/cafeteria_opening/cafeteria_opening_bloc.dart';
import '../../model/opening_times/cafeteria_opening_hours.dart';
import '../widgets/details/detail_opening_times.dart';

class CafeteriaDetailPage extends StatefulWidget {
  final String name;

  const CafeteriaDetailPage({
    Key? key,
    required this.name,
  }) : super(key: key);

  @override
  _CafeteriaDetailPageState createState() => _CafeteriaDetailPageState();
}

class _CafeteriaDetailPageState extends State<CafeteriaDetailPage>
    with TickerProviderStateMixin {
  final _scrollController = ScrollController();

  var _isToggledToSeeMore = false;

  late CafeteriaOpeningHours _hours;

  @override
  void initState() {
    super.initState();
    _hours = BlocProvider.of<CafeteriaOpeningBloc>(context)
        .state
        .cafeteriaOpeningHours
        .firstWhere(
          (element) => element.name == widget.name,
          orElse: CafeteriaOpeningHours.empty,
        );
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        body: Stack(
          children: <Widget>[
            ListView(
              padding: EdgeInsets.zero,
              physics: ClampingScrollPhysics(),
              controller: _scrollController,
              children: <Widget>[
                ColumnSuper(
                  innerDistance: -10,
                  children: <Widget>[
                    _hours.image.isEmpty
                        ? Image.asset(
                            AssetAdapter.module(ModuleModel.cafeteria),
                            fit: BoxFit.fitHeight,
                          )
                        : AnimatedSize(
                            duration: Duration(milliseconds: 320),
                            child: CachedNetworkImage(
                              placeholderFadeInDuration:
                                  Duration(milliseconds: 320),
                              fadeOutDuration: Duration(milliseconds: 320),
                              fadeInDuration: Duration(milliseconds: 320),
                              imageUrl: _hours.image,
                              placeholder: (context, url) => Container(
                                color: Colors.grey[800],
                                height: 250,
                                child: Center(
                                  child: CircularProgressIndicator(
                                    backgroundColor: Colors.grey[600],
                                    valueColor: AlwaysStoppedAnimation<Color>(
                                        Colors.white54),
                                  ),
                                ),
                              ),
                              fit: BoxFit.cover,
                              errorWidget: (context, url, _) => Image.asset(
                                AssetAdapter.module(ModuleModel.cafeteria),
                                fit: BoxFit.fitHeight,
                              ),
                            ),
                          ),
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Colors.white,
                      ),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                          horizontal: 15,
                          vertical: 25,
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            (_hours.latitude.isEmpty ||
                                    _hours.longitude.isEmpty)
                                ? Container(
                                    height: 1,
                                  )
                                : TCButton(
                                    alternativeWidget: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Padding(
                                          padding:
                                              const EdgeInsets.only(right: 5),
                                          child: Icon(
                                            Icons.map,
                                            color:
                                                CorporateColors.tinyCampusBlue,
                                          ),
                                        ),
                                        I18nText(
                                          'modules.cafeteria'
                                          '.details.show_map',
                                          child: Text(
                                            '',
                                            style: TextStyle(
                                              fontSize: 20,
                                              color: CorporateColors
                                                  .tinyCampusBlue,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    backgroundColor: Colors.white,
                                    borderThickness: 2,
                                    borderColor: CorporateColors.tinyCampusBlue,
                                    foregroundColor:
                                        CorporateColors.tinyCampusBlue,
                                    onPressedCallback: () =>
                                        MapFunctions.openMapsSheet(
                                      context,
                                      double.parse(_hours.latitude),
                                      double.parse(_hours.longitude),
                                      widget.name,
                                    ),
                                  ),
                            Padding(
                              padding: const EdgeInsets.only(
                                top: 24.0,
                                // bottom: 8.0,
                              ),
                              child: Text(
                                widget.name,
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                    fontSize: 30,
                                    color: CorporateColors.tinyCampusDarkBlue,
                                    fontWeight: FontWeight.w400,
                                    letterSpacing: -1.2),
                              ),
                            ),
                            if (_hours.news.isNotEmpty)
                              Container(
                                margin:
                                    EdgeInsets.only(top: 24.0, bottom: 24.0),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Container(
                                        margin: EdgeInsets.only(right: 12.0),
                                        child: Icon(
                                          Icons.info,
                                          size: 32,
                                          color: CorporateColors
                                              .tinyCampusIconGrey
                                              .withOpacity(0.5),
                                        )),
                                    Expanded(
                                      child: Markdown(
                                        padding: EdgeInsets.zero,
                                        data: _hours.news,
                                        onTapLink: (_, url, __) async {
                                          if (url == null) return;

                                          if (!url.toLowerCase().startsWith(
                                              RegExp(r'^https?://'))) {
                                            url = 'https://$url';
                                          }

                                          if (await canLaunch(url)) {
                                            await launch(url);
                                          }
                                        },
                                        styleSheet:
                                            TinyCampusMarkdownStylesheet(
                                                    context)
                                                .copyWith(
                                                    p: TextStyle(
                                                        color: CurrentTheme()
                                                            .tcBlue,
                                                        fontSize: 16.0)),
                                        physics: BouncingScrollPhysics(),
                                        shrinkWrap: true,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            Stack(
                              children: [
                                ClipRRect(
                                  child: AnimatedSize(
                                    curve: Curves.easeOut,
                                    duration: Duration(
                                        milliseconds:
                                            _isToggledToSeeMore ? 720 : 320),
                                    alignment: Alignment.topCenter,
                                    child: SizedBox(
                                      height: _isToggledToSeeMore ? null : 100,
                                      child: Padding(
                                        padding: const EdgeInsets.fromLTRB(
                                            0, 8, 0, 5),
                                        child: Text(
                                          _hours.description,
                                          overflow: TextOverflow.visible,
                                          style: TextStyle(
                                            fontSize: 18,
                                            color: CurrentTheme()
                                                .textSoftLightBlue,
                                            letterSpacing: -1.1,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                Positioned(
                                  top: 0,
                                  bottom: 0,
                                  right: 0,
                                  left: 0,
                                  child: AnimatedContainer(
                                    duration: Duration(milliseconds: 320),
                                    decoration: BoxDecoration(
                                        gradient: LinearGradient(
                                            begin: Alignment.topCenter,
                                            end: Alignment.bottomCenter,
                                            stops: [
                                          _isToggledToSeeMore ? 1.0 : 0.8,
                                          1.0
                                        ],
                                            colors: [
                                          Theme.of(context)
                                              .primaryColor
                                              .withOpacity(0.0),
                                          Theme.of(context).primaryColor,
                                        ])),
                                  ),
                                ),
                              ],
                            ),
                            Padding(
                              padding: const EdgeInsets.only(bottom: 10),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(right: 8),
                                    child: GestureDetector(
                                      onTap: () {
                                        setState(() {
                                          _isToggledToSeeMore =
                                              !_isToggledToSeeMore;
                                          if (!_isToggledToSeeMore) {
                                            if (_hours.description.length >
                                                420) {
                                              _scrollController.animateTo(
                                                _scrollController.position
                                                            .maxScrollExtent >
                                                        250
                                                    ? 250
                                                    : 0,
                                                duration:
                                                    Duration(milliseconds: 320),
                                                curve: Curves.easeOut,
                                              );
                                            }
                                          }
                                        });
                                      },
                                      child: I18nText(
                                        _isToggledToSeeMore
                                            ? 'modules.cafeteria.details.less'
                                            : 'modules.cafeteria.details.more',
                                        child: Text(
                                          '',
                                          style: TextStyle(
                                            color: Colors.redAccent,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(bottom: 12.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.stretch,
                                children: [
                                  I18nText(
                                    'modules.cafeteria.items.disclaimer',
                                    child: Text(
                                      '',
                                      style:
                                          Theme.of(context).textTheme.bodyText1,
                                    ),
                                  ),
                                  Text(
                                    'Studentenwerk Gießen A.d.ö.R.',
                                    style:
                                        Theme.of(context).textTheme.bodyText1,
                                  ),
                                ],
                              ),
                            ),
                            OpeningTimes(
                              name: widget.name,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
                Container(height: 260),
              ],
            ),
            Padding(
              padding:
                  const EdgeInsets.symmetric(vertical: 30.0, horizontal: 0.0),
              child: Card(
                elevation: 4,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(20),
                    bottomRight: Radius.circular(20),
                  ),
                ),
                margin: EdgeInsets.zero,
                child: BackButton(),
              ),
            ),
          ],
        ),
      );
}

/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../../../common/assets_adapter.dart';
import '../../../../common/constants/routing_constants.dart';
import '../../../../common/tc_theme.dart';
import '../../api_helper.dart';
import '../../bloc/cafeteria_feedback/cafeteria_feedback_bloc.dart';
import '../../bloc/cafeteria_settings/cafeteria_settings_bloc.dart';
import '../../cafeteria_item_page_args.dart';
import '../../model/cafeteria_item.dart';
import '../../model/cafeteria_settings.dart';
import '../text_helper.dart';
import 'characteristics_banner.dart';

///ItemCard, which shows a specific dish, with any important flags.
class LunchCard extends StatelessWidget {
  final CafeteriaItem cafeteriaItem;
  final String name;

  ///Default constructor which takes a dish
  const LunchCard({
    Key? key,
    required this.cafeteriaItem,
    required this.name,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _settings =
        BlocProvider.of<CafeteriaSettingsBloc>(context).state.settings;

    String price;
    switch (_settings.priceClass) {
      case PriceClass.student:
        price = cafeteriaItem.prices.student;
        break;
      case PriceClass.staff:
        price = cafeteriaItem.prices.staff;
        break;
      case PriceClass.guest:
        price = cafeteriaItem.prices.guest;
        break;
    }

    var detectedAllergens = _settings.allergens
        .where((e) => cafeteriaItem.allergens.containsKey(e.id.toString()))
        .map((e) => FlutterI18n.translate(context, e.name))
        .toList();

    final characteristics =
        cafeteriaItem.characteristics.map((e) => e.abbreviation);

    final detectedDislikes = _settings.dislikedIngredients
        .where((e) => characteristics.contains(e.abbreviation))
        .map((e) => FlutterI18n.translate(context, e.name))
        .toList();

    return Card(
      elevation: 3.0,
      clipBehavior: Clip.antiAlias,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(12)),
      ),
      margin: const EdgeInsets.all(6),
      child: InkWell(
        onTap: () {
          BlocProvider.of<CafeteriaFeedbackBloc>(context).add(
            LoadFeedbackEvent(
              cafeteriaItem.getUniqueId(cafeteriaName: name),
            ),
          );
          Navigator.of(context).pushNamed(
            cafeteriaItemRoute,
            arguments: CafeteriaItemPageArguments(
              item: cafeteriaItem,
              name: name,
            ),
          );
        },
        child: Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Column(
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Row(
                    children: <Widget>[
                      _generateAllergenBanner(
                        detectedAllergens,
                        detectedDislikes,
                        context,
                      ),
                      Expanded(
                        flex: 1,
                        child: Container(
                          clipBehavior: Clip.antiAlias,
                          decoration: BoxDecoration(),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              (cafeteriaItem.image == invalidImageUrl)
                                  ? Expanded(
                                      child: Image.asset(
                                        AssetAdapter.module(
                                            ModuleModel.cafeteria),
                                        fit: BoxFit.cover,
                                      ),
                                    )
                                  : Expanded(
                                      child: CachedNetworkImage(
                                        placeholderFadeInDuration:
                                            Duration(milliseconds: 350),
                                        fadeOutDuration:
                                            Duration(milliseconds: 350),
                                        fadeInDuration:
                                            Duration(milliseconds: 350),
                                        imageUrl: cafeteriaItem.image,
                                        placeholder: (context, url) =>
                                            Container(
                                          color: Colors.grey[800],
                                          child: Center(
                                            child: CircularProgressIndicator(
                                              backgroundColor: Colors.grey[600],
                                              valueColor:
                                                  AlwaysStoppedAnimation<Color>(
                                                      Colors.white54),
                                            ),
                                          ),
                                        ),
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                            ],
                          ),
                        ),
                        //  ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        flex: 5,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 8.0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Align(
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  cleanupText(cafeteriaItem.artikelClean),
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                    letterSpacing: -0.6,
                                    height: 1.0,
                                    fontSize: 16.0,
                                  ),
                                  maxLines: 2,
                                  textAlign: TextAlign.left,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 3,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Expanded(
                              flex: 1,
                              child: Padding(
                                padding: const EdgeInsets.only(left: 8.0),
                                child: Text(
                                  _formatPrice(price),
                                  style: TextStyle(
                                      fontSize: 20.0,
                                      letterSpacing: -1.0,
                                      fontWeight: FontWeight.w400),
                                  textAlign: TextAlign.left,
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child: _generateBanner(),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Center(
              child: _generateOpaqueLayer(
                detectedAllergens,
                detectedDislikes,
                _settings,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _generateOpaqueLayer(
    List<dynamic> detectedAllergens,
    List<dynamic> detectedDislikes,
    CafeteriaSettings settings,
  ) {
    var checkDisplay = false;
    if (settings.filter == CafeteriaFilter.vegan ||
        settings.filter == CafeteriaFilter.vegetarian) {
      checkDisplay = true;
    }
    if (cafeteriaItem.characteristics.isNotEmpty) {
      checkDisplay = true;
      switch (settings.filter) {
        case CafeteriaFilter.none:
          checkDisplay = false;
          break;
        case CafeteriaFilter.vegetarian:
          for (var characteristic in cafeteriaItem.characteristics) {
            if (characteristic.abbreviation == "VEG") {
              checkDisplay = false;
            }
            if (characteristic.abbreviation == "V") {
              checkDisplay = false;
            }
          }
          break;
        case CafeteriaFilter.vegan:
          for (var characteristic in cafeteriaItem.characteristics) {
            if (characteristic.abbreviation == "VEG") {
              checkDisplay = false;
            }
          }
          break;
      }
      if (checkDisplay == true) {
        return Center(
          child: Container(
            color: Color.fromRGBO(255, 255, 255, 0.6),
          ),
        );
      } else {
        return Center();
      }
    } else if (checkDisplay == true) {
      return Center(
        child: Container(
          color: Color.fromRGBO(255, 255, 255, 0.6),
        ),
      );
    } else {
      return Center();
    }
  }

  Widget _generateBanner() {
    if (cafeteriaItem.characteristics.isNotEmpty) {
      for (var characteristic in cafeteriaItem.characteristics) {
        if (characteristic.abbreviation == "V") {
          return VegetarianBanner();
        }
        if (characteristic.abbreviation == "VEG") {
          return VeganBanner();
        }
      }
      return Container();
    }
    return Container();
  }

  Widget _generateAllergenBanner(
    List<dynamic> detectedAllergens,
    List<dynamic> detectedDislikes,
    BuildContext context,
  ) {
    if (detectedAllergens.isNotEmpty || detectedDislikes.isNotEmpty) {
      return Expanded(
        flex: 1,
        child: Container(
          clipBehavior: Clip.antiAlias,
          decoration: BoxDecoration(
            // borderRadius: BorderRadius.only(bottomRight: Radius.circular(8)),
            color: detectedAllergens.isNotEmpty
                ? CorporateColors.cafeteriaCautionRed
                : CorporateColors.cafeteriaCautionYellow,
          ),
          child: Padding(
            padding: const EdgeInsets.all(6.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  child: Center(
                    child: Text(
                      (detectedAllergens + detectedDislikes).toSet().join(', '),
                      maxLines: 3,
                      textAlign: TextAlign.center,
                      style: Theme.of(context)
                          .textTheme
                          .bodyText2
                          ?.copyWith(color: Colors.white),
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    } else {
      return Expanded(
        flex: 0,
        child: Container(),
      );
    }
  }

  String _formatPrice(String rawPrice) =>
      rawPrice == 'k.A.' ? rawPrice : '$rawPrice €';
}

/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../common/assets_adapter.dart';
import '../../../../../common/constants/routing_constants.dart';
import '../../../api_helper.dart';
import '../../../bloc/cafeteria_feedback/cafeteria_feedback_bloc.dart';
import '../../../cafeteria_item_page_args.dart';
import '../../../model/cafeteria_item.dart';

/// A Widget that displays every [CafeteriaItem] for the [Cafeteria] that the
/// [CafeteriaHeaderCard] belongs to which is under the Category "Information"
class CafeteriaHeaderInformationBlock extends StatelessWidget {
  const CafeteriaHeaderInformationBlock({
    Key? key,
    required this.informations,
    required this.name,
  }) : super(key: key);

  final List<CafeteriaItem> informations;
  final String name;

  @override
  Widget build(BuildContext context) {
    if (informations.isEmpty) {
      return Container();
    }

    return Column(
      children: <Widget>[
        Divider(
          color: Colors.grey,
          indent: 12.0,
          endIndent: 12.0,
        ),
        ..._buildInformationList(context, informations)
      ],
    );
  }

  List<Widget> _buildInformationList(
      BuildContext context, List<CafeteriaItem> informations) {
    var array = <Widget>[];
    for (var item in informations) {
      array.add(_buildSingleInformationTile(context, item));
    }
    return array;
  }

  Widget _buildSingleInformationTile(
          BuildContext context, CafeteriaItem item) =>
      ListTile(
        title: Text(item.descriptionClean),
        onTap: () {
          BlocProvider.of<CafeteriaFeedbackBloc>(context).add(
            LoadFeedbackEvent(
              item.getUniqueId(cafeteriaName: name),
            ),
          );
          Navigator.of(context).pushNamed(
            cafeteriaItemRoute,
            arguments: CafeteriaItemPageArguments(
              item: item,
              name: name,
            ),
          );
        },
        leading: Container(
          width: 55,
          height: 55,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(6),
          ),
          clipBehavior: Clip.antiAlias,
          child: item.image == invalidImageUrl
              ? Image.asset(
                  AssetAdapter.module(ModuleModel.cafeteria),
                  fit: BoxFit.fitHeight,
                )
              : CachedNetworkImage(
                  placeholderFadeInDuration: Duration(milliseconds: 350),
                  fadeOutDuration: Duration(milliseconds: 350),
                  fadeInDuration: Duration(milliseconds: 350),
                  imageUrl: item.image,
                  placeholder: (context, _) => Image.asset(
                      AssetAdapter.module(ModuleModel.cafeteria),
                      fit: BoxFit.fitHeight),
                  fit: BoxFit.cover,
                ),
        ),
      );
}

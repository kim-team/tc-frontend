/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../bloc/cafeteria_opening/cafeteria_opening_bloc.dart';
import '../../bloc/cafeteria_settings/cafeteria_settings_bloc.dart';
import '../../model/cafeteria.dart';
import '../../model/cafeteria_item.dart';
import '../../model/opening_times/cafeteria_opening_hours.dart';
import 'cafeteria_category_header.dart';
import 'cafeteria_header_card.dart';
import 'lunch_card.dart';

///A Widget which shows a specific day of the Cafeteria
class CafeteriaDay extends StatelessWidget {
  ///A List of items which all are served on the same Day.
  final List<CafeteriaItem> todaysItems;
  final String name;
  final int weekDay;

  ///Standard Constructor with positional parameter
  const CafeteriaDay(
    this.todaysItems,
    this.name,
    this.weekDay, {
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) =>
      BlocBuilder<CafeteriaSettingsBloc, CafeteriaSettingsState>(
        builder: (context, state) => Column(
          children: <Widget>[
            CafeteriaHeaderCard(
                name: name,
                eatingHours: _getEatingHours(
                  context,
                  weekDay,
                  name,
                ),
                closures: _getClosures(
                  context,
                  name,
                ),
                weekDay: weekDay,
                isToday: todaysItems.isNotEmpty
                    ? _isToday(todaysItems[0].date)
                    : false,
                informationItems:
                    Cafeteria.filterInformationItems(todaysItems)),
            for (var category in Cafeteria.readCategoriesWithExclusion(
                todaysItems, ["information"]))
              Container(
                margin: EdgeInsets.symmetric(horizontal: 8.0),
                child: Column(
                  children: <Widget>[
                    CafeteriaCategoryHeader(category: category),
                    GridView.builder(
                      physics: NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        crossAxisSpacing: 0,
                      ),
                      itemCount: todaysItems
                          .where(
                            (item) => item.category == category,
                          )
                          .toList()
                          .length,
                      itemBuilder: (context, index) => LunchCard(
                        cafeteriaItem: todaysItems
                            .where((item) => item.category == category)
                            .toList()[index],
                        name: name,
                      ), //LunchCard
                    ),
                  ],
                ),
              ),
          ],
        ),
      );

  List<Map<String, String>> _getEatingHours(
    BuildContext context,
    int weekDay,
    String name,
  ) {
    var cafeteriaOpeningHours = BlocProvider.of<CafeteriaOpeningBloc>(context)
        .state
        .cafeteriaOpeningHours;

    if (cafeteriaOpeningHours.isEmpty) {
      return <Map<String, String>>[];
    }

    var temp = cafeteriaOpeningHours.firstWhere(
      (e) => e.name == name,
      orElse: CafeteriaOpeningHours.empty,
    );

    String _weekDay;
    switch (weekDay) {
      case 1:
        _weekDay = 'monday';
        break;
      case 2:
        _weekDay = 'tuesday';
        break;
      case 3:
        _weekDay = 'wednesday';
        break;
      case 4:
        _weekDay = 'thursday';
        break;
      case 5:
        _weekDay = 'friday';
        break;
      case 6:
        _weekDay = 'saturday';
        break;
      case 7:
        _weekDay = 'sunday';
        break;
      default:
        _weekDay = '';
    }
    return temp.eatingHours[_weekDay] ?? <Map<String, String>>[];
  }

  List<Map<String, String>> _getClosures(BuildContext context, String name) {
    var cafeteriaOpeningHours = BlocProvider.of<CafeteriaOpeningBloc>(context)
        .state
        .cafeteriaOpeningHours;

    if (cafeteriaOpeningHours.isEmpty) {
      return <Map<String, String>>[];
    }

    return cafeteriaOpeningHours
        .firstWhere(
          (e) => e.name == name,
          orElse: CafeteriaOpeningHours.empty,
        )
        .closures;
  }

  bool _isToday(String date) =>
      (DateTime.parse(date).difference(DateTime.now()).inDays == 0 &&
          DateTime.parse(date).weekday == DateTime.now().weekday);
}

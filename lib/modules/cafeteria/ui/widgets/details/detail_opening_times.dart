/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

import '../../../../../common/tc_theme.dart';
import '../../../../organizer/ui/organizer/o_drawer/i18n_common.dart';
import '../../../bloc/cafeteria_opening/cafeteria_opening_bloc.dart';
import '../../../model/opening_times/cafeteria_opening_hours.dart';
import 'detail_opening_row.dart';

/// A widget that takes the name of a [Cafeteria] and returns
/// a display of the opening times for every weekday for that [Cafeteria]
class OpeningTimes extends StatefulWidget {
  /// name of the [Cafeteria]
  final String name;

  const OpeningTimes({
    Key? key,
    required this.name,
  }) : super(key: key);

  @override
  _OpeningTimesState createState() => _OpeningTimesState();
}

class _OpeningTimesState extends State<OpeningTimes> {
  late Map<String, List<Map<String, String>>> _openTimes;
  late Map<String, List<Map<String, String>>> _lunchTimes;
  final List<String> weekDays = [
    "monday",
    "tuesday",
    "wednesday",
    "thursday",
    "friday",
    "saturday",
    "sunday",
  ];

  @override
  void initState() {
    super.initState();
    _openTimes = _getOpeningHours(widget.name).openingHours;
    _lunchTimes = _getOpeningHours(widget.name).eatingHours;
  }

  final leftOpeningTimeWidth = 110.0;

  @override
  Widget build(BuildContext context) => Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                for (var day in weekDays)
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 5),
                    child: Column(
                      children: [
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            SizedBox(
                              width: leftOpeningTimeWidth,
                              child: I18nText(
                                'modules.cafeteria.overview.weekdays_full.$day',
                                child: Text(
                                  '',
                                  style: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.w600,
                                    color: CorporateColors.tinyCampusBlue,
                                    letterSpacing: -1.1,
                                  ),
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 11,
                              child: SizedBox(
                                height: 16,
                                child: DetailOpeningRow(
                                  showBorderTimes: false,
                                  cafeteriaName: widget.name,
                                  weekDay: day,
                                ),
                              ),
                            ),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 2.5),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                margin: EdgeInsets.only(top: 12.0),
                                width: leftOpeningTimeWidth,
                                child: I18nText(
                                  'modules.cafeteria.details.'
                                  'lunch_service',
                                  child: Text(
                                    '',
                                    style: Theme.of(context)
                                        .textTheme
                                        .bodyText1
                                        ?.copyWith(
                                            color: CorporateColors
                                                .tinyCampusIconGrey),
                                  ),
                                ),
                              ),
                              Expanded(
                                flex: 11,
                                child: Wrap(
                                  runSpacing: 0,
                                  spacing: 10,
                                  children: [
                                    ...?_lunchTimes[day]?.map(
                                      (lunch) => Chip(
                                        padding: EdgeInsets.zero,
                                        backgroundColor: CorporateColors
                                            .tinyCampusIconGrey
                                            .withOpacity(0.05),
                                        label: Text(
                                          '${_getLocalizedTime(
                                            lunch["open"],
                                            context,
                                          )}'
                                          ' - '
                                          '${_getLocalizedTime(
                                            lunch["close"],
                                            context,
                                          )}',
                                          style: Theme.of(context)
                                              .textTheme
                                              .bodyText2,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 2.5),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                margin: EdgeInsets.only(top: 12.0),
                                width: leftOpeningTimeWidth,
                                child: I18nText(
                                  'modules.cafeteria.details.'
                                  'opening_times',
                                  child: Text(
                                    '',
                                    style: Theme.of(context)
                                        .textTheme
                                        .bodyText1
                                        ?.copyWith(
                                            color: CorporateColors
                                                .tinyCampusIconGrey),
                                  ),
                                ),
                              ),
                              Expanded(
                                flex: 11,
                                child: Wrap(
                                  runSpacing: 0,
                                  spacing: 10,
                                  children: [
                                    ...?_openTimes[day]?.map(
                                      (open) => Chip(
                                        padding: EdgeInsets.zero,
                                        backgroundColor: CorporateColors
                                            .tinyCampusIconGrey
                                            .withOpacity(0.05),
                                        label: Text(
                                          '${_getLocalizedTime(
                                            open["open"],
                                            context,
                                          )}'
                                          ' - '
                                          '${_getLocalizedTime(
                                            open["close"],
                                            context,
                                          )}',
                                          style: Theme.of(context)
                                              .textTheme
                                              .bodyText2
                                              ?.copyWith(fontSize: 14.0),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
              ],
            ),
          ),
        ],
      );

  /// A function that returns an instance of [CafeteriaOpeningHours] for the
  /// current [Cafeteria]
  CafeteriaOpeningHours _getOpeningHours(String name) =>
      BlocProvider.of<CafeteriaOpeningBloc>(context)
          .state
          .cafeteriaOpeningHours
          .firstWhere(
            (element) => element.name == name,
            orElse: CafeteriaOpeningHours.empty,
          );
}

String _getLocalizedTime(String? time, BuildContext context) {
  if (time == null) return '';

  try {
    return DateFormat.jm(
      FlutterI18n.currentLocale(context)?.languageCode ?? 'de',
    ).format(DateFormat.Hm().parse(time));
  } on Exception {
    return '';
  }
}

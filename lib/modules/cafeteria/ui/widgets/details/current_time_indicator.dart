/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

class CurrentTimeIndicator extends StatelessWidget {
  final double offset;
  final double maxWidth;

  final _height = 30.0;

  final bool leftAligned;

  const CurrentTimeIndicator({
    Key? key,
    required this.offset,
    required this.maxWidth,
  })  : leftAligned = offset < maxWidth / 2,
        super(key: key);

  @override
  Widget build(BuildContext context) => Positioned(
        left: offset - 3,
        child: Stack(
          clipBehavior: Clip.none,
          children: [
            Positioned(
              right: leftAligned ? null : 8,
              left: leftAligned ? 8 : null,
              top: -2,
              child: Padding(
                padding: const EdgeInsets.only(left: 2),
                child: Text(
                  FlutterI18n.translate(context, 'modules.cafeteria.header.now')
                      .toUpperCase(),
                  style: TextStyle(
                    fontSize: 10,
                    fontWeight: FontWeight.bold,
                    color: Colors.red,
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 40.5,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(height: _height, width: 2, color: Colors.white),
                      Container(height: _height, width: 2, color: Colors.red),
                      Container(height: _height, width: 2, color: Colors.white),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      );
}

/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

import '../../../../../common/tc_theme.dart';
import '../../../bloc/cafeteria_opening/cafeteria_opening_bloc.dart';
import '../../../model/opening_times/cafeteria_opening_hours.dart';
import 'current_time_indicator.dart';
import 'detail_opening_bar_element.dart';
import 'padded_substring_extension.dart';

class DetailOpeningRow extends StatefulWidget {
  final bool showBorderTimes;
  final String cafeteriaName;
  final String weekDay;
  final bool isToday;

  const DetailOpeningRow({
    Key? key,
    required this.showBorderTimes,
    required this.cafeteriaName,
    required this.weekDay,
    this.isToday = false,
  }) : super(key: key);

  @override
  _DetailOpeningRowState createState() => _DetailOpeningRowState();
}

class _DetailOpeningRowState extends State<DetailOpeningRow> {
  late List<Map<String, String>> _openTimes;
  late List<Map<String, String>> _lunchTimes;
  final barHeight = 14.0;

  @override
  void initState() {
    super.initState();
    _openTimes = _getOpeningHours(widget.cafeteriaName, widget.weekDay);
    _lunchTimes = _getEatingHours(widget.cafeteriaName, widget.weekDay);
  }

  @override
  Widget build(BuildContext context) => LayoutBuilder(
        builder: (context, constraints) => Row(
          mainAxisSize: MainAxisSize.max,
          children: [
            Column(
              children: [
                Row(
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    SizedBox(
                      height: widget.showBorderTimes ? 48 : 16,
                      width: constraints.maxWidth,
                      child: Stack(
                        children: [
                          Positioned(
                            top: widget.showBorderTimes ? 15.5 : 0,
                            child: SizedBox(
                              height: 37.5,
                              child: Stack(
                                children: [
                                  Container(
                                    width: constraints.maxWidth,
                                    height: barHeight,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(4),
                                      color: CorporateColors
                                          .cafeteriaOpeningBarGrey,
                                    ),
                                  ),
                                  ..._openTimes
                                      .where((e) =>
                                          e.containsKey('open') &&
                                          e.containsKey('close'))
                                      .map(
                                        (element) => DetailOpeningBarElement(
                                          color: CorporateColors
                                              .cafeteriaLightGreen,
                                          offset: _getHorizontalPosition(
                                              element['open'] ?? '',
                                              constraints.maxWidth),
                                          width: _getWidth(
                                              element['open'] ?? '',
                                              element['close'] ?? '',
                                              constraints.maxWidth),
                                        ),
                                      ),
                                  ..._lunchTimes
                                      .where((e) =>
                                          e.containsKey('open') &&
                                          e.containsKey('close'))
                                      .map(
                                        (element) => DetailOpeningBarElement(
                                          color: CorporateColors
                                              .cafeteriaDarkGreen,
                                          offset: _getHorizontalPosition(
                                              element['open'] ?? '',
                                              constraints.maxWidth),
                                          width: _getWidth(
                                              element['open'] ?? '',
                                              element['close'] ?? '',
                                              constraints.maxWidth),
                                          showBorderTimes:
                                              widget.showBorderTimes,
                                        ),
                                      )
                                ],
                              ),
                            ),
                          ),
                          if (widget.showBorderTimes && widget.isToday)
                            CurrentTimeIndicator(
                              offset: _getCurrentHorizontalPosition(
                                constraints.maxWidth,
                              ),
                              maxWidth: constraints.maxWidth,
                            ),
                        ],
                      ),
                    ),
                  ],
                ),
                if (widget.showBorderTimes)
                  Row(
                    children: [
                      SizedBox(
                        height: 12,
                        width: constraints.maxWidth,
                        child: Stack(
                          children: [
                            if (_lunchTimes.isNotEmpty)
                              for (var open in _lunchTimes)
                                Positioned(
                                  left: _getHorizontalPosition(
                                          open['open'] ?? '',
                                          constraints.maxWidth) -
                                      13,
                                  child: Text(
                                    open['open'].sub(0, 5),
                                    style: TextStyle(
                                      fontSize: 12,
                                      letterSpacing: -0.7,
                                    ),
                                  ),
                                ),
                            if (_lunchTimes.isNotEmpty)
                              for (var close in _lunchTimes)
                                Positioned(
                                  left: _getHorizontalPosition(
                                          close['close'] ?? '',
                                          constraints.maxWidth) -
                                      14,
                                  child: Text(
                                    close['close'].sub(0, 5),
                                    style: TextStyle(
                                      fontSize: 12,
                                      letterSpacing: -0.7,
                                    ),
                                  ),
                                ),
                          ],
                        ),
                      ),
                    ],
                  ),
              ],
            ),
          ],
        ),
      );

  /// A function that takes a relative [double] from [toDouble] and the
  /// [maxWidth] from [LayoutBuilder] to return a Position on the horizontal
  /// Axis
  double _getHorizontalPosition(String myTime, double maxWidth) {
    try {
      return _getCurrentHorizontalPosition(maxWidth,
          time: DateFormat.Hm().parse(myTime));
    } on Exception {
      return double.negativeInfinity;
    }
  }

  double _getCurrentHorizontalPosition(double maxWidth, {DateTime? time}) =>
      maxWidth * ((((toDouble(time ?? DateTime.now())) - 7) / 0.15) / 100);

  /// A function that takes takes the opening and closing times and converts
  /// returns the resulting relative width
  double _getWidth(String open, String close, double maxWidth) =>
      (_getHorizontalPosition(close, maxWidth) -
          _getHorizontalPosition(open, maxWidth));

  /// A function that takes a [DateTime] and returns it as a relative [double]
  double toDouble(DateTime time) {
    var myTime = TimeOfDay.fromDateTime(time);
    return myTime.hour + myTime.minute / 60.0;
  }

  /// A function that returns an instance of [CafeteriaOpeningHours]
  /// openingHours for the current [Cafeteria]
  List<Map<String, String>> _getOpeningHours(String name, String weekDay) =>
      BlocProvider.of<CafeteriaOpeningBloc>(context)
          .state
          .cafeteriaOpeningHours
          .firstWhere(
            (element) => element.name == name,
            orElse: CafeteriaOpeningHours.empty,
          )
          .openingHours[weekDay] ??
      [{}];

  /// A function that returns an instance of [CafeteriaOpeningHours] eatingHours
  /// for the current [Cafeteria]
  List<Map<String, String>> _getEatingHours(String name, String weekDay) =>
      BlocProvider.of<CafeteriaOpeningBloc>(context)
          .state
          .cafeteriaOpeningHours
          .firstWhere(
            (element) => element.name == name,
            orElse: CafeteriaOpeningHours.empty,
          )
          .eatingHours[weekDay] ??
      [{}];
}

/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:math';

import '../../../../common/constants/api_constants.dart';
import '../../model/cafeteria_item.dart';
import '../../model/characteristic.dart';
import '../../model/prices.dart';

final omnivoreDishes = [
  [
    'Lasagne Bolognese Fleisch Geprüfte Qualität - HESSEN',
    "$cafeteriaImageHost/daten-extern/sw-giessen/html/fotos/big/5nc5695cr_3.jpg"
  ],
  [
    'Feuriges Gemüse-Fisch-Curry mit Reis',
    "$cafeteriaImageHost/daten-extern/sw-giessen/html/fotos/big/7m8r6c776_img_2269.jpg"
  ],
  // [
  //   'Burger \"American Style\" mit Rindfleisch, Baconjam, BBQ-Ketchup, karamellisierte  roten Zwiebeln und Coleslaw im Roggen Bun  dazu Pommes frites  Fleisch \"Geprüfte Qualität - HESSEN\"',
  //   "$cafeteriaImageHost/daten-extern/sw-giessen/html/fotos/big/n35m96r31_burger_american_style.jpg"
  // ],
];

final vegetarianDishes = [
  [
    // ignore: no_adjacent_strings_in_list
    'Vegetarischer-Burger mit Quorn-Patty, karamellisierten roten Zwiebeln,  '
        'Kerbel und BBQ-Ketchup im Red Love Bun  dazu Pommes frites',
    "$cafeteriaImageHost/daten-extern/sw-giessen/html/fotos/big/s281r6857_burger-tag.jpg"
  ],
  [
    'Vollkornspaghetti mit Lauch-Käse-Sauce',
    "$cafeteriaImageHost/daten-extern/sw-giessen/html/fotos/big/87mn17esc_img_4220.jpg"
  ],
  // [
  //   'Ofenkartoffeln mit Quark von Grüner Sauce  an einer Salatgarnitur  Kartoffeln \"Geprüfte Qualität - HESSEN\"',
  //   "$cafeteriaImageHost/daten-extern/sw-giessen/html/fotos/big/s836338s2_gemuese_kraeuter.jpg"
  // ],
];

final veganDishes = [
  [
    'Linsen-Kartoffel-Kokos-Pfanne dazu Blattsalat mit Himbeerdressing',
    "$cafeteriaImageHost/daten-extern/sw-giessen/html/fotos/big/621n9rer5_3.jpg"
  ],
  [
    // ignore: no_adjacent_strings_in_list
    'Scharfes Kürbisgemüse mit roten Linsen und Rucola  auf Pasta p.P. '
        '707 kcal &bull; 89 KH &bull; 23g E &bull; 23g F',
    "$cafeteriaImageHost/daten-extern/sw-giessen/html/fotos/big/66c62m612_img_4202.jpg"
  ],
  // [
  //   'Sojafrikassee  mit buntem Gemüse  und Reis p.P.
  //   761 kcal &bull; 92 KH &bull; 29g E &bull; 27g F',
  //   "$cafeteriaImageHost/daten-extern/sw-giessen/html/fotos/big/874199251_img_4103.jpg"
  // ],
];

//TODO: noch nicht vollständig
final allergensList = [
  ["32", "Eier"],
  ["36", "Milch"],
  ["41", "Schwefeldioxid und Sulfite"],
  ["30", "Glutenhaltiges Getreide"],
  ["30a", "Weizen"],
  ["38", "Sellerie"],
  ["30a", "Weizen"],
  ["36", "Milch"],
];

//TODO: noch nicht vollständig
final additivesList = [
  [1, "Farbstoff"],
  [2, "Konservierungsstoff"],
  [3, "Antioxidationsmittel"],
  [5, "Geschwefelt"],
  [6, "Geschwärzt"],
  [9, "Süßungsmittel"],
  [10, "Alkohol"],
];
final List<Characteristic> characteristicsList = [
  Characteristic(
      abbreviation: "GQH",
      name: "Geprüfte Qualität Hessen",
      image: "$cafeteriaImageHost/daten-extern/sw-giessen/html/icons/GQH.png"),
  Characteristic(
      abbreviation: "S",
      name: "Schwein",
      image: "$cafeteriaImageHost/daten-extern/sw-giessen/html/icons/S.png"),
  Characteristic(
      abbreviation: "EIG",
      name: "Eigenproduktion",
      image: "$cafeteriaImageHost/daten-extern/sw-giessen/html/icons/EIG.png"),
  // Characteristic(
  //     abbreviation: "V",
  //     name: "vegetarisch",
  //     image:
  //         "$cafeteriaImageHost/daten-extern/sw-giessen/html/icons/V.png"),
  // Characteristic(
  //     abbreviation: "VEG",
  //     name: "Vegan",
  //     image:
  //         "$cafeteriaImageHost/daten-extern/sw-giessen/html/icons/VEG.png"),
  Characteristic(
      abbreviation: "R",
      name: "Rind",
      image: "$cafeteriaImageHost/daten-extern/sw-giessen/html/icons/R.png"),
  Characteristic(
      abbreviation: "KNO",
      name: "Knoblauch",
      image: "$cafeteriaImageHost/daten-extern/sw-giessen/html/icons/KNO.png"),
  Characteristic(
      abbreviation: "F",
      name: "Fisch",
      image: "$cafeteriaImageHost/daten-extern/sw-giessen/html/icons/F.png"),
  Characteristic(
      abbreviation: "MSC",
      name: "Fisch",
      image: "$cafeteriaImageHost/daten-extern/sw-giessen/html/icons/MSC.png"),
  Characteristic(
      abbreviation: "G",
      name: "Geflügel",
      image: "$cafeteriaImageHost/daten-extern/sw-giessen/html/icons/G.png"),
  Characteristic(
      abbreviation: "L",
      name: "Lamm",
      image: "$cafeteriaImageHost/daten-extern/sw-giessen/html/icons/L.png"),
  Characteristic(
      abbreviation: "A",
      name: "Alkohol",
      image: "$cafeteriaImageHost/daten-extern/sw-giessen/html/icons/A.png"),
  Characteristic(
      abbreviation: "GL",
      name: "Gelatine",
      image: "$cafeteriaImageHost/daten-extern/sw-giessen/html/icons/GL.png"),
  Characteristic(
      abbreviation: "MV",
      name: "mensaVital",
      image: "$cafeteriaImageHost/daten-extern/sw-giessen/html/icons/MV.png"),
  Characteristic(
      abbreviation: "HOT",
      name: "scharf",
      image: "$cafeteriaImageHost/daten-extern/sw-giessen/html/icons/HOT.png"),
  Characteristic(
      abbreviation: "NEU",
      name: "Neu",
      image: "$cafeteriaImageHost/daten-extern/sw-giessen/html/icons/NEU.png"),
];

//  "category": "Information",
//       "date": "2020-03-27",
//       "description_clean": "In unserem Grillwagen: Grillklassiker  • Putensteak  • Rindswurst  • Bratwurst  Rind- und Schweinefleisch \"Geprüfte Qualität - HESSEN\"",
//       "description": "In unserem Grillwagen: Grillklassiker  • Putensteak  • Rindswurst (2,3,8,39)  • Bratwurst (3,8,39)  Rind- und Schweinefleisch \"Geprüfte Qualität - HESSEN\"",
//       "artikel": "In unserem Grillwagen:",
//       "beschreibung": "Grillklassiker <br>• Putensteak <br>• Rindswurst (2,3,8,39) <br>• Bratwurst (3,8,39) #Rind- und Schweinefleisch \"Geprüfte Qualität - HESSEN\"",
//       "artikel_clean": "In unserem Grillwagen:",
//       "beschreibung_clean": "Grillklassiker <br>• Putensteak <br>• Rindswurst <br>• Bratwurst #Rind- und Schweinefleisch \"Geprüfte Qualität - HESSEN\"",
//       "prices": {
//           "staff": "k.A.",
//           "guest": "k.A.",
//           "student": "k.A."

// TODO: Cleanup
class CafeteriaItemGenerator {
  static List<CafeteriaItem> generateRandomItemList({
    required int input,
  }) {
    var cafeteriaItemList = <CafeteriaItem>[];
    cafeteriaItemList.add(CafeteriaItem.defaultItem());
    cafeteriaItemList.add(CafeteriaItem.defaultVegetarianItem());
    cafeteriaItemList.add(CafeteriaItem.defaultVeganItem());
    for (var i = 0; i < input; i++) {
      cafeteriaItemList.add(getRandomItem());
    }
    return cafeteriaItemList;
  }

  static CafeteriaItem getRandomItem() {
    var categoryList = [
      'Ausgabe 1',
      'Ausgabe 2',
      'Ausgabe 3',
    ];
    var category = (categoryList..shuffle()).first;

    var dateList = [
      '2020-04-06',
      '2020-04-07',
      '2020-04-08',
      '2020-04-09',
      '2020-04-10',
    ];
    var date = (dateList..shuffle()).first;

    var type = CafeteriaItemType
        .values[Random().nextInt(CafeteriaItemType.values.length)];
    final dish = <String>[];
    if (type == CafeteriaItemType.omnivore) {
      dish.addAll((omnivoreDishes..shuffle()).first);
    } else if (type == CafeteriaItemType.vegetarian) {
      dish.addAll((vegetarianDishes..shuffle()).first);
    } else if (type == CafeteriaItemType.vegan) {
      dish.addAll((veganDishes..shuffle()).first);
    }

    var descriptionClean = dish[0];
    var description = dish[0];
    var artikel = dish[0];
    var beschreibung = dish[0];
    var artikelClean = dish[0];
    var beschreibungClean = dish[0];
    var image = dish[1];
    var prices = Prices("4,90", "5,40", "2,90");
    // var allergens = {
    //   "2": "Konservierungsstoff",
    //   "3": "Antioxidationsmittel",
    //   "5": "Geschwefelt"
    // };

    // var additives = {
    //   2: "Konservierungsstoff",
    //   3: "Antioxidationsmittel",
    //   5: "Geschwefelt"
    // };

    //characteristics
    var tempListCharacteristics = <Characteristic>[];
    var characteristics = characteristicsList..shuffle();
    var amountCharacteristics =
        Random().nextInt(characteristicsList.length) % 6;
    for (var i = 0; i < amountCharacteristics; i++) {
      var abb = characteristics[i].abbreviation;
      if (type == CafeteriaItemType.omnivore) {
        if (abb == "V" || abb == "VEG") continue;
      }
      if (type == CafeteriaItemType.vegetarian ||
          type == CafeteriaItemType.vegan) {
        if (abb == "S" ||
            abb == "R" ||
            abb == "G" ||
            abb == "L" ||
            abb == "F") {
          continue;
        }
      }
      if (type == CafeteriaItemType.vegan) {
        if (abb == "GL") continue;
      }
      tempListCharacteristics.add(characteristics[i]);
    }

    if (type == CafeteriaItemType.omnivore) {
      //nothing
    }
    if (type == CafeteriaItemType.vegetarian) {
      tempListCharacteristics.add(Characteristic(
          abbreviation: "V",
          name: "vegetarisch",
          image:
              "$cafeteriaImageHost/daten-extern/sw-giessen/html/icons/V.png"));
    }
    if (type == CafeteriaItemType.vegan) {
      tempListCharacteristics.add(Characteristic(
          abbreviation: "VEG",
          name: "Vegan",
          image:
              "$cafeteriaImageHost/daten-extern/sw-giessen/html/icons/VEG.png"));
    }
    characteristics = tempListCharacteristics;

    //allergens
    var tempAllergensMap = <String, String>{};
    var allergensTempList = allergensList..shuffle();
    var amountAllergens = Random().nextInt(allergensTempList.length) % 6;
    for (var i = 0; i < amountAllergens; i++) {
      tempAllergensMap[allergensTempList[i].first] = allergensTempList[i].last;
    }
    var allergens = tempAllergensMap;

    //additives
    var tempAdditivesMap = <int, String>{};
    var additivesTempList = additivesList..shuffle();
    var amountAdditives = Random().nextInt(additivesTempList.length) % 6;
    for (var i = 0; i < amountAdditives; i++) {
      tempAdditivesMap[additivesTempList[i].first as int] =
          additivesTempList[i].last as String;
    }
    var additives = tempAdditivesMap;

    return CafeteriaItem(
      category: category,
      date: date,
      descriptionClean: descriptionClean,
      description: description,
      artikel: artikel,
      beschreibung: beschreibung,
      artikelClean: artikelClean,
      beschreibungClean: beschreibungClean,
      prices: prices,
      allergens: allergens,
      additives: additives,
      characteristics: characteristics,
      image: image,
    );
  }
}

enum CafeteriaItemType { omnivore, vegetarian, vegan }

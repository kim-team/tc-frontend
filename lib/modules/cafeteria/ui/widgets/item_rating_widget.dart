/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../../../common/tc_theme.dart';
import '../../../../common/tinycampus_icons.dart';
import '../../bloc/cafeteria_feedback/cafeteria_feedback_bloc.dart';

class ItemRatingWidget extends StatefulWidget {
  ///Callback for a rating function
  final void Function(double) setRating;

  ///Constructor for the [ItemRatingWidget]
  ItemRatingWidget({
    Key? key,
    required this.setRating,
  }) : super(key: key);

  @override
  _ItemRatingWidgetState createState() => _ItemRatingWidgetState();
}

class _ItemRatingWidgetState extends State<ItemRatingWidget> {
  bool didRate = false;
  bool isAllowedToRate = true;
  int localRating = 5;
  bool didInitialize = false;

  int roundTo(double n, int multipleOf) {
    var divisions = (n / multipleOf).floor();
    return divisions * multipleOf;
  }

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(milliseconds: 800)).then((value) {
      debugPrint(context
          .read<CafeteriaFeedbackBloc>()
          .state
          .itemFeedback
          .rating
          .userRating
          .toString());
      setState(() {
        localRating = context
            .read<CafeteriaFeedbackBloc>()
            .state
            .itemFeedback
            .rating
            .userRating;
        didRate = localRating != -1;
        didInitialize = true;
      });
    });
  }

  IconData determineIcon(double val) => val == 0
      ? TinyCampusIcons.star_0p
      : val == 25
          ? TinyCampusIcons.star_25p
          : val == 50
              ? TinyCampusIcons.star_50p
              : val == 75
                  ? TinyCampusIcons.star_75p
                  : val == 100
                      ? TinyCampusIcons.star_100p
                      : TinyCampusIcons.star_0p;

  @override
  Widget build(BuildContext context) =>
      BlocBuilder<CafeteriaFeedbackBloc, CafeteriaFeedbackState>(
        builder: (context, state) => Column(
          children: <Widget>[
            BlocListener<CafeteriaFeedbackBloc, CafeteriaFeedbackState>(
              listener: (context, listenerState) {
                if (listenerState is CafeteriaFeedbackErrorState) {
                  setState(() {
                    isAllowedToRate = false;
                  });
                }
              },
              child: Container(),
            ),
            AnimatedOpacity(
              duration: Duration(milliseconds: 120),
              opacity: didInitialize ? 1 : 0.2,
              child: Wrap(
                alignment: WrapAlignment.center,
                spacing: 8,
                children: [
                  ...List<TweenAnimationBuilder>.generate(
                    5,
                    (index) {
                      var selected = (localRating == index + 1) & didRate;
                      var percent = 100.0;
                      if (state.itemFeedback.rating.avgRating >= index) {
                        percent = 100.0;
                      } else {
                        percent = 0;
                      }
                      if ((state.itemFeedback.rating.avgRating.floor()) ==
                          index) {
                        percent = roundTo(
                                (state.itemFeedback.rating.avgRating % 1) * 100,
                                25)
                            .toDouble();
                      }
                      return TweenAnimationBuilder<double>(
                        duration: Duration(milliseconds: selected ? 700 : 1200),
                        curve: Curves.elasticOut,
                        tween: Tween<double>(
                          begin: 0.8,
                          end: selected ? 1.25 : 1.0,
                        ),
                        builder: (context, i, child) => GestureDetector(
                          onTap: () {
                            setState(() {
                              if (isAllowedToRate) {
                                widget.setRating(index.toDouble() + 1.0);
                                didRate = true;
                                localRating = index + 1;
                              } else {
                                // so that the user sees that he can't do it
                                widget.setRating(index.toDouble() + 1.0);
                              }
                            });
                          },
                          child: Transform.rotate(
                            angle: selected ? sin(i * pi * 2 * 2) / 12 : 0.0,
                            child: Transform.scale(
                              scale: i,
                              child: TweenAnimationBuilder<Color?>(
                                duration: Duration(milliseconds: 250),
                                tween: ColorTween(
                                  begin: CorporateColors.tinyCampusIconGrey,
                                  end: didRate
                                      ? CorporateColors.tinyCampusOrange
                                          .withOpacity(selected ? 1.0 : 0.9)
                                      : CorporateColors.tinyCampusIconGrey
                                          .withOpacity(0.5),
                                ),
                                builder: (context, value, child) => Icon(
                                    !didInitialize
                                        ? TinyCampusIcons.star_0p
                                        : didRate &&
                                                localRating.toInt() >= index + 1
                                            ? TinyCampusIcons.star_100p
                                            : didRate &&
                                                    localRating.toInt() <
                                                        index + 1
                                                ? TinyCampusIcons.star_0p
                                                : determineIcon(percent),
                                    size: 40,
                                    color: value),
                              ),
                            ),
                          ),
                        ),
                      );
                    },
                  ),
                ],
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                if (state.itemFeedback.rating.ratings == 0) ...[
                  I18nText(
                    'modules.cafeteria.items.feedback.no_rating',
                    child: Text(
                      "Noch keine Bewertungen",
                    ),
                  ),
                ] else ...[
                  Text(
                    "⌀ ",
                    style: TextStyle(
                      fontSize: 21,
                      color: Colors.grey[600],
                    ),
                  ),
                  TweenAnimationBuilder<double>(
                    duration: Duration(milliseconds: 600),
                    curve: Curves.easeOut,
                    tween: Tween<double>(
                        begin: 0, end: state.itemFeedback.rating.avgRating),
                    builder: (context, value, child) => Text(
                      value.toStringAsFixed(2),
                      style: TextStyle(fontWeight: FontWeight.w300),
                    ),
                  ),
                  Text(
                    "  (${state.itemFeedback.rating.ratings.toString()} "
                    "${FlutterI18n.translate(context, 'modules.cafeteria.'
                        'items.feedback.ratings')}",
                    style: TextStyle(fontWeight: FontWeight.w300),
                  ),
                ]
              ],
            ),
          ],
        ),
      );
}

/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../../../common/tc_theme.dart';

/// A Widget that recommends the Wizard to the User
class CafeteriaWizardRecommendationWidget extends StatelessWidget {
  const CafeteriaWizardRecommendationWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: 15,
          vertical: 20,
        ),
        child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(12),
            ),
          ),
          elevation: 0,
          child: Padding(
            padding: const EdgeInsets.only(
              left: 32,
              right: 16,
              top: 12,
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 15),
                  child: I18nText(
                    'modules.cafeteria.wizard.intro.intro_first',
                    child: Text(
                      '',
                      style: Theme.of(context).textTheme.headline1,
                    ),
                  ),
                ),
                I18nText(
                  'modules.cafeteria.wizard.intro.intro_second',
                  child: Text(
                    '',
                    style: Theme.of(context)
                        .textTheme
                        .headline4
                        ?.copyWith(color: CorporateColors.tinyCampusIconGrey),
                  ),
                ),
                Container(height: 18),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(right: 4.0),
                      child: Icon(
                        Icons.check_circle,
                        color: CorporateColors.tinyCampusGreen,
                      ),
                    ),
                    Expanded(
                      child: I18nText(
                        'modules.cafeteria.wizard.intro.selection_and_order',
                        child: Text(
                          '',
                          style: Theme.of(context).textTheme.headline4,
                        ),
                      ),
                    ),
                  ],
                ),
                Container(
                  height: 4.0,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(right: 4.0),
                      child: Icon(
                        Icons.check_circle,
                        color: CorporateColors.tinyCampusGreen,
                      ),
                    ),
                    Expanded(
                      child: I18nText(
                        'modules.cafeteria.wizard.intro.diet',
                        child: Text(
                          '',
                          style: Theme.of(context).textTheme.headline4,
                        ),
                      ),
                    ),
                  ],
                ),
                Container(
                  height: 4.0,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(right: 4.0),
                      child: Icon(
                        Icons.check_circle,
                        color: CorporateColors.tinyCampusGreen,
                      ),
                    ),
                    Expanded(
                      child: I18nText(
                        'modules.cafeteria.wizard.intro.allergies',
                        child: Text(
                          '',
                          style: Theme.of(context).textTheme.headline4,
                        ),
                      ),
                    ),
                  ],
                ),
                Container(
                  height: 4.0,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(right: 4.0),
                      child: Icon(
                        Icons.check_circle,
                        color: CorporateColors.tinyCampusGreen,
                      ),
                    ),
                    Expanded(
                      child: I18nText(
                        'modules.cafeteria.wizard.intro.dislikes',
                        child: Text(
                          '',
                          style: Theme.of(context).textTheme.headline4,
                        ),
                      ),
                    ),
                  ],
                ),
                Container(
                  height: 4.0,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(right: 4.0),
                      child: Icon(
                        Icons.check_circle,
                        color: CorporateColors.tinyCampusGreen,
                      ),
                    ),
                    Expanded(
                      child: I18nText(
                        'modules.cafeteria.wizard.intro.price',
                        child: Text(
                          '',
                          style: Theme.of(context).textTheme.headline4,
                        ),
                      ),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(
                    top: 20,
                    bottom: 28,
                  ),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: I18nText(
                          'modules.cafeteria.wizard.intro.recommended',
                          child: Text(
                            '',
                            style: TextStyle(
                              fontSize: 18,
                              color: CorporateColors.tinyCampusIconGrey,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      );
}

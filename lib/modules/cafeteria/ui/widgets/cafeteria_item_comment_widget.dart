/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../../../common/tc_theme.dart';
import '../../../../common/widgets/code_of_conduct/coc_info_text.dart';
import '../../../../common/widgets/tc_form_field/tc_form_field.dart';
import '../../../../common/widgets/tc_form_field/tc_form_field_model.dart';
import '../../bloc/cafeteria_feedback/cafeteria_feedback_bloc.dart';

/// A Widget that uses void callbacks to allow the [User] to comment a
/// [CafeteriaItem]
class CafeteriaItemCommentWidget extends StatefulWidget {
  final void Function(String) submitComment;

  const CafeteriaItemCommentWidget({
    Key? key,
    required this.submitComment,
  }) : super(key: key);

  @override
  _CafeteriaItemCommentWidgetState createState() =>
      _CafeteriaItemCommentWidgetState();
}

class _CafeteriaItemCommentWidgetState
    extends State<CafeteriaItemCommentWidget> {
  final TextEditingController _controller = TextEditingController();

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) =>
      BlocBuilder<CafeteriaFeedbackBloc, CafeteriaFeedbackState>(
        builder: (context, state) => state.commenting
            ? Column(
                children: <Widget>[
                  TCFormField(
                    formFieldModel: TCFormFieldModel(
                      title: FlutterI18n.translate(
                        context,
                        'modules.cafeteria.items.feedback.your_comment',
                      ),
                      hintText: FlutterI18n.translate(
                        context,
                        'modules.cafeteria.items.feedback.feedback_hint',
                      ),
                      maxLength: 140,
                      valid: [
                        (value) {
                          if (value.trim().isEmpty) {
                            return FlutterI18n.translate(context,
                                'modules.q_and_a.number_of_characters');
                          }
                          return null;
                        }
                      ],
                      controller: _controller,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                      left: 5,
                      right: 5,
                      top: 8,
                      bottom: 24,
                    ),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: CocInfoTextWidget(),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(
                      bottom: 10,
                      left: 5,
                      right: 10,
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Expanded(
                          flex: 1,
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(0, 0, 10, 0),
                            child: RaisedButton(
                              padding: EdgeInsets.symmetric(vertical: 16.0),
                              color: Colors.white,
                              textColor: CorporateColors.tinyCampusBlue,
                              clipBehavior: Clip.antiAlias,
                              child: I18nText(
                                'common.actions.cancel',
                                child: Text(
                                  '',
                                  style: Theme.of(context).textTheme.headline4,
                                ),
                              ),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8),
                                side: BorderSide(
                                  color: CorporateColors.tinyCampusBlue,
                                ),
                              ),
                              onPressed: () {
                                BlocProvider.of<CafeteriaFeedbackBloc>(context)
                                    .add(ChangeCommentingEvent(
                                        commenting: false));
                              },
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 1,
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                            child: RaisedButton(
                              padding: EdgeInsets.symmetric(vertical: 16.0),
                              color: CorporateColors.tinyCampusBlue,
                              textColor: Colors.white,
                              clipBehavior: Clip.antiAlias,
                              child: I18nText(
                                'common.actions.confirm',
                                child: Text(
                                  '',
                                  style: Theme.of(context)
                                      .textTheme
                                      .headline4
                                      ?.copyWith(color: Colors.white),
                                ),
                              ),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8),
                                side: BorderSide(
                                  color: CorporateColors.tinyCampusBlue,
                                ),
                              ),
                              onPressed: () {
                                widget.submitComment(_controller.text);
                                if (_controller.text.trim().isNotEmpty) {
                                  _controller.clear();
                                }
                              },
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              )
            : Card(
                elevation: 0.0,
                child: FlatButton(
                  onPressed: () {
                    BlocProvider.of<CafeteriaFeedbackBloc>(context)
                        .add(ChangeCommentingEvent(commenting: true));
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 10, vertical: 5),
                        child: CircleAvatar(
                          backgroundColor: Color.fromARGB(255, 240, 240, 240),
                          foregroundColor: CorporateColors.tinyCampusIconGrey,
                          child: Icon(Icons.add),
                        ),
                      ),
                      I18nText(
                        'modules.cafeteria.'
                        'items.feedback.comment',
                        child: Text(
                          '',
                          style: Theme.of(context).textTheme.headline4,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
      );
}

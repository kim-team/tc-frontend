/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../bloc/cafeteria_feedback/cafeteria_feedback_bloc.dart';
import '../../model/cafeteria_item.dart';
import '../../model/cafeteria_settings.dart';
import '../../model/characteristic.dart';
import '../text_helper.dart';
import 'cafeteria_item_comment_column.dart';
import 'cafeteria_item_comment_widget.dart';
import 'cafeteria_item_page_allergen_row.dart';
import 'cafeteria_item_price_row.dart';
import 'item_rating_widget.dart';

/// A drawer Widget for the [CafeteriaItemPage]
class CafeteriaItemPageDrawer extends StatelessWidget {
  final CafeteriaItem item;
  final String name;
  final String detectedAllergens;
  final String detectedDislikes;
  final CafeteriaSettings settings;
  final void Function(double) getPosition;

  /// Flag to hide certain fields for announcements
  final bool _isNotAnnouncement;

  CafeteriaItemPageDrawer({
    Key? key,
    required this.item,
    required this.settings,
    required this.name,
    required this.detectedAllergens,
    required this.detectedDislikes,
    required this.getPosition,
  })  : _isNotAnnouncement = (item.category.toLowerCase()) != 'information',
        super(key: key);

  @override
  Widget build(BuildContext context) =>
      BlocBuilder<CafeteriaFeedbackBloc, CafeteriaFeedbackState>(
        builder: (state, context) => ClipRRect(
          borderRadius: BorderRadius.circular(30.0),
          child: LayoutBuilder(
            builder: (context, constraints) => Container(
              width: constraints.maxWidth,
              color: Colors.white,
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.fromLTRB(
                      30,
                      30,
                      30,
                      15,
                    ),
                    child: SizedBox(
                      width: constraints.maxWidth,
                      child: Text(
                        cleanupText(_isNotAnnouncement
                            ? item.artikelClean
                            : item.descriptionClean),
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 18.0,
                        ),
                      ),
                    ),
                  ),
                  if (_isNotAnnouncement) ...[
                    CafeteriaItemPriceRow(
                      item: item,
                      settings: settings,
                    ),
                    Container(
                      padding: EdgeInsets.fromLTRB(30, 30, 30, 0),
                      width: constraints.maxWidth,
                      child: I18nText(
                        'modules.cafeteria.items.description',
                        child: Text(
                          'Beschreibung',
                          style: Theme.of(context).textTheme.headline3,
                        ),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.fromLTRB(30, 5, 30, 30),
                      width: constraints.maxWidth,
                      child: Text(cleanupText(item.description)),
                    ),
                    CafeteriaItemPageAllergenRow(
                      constraints: constraints,
                      item: item,
                      settings: settings,
                    ),
                    _generatePersonalHint(
                      detectedAllergens,
                      detectedDislikes,
                      settings,
                      context,
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(30, 0, 30, 15),
                      child: I18nText(
                        'modules.cafeteria.items.filter_hint',
                        child: Text(
                          "In den Einstellung kannst du anpassen, "
                          "welche Allergien du hast und was du nicht "
                          "essen möchtest.",
                          style: TextStyle(
                            color: Colors.grey,
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(30, 0, 30, 15),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          I18nText(
                            'modules.cafeteria.items.disclaimer',
                            child: Text(
                              '',
                              style: Theme.of(context).textTheme.bodyText1,
                            ),
                          ),
                          Text(
                            'Studentenwerk Gießen A.d.ö.R.',
                            style: Theme.of(context).textTheme.bodyText1,
                          ),
                        ],
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        for (var e in item.characteristics)
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Image.network(
                              e.image,
                              scale: 4,
                            ),
                          ),
                      ],
                    ),
                  ],
                  _feedbackColumn(context),
                ],
              ),
            ),
          ),
        ),
      );

  Widget _feedbackColumn(BuildContext context) => Column(
        children: [
          if (_isNotAnnouncement)
            Padding(
              padding: const EdgeInsets.symmetric(
                vertical: 10,
              ),
              child: ItemRatingWidget(
                setRating: (rating) {
                  var _rating = rating > 0 ? rating : rating + 1;
                  BlocProvider.of<CafeteriaFeedbackBloc>(context)
                      .add(PostRatingEvent(
                    item.getUniqueId(cafeteriaName: name),
                    _rating.toInt(),
                  ));
                },
              ),
            ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 30, vertical: 10),
            child: CafeteriaItemCommentWidget(
              submitComment: (text) {
                if (text.trim().isNotEmpty) {
                  BlocProvider.of<CafeteriaFeedbackBloc>(context).add(
                    PostCommentEvent(
                        item.getUniqueId(cafeteriaName: name), text.trim()),
                  );
                  BlocProvider.of<CafeteriaFeedbackBloc>(context).add(
                    ChangeCommentingEvent(commenting: false),
                  );
                }
              },
            ),
          ),
          CafeteriaCommentColumn(
            name: name,
            item: item,
          ),
          Container(height: 210),
        ],
      );

  /// A function that takes a List of [Characteristic] and a [String]
  /// and returns true if [abbr] is contained in [characteristics] as an
  /// abbreviation field
  bool _contains(List<Characteristic> characteristics, String abbr) =>
      characteristics.any((c) => c.abbreviation == abbr);

  /// A function that returns a Widget that contains a warning for the User if
  /// the [CafeteriaItem] contains ingredients that the User has selected
  /// to avoid in the [CafeteriaSettings]
  Widget _generatePersonalHint(
    String detectedAllergens,
    String detectedDislikes,
    CafeteriaSettings settings,
    BuildContext context,
  ) {
    if (item.characteristics.isNotEmpty) {
      var filterWarning = "";
      var allergenWarning = "";
      var dislikesWarning = "";

      if (settings.filter == CafeteriaFilter.vegetarian &&
          !_contains(item.characteristics, "V") &&
          !_contains(item.characteristics, "VEG")) {
        filterWarning = 'not_vegetarian';
      }
      if (filterWarning.isEmpty &&
          settings.filter == CafeteriaFilter.vegan &&
          !_contains(item.characteristics, "VEG")) {
        filterWarning = "not_vegan";
      }
      if (detectedAllergens.isNotEmpty) {
        allergenWarning = "allergic";
      }
      if (detectedDislikes.isNotEmpty) {
        dislikesWarning = "dislike";
      }
      if (filterWarning.isNotEmpty ||
          allergenWarning.isNotEmpty ||
          dislikesWarning.isNotEmpty) {
        return SizedBox(
          width: MediaQuery.of(context).size.width,
          child: Padding(
            padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                I18nText(
                  'modules.cafeteria.items.warnings.personal_notes',
                  child: Text(
                    "Persönliche Hinweise",
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                if (allergenWarning.isNotEmpty)
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 5, 0, 2.5),
                    child: I18nText(
                      'modules.cafeteria.items.warnings.allergic',
                      child: Text(
                        allergenWarning,
                        style: TextStyle(
                          color: Colors.red,
                        ),
                      ),
                    ),
                  ),
                if (dislikesWarning.isNotEmpty)
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 2.5, 0, 5),
                    child: I18nText(
                      'modules.cafeteria.items.warnings.dislike',
                      child: Text(
                        dislikesWarning,
                        style: TextStyle(
                          color: Colors.red,
                        ),
                      ),
                    ),
                  ),
                if (filterWarning.isNotEmpty)
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 2.5, 0, 5),
                    child: I18nText(
                      'modules.cafeteria.items.warnings.$filterWarning',
                      child: Text(
                        filterWarning,
                        style: TextStyle(
                          color: Colors.red,
                        ),
                      ),
                    ),
                  ),
              ],
            ),
          ),
        );
      } else {
        return Container();
      }
    } else {
      return Container();
    }
  }
}

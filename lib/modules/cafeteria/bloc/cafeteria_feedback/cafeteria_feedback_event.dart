/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

part of 'cafeteria_feedback_bloc.dart';

/// A [CafeteriaFeedbackEvent] handles user input and server requests
/// for [CafeteriaItemFeedback]
@immutable
abstract class CafeteriaFeedbackEvent extends Equatable {
  const CafeteriaFeedbackEvent();

  CafeteriaFeedbackState _getNextState(CafeteriaFeedbackBloc bloc);
  Future<void> _performAction(CafeteriaFeedbackBloc bloc);

  @override
  List<Object> get props => [];
}

/// A [CafeteriaFeedbackEvent] that takes an [id] and gets feedback data from
/// backend by [id]
/// The data is used to construct a [CafeteriaItemFeedback]
/// Upon success, the [CafeteriaItemFeedback] is used
/// to call [LoadedFeedbackEvent]
class LoadFeedbackEvent extends CafeteriaFeedbackEvent {
  final String id;

  LoadFeedbackEvent(this.id);

  @override
  CafeteriaFeedbackState _getNextState(CafeteriaFeedbackBloc bloc) =>
      LoadingCafeteriaFeedbackState._fromState(
          bloc.state.copyWith(commenting: false));

  @override
  Future<void> _performAction(CafeteriaFeedbackBloc bloc) async {
    var rating = CafeteriaItemRating.initialData();
    var comments = <CafeteriaItemComment>[];

    try {
      var ratingResult = await bloc._httpRepo.read(ratingsByItemIdUrl(id));
      var ratingJson = jsonDecode(ratingResult) as Map<String, dynamic>;
      rating = CafeteriaItemRating.fromJson(ratingJson);
    } on Exception catch (e) {
      // Todo: Maybe Localize?
      bloc.add(CafeteriaFeedbackErrorEvent(
        (e is HttpRepoException) ? e.message : e.toString(),
      ));
    }

    try {
      var commentResult = await bloc._httpRepo.read(commentsByItemIdUrl(id));
      var jsonList = jsonDecode(commentResult) as List<dynamic>;
      comments = [for (var map in jsonList) CafeteriaItemComment.fromJson(map)];
    } on Exception catch (e) {
      // Todo: Maybe Localize?
      bloc.add(CafeteriaFeedbackErrorEvent(
        (e is HttpRepoException) ? e.message : e.toString(),
      ));
    }

    bloc.add(LoadedFeedbackEvent(
        CafeteriaItemFeedback(rating: rating, comments: comments)));
  }

  @override
  List<Object> get props => [id];
}

/// A [CafeteriaFeedbackEvent] that takes an [id] and a [rating] to post the
/// [rating] to backend by [id]
/// On receiving a [TooManyRequestsException] it
/// calls a [CafeteriaFeedbackErrorEvent]
/// Otherwise it calls [LoadFeedbackEvent] using the [id]
class PostRatingEvent extends CafeteriaFeedbackEvent {
  final String id;
  final int rating;

  PostRatingEvent(this.id, this.rating);

  @override
  CafeteriaFeedbackState _getNextState(CafeteriaFeedbackBloc bloc) =>
      LoadingCafeteriaFeedbackState._fromState(bloc.state);

  @override
  Future<void> _performAction(CafeteriaFeedbackBloc bloc) async {
    var postedRating = {"rating": rating};
    var ratingJson = jsonEncode(postedRating);
    try {
      await bloc._httpRepo.post(ratingsByItemIdUrl(id), body: ratingJson);
      bloc.add(LoadFeedbackEvent(id));
    } on TooManyRequestsException catch (_) {
      bloc.add(CafeteriaFeedbackErrorEvent("AlreadyPosted"));
    } on SocketException catch (e) {
      // this happens for example if you leave your wlan and enter mobile data
      bloc.add(CafeteriaFeedbackErrorEvent(
        e.toString(),
      ));
    } on Exception catch (e) {
      bloc.add(CafeteriaFeedbackErrorEvent(
        (e is HttpRepoException) ? e.message : e.toString(),
      ));
    }
  }

  @override
  List<Object> get props => [id, rating];
}

/// A [CafeteriaFeedbackEvent] that takes a error [message] and
/// passes it to a [CafeteriaFeedbackErrorState]
/// Then it constructs a [CafeteriaItemRating] from either
/// persisted, currently held or default data and calls
/// a [LoadedFeedbackEvent]
class CafeteriaFeedbackErrorEvent extends CafeteriaFeedbackEvent {
  final String message;

  CafeteriaFeedbackErrorEvent(this.message);

  @override
  CafeteriaFeedbackState _getNextState(CafeteriaFeedbackBloc bloc) =>
      CafeteriaFeedbackErrorState._fromState(bloc.state, message);

  @override
  Future<void> _performAction(CafeteriaFeedbackBloc bloc) => Future(() {
        var _feedBack = CafeteriaItemFeedback(
          rating: CafeteriaItemRating(
            avgRating: bloc.state.itemFeedback.rating.avgRating,
            ratings: bloc.state.itemFeedback.rating.ratings,
            userRating: bloc.state.itemFeedback.rating.userRating,
          ),
          comments: bloc.state.itemFeedback.comments,
        );
        bloc.add(LoadedFeedbackEvent(_feedBack));
      });
}

/// A [CafeteriaFeedbackEvent] that takes an [id] and a [comment] to post
/// the [comment] to backend by [id]
/// On success it calls [LoadedFeedbackEvent]
class PostCommentEvent extends CafeteriaFeedbackEvent {
  final String id;
  final String comment;

  PostCommentEvent(this.id, this.comment);

  @override
  CafeteriaFeedbackState _getNextState(CafeteriaFeedbackBloc bloc) =>
      PostCommentState._fromState(bloc.state);

  @override
  Future<void> _performAction(CafeteriaFeedbackBloc bloc) async {
    debugPrint(comment);
    var postedComment = {"text": comment};
    var commentJson = jsonEncode(postedComment);
    try {
      await bloc._httpRepo.post(commentsByItemIdUrl(id), body: commentJson);
    } on Exception catch (e) {
      bloc.add(CafeteriaFeedbackErrorEvent(
        (e is HttpRepoException) ? e.message : e.toString(),
      ));
    }
    try {
      bloc.add(LoadFeedbackEvent(id));
    } on Exception catch (e) {
      bloc.add(CafeteriaFeedbackErrorEvent(
        (e is HttpRepoException) ? e.message : e.toString(),
      ));
    }
  }

  @override
  List<Object> get props => [id, comment];
}

/// A [CafeteriaFeedbackEvent] that takes [commenting] and passes it to a
/// [LoadedFeedbackEvent]
class ChangeCommentingEvent extends CafeteriaFeedbackEvent {
  final bool commenting;

  ChangeCommentingEvent({this.commenting = false});
  @override
  CafeteriaFeedbackState _getNextState(CafeteriaFeedbackBloc bloc) =>
      CommentingCafeteriaFeedbackState._fromState(
          bloc.state.copyWith(commenting: commenting));

  @override
  Future<void> _performAction(CafeteriaFeedbackBloc bloc) =>
      Future(() => bloc.add(LoadedFeedbackEvent(bloc.state.itemFeedback)));
}

/// A [CafeteriaFeedbackEvent] that takes [currentlyLiked], an [id] and [concId]
/// depending on [currentlyLiked] it either calls [unlikeItemCommentByIdUrl] or
/// [likeItemCommentByIdUrl] by [id] to backend
/// On success it calls [LoadFeedbackEvent] by [concId]
class LikeChangeEvent extends CafeteriaFeedbackEvent {
  final bool currentlyLiked;
  final int id;
  final String concId;

  LikeChangeEvent(
      {required this.concId, required this.currentlyLiked, required this.id});
  @override
  CafeteriaFeedbackState _getNextState(CafeteriaFeedbackBloc bloc) =>
      LikeChangeFeedbackState._fromState(bloc.state);

  @override
  Future<void> _performAction(CafeteriaFeedbackBloc bloc) {
    try {
      currentlyLiked
          ? bloc._httpRepo.post(unlikeItemCommentByIdUrl(id))
          : bloc._httpRepo.post(likeItemCommentByIdUrl(id));
    } on Exception catch (e) {
      bloc.add(CafeteriaFeedbackErrorEvent(
        (e is HttpRepoException) ? e.message : e.toString(),
      ));
    }
    bloc.add(LoadFeedbackEvent(concId));
    return Future.value();
  }
}

/// A [CafeteriaFeedbackEvent] that takes a [feedback] to go to
/// [IdleCafeteriaFeedbackState]
class LoadedFeedbackEvent extends CafeteriaFeedbackEvent {
  final CafeteriaItemFeedback feedback;

  LoadedFeedbackEvent(this.feedback);
  @override
  CafeteriaFeedbackState _getNextState(CafeteriaFeedbackBloc bloc) =>
      IdleCafeteriaFeedbackState._fromState(
          bloc.state.copyWith(itemFeedback: feedback));

  @override
  Future<void> _performAction(CafeteriaFeedbackBloc bloc) => Future.value();

  @override
  List<Object> get props => [feedback];
}

/// A [CafeteriaFeedbackEvent] that takes an [id]
/// and deletes a comment (given the fact that it "canEdit" is true)
class DeleteCommentEvent extends CafeteriaFeedbackEvent {
  final int id;
  final String concId;

  DeleteCommentEvent({
    required this.concId,
    required this.id,
  });
  @override
  CafeteriaFeedbackState _getNextState(CafeteriaFeedbackBloc bloc) =>
      DeleteCommentCafeteriaFeedbackState._fromState(bloc.state);
  // TODO: Check if we should add an additional state for deletion

  @override
  Future<void> _performAction(CafeteriaFeedbackBloc bloc) async {
    try {
      await bloc._httpRepo.delete(deleteItemCommentByIdUrl(id));
    } on Exception {
      bloc.add(CafeteriaFeedbackErrorEvent(
        "RemoveError",
      ));
    }
    bloc.add(LoadFeedbackEvent(concId));
    return Future.value();
  }
}

class CafeteriaFeedbackResetEvent extends CafeteriaFeedbackEvent {
  const CafeteriaFeedbackResetEvent();

  @override
  CafeteriaFeedbackState _getNextState(CafeteriaFeedbackBloc bloc) =>
      InitialCafeteriaFeedbackState();

  @override
  Future<void> _performAction(CafeteriaFeedbackBloc bloc) async {}
}

/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:async';
import 'dart:convert';

import 'package:copy_with_extension/copy_with_extension.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:meta/meta.dart';

import '../../../../bloc/perm_hyd_bloc/perm_hyd_bloc.dart';
import '../../../../repositories/http/http_repository.dart';
import '../../api_helper.dart';
import '../../model/opening_times/cafeteria_opening_hours.dart';

part 'cafeteria_opening_bloc.g.dart';
part 'cafeteria_opening_event.dart';
part 'cafeteria_opening_state.dart';

/// A BloC for [CafeteriaOpeningHours]
class CafeteriaOpeningBloc
    extends PermHydBloc<CafeteriaOpeningEvent, CafeteriaOpeningState> {
  final _httpRepo = HttpRepository();

  CafeteriaOpeningBloc() : super(InitialCafeteriaOpeningState());

  @override
  Stream<CafeteriaOpeningState> mapEventToState(
      CafeteriaOpeningEvent event) async* {
    yield event._getNextState(this);
    event._performAction(this);
  }

  @override
  CafeteriaOpeningState fromJson(Map<String, dynamic> json) =>
      CafeteriaOpeningState.fromJson(json);

  @override
  Map<String, dynamic> toJson(CafeteriaOpeningState state) => state.toJson();
}

/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

part of 'cafeteria_opening_bloc.dart';

/// A [CafeteriaOpeningEvent] handles the [CafeteriaOpeningHours] data
/// from backend
@immutable
abstract class CafeteriaOpeningEvent {
  CafeteriaOpeningState _getNextState(CafeteriaOpeningBloc bloc);
  Future<void> _performAction(CafeteriaOpeningBloc bloc);
}

/// A [CafeteriaOpeningEvent] that loads data from backend and creates
/// a [List] of [CafeteriaOpeningHours] for every cafeteria
/// and converts it to a [CafeteriaOpeningState] with which it then calls
/// [LoadedEvent]
class LoadOpeningEvent extends CafeteriaOpeningEvent {
  @override
  CafeteriaOpeningState _getNextState(CafeteriaOpeningBloc bloc) =>
      LoadingOpeningState.fromState(bloc.state);

  @override
  Future<void> _performAction(CafeteriaOpeningBloc bloc) async {
    try {
      var result = await bloc._httpRepo.read(getAllOpeningTimesUrl());
      var jsonList = <dynamic>[];
      List<CafeteriaOpeningHours> openingHours;
      jsonList = jsonDecode(result) as List<dynamic>;
      openingHours = [
        for (var json in jsonList) CafeteriaOpeningHours.fromJson(json)
      ];
      var loadingState =
          bloc.state.copyWith(cafeteriaOpeningHours: openingHours);
      bloc.add(LoadedEvent(state: loadingState));
    } on Exception catch (e) {
      bloc.add(ErrorEvent(
        message: (e is HttpRepoException) ? e.message : e.toString(),
      ));
    }
  }
}

/// A [CafeteriaOpeningEvent] that takes a [message] and passes it
/// to [ErrorOpeningState]
/// Finally it calls [LoadedEvent]
class ErrorEvent extends CafeteriaOpeningEvent {
  final String message;

  ErrorEvent({required this.message});
  @override
  CafeteriaOpeningState _getNextState(CafeteriaOpeningBloc bloc) =>
      ErrorOpeningState.fromState(bloc.state, message: message);

  @override
  Future<void> _performAction(CafeteriaOpeningBloc bloc) async {
    bloc.add(LoadedEvent(state: bloc.state));
  }
}

/// A [CafeteriaOpeningEvent] that takes a [state] and passes it
/// to [LoadedOpeningState]
class LoadedEvent extends CafeteriaOpeningEvent {
  final CafeteriaOpeningState state;
  LoadedEvent({required this.state});
  @override
  CafeteriaOpeningState _getNextState(CafeteriaOpeningBloc bloc) =>
      LoadedOpeningState.fromState(state);

  @override
  Future<void> _performAction(CafeteriaOpeningBloc bloc) => Future.value();
}

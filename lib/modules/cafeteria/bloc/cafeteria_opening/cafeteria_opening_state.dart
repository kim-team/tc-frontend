/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

part of 'cafeteria_opening_bloc.dart';

/// A [CafeteriaOpeningState] contains a [List] of [CafeteriaOpeningHours]
@CopyWith()
@JsonSerializable(anyMap: true, explicitToJson: true)
class CafeteriaOpeningState extends Equatable {
  final List<CafeteriaOpeningHours> cafeteriaOpeningHours;

  CafeteriaOpeningState({this.cafeteriaOpeningHours = const []});

  CafeteriaOpeningState._fromState(CafeteriaOpeningState state)
      : cafeteriaOpeningHours = state.cafeteriaOpeningHours;

  /// Creates a [CafeteriaOpeningState] from a Json.
  factory CafeteriaOpeningState.fromJson(Map<String, dynamic> json) =>
      _$CafeteriaOpeningStateFromJson(json);

  /// Maps a [CafeteriaState] to a Json.
  Map<String, dynamic> toJson() => _$CafeteriaOpeningStateToJson(this);

  @override
  List<Object> get props => [cafeteriaOpeningHours];
}

/// A [CafeteriaOpeningState] that is called when checking for persisted data
class InitialCafeteriaOpeningState extends CafeteriaOpeningState {
  InitialCafeteriaOpeningState();
  InitialCafeteriaOpeningState.fromState(CafeteriaOpeningState state)
      : super._fromState(state);
}

/// A [CafeteriaOpeningState] that is called when loading
/// [CafeteriaOpeningHours] data from backend
class LoadingOpeningState extends CafeteriaOpeningState {
  LoadingOpeningState();
  LoadingOpeningState.fromState(CafeteriaOpeningState state)
      : super._fromState(state);
}

/// A [CafeteriaOpeningState] that is called when backend communication fails
/// it takes a [message] that can be listened to
class ErrorOpeningState extends CafeteriaOpeningState {
  final String message;
  ErrorOpeningState({required this.message});
  ErrorOpeningState.fromState(CafeteriaOpeningState state, {this.message = ""})
      : super._fromState(state);
}

/// A [CafeteriaOpeningState] that is called when no other action is required
class LoadedOpeningState extends CafeteriaOpeningState {
  LoadedOpeningState();
  LoadedOpeningState.fromState(CafeteriaOpeningState state)
      : super._fromState(state);
}

/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

part of 'cafeteria_settings_bloc.dart';

/// A [CafeteriaSettingsState] holds [CafeteriaSettings] and
/// a [List] of [cafeteriaSelection]
@JsonSerializable(anyMap: true, explicitToJson: true)
@CopyWith()
class CafeteriaSettingsState extends Equatable {
  final CafeteriaSettings settings;
  final List<String> cafeteriaSelection;

  CafeteriaSettingsState(
      {List<String>? cafeteriaSelection, CafeteriaSettings? settings})
      : settings = settings ?? CafeteriaSettings.initialSettings(),
        cafeteriaSelection = cafeteriaSelection ?? [];

  CafeteriaSettingsState._fromState(CafeteriaSettingsState state)
      : settings = state.settings,
        cafeteriaSelection = state.cafeteriaSelection;

  factory CafeteriaSettingsState.fromJson(Map<String, dynamic> json) =>
      _$CafeteriaSettingsStateFromJson(json);

  Map<String, dynamic> toJson() => _$CafeteriaSettingsStateToJson(this);

  @override
  String toString() => "$runtimeType: ${toJson()}";

  @override
  List<Object> get props => [settings];
}

/// A [CafeteriaSettingsState] that is called when checking for persisted data
class InitialCafeteriaSettingsState extends CafeteriaSettingsState {
  InitialCafeteriaSettingsState()
      : super(
          settings: CafeteriaSettings.initialSettings(),
          cafeteriaSelection: [],
        );
  InitialCafeteriaSettingsState.fromState(CafeteriaSettingsState state)
      : super._fromState(state);
}

/// A [CafeteriaSettingsState] that is called when checking for
/// old [Cafeteria] selections
class ModulePressedState extends CafeteriaSettingsState {
  final List<String> cafeteriaNames;

  ModulePressedState.fromState(CafeteriaSettingsState state,
      {this.cafeteriaNames = const []})
      : super._fromState(state);
}

/// A [CafeteriaSettingsState] that is called when [CafeteriaSettings]
/// are changed
class SettingsChangedState extends CafeteriaSettingsState {
  SettingsChangedState.fromState(CafeteriaSettingsState state)
      : super._fromState(state);
}

/// A [CafeteriaSettingsState] that is called when no other action is required
class SettingsIdleState extends CafeteriaSettingsState {
  SettingsIdleState.fromState(CafeteriaSettingsState state)
      : super._fromState(state);
}

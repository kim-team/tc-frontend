/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:async';

import 'package:copy_with_extension/copy_with_extension.dart';
import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:meta/meta.dart';

import '../../../../bloc/local_user_hyd_bloc/local_user_hyd_bloc.dart';
import '../../model/cafeteria_settings.dart';

part 'cafeteria_settings_bloc.g.dart';
part 'cafeteria_settings_event.dart';
part 'cafeteria_settings_state.dart';

/// BloC used for handling the [CafeteriaSettings]
class CafeteriaSettingsBloc
    extends LocalUserHydBloc<CafeteriaSettingsEvent, CafeteriaSettingsState> {
  CafeteriaSettingsBloc() : super(InitialCafeteriaSettingsState());

  @override
  Stream<CafeteriaSettingsState> mapEventToState(
      CafeteriaSettingsEvent event) async* {
    yield event._getNextState(this);
    event._performAction(this);
  }

  @override
  CafeteriaSettingsState fromJson(Map<String, dynamic> json) =>
      CafeteriaSettingsState.fromJson(json);

  @override
  Map<String, dynamic> toJson(CafeteriaSettingsState state) => state.toJson();
}

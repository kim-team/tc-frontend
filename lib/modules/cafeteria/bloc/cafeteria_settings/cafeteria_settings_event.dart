/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

part of 'cafeteria_settings_bloc.dart';

@immutable
abstract class CafeteriaSettingsEvent extends Equatable {
  const CafeteriaSettingsEvent();

  CafeteriaSettingsState _getNextState(CafeteriaSettingsBloc bloc);
  Future<void> _performAction(CafeteriaSettingsBloc bloc);

  @override
  List<Object?> get props => [];
}

/// A [CafeteriaSettingsEvent] that is called when checking for persisted data
class InitialEvent extends CafeteriaSettingsEvent {
  @override
  CafeteriaSettingsState _getNextState(CafeteriaSettingsBloc bloc) =>
      SettingsIdleState.fromState(bloc.state);

  @override
  Future<void> _performAction(CafeteriaSettingsBloc bloc) => Future.value();
}

/// A [CafeteriaSettingsEvent] that is called when opening the Module
/// It takes a [List] of [cafeteriaNames] from [CafeteriaBloc] and
/// checks if [cafeteriaSelection] has entries that are not part
/// of [cafeteriaNames], in which case it calls [SelectCafeteriaEvent] with
/// an empty [List]
/// This way, there can be no [Cafeteria] selected that isn't provided by
/// external API
class ModulePressedEvent extends CafeteriaSettingsEvent {
  final List<String> cafeteriaNames;

  ModulePressedEvent({this.cafeteriaNames = const []});
  @override
  CafeteriaSettingsState _getNextState(CafeteriaSettingsBloc bloc) =>
      ModulePressedState.fromState(
          bloc.state.copyWith(
              settings: bloc.state.settings.copyWith(showTestApi: false)),
          cafeteriaNames: cafeteriaNames);

  @override
  Future<void> _performAction(CafeteriaSettingsBloc bloc) {
    for (var name in bloc.state.cafeteriaSelection) {
      if (!cafeteriaNames.contains(name)) {
        bloc.add(SelectCafeteriaEvent([]));
      }
    }
    bloc.add(InitialEvent());
    return Future.value();
  }
}

/// A [CafeteriaSettingsEvent] that takes a [List] of [cafeteriaNames] and
/// is called when selecting or deselecting a [Cafeteria]
class SelectCafeteriaEvent extends CafeteriaSettingsEvent {
  final List<String> cafeteriaNames;

  SelectCafeteriaEvent(this.cafeteriaNames);
  @override
  CafeteriaSettingsState _getNextState(CafeteriaSettingsBloc bloc) =>
      SettingsChangedState.fromState(
          bloc.state.copyWith(cafeteriaSelection: cafeteriaNames));

  @override
  Future<void> _performAction(CafeteriaSettingsBloc bloc) =>
      Future(() => bloc.add(SettingsChangedEvent(
            cafeteriaSelection: cafeteriaNames,
          )));
}

/// A [CafeteriaSettingsEvent] that takes [CafeteriaSettings] to then call
/// [SettingsChangedEvent], creating a transition from [SettingsChangedState]
/// to [SettingsIdleState] to update new [CafeteriaSettings] and persist them
/// using [HydratedBloc]
class ChangeSettingsEvent extends CafeteriaSettingsEvent {
  final CafeteriaSettings settings;

  ChangeSettingsEvent({required this.settings});
  @override
  CafeteriaSettingsState _getNextState(CafeteriaSettingsBloc bloc) =>
      SettingsChangedState.fromState(bloc.state.copyWith(settings: settings));

  @override
  Future<void> _performAction(CafeteriaSettingsBloc bloc) =>
      Future(() => bloc.add(SettingsChangedEvent(
            settings: settings,
          )));
}

/// A [CafeteriaSettingsEvent] that accepts [CafeteriaSettings] or a
/// List of [Cafeteria] that is different from the ones currently
/// held in the [CafeteriaSettingsState].
/// It then uses the _getNextState() function to change the State using copyWith
class SettingsChangedEvent extends CafeteriaSettingsEvent {
  final CafeteriaSettings? settings;
  final List<String>? cafeteriaSelection;

  SettingsChangedEvent({
    this.cafeteriaSelection,
    this.settings,
  });

  @override
  CafeteriaSettingsState _getNextState(CafeteriaSettingsBloc bloc) =>
      SettingsIdleState.fromState(bloc.state.copyWith(
        settings: settings ?? bloc.state.settings,
        cafeteriaSelection: cafeteriaSelection ?? bloc.state.cafeteriaSelection,
      ));

  @override
  Future<void> _performAction(CafeteriaSettingsBloc bloc) async =>
      Future.value();

  @override
  List<Object?> get props => [settings];
}

class CafeteriaSettingsResetEvent extends CafeteriaSettingsEvent {
  const CafeteriaSettingsResetEvent();

  @override
  CafeteriaSettingsState _getNextState(CafeteriaSettingsBloc bloc) =>
      InitialCafeteriaSettingsState();

  @override
  Future<void> _performAction(CafeteriaSettingsBloc bloc) async {}
}

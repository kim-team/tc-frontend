/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:async';
import 'dart:convert';

import 'package:copy_with_extension/copy_with_extension.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';

import '../../../bloc/local_user_hyd_bloc/local_user_hyd_bloc.dart';
import '../../../common/constants/api_constants.dart';
import '../../../repositories/http/http_repository.dart';
import '../model/cafeteria.dart';
import '../ui/widgets/cafeteria_generator.dart';

part 'cafeteria_bloc.g.dart';
part 'cafeteria_event.dart';
part 'cafeteria_state.dart';

///BLoC used for the Cafeteria Module
class CafeteriaBloc extends LocalUserHydBloc<CafeteriaEvent, CafeteriaState> {
  final _httpRepo = HttpRepository();

  CafeteriaBloc() : super(InitialCafeteriaState()) {
    if (state is InitialCafeteriaState) {
      add(LoadingEvent(loadTest: false));
    }
  }

  @override
  Stream<CafeteriaState> mapEventToState(CafeteriaEvent event) async* {
    yield event._getNextState(this);
    event._performAction(this);
  }

  @override
  CafeteriaState fromJson(Map<String, dynamic> json) =>
      CafeteriaState.fromJson(json);

  @override
  Map<String, dynamic> toJson(CafeteriaState state) => state.toJson();
}

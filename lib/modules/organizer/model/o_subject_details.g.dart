// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'o_subject_details.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OSubjectDetails _$OSubjectDetailsFromJson(Map<String, dynamic> json) =>
    OSubjectDetails(
      id: json['id'] as int?,
      name: json['name'] as String?,
      departmentID: json['departmentID'] as int?,
      lessons: (json['lessons'] as List<dynamic>?)
          ?.map((e) => OLesson.fromJson(e as Map<String, dynamic>))
          .toList(),
      moduleCode: json['moduleCode'] as String?,
      executors: (json['executors'] as List<dynamic>?)
          ?.map((e) => OPerson.fromJson(e as Map<String, dynamic>))
          .toList(),
      teachers: (json['teachers'] as List<dynamic>?)
          ?.map((e) => OPerson.fromJson(e as Map<String, dynamic>))
          .toList(),
      description: json['description'] as String?,
      objective: json['objective'] as String?,
      content:
          (json['content'] as List<dynamic>?)?.map((e) => e as String).toList(),
      expertise: json['expertise'] as String?,
      methodCompetence: json['methodCompetence'] as String?,
      socialCompetence: json['socialCompetence'] as String?,
      selfCompetence: json['selfCompetence'] as String?,
      duration: json['duration'] as int?,
      instructionLanguage: json['instructionLanguage'] as String?,
      expenditure: json['expenditure'] as String?,
      sws: json['sws'] as int?,
      method: json['method'] as String?,
      preliminaryWork: json['preliminaryWork'] as String?,
      proof: json['proof'] as String?,
      evaluation: json['evaluation'] as String?,
      availability: json['availability'] as String?,
      literature: json['literature'] as String?,
      aids: json['aids'] as String?,
      prerequisites: json['prerequisites'] as String?,
      preRequisiteModules: (json['preRequisiteModules'] as List<dynamic>?)
          ?.map((e) => OSubject.fromJson(e as Map<String, dynamic>))
          .toList(),
      recommendedPrerequisites: json['recommendedPrerequisites'] as String?,
      prerequisiteFor: json['prerequisiteFor'] as String?,
      postRequisiteModules: (json['postRequisiteModules'] as List<dynamic>?)
          ?.map((e) => OSubject.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$OSubjectDetailsToJson(OSubjectDetails instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'departmentID': instance.departmentID,
      'lessons': instance.lessons,
      'moduleCode': instance.moduleCode,
      'executors': instance.executors,
      'teachers': instance.teachers,
      'description': instance.description,
      'objective': instance.objective,
      'content': instance.content,
      'expertise': instance.expertise,
      'methodCompetence': instance.methodCompetence,
      'socialCompetence': instance.socialCompetence,
      'selfCompetence': instance.selfCompetence,
      'duration': instance.duration,
      'instructionLanguage': instance.instructionLanguage,
      'expenditure': instance.expenditure,
      'sws': instance.sws,
      'method': instance.method,
      'preliminaryWork': instance.preliminaryWork,
      'proof': instance.proof,
      'evaluation': instance.evaluation,
      'availability': instance.availability,
      'literature': instance.literature,
      'aids': instance.aids,
      'prerequisites': instance.prerequisites,
      'preRequisiteModules': instance.preRequisiteModules,
      'recommendedPrerequisites': instance.recommendedPrerequisites,
      'prerequisiteFor': instance.prerequisiteFor,
      'postRequisiteModules': instance.postRequisiteModules,
    };

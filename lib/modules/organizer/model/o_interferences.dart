/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'o_pool.dart';
import 'o_program.dart';
import 'o_subject.dart';

class OInterferences {
  List<OPool> referencedPools = <OPool>[];
  List<OSubject> referencedSubjects = <OSubject>[];
  OInterferences({
    List<OPool>? referencedPools,
    List<OSubject>? referencedSubjects,
  })  : referencedSubjects = referencedSubjects ?? <OSubject>[],
        referencedPools = referencedPools ?? <OPool>[];

  void calculateInterferences({
    OProgram? originProgram,
    OPool? originPool,
    OSubject? originSubject,
  }) {
    referencedPools = <OPool>[];
    // null checks
    if (originProgram == null || originSubject == null) {
      return;
    }
    // look in program
    referencedPools = <OPool>[];
    if (originProgram.pools.isNotEmpty) {
      for (var pool in originProgram.pools) {
        // if (originPool.id != pool.id) { //only check if different pool
        if (pool.subjects.isNotEmpty) {
          for (var subject in pool.subjects) {
            if ((originPool?.id ?? -1) != pool.id) {
              // only check other pools
              if (originSubject.id == subject.id) {
                referencedPools.add(pool);
              }
            }
            // if (subject.lessons != null) {
            // }
          }
        }
        // }
      }
    }
    // get base pool

    // return delta
    // return null;#
    return;
  }
}

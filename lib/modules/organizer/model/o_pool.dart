/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:json_annotation/json_annotation.dart';

import 'o_change_record.dart';
import 'o_lesson.dart';
import 'o_subject.dart';

part 'o_pool.g.dart';

@JsonSerializable(anyMap: true, explicitToJson: true)
class OPool {
  OPool({
    this.name = "",
    this.id = -1,
    List<OSubject>? subjects,
  }) : subjects = subjects ?? <OSubject>[];

  String name;
  int id;
  List<OSubject> subjects;

  factory OPool.fromJson(Map<String, dynamic> json) => _$OPoolFromJson(json);

  Map<String, dynamic> toJson() => _$OPoolToJson(this);

  OSubject? findSubjectById(int subjectId) {
    if (subjects.isEmpty) {
      return null;
    }
    for (var i = 0; i < subjects.length; i++) {
      if (subjects[i].id == subjectId) {
        return subjects[i];
      }
    }
    // if nothing was found
    return null;
  }

  bool hasSubjectById(int subjectId) {
    if (subjects.isEmpty) {
      return false;
    }
    for (var i = 0; i < subjects.length; i++) {
      if (subjects[i].id == subjectId) {
        return true;
      }
    }
    // if nothing was found
    return false;
  }

  OLesson? findLessonByIds(int subjectId, int lessonId) {
    var subject = findSubjectById(subjectId);
    if (subjects.isEmpty) return null;
    return subject?.findLessonById(lessonId);
  }

  bool hasLessonByIds(int subjectId, int lessonId) {
    var subject = findSubjectById(subjectId);
    if (subject == null) return false;
    return subject.hasLessonById(lessonId);
  }

  /// [other] is regarded as the most recent compared object
  OChangeRecord? identifyChanges(
      OPool? other, List<OPool> siblings, List<OPool> otherSiblings) {
    var record = OChangeRecord();
    record.type = runtimeType.toString();
    record.oldRef = shallowCopy().toJson();
    record.userChecked = false;
    record.delta = <ODelta>[];
    if (other == null) {
      record.delta.add(ODelta.removed);
      return record;
    }
    record.contextId = other.toContextString();
    record.newRef = other.shallowCopy().toJson();
    // check if name is the same
    if (name != other.name) {
      record.delta.add(ODelta.nameChanged);
    }
    if (subjects.isNotEmpty && other.subjects.isNotEmpty) {
      // check if new subtree length is smaller
      if (subjects.length > other.subjects.length) {
        record.delta.add(ODelta.amountSubtreeDecreased);
      }
      // check if new subtree length is bigger
      if (subjects.length < other.subjects.length) {
        record.delta.add(ODelta.amountSubtreeIncreased);
      }
    }
    // check if it was added
    if (siblings.isNotEmpty && otherSiblings.isNotEmpty) {
      var list = siblings.map((e) => e.id).toList();
      var otherList = otherSiblings.map((e) => e.id).toList();
      if (!list.contains(other.id)) {
        record.delta.add(ODelta.added);
      }
      if (!otherList.contains(id)) {
        record.delta.add(ODelta.removed);
      }
    }
    if (siblings.isEmpty && otherSiblings.isNotEmpty) {
      record.delta.add(ODelta.added);
    }
    // check if there are changes
    if (record.delta.isEmpty) {
      return null;
    }
    return record;
  }

  static OChangeRecord wasAdded(OPool a) {
    // here, b is the "older reference", since we need to track backwards
    var record = OChangeRecord();
    record.contextId = a.toContextString();
    record.type = a.runtimeType.toString();
    record.oldRef = null;
    record.newRef = a.shallowCopy().toJson();
    record.userChecked = false;
    record.delta = <ODelta>[];
    record.delta.add(ODelta.added);
    return record;
  }

  OPool shallowCopy() => OPool(id: id, name: name, subjects: subjects);

  String toContextString() => id.toString();
}

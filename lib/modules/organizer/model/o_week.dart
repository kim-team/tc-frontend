/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:intl/intl.dart';
import 'package:json_annotation/json_annotation.dart';

import '../ui/organizer/organizer_enums.dart';
import '../utility/organizer_common_functions.dart';
import 'o_element.dart';
import 'o_element_wrapper.dart';

part 'o_week.g.dart';

@JsonSerializable(anyMap: true, explicitToJson: true)
class OWeek {
  OWeek({this.weekSinceEpoch1970, this.containsData, this.past, this.future}) {
    lastUpdated = DateTime.now();
  }

  DateTime? lastUpdated;
  int maxLayers = 0;
  Map<int, List<OElementWrapper>> days = <int, List<OElementWrapper>>{};
  ContainingWeekdays? containingWeekdays;
  int? weekSinceEpoch1970;
  @JsonKey(fromJson: OCommonFunctions.forceInt)
  int latestMinuteForEntireWeek = 0;
  @JsonKey(fromJson: OCommonFunctions.forceInt)
  int firstMinuteForEntireWeek = 0;
  bool? containsData;
  DateTime? past;
  DateTime? future;

  factory OWeek.fromJson(Map<String, dynamic> json) => _$OWeekFromJson(json);
  Map<String, dynamic> toJson() => _$OWeekToJson(this);

  void initialize() {
    containsData = false;
    // days = <int, List<OElementWrapper>>{};
    if (days[DateTime.monday] == null) {
      days[DateTime.monday] = <OElementWrapper>[];
    }
    if (days[DateTime.tuesday] == null) {
      days[DateTime.tuesday] = <OElementWrapper>[];
    }
    if (days[DateTime.wednesday] == null) {
      days[DateTime.wednesday] = <OElementWrapper>[];
    }
    if (days[DateTime.thursday] == null) {
      days[DateTime.thursday] = <OElementWrapper>[];
    }
    if (days[DateTime.friday] == null) {
      days[DateTime.friday] = <OElementWrapper>[];
    }
    if (days[DateTime.saturday] == null) {
      days[DateTime.saturday] = <OElementWrapper>[];
    }
    if (days[DateTime.sunday] == null) {
      days[DateTime.sunday] = <OElementWrapper>[];
    }
  }

  void constructWeek(List<OElement> oElements) {
    initialize();
    assignOElementsToWeek(oElements);
    calculateLayeringAndIntersections();
    setWeekdaysState();
  }

  bool containsEntriesOn(int weekday) {
    if (weekday > 0 && weekday < 8) {
      return days[weekday]?.isNotEmpty ?? false;
    }
    return false;
  }

  DateTime dti() => DateTime.fromMicrosecondsSinceEpoch(0);

  void assignOElementsToWeek(List<OElement> oElements) {
    if (oElements.length == 1 && oElements.first.type == "NODATA") {
      containsData = false;
      past = oElements.first.start;
      future = oElements.first.end;
      return;
    }
    containsData = true;
    firstMinuteForEntireWeek = 8 * 60; // 8:00 AM
    latestMinuteForEntireWeek = 20 * 60; // 20:00 PM

    for (var i = 0; i < oElements.length; i++) {
      var start = (oElements[i].start ?? dti());
      var end = (oElements[i].end ?? dti());

      var dateForCalculation =
          DateTime.parse(DateFormat('yyyy-MM-dd').format(start));

      var startAbs = dateForCalculation.difference(start).inMinutes.abs();
      var endAbs = dateForCalculation.difference(end).inMinutes.abs();

      if (startAbs < firstMinuteForEntireWeek) {
        firstMinuteForEntireWeek = startAbs;
      }
      if (endAbs > latestMinuteForEntireWeek) {
        latestMinuteForEntireWeek = endAbs;
      }

      switch (oElements[i].start?.weekday ?? false) {
        case 1:
          days[1]?.add(OElementWrapper(
            ele: oElements[i],
            layer: 0,
          ));
          break;
        case 2:
          days[DateTime.tuesday]
              ?.add(OElementWrapper(ele: oElements[i], layer: 0));
          break;
        case DateTime.wednesday:
          days[DateTime.wednesday]
              ?.add(OElementWrapper(ele: oElements[i], layer: 0));
          break;
        case DateTime.thursday:
          days[DateTime.thursday]
              ?.add(OElementWrapper(ele: oElements[i], layer: 0));
          break;
        case DateTime.friday:
          days[DateTime.friday]
              ?.add(OElementWrapper(ele: oElements[i], layer: 0));
          break;
        case DateTime.saturday:
          days[DateTime.saturday]
              ?.add(OElementWrapper(ele: oElements[i], layer: 0));
          break;
        case DateTime.sunday:
          days[DateTime.sunday]
              ?.add(OElementWrapper(ele: oElements[i], layer: 0));
          break;
      }
    }
  }

  bool intersects(OElement a, OElement b) {
    if (a.start == null || b.start == null) return false;
    if (a.end == null || b.end == null) return false;
    return ((a.start!.millisecondsSinceEpoch <= b.end!.millisecondsSinceEpoch) &
        (b.start!.millisecondsSinceEpoch <= a.end!.millisecondsSinceEpoch));
  }

  bool intersectsIncludingLayer(OElementWrapper a, OElementWrapper b) {
    if (a.layer != b.layer) return false;
    // if (a.ele == null || b.ele == null) return false;
    return intersects(a.ele, b.ele);
  }

  void calculateLayeringAndIntersections() {
    for (var k = 1; k <= DateTime.sunday; k++) {
      if (days[k]?.isEmpty ?? true) continue;
      for (var i = 0; i < days[k]!.length; i++) {
        for (var j = 0; j < days[k]!.length; j++) {
          if (i != j) {
            if (intersectsIncludingLayer(days[k]![i], days[k]![j])) {
              days[k]![j].layer++;
              days[k]![i].isIntersected = true;
              days[k]![j].isIntersected = true;
              days[k]![j].intersectsWith = days[k]![i];
              days[k]![i].countIntersections++;
              days[k]![j].countIntersections++;

              if (maxLayers < days[k]![j].layer) {
                maxLayers++;
              }
            }
            // count++;
          }
        }
      }
    }
  }

  void setWeekdaysState() {
    var weekdays = <int>[];
    containingWeekdays = ContainingWeekdays.none;
    if (containsEntriesOn(DateTime.monday)) {
      containingWeekdays = ContainingWeekdays.error;
    }
    if (containsEntriesOn(DateTime.saturday) &&
        containsEntriesOn(DateTime.sunday)) {
      containingWeekdays = ContainingWeekdays.normalIncludingWeekend;
    } else if (weekdays.contains(6) && !weekdays.contains(7)) {
      containingWeekdays = ContainingWeekdays.normalIncludingSaturday;
    } else if (containsEntriesOn(DateTime.monday) ||
        containsEntriesOn(DateTime.tuesday) ||
        containsEntriesOn(DateTime.wednesday) ||
        containsEntriesOn(DateTime.thursday) ||
        containsEntriesOn(DateTime.friday)) {
      containingWeekdays = ContainingWeekdays.normal;
    }
  }
}

// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'o_week.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OWeek _$OWeekFromJson(Map json) => OWeek(
      weekSinceEpoch1970: json['weekSinceEpoch1970'] as int?,
      containsData: json['containsData'] as bool?,
      past:
          json['past'] == null ? null : DateTime.parse(json['past'] as String),
      future: json['future'] == null
          ? null
          : DateTime.parse(json['future'] as String),
    )
      ..lastUpdated = json['lastUpdated'] == null
          ? null
          : DateTime.parse(json['lastUpdated'] as String)
      ..maxLayers = json['maxLayers'] as int
      ..days = (json['days'] as Map).map(
        (k, e) => MapEntry(
            int.parse(k as String),
            (e as List<dynamic>)
                .map((e) => OElementWrapper.fromJson(
                    Map<String, dynamic>.from(e as Map)))
                .toList()),
      )
      ..containingWeekdays = $enumDecodeNullable(
          _$ContainingWeekdaysEnumMap, json['containingWeekdays'])
      ..latestMinuteForEntireWeek =
          OCommonFunctions.forceInt(json['latestMinuteForEntireWeek'])
      ..firstMinuteForEntireWeek =
          OCommonFunctions.forceInt(json['firstMinuteForEntireWeek']);

Map<String, dynamic> _$OWeekToJson(OWeek instance) => <String, dynamic>{
      'lastUpdated': instance.lastUpdated?.toIso8601String(),
      'maxLayers': instance.maxLayers,
      'days': instance.days.map(
          (k, e) => MapEntry(k.toString(), e.map((e) => e.toJson()).toList())),
      'containingWeekdays':
          _$ContainingWeekdaysEnumMap[instance.containingWeekdays],
      'weekSinceEpoch1970': instance.weekSinceEpoch1970,
      'latestMinuteForEntireWeek': instance.latestMinuteForEntireWeek,
      'firstMinuteForEntireWeek': instance.firstMinuteForEntireWeek,
      'containsData': instance.containsData,
      'past': instance.past?.toIso8601String(),
      'future': instance.future?.toIso8601String(),
    };

const _$ContainingWeekdaysEnumMap = {
  ContainingWeekdays.none: 'none',
  ContainingWeekdays.error: 'error',
  ContainingWeekdays.noneWithFuture: 'noneWithFuture',
  ContainingWeekdays.noneWithPast: 'noneWithPast',
  ContainingWeekdays.noneWithPastAndFuture: 'noneWithPastAndFuture',
  ContainingWeekdays.normal: 'normal',
  ContainingWeekdays.normalIncludingSaturday: 'normalIncludingSaturday',
  ContainingWeekdays.normalIncludingWeekend: 'normalIncludingWeekend',
};

// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'o_change_record.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OChangeRecord _$OChangeRecordFromJson(Map json) => OChangeRecord(
      userChecked: json['userChecked'] as bool? ?? false,
      oldRef: (json['oldRef'] as Map?)?.map(
        (k, e) => MapEntry(k as String, e),
      ),
      newRef: (json['newRef'] as Map?)?.map(
        (k, e) => MapEntry(k as String, e),
      ),
      type: json['type'] as String? ?? "",
      contextId: json['contextId'] as String? ?? "",
    )..delta = (json['delta'] as List<dynamic>)
        .map((e) => $enumDecode(_$ODeltaEnumMap, e))
        .toList();

Map<String, dynamic> _$OChangeRecordToJson(OChangeRecord instance) =>
    <String, dynamic>{
      'contextId': instance.contextId,
      'delta': instance.delta.map((e) => _$ODeltaEnumMap[e]).toList(),
      'userChecked': instance.userChecked,
      'oldRef': instance.oldRef,
      'newRef': instance.newRef,
      'type': instance.type,
    };

const _$ODeltaEnumMap = {
  ODelta.added: 'added',
  ODelta.nameChanged: 'nameChanged',
  ODelta.amountSubtreeIncreased: 'amountSubtreeIncreased',
  ODelta.amountSubtreeDecreased: 'amountSubtreeDecreased',
  ODelta.removed: 'removed',
  ODelta.descriptionChanged: 'descriptionChanged',
};

/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:json_annotation/json_annotation.dart';

import '../utility/organizer_common_functions.dart';
import 'o_room.dart';

part 'o_resource.g.dart';

@JsonSerializable(anyMap: true, explicitToJson: true)
class OResource {
  OResource({
    this.resourceID = -1,
    this.code = "",
    this.person = "",
    this.role = "",
    this.roleID = -1,
    // this.status,
    // this.groups,
    List<ORoom>? rooms,
  }) : rooms = rooms ?? <ORoom>[];

  // int assocID; // e.g. 123456
  @JsonKey(fromJson: OCommonFunctions.forceInt)
  int resourceID; // e.g. "1"
  String code; // e.g. "DOZ"
  String person; // name of the person
  String role; //e.g. "Dozent"

  @JsonKey(fromJson: OCommonFunctions.forceInt)
  int roleID; // e.g. 1
  // String status;
  // List<OPool> groups;
  List<ORoom> rooms; // TODO NNBD MIGRATION

  @override
  String toString() => person;

  factory OResource.fromJson(Map<String, dynamic> json) =>
      _$OResourceFromJson(json);

  Map<String, dynamic> toJson() => _$OResourceToJson(this);
}

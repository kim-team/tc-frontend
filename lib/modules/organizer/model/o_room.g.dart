// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'o_room.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ORoom _$ORoomFromJson(Map json) => ORoom(
      name: json['name'] as String? ?? "",
      id: json['id'] == null ? -1 : OCommonFunctions.forceInt(json['id']),
    );

Map<String, dynamic> _$ORoomToJson(ORoom instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
    };

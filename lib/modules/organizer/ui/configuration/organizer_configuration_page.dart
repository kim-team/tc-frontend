/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

import '../../../../common/constants/routing_constants.dart';
import '../../../../common/tc_theme.dart';
import '../../../../common/widgets/dialog/tc_dialog.dart' show TCDialog;
import '../../../../repositories/http/http_repository.dart';
import '../../blocs/o_lesson_selection_bloc/o_lesson_selection_bloc.dart';
import '../../blocs/o_program_configuration_bloc/o_program_configuration_bloc.dart';
import '../../blocs/o_program_history_bloc/o_program_history_bloc.dart';
import '../../model/o_pool.dart';
import '../../model/o_program.dart';
import '../../utility/organizer_program_functions.dart';
import '../organizer/o_utility_functions.dart';
import '../organizer/organizer_constants.dart';
import '../organizer/program_history/organizer_program_history_page.dart'
    show OrganizerProgramHistoryPageArguments;
import '../pool_config/organizer_pool_config_page.dart'
    show OrganizerPoolConfigPageArguments;
import '../search_modules/o_search_modules_page.dart'
    show OSearchModulesPageArguments;
import '../selected_modules/o_selected_modules_page.dart'
    show OSelectedModulesPageArguments;
import '../shared_widgets/delta_widget.dart';

class OrganizerConfigurationPage extends StatelessWidget {
  const OrganizerConfigurationPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
        // backgroundColor: CorporateColors.passiveBackgroundLight,
        appBar: AppBar(
          title: I18nText('$i18nOCKey.title'),
          leading: Hero(
            tag: "BACKBUTTON",
            child: Material(
              color: Colors.transparent,
              child: BackButton(
                color: Colors.black,
              ),
            ),
          ),
          actions: <Widget>[
            PopupMenuButton(
              icon: Icon(Icons.more_vert),
              itemBuilder: (context) => [
                PopupMenuItem(
                  child: Padding(
                      padding: EdgeInsets.symmetric(vertical: 12.0),
                      child: I18nText('$i18nOCKey.add_study_course')),
                  value: 0,
                ),
                PopupMenuItem(
                  child: Padding(
                    padding: EdgeInsets.symmetric(vertical: 12.0),
                    child: I18nText(
                      'modules.organizer.ui.organizer.page'
                      '.reset_entire_organizer',
                      child: Text(
                        'Stundenplan vollständig zurücksetzen',
                      ),
                    ),
                  ),
                  value: 1,
                ),
                PopupMenuItem(
                  child: Padding(
                    padding: EdgeInsets.symmetric(vertical: 12.0),
                    child: I18nText(
                      '$i18nOCKey'
                      '.reset_all_modules',
                      child: Text(
                        'Gesamte Modulauswahl zurücksetzen',
                      ),
                    ),
                  ),
                  // child: Text("Gesamte Modulauswahl zurücksetzen"),
                  value: 2,
                ),
                PopupMenuItem(
                  child: Padding(
                    padding: EdgeInsets.symmetric(vertical: 12.0),
                    child: I18nText(
                      '$i18nOCHKey'
                      '.reset_all_notifications',
                      child: Text(
                        '',
                      ),
                    ),
                  ),
                  // child: Text("Gesamte Modulauswahl zurücksetzen"),
                  value: 3,
                ),
              ],
              onSelected: (result) {
                if (result == 0) {
                  Navigator.pushNamed(context, orgaSelectProgramRoute);
                }
                if (result == 1) {
                  TCDialog.showCustomDialog(
                    context: context,
                    onConfirm: () => OUtil.resetAllBlocs(context),
                    headlineText: FlutterI18n.translate(
                        context, '$i18nOPKey.reset_entire_organizer_question'),
                    bodyText: FlutterI18n.translate(
                        context, '$i18nOPKey.reset_warning'),
                    functionActionText:
                        FlutterI18n.translate(context, '$i18nOPKey.reset'),
                    functionActionColor: CorporateColors.cafeteriaCautionRed,
                  );
                }
                if (result == 2) {
                  TCDialog.showCustomDialog(
                    context: context,
                    onConfirm: OUtil.deleteAllSelectedLessons,
                    headlineText: FlutterI18n.translate(
                        context, '$i18nOCKey.remove_modules_head'),
                    bodyText: FlutterI18n.translate(
                        context, '$i18nOCKey.remove_element_body'),
                    functionActionText:
                        FlutterI18n.translate(context, 'common.actions.remove'),
                    functionActionColor: CorporateColors.cafeteriaCautionRed,
                  );
                }
                if (result == 3) {
                  TCDialog.showCustomDialog(
                    context: context,
                    onConfirm: OUtil.resetOProgramHistory,
                    headlineText: FlutterI18n.translate(
                        context, '$i18nOCHKey.reset_all_notifications_head'),
                    bodyText: FlutterI18n.translate(
                        context, '$i18nOCHKey.reset_all_notifications_body'),
                    functionActionText:
                        FlutterI18n.translate(context, 'common.actions.remove'),
                    functionActionColor: CorporateColors.cafeteriaCautionRed,
                  );
                }
              },
            ),
          ],
        ),
        body: _buildOProgramList(context),
      );

  Widget _buildOProgramList(BuildContext context) =>
      BlocBuilder<OProgramConfigurationBloc, OProgramConfigurationState>(
          builder: (context, state) {
        if (state is OProgramConfigurationEmptyState) {
          return SingleChildScrollView(
            child: Column(
              children: <Widget>[
                _buildAddProgramButton(context),
              ],
            ),
          );
        } else if (state is OProgramConfigurationReadyState) {
          if (state.programs.isEmpty) {
            return SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  _buildAddProgramButton(context),
                ],
              ),
            );
          }
          return Stack(
            children: <Widget>[
              Column(
                children: <Widget>[
                  Expanded(
                    child: SingleChildScrollView(
                      physics: BouncingScrollPhysics(),
                      child: Column(
                        children: <Widget>[
                          if (kDebugMode)
                            BlocBuilder<OLessonSelectionBloc,
                                OLessonSelectionState>(
                              builder: (context, state) => Text(
                                  // important for debugging
                                  (OUtil.getAllSelectedLessonIds()?.length ?? 0)
                                      .toString()),
                            ),
                          _buildPrograms(state.programs, context),
                          _buildAddProgramButton(context),
                          Container(
                            height: 120,
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              Positioned(
                bottom: 16,
                left: 0,
                // top: 0,
                right: 0,
                child: _buildAcceptButton(context),
              ),
            ],
          );
        }
        return Container();
      });

  Widget _buildAcceptButton(BuildContext context) =>
      BlocBuilder<OLessonSelectionBloc, OLessonSelectionState>(
        builder: (context, state) {
          if (state.lessons.isEmpty) {
            return Container();
          }
          return Padding(
            padding: const EdgeInsets.symmetric(
              vertical: 8.0,
              horizontal: 12.0,
            ),
            child: RaisedButton(
              elevation: 4,
              onPressed: () {
                Navigator.of(context).pop();
              },
              color: CorporateColors.cafeteriaVeganGreen,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8.0)),
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: I18nText(
                  '$i18nOCKey.confirm',
                  child: Text(
                    'Bestätigen und zum Stundenplan',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 16.0,
                    ),
                  ),
                ),
              ),
            ),
          );
        },
      );

  Widget _buildAddProgramButton(BuildContext context) => Container(
        height: 80,
        margin: EdgeInsets.only(
          top: 8.0,
          bottom: 120.0,
          right: 8.0,
          left: 8.0,
        ),
        child: Card(
          elevation: 4.0,
          child: FlatButton(
              onPressed: () =>
                  Navigator.pushNamed(context, orgaSelectProgramRoute),
              child: Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    CircleAvatar(
                      backgroundColor: Color.fromARGB(255, 240, 240, 240),
                      foregroundColor: CorporateColors.tinyCampusIconGrey,
                      child: Icon(Icons.add),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 16.0),
                      child: I18nText('$i18nOCKey.add_study_course'),
                    ),
                  ],
                ),
              )),
        ),
      );

  Widget _buildPrograms(List<OProgram> programs, BuildContext context) =>
      ListView.builder(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        itemCount: programs.length,
        itemBuilder: (context, index) {
          final item = programs[index];
          return Container(
            margin: EdgeInsets.symmetric(vertical: 8.0),
            child: Material(
              color: Theme.of(context).primaryColor,
              elevation: 4.0,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(
                      left: 16.0,
                      right: 16.0,
                      top: 16.0,
                      bottom: 8,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Expanded(
                              flex: 6,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Material(
                                    color: Colors.transparent,
                                    child: Text(
                                      item.name,
                                      style: Theme.of(context)
                                          .textTheme
                                          .headline2
                                          ?.copyWith(
                                            fontWeight: FontWeight.w600,
                                            color: CorporateColors
                                                .tinyCampusTextSoft,
                                            fontSize: 22.0,
                                          ),
                                      // style: TextStyle(
                                      //   fontSize: 26.0,
                                      //   letterSpacing: -1.1,
                                      //   fontWeight: FontWeight.w600,
                                      // ),
                                    ),
                                  ),
                                  FlatButton(
                                    padding: EdgeInsets.zero,
                                    onPressed: () => Navigator.pushNamed(
                                      context,
                                      orgaProgramHistoryRoute,
                                      arguments:
                                          OrganizerProgramHistoryPageArguments(
                                        OProgram.fromJson(
                                          programs[index].toJson(),
                                        ),
                                      ),
                                    ),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        CountUncheckedDeltasWidget(
                                            program: programs[index]),
                                      ],
                                    ),
                                  ),
                                  // OConfigUpdateButton(
                                  //   program: programs[index],
                                  // ),
                                ],
                              ),
                            ),
                            PopupMenuButton(
                              icon: Icon(Icons.settings),
                              itemBuilder: (context) => [
                                PopupMenuItem(
                                  child: I18nText(
                                    '$i18nOCKey.remove_element',
                                    translationParams: {
                                      'element': programs[index].name
                                    },
                                  ),
                                  value: 0,
                                ),
                                PopupMenuItem(
                                  child: I18nText(
                                    'modules.organizer.ui.refresh',
                                  ),
                                  value: 1,
                                ),
                                PopupMenuItem(
                                  child: I18nText(
                                    'modules.organizer.ui.changes.'
                                    'popup_clear_notifications',
                                  ),
                                  value: 3,
                                ),
                                // PopupMenuItem(
                                //   child:
                                // Text("Module nur hier zurücksetzen"),
                                //   value: 2,
                                // ),
                              ],
                              onSelected: (result) {
                                if (result == 0) {
                                  TCDialog.showCustomDialog(
                                      context: context,
                                      onConfirm: () {
                                        OUtil.deleteSingleProgram(
                                            programs[index]);
                                      },
                                      headlineText: FlutterI18n.translate(
                                          context,
                                          'modules.organizer.ui.config.'
                                          'remove_program_head'),
                                      // headlineText: programs[index].name,
                                      bodyText: FlutterI18n.translate(
                                          context,
                                          'modules.organizer.ui.config.'
                                          'remove_program_body'));
                                }
                                if (result == 1) {
                                  OUtil.updateSingleProgram(programs[index],
                                      removeOrphanedLessons: true);
                                }
                                // if (result == 2) {
                                //   _deleteAllLessonsFromProgram(
                                //       context, programs[index]);
                                // }
                                if (result == 3) {
                                  TCDialog.showCustomDialog(
                                    context: context,
                                    onConfirm: () =>
                                        OUtil.resetSpecificOProgramHistory(
                                            programs[index]),
                                    headlineText: FlutterI18n.translate(context,
                                        '$i18nOCHKey.clear_notifications_head'),
                                    bodyText: FlutterI18n.translate(context,
                                        '$i18nOCHKey.clear_notifications_body'),
                                  );
                                }
                              },
                            )
                          ],
                        ),

                        // InkWell(
                        //   onTap: () {},
                        //   child: Padding(
                        //     padding: const EdgeInsets.only(
                        //       top: 8.0,
                        //       bottom: 8.0,
                        //       right: 8.0,
                        //     ),
                        //     child: Text(
                        //       "AKTUALISIEREN",
                        //       style: TextStyle(
                        //           color: CorporateColors.tinyCampusOrange),
                        //     ),
                        //   ),
                        // ),
                      ],
                    ),
                  ),

                  // _divider(),
                  _buildModuleBlock(context, programs[index]),
                  // Container(
                  //   padding: EdgeInsets.only(top: 12),
                  //   margin: EdgeInsets.symmetric(
                  //     horizontal: 16.0,
                  //     vertical: 2.0,
                  //   ),
                  //   child: I18nText(
                  //     '$i18nOCKey.categories',
                  //   ),
                  // ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 8.0),
                    child: _buildPoolList(item.pools, context, item),
                  ),
                ],
              ),
            ),
          );
        },
      );

  Widget _buildModuleBlock(
    BuildContext context,
    OProgram program,
  ) {
    final textTheme = TextStyle(
      fontSize: 46,
      letterSpacing: -1.0,
      color: CurrentTheme().tcBlueFont,
      fontWeight: FontWeight.w500,
    );

    return BlocBuilder<OLessonSelectionBloc, OLessonSelectionState>(
        builder: (context, state) {
      // if (state is OLessonSelectionReadyState) {
      var distinctSubjects =
          state.getDistinctSelectedSubjectsInProgram(program: program);
      return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        // Container(
        //     padding: EdgeInsets.only(top: 12),
        //     margin: EdgeInsets.symmetric(
        //       horizontal: 16.0,
        //       vertical: 2.0,
        //     ),
        //     child: I18nText('$i18nOCKey.modules')),
        // _buildHintBlock(),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 4.0),
          child: Row(
            children: [
              if (distinctSubjects > 0)
                Expanded(
                  child: SizedBox(
                    height: 120,
                    child: FlatButton(
                        onPressed: () => Navigator.pushNamed(
                              context,
                              orgaSelectedModulesRoute,
                              arguments: OSelectedModulesPageArguments(
                                program: program,
                              ),
                            ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(distinctSubjects.toString(), style: textTheme),
                            I18nText(
                              '$i18nOCKey.selected_modules',
                              child: Text(
                                "",
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ],
                        ),
                        color: Theme.of(context).backgroundColor,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(12.0))),
                  ),
                ),
              if (distinctSubjects > 0) Container(width: 16),
              Expanded(
                child: SizedBox(
                  height: 120,
                  child: FlatButton(
                      onPressed: () => Navigator.pushNamed(
                            context,
                            orgaSearchModulesRoute,
                            arguments: OSearchModulesPageArguments(
                              program: program,
                            ),
                          ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.search,
                            size: 52.0,
                            color: CurrentTheme().tcBlueFont,
                          ),
                          I18nText(
                            '$i18nOCKey.search_modules',
                            child: Text(
                              "",
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ],
                      ),
                      color: Theme.of(context).backgroundColor,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(12.0))),
                ),
              ),
            ],
          ),
        ),
      ]);
    });
  }

  Widget _buildPoolList(
      List<OPool> pools, BuildContext context, OProgram program) {
    var array = <Widget>[];
    pools.sort((a, b) => a.name.compareTo(b.name));
    for (var i = 0; i < pools.length; i++) {
      if (pools[i].subjects.isEmpty) {
        array.add(
          ListTile(
            title: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(
                    flex: 6,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          pools[i].name,
                          style: TextStyle(
                              color: (pools[i].subjects.isEmpty
                                  ? Colors.grey
                                  : CorporateColors.tinyCampusDarkBlue)),
                        ),
                        (pools[i].subjects.isEmpty
                            ? I18nText(
                                '$i18nOCKey.no_modules_found',
                                child: Text(
                                  "keine Module gefunden",
                                  style: TextStyle(
                                    fontSize: 12.0,
                                    color: Colors.grey,
                                  ),
                                ),
                              )
                            : Container()),
                      ],
                    )),
              ],
            ),
          ),
        );
      } else {
        array.add(
          ListTile(
            title: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(
                    flex: 6,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Material(
                          type: MaterialType.transparency,
                          child: Text(
                            pools[i].name,
                            style: TextStyle(
                                fontSize: 18.0,
                                color: (pools[i].subjects.isEmpty
                                    ? Colors.grey
                                    : CurrentTheme().tcBlueFont)),
                          ),
                        ),
                        DeltaWidget(
                          program: program,
                          pool: program.pools[i],
                          showIndirectRemoved: true,
                        ),
                        (pools[i].subjects.isEmpty
                            ? I18nText(
                                '$i18nOCKey.no_modules_found',
                                child: Text(
                                  "keine Module gefunden",
                                  style: TextStyle(
                                    fontSize: 12.0,
                                    color: Colors.grey,
                                  ),
                                ),
                              )
                            : Container()),
                      ],
                    )),
                _buildPoolCountSelectedModules(pools, i),
              ],
            ),
            trailing: BlocBuilder<OLessonSelectionBloc, OLessonSelectionState>(
              builder: (context, state) {
                if (state is OLessonSelectionReadyState) {
                  var percentage =
                      state.getDistinctSelectedSubjectsInPool(pool: pools[i]);
                  return Icon(Icons.navigate_next,
                      color: percentage > 0
                          ? CorporateColors.tinyCampusBlue
                          : null);
                } else {
                  return Icon(Icons.navigate_next);
                }
              },
            ),
            onTap: () {
              OUtil.addOHistoryBlocEvent(
                UserCheckedChangeEvent(
                    OUtil.concatPP(program, program.pools[i])),
              );
              Navigator.pushNamed(
                context,
                orgaPoolConfigRoute,
                arguments: OrganizerPoolConfigPageArguments(
                  pool: pools[i],
                  program: program,
                ),
              );
            },
          ),
        );
      }
      if (i + 1 < pools.length) {
        array.add(Divider(
          endIndent: 16.0,
          indent: 16.0,
          thickness: 1.0,
          height: 1.0,
          color: Colors.black12,
        ));
      }
    }
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: array,
    );
  }

  Widget _buildPoolCountSelectedModules(List<OPool> pools, int i) => Expanded(
        flex: 2,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            // BlocBuilder<OLessonSelectionBloc, OLessonSelectionState>(
            //     builder: (context, state) {
            //   if (state is OLessonSelectionReadyState) {
            //     var percentage =
            //         state.getDistinctSelectedSubjectsInPool(pool: pools[i]);
            //     if (percentage == 0.0) {
            //       return I18nText(
            //         '$i18nOCKey.no_modules',
            //         child: Text(
            //           "keine Module",
            //           textAlign: TextAlign.center,
            //           style: TextStyle(
            //             fontSize: 12.0,
            //             color:
            //                 CorporateColors.tinyCampusIconGrey.
            // withOpacity(0.5),
            //           ),
            //         ),
            //       );
            //     } else if (percentage > 1.0 && percentage < 100) {
            //       return TweenAnimationBuilder(
            //           tween: IntTween(
            //             begin: 0,
            //             end: percentage.toInt(),
            //           ),
            //           curve: Curves.easeInOutQuad,
            //           duration: Duration(milliseconds: 900),
            //           builder: (context, i, child) => I18nPlural(
            //                 '$i18nOCKey.module_count.times',
            //                 i,
            //                 child: Text(
            //                   "$i Module",
            //                   textAlign: TextAlign.center,
            //                   style: TextStyle(
            //                     fontSize: 12.0,
            //                   ),
            //                 ),
            //               ));
            //     } else if (percentage == 1.0) {
            //       return TweenAnimationBuilder(
            //           tween: IntTween(
            //             begin: 0,
            //             end: percentage.toInt(),
            //           ),
            //           curve: Curves.easeInOutQuad,
            //           duration: Duration(milliseconds: 1620),
            //           builder: (context, i, child) => I18nPlural(
            //                 '$i18nOCKey.module_count.times',
            //                 i,
            //                 child: Text(
            //                   "$i Module",
            //                   textAlign: TextAlign.center,
            //                   style: TextStyle(
            //                     fontSize: 12.0,
            //                   ),
            //                 ),
            //               ));
            //     } else if (percentage == 100) {
            //       return TweenAnimationBuilder(
            //           tween: IntTween(
            //             begin: 0,
            //             end: percentage.toInt(),
            //           ),
            //           curve: Curves.easeInOutQuad,
            //           onEnd: () {},
            //           duration: Duration(milliseconds: 1620),
            //           builder: (context, i, child) => I18nPlural(
            //                 '$i18nOCKey.module_count.times',
            //                 i,
            //                 child: Text(
            //                   "$i Module",
            //                   textAlign: TextAlign.center,
            //                   style: TextStyle(
            //                     fontSize: 12.0,
            //                   ),
            //                 ),
            //               ));
            //     } else {
            //       var stringAsFixed = percentage.toStringAsFixed(1);
            //       return I18nText(
            //         '$i18nOCKey.error_msg',
            //         translationParams: {'msg': stringAsFixed},
            //       );
            //     }
            //   } else {
            //     return Container();
            //   }
            // }),
            // I18nText(
            //   '$i18nOCKey.selected',
            //   child: Text(
            //     "ausgewählt",
            //     textAlign: TextAlign.center,
            //     style: TextStyle(
            //       fontSize: 10.0,
            //       color: CorporateColors.tinyCampusIconGrey.withOpacity(0.5),
            //     ),
            //   ),
            // ),
          ],
        ),
      );

  // Widget _buildHintBlock(BuildContext context) =>
  //     BlocBuilder<OLessonSelectionBloc, OLessonSelectionState>(
  //       builder: (context, state) {
  //         if (state is OLessonSelectionReadyState) {
  //           if (state.lessons == null) {
  //             return _hintBlock(context);
  //           }
  //           if (state.lessons.length == 0) {
  //             return _hintBlock(context);
  //           }
  //         }
  //         return Container();
  //       },
  //     );

  // Widget _hintBlock(BuildContext context) => Column(
  //       children: <Widget>[
  //         ODismissibleHintWidget(
  //           hintType: HintType.notice,
  //           hintText: FlutterI18n.translate
  // (context, '$i18nOCKey.lookup_hint'),
  //         ),
  //         ODismissibleHintWidget(
  //           hintType: HintType.friendly,
  //           hintText:
  //               FlutterI18n.translate(context, '$i18nOCKey.saving_hint'),
  //         ),
  //       ],
  //     );
}

class CountUncheckedDeltasWidget extends StatelessWidget {
  const CountUncheckedDeltasWidget({
    Key? key,
    this.program,
    this.noDescription = false,
  }) : super(key: key);

  final OProgram? program;
  final bool noDescription;

  @override
  Widget build(BuildContext context) => Column(
        children: [
          BlocBuilder<OProgramHistoryBloc, OProgramHistoryState>(
            builder: (context, state) {
              var amountChanges = 0;
              if (program != null) {
                amountChanges = state.countUncheckedDeltas(
                  program!.toContextString() + contextStringSeparator,
                );
              } else {
                amountChanges = state.countAllUncheckedDeltas();
              }
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  if (!noDescription)
                    Text(
                      "${FlutterI18n.translate(
                        context,
                        'modules.organizer.ui.organizer'
                        '.week_widget.last_updated',
                      )}: ${DateFormat.yMd(
                        FlutterI18n.currentLocale(context)?.languageCode ??
                            Locale('de', 'DE'),
                      ).add_jm().format(state.lastUpdated)}",
                      style: Theme.of(context)
                          .textTheme
                          .bodyText2
                          ?.copyWith(color: CorporateColors.tinyCampusIconGrey),
                    ),
                  if (amountChanges > 0)
                    Chip(
                      backgroundColor: CorporateColors.cafeteriaCautionRed,
                      label: Text(
                          amountChanges.toString() +
                              (noDescription
                                  ? ""
                                  : " ${FlutterI18n.translate(
                                      context,
                                      'modules.organizer.ui.changes.chip_label',
                                    )}"),
                          style: Theme.of(context)
                              .textTheme
                              .headline4
                              ?.copyWith(color: Colors.white)),
                    ),
                ],
              );
            },
          ),
        ],
      );
}

class OConfigUpdateButton extends StatelessWidget {
  const OConfigUpdateButton({
    Key? key,
    this.program,
  }) : super(key: key);

  final OProgram? program;

  @override
  Widget build(BuildContext context) => Row(
        children: [
          Expanded(
            child: Wrap(
              children: [
                FlatButton(
                  color: CorporateColors.tinyCampusOrange,
                  onPressed: () => _reloadProgramClean(context, program),
                  child: Text(
                    "Aktualisieren",
                    style: Theme.of(context)
                        .textTheme
                        .headline4
                        ?.copyWith(color: Colors.white),
                  ),
                ),
                if (kDebugMode)
                  FlatButton(
                    color: CorporateColors.cafeteriaCautionYellow,
                    onPressed: () {},
                    child: Text(
                      "Fake Update",
                      style: Theme.of(context)
                          .textTheme
                          .headline4
                          ?.copyWith(color: Colors.white),
                    ),
                  ),
              ],
            ),
          ),
        ],
      );

  void _reloadProgramClean(BuildContext context, OProgram? program) {
    if (program != null) {
      OProgramFunctions.fillEntireProgramWithPoolsSubjectsLessonsUpdated(
        program.id,
        program.name,
        HttpRepository(),
      ).then(
        (content) {
          if (content != null) OUtil.addOProgram(content);
        },
        onError: (e) => debugPrint(e.toString()),
      );
    }
  }
}

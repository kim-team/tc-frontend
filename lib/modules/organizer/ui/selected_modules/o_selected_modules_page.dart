/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../../../common/constants/routing_constants.dart';
import '../../../../common/tc_theme.dart';
import '../../../../common/widgets/dialog/tc_dialog.dart' show TCDialog;
import '../../blocs/o_lesson_selection_bloc/o_lesson_selection_bloc.dart';
import '../../model/o_lesson.dart';
import '../../model/o_pool.dart';
import '../../model/o_program.dart';
import '../pool_config/o_subject_list_tile.dart';

const String _i18nKey = "modules.organizer.ui.selected_modules";

class OSelectedModulesPageArguments {
  OSelectedModulesPageArguments({required this.program});

  OProgram program;
}

class OSelectedModulesPage extends StatefulWidget {
  const OSelectedModulesPage({Key? key, required this.program})
      : super(key: key);
  final OProgram program;

  @override
  _OSelectedModulesPageState createState() => _OSelectedModulesPageState();
}

class _OSelectedModulesPageState extends State<OSelectedModulesPage> {
  Map<int, List<String>> subjectPools = <int, List<String>>{};
  Map<int, List<String>> subjectNames = <int, List<String>>{};
  Map<int, List<OLesson>> subjectLessons = <int, List<OLesson>>{};
  Map<int, List<String>> lessonPoolName = <int, List<String>>{};

  @override
  void initState() {
    super.initState();
    // filter program by selected lessons
    // create maps
    // id -> pool
    // id -> subjectName
    // id -> List<OLesson>
    // lessonId -> poolName
    initEnhancedSubjectMaps();
  }

  void initEnhancedSubjectMaps() {
    var program = widget.program;
    // if (program != null) {
    for (var pool in program.pools) {
      // if (pool != null) {
      for (var subject in pool.subjects) {
        // if (subject != null) {
        if (subjectPools[subject.id] == null) {
          subjectPools[subject.id] = <String>[];
        }
        if (subjectNames[subject.id] == null) {
          subjectNames[subject.id] = <String>[];
        }
        subjectPools[subject.id]?.add(pool.name);
        subjectNames[subject.id]?.add(subject.name);
        if (subject.lessons.isNotEmpty) {
          for (var lesson in subject.lessons) {
            if (subjectLessons[subject.id] == null) {
              subjectLessons[subject.id] = <OLesson>[];
            }
            if (lessonPoolName[lesson.id] == null) {
              lessonPoolName[lesson.id!] = <String>[];
            }
            subjectLessons[subject.id]?.add(lesson);
            lessonPoolName[lesson.id]?.add(pool.name);
          }
        }
        // }
      }
      // }
    }
    // }
    for (var key in subjectLessons.keys) {
      var tempLessonList = <int>[];
      var tempLessonListObjects = <OLesson>[];
      for (var lesson in subjectLessons[key]!) {
        if (!tempLessonList.contains(lesson.id)) {
          tempLessonListObjects.add(lesson);
        }
        if (lesson.id != null) {
          tempLessonList.add(lesson.id!);
        }
      }
      subjectLessons[key] = tempLessonListObjects;
    }

    for (var key in subjectPools.keys) {
      subjectPools[key] = subjectPools[key]?.toSet().toList() ?? <String>[];
    }
    for (var key in subjectNames.keys) {
      subjectNames[key] = subjectNames[key]?.toSet().toList() ?? <String>[];
    }
    for (var key in subjectLessons.keys) {
      subjectLessons[key] =
          subjectLessons[key]?.toSet().toList() ?? <OLesson>[];
    }
    for (var key in lessonPoolName.keys) {
      lessonPoolName[key] = lessonPoolName[key]?.toSet().toList() ?? <String>[];
    }
  }

  //Widget _buildCustomMergedModules() =>
  //    BlocBuilder<OLessonSelectionBloc, OLessonSelectionState>(
  //        builder: (context, state) => ListView.builder(
  //              shrinkWrap: true,
  //              physics: BouncingScrollPhysics(),
  //              itemCount: subjectPools.length,
  //              // itemCount: pool.subjects.length,
  //              itemBuilder: (context, index) {
  //                var poolNameList =
  //                    subjectPools[subjectPools.keys.elementAt(index)];
  //                var subjectNameList =
  //                    subjectNames[subjectNames.keys.elementAt(index)];
  //                var subjectLessonsList =
  //                    subjectLessons[subjectLessons.keys.elementAt(index)];
  //                // var lessonPoolNameList =
  //                //     lessonPoolName[lessonPoolName.keys.elementAt(index)];
  //
  //                return OMergedSubjectWidget(
  //                  lessonPoolName: lessonPoolName,
  //                  subjectLessons: subjectLessonsList ?? <OLesson>[],
  //                  subjectNames: subjectNameList ?? <String>[],
  //                  subjectPools: poolNameList ?? <String>[],
  //                );
  //                // return Container(
  //                //   margin: EdgeInsets.symmetric(vertical: 16.0),
  //                //   child: Column(
  //                //     children: <Widget>[
  //                //       Text(subjectPools.keys.elementAt(index).toString()),
  //                //       Text(poolNameList.toString()),
  //                //       Text(
  //                //         subjectNameList.toString(),
  //                //         style: TextStyle(color: Colors.blue),
  //                //       ),
  //                //       Text(subjectLessonsList.length.toString()),
  //                //       Text(lessonPoolName[subjectLessonsList.
  //                //       first.id].toString()),
  //                //       // Text(lessonPoolName.toString()),
  //                //     ],
  //                //   ),
  //                // );
  //                // var visible = true;
  //                // if (visible) {
  //                // return OSubjectListTile(
  //                //   arguments: OrganizerSubjectConfigPageArguments(
  //                //       pool: pool, subject: pool.subjects[index]),
  //                //   context: context,
  //                //   index: index,
  //                //   subject: item,
  //                //   program: widget.program,
  //                //   pool: pool,
  //                //   onlyShowIfPreSelected: true,
  //                //   showDialogOnRemove: true,
  //                //   // hasSelectedLessons: false,
  //                // );
  //                // } else {
  //                // return Container();
  //                // }
  //              },
  //            ));

  // void _showDialog(BuildContext context) {
  //   // set up the buttons
  //   Widget cancelButton = FlatButton(
  //     child: Text("Abbrechen"),
  //     onPressed: () {
  //       Navigator.of(context).pop();
  //     },
  //   );
  //   Widget continueButton = FlatButton(
  //     child: Text(
  //       "Alles entfernen",
  //       textAlign: TextAlign.start,
  //       style: TextStyle(color: CorporateColors.cafeteriaCautionRed),
  //     ),
  //     onPressed: () {
  //       // var lessonConfigBloc = BlocProvider.of<OLessonSelectionBloc>(context);
  //       // var previousLessonList = lessonConfigBloc.state.lessons;
  //       _deleteAllLessonsFromProgram(
  //           context, widget.program);
  //       Navigator.of(context).pop();
  //     },
  //   );

  //   // set up the AlertDialog
  //   var alert = AlertDialog(
  //     // title: Text("Modul entfernen"),
  //     content: IntrinsicHeight(
  //       child: Column(
  //         crossAxisAlignment: CrossAxisAlignment.start,
  //         children: <Widget>[
  //           Row(
  //             mainAxisAlignment: MainAxisAlignment.start,
  //             children: <Widget>[
  //               // Icon(Icons.delete),
  //               Text(
  //                 "Entfernen",
  //                 style: TextStyle(color: CorporateColors.
  //                 tinyCampusIconGrey),
  //               ),
  //             ],
  //           ),
  //           Container(
  //               margin: EdgeInsets.symmetric(vertical: 38.0),
  //               child: Text("Alle ausgwählten Module werden entfernt.")),
  //           Row(
  //             mainAxisAlignment: MainAxisAlignment.spaceBetween,
  //             children: <Widget>[
  //               cancelButton,
  //               continueButton,
  //             ],
  //           )
  //         ],
  //       ),
  //     ),
  //   );

  //   // show the dialog
  //   showDialog(
  //     context: context,
  //     builder: (BuildContext context) => alert,
  //   );
  // }

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: PreferredSize(
            preferredSize: Size.fromHeight(220.0), // here the desired height
            child: Container(
              height: 120.0,
              decoration: BoxDecoration(
                color: Theme.of(context).primaryColor,
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.2),
                    spreadRadius: 0,
                    blurRadius: 3,
                    offset: Offset(0, 3), // changes position of shadow
                  ),
                ],
              ),
              child: SafeArea(
                child: Row(
                  children: <Widget>[
                    Hero(
                      tag: "BACKBUTTON",
                      child: Material(
                        color: Colors.transparent,
                        child: BackButton(
                          color: Colors.black,
                        ),
                      ),
                    ),
                    BlocBuilder<OLessonSelectionBloc, OLessonSelectionState>(
                        builder: (context, state) {
                      // if (state is OLessonSelectionReadyState) {
                      var distinctSubjects =
                          state.getDistinctSelectedSubjectsInProgram(
                              program: widget.program);

                      return Text(
                        distinctSubjects.toString(),
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          fontSize: 46.0,
                          fontWeight: FontWeight.w500,
                          color: CorporateColors.tinyCampusBlue,
                        ),
                      );
                    }),
                    Container(
                        margin: EdgeInsets.only(left: 8.0),
                        width: 80.0,
                        child: I18nText(
                          '$_i18nKey.selected_modules',
                          child: Text(
                            "ausgewählte Module",
                          ),
                        )),
                    Expanded(
                      flex: 6,
                      child: Container(),
                    ),
                    Expanded(
                      flex: 5,
                      child: Container(
                        margin: EdgeInsets.all(8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Expanded(
                              child: Card(
                                elevation: 0.0,
                                margin: EdgeInsets.all(0.0),
                                child: FlatButton(
                                  padding: EdgeInsets.all(8.0),
                                  onPressed: () => TCDialog.showCustomDialog(
                                    context: context,
                                    onConfirm: () =>
                                        _deleteAllLessonsFromProgram(
                                      context,
                                      widget.program,
                                    ),
                                    headlineText: FlutterI18n.translate(
                                        context, '$_i18nKey.headline'),
                                    bodyText: FlutterI18n.translate(
                                        context, '$_i18nKey.body_text'),
                                    functionActionText: FlutterI18n.translate(
                                        context, '$_i18nKey.remove'),
                                    functionActionColor:
                                        CorporateColors.cafeteriaCautionRed,
                                  ),
                                  child: Center(
                                      child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Icon(
                                        Icons.delete_outline,
                                      ),
                                      I18nText(
                                        '$_i18nKey.remove_all',
                                        child: Text(
                                          "alle entfernen",
                                          softWrap: true,
                                          maxLines: 2,
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              fontWeight: FontWeight.w300,
                                              fontSize: 12.0),
                                        ),
                                      ),
                                      // ),
                                    ],
                                  )),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            )),
        // body: _buildCustomMergedModules(),
        body: _buildBody(),
        // body: Container(child: Text(program.toString())),
      );

  void _deleteAllLessonsFromProgram(BuildContext context, OProgram oProgram) {
    var lessonConfigBloc = BlocProvider.of<OLessonSelectionBloc>(context);
    var previousLessonList = lessonConfigBloc.state.lessons;
    var programConfigBloc = BlocProvider.of<OLessonSelectionBloc>(context);
    programConfigBloc.add(
      RemoveOLessonsFromProgramEvent(
          previousLessonList: previousLessonList, program: oProgram),
    );
  }

  Widget _buildBody() => Scrollbar(
        child: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Expanded(
                    child: Container(
                      margin: EdgeInsets.symmetric(horizontal: 12.0),
                      child: FlatButton(
                        onPressed: () => Navigator.pushNamed(
                            context, orgaMultipleModulesExplanation),
                        child: Container(
                          padding: EdgeInsets.symmetric(
                              vertical: 24.0, horizontal: 12.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Icon(
                                  Icons.info,
                                  color: CorporateColors.tinyCampusIconGrey
                                      .withOpacity(0.5),
                                  size: 24.0,
                                ),
                              ),
                              Expanded(
                                child: I18nText(
                                  'modules.organizer.ui.explanations.'
                                  'multiple_shown',
                                  child: Text(
                                    "Warum werden bestimmte Veranstaltungen "
                                    "mehrfach angezeigt?",
                                    style:
                                        Theme.of(context).textTheme.headline4,
                                    textAlign: TextAlign.left,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              ListView.builder(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  itemCount: widget.program.pools.length,
                  itemBuilder: (context, index) {
                    final item = widget.program.pools[index];
                    if (widget.program.pools[index].subjects.isNotEmpty) {
                      //has selected subjects/lessons

                      return BlocBuilder<OLessonSelectionBloc,
                          OLessonSelectionState>(builder: (context, state) {
                        if (state.getDistinctSelectedSubjectsInPool(
                                pool: widget.program.pools[index]) !=
                            0) {
                          return Container(
                            margin: EdgeInsets.symmetric(
                              horizontal: 12.0,
                              vertical: 6.0,
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Container(
                                  margin:
                                      EdgeInsets.symmetric(horizontal: 16.0),
                                  child: Text(widget.program.pools[index].name,
                                      style: Theme.of(context)
                                          .textTheme
                                          .headline2
                                          ?.copyWith(fontSize: 22)),
                                ),
                                ClipRRect(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(12.0)),
                                    child: _buildPools(item)),
                              ],
                            ),
                          );
                        } else {
                          return Container();
                        }
                      });
                    } else {
                      return Container(
                        height: 5,
                      );
                    }
                  })
            ],
          ),
        ),
      );

  Widget _buildPools(OPool pool) {
    if (pool.subjects.isEmpty) {
      return Container();
    }
    // return Text(pool.name);
    return ListView.builder(
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      itemCount: pool.subjects.length,
      itemBuilder: (context, index) {
        final item = pool.subjects[index];
        return OSubjectListTile(
          // arguments: OrganizerSubjectConfigPageArguments(
          //     pool: pool, subject: pool.subjects[index]),
          // context: context,
          // index: index,
          subject: item,
          program: widget.program,
          pool: pool,
          onlyShowIfPreSelected: true,
          showDialogOnRemove: true,
          // hasSelectedLessons: false,
        );
      },
    );
  }
}

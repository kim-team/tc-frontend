/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

import '../../../../repositories/http/http_repository.dart';
import '../../blocs/o_animation_bloc/o_animation_bloc.dart';
import '../../blocs/o_calendar_bloc/o_calendar_bloc.dart';
import '../../blocs/o_color_mapping_bloc/o_color_mapping_bloc.dart';
import '../../blocs/o_drawer_bloc/o_drawer_bloc.dart';
import '../../blocs/o_lesson_selection_bloc/o_lesson_selection_bloc.dart';
import '../../blocs/o_loading_bloc/o_loading_bloc.dart';
import '../../blocs/o_message_bloc/o_message_bloc.dart';
import '../../blocs/o_program_configuration_bloc/o_program_configuration_bloc.dart';
import '../../blocs/o_program_history_bloc/o_program_history_bloc.dart';
import '../../blocs/o_settings_bloc/o_settings_bloc.dart';
import '../../blocs/o_view_mode_bloc/o_view_mode_bloc.dart';
import '../../model/o_block.dart';
import '../../model/o_element.dart';
import '../../model/o_element_wrapper.dart';
import '../../model/o_interferences.dart';
import '../../model/o_lesson.dart';
import '../../model/o_pool.dart';
import '../../model/o_program.dart';
import '../../model/o_resource.dart';
import '../../model/o_subject.dart';
import '../../model/o_week.dart';
import '../../utility/organizer_data_loader.dart';
import '../../utility/organizer_instance_functions.dart';
import '../../utility/organizer_program_functions.dart';
import 'organizer_constants.dart';

abstract class OUtil {
  static String concatStringsWithSeparator(List<OResource> items) {
    if (items.isEmpty) return "";
    String result;
    result = "";
    for (var item in items) {
      result = "$result${item.person}, ";
    }
    if (result.isEmpty) {
      return result;
    } else {
      return result.substring(0, result.length - 2);
    }
  }

  static String concatRoomStrings(List<OResource> items) {
    if (items.isEmpty) return "";
    String result;
    result = "";
    for (var resource in items) {
      if (resource.rooms.isNotEmpty) {
        result = resource.rooms.join(" - ");
        // for (var room in resource.rooms) {
        //   result = "$result${room.toString()}, ";
        // }
      }
    }
    if (result.isEmpty) {
      return result;
    } else {
      return result.substring(0, result.length - 2);
    }
  }

  /// concats OProgram, OPool with separator
  static String concatPP(OProgram? program, OPool? pool) {
    if (program == null || pool == null) {
      return "";
    }
    return program.toContextString() +
        contextStringSeparator +
        pool.toContextString();
  }

  /// concats OProgram, OPool, OSubject with separator
  static String concatPPS(OProgram? program, OPool? pool, OSubject? subject) {
    if (program == null || pool == null || subject == null) {
      return "";
    }
    return program.toContextString() +
        contextStringSeparator +
        pool.toContextString() +
        contextStringSeparator +
        subject.toContextString();
  }

  static List<OBlock> getBlocks() =>
      oSettingsBloc.state.getCurrentIntervals().blocks;

  /// concats OProgram, OPool, OSubject, OLesson with separator
  static String concatPPSL(
      OProgram? program, OPool? pool, OSubject? subject, OLesson? lesson) {
    if (program == null || pool == null || subject == null || lesson == null) {
      return "";
    }
    return program.toContextString() +
        contextStringSeparator +
        pool.toContextString() +
        contextStringSeparator +
        subject.toContextString() +
        contextStringSeparator +
        lesson.toContextString();
  }

  /// concats OProgram, OPool, OSubject, OLesson with separator
  static String concatPPSLtrimmed(
      OProgram? program, OPool? pool, OSubject? subject, OLesson? lesson) {
    var result = "";
    if (program == null) return result;
    result += program.toContextString();
    if (pool == null) return result;
    result += contextStringSeparator + pool.toContextString();
    if (subject == null) return result;
    result += contextStringSeparator + subject.toContextString();
    if (lesson == null) return result;
    result += contextStringSeparator + lesson.toContextString();
    return result;
  }

  static String concatPoolStrings(OInterferences? interferences) {
    if (interferences == null) return "";
    if (interferences.referencedPools.isEmpty) return "";
    String result;
    result = "";
    for (var pool in interferences.referencedPools) {
      // if (resource.rooms != null) {
      // for (var room in resource.rooms) {
      result = "$result${pool.name.toString()}; ";
      // }
      // }
    }
    if (result.isEmpty) {
      return result;
    } else {
      return result.substring(0, result.length - 2);
    }
  }

  static void addOProgram(OProgram program) {
    var programConfigBloc = oProgramBloc;

    var state = programConfigBloc.state;
    // check if old program exists
    var oldProgram = getSpecificProgramIfExists(program);
    if (oldProgram != null) {
      addOProgramHistory(oldProgram: oldProgram, newerProgram: program);
    } else {
      addOHistoryBlocEvent(AddLastCheckedEvent());
    }
    if (state is OProgramConfigurationEmptyState) {
      programConfigBloc.add(
        AddOProgramEvent(
          program: program,
          previousProgramList: <OProgram>[],
        ),
      );
    } else if (state is OProgramConfigurationReadyState) {
      programConfigBloc.add(
        AddOProgramEvent(
          program: program,
          previousProgramList: state.programs,
        ),
      );
    }
  }

  static void addOProgramHistory({
    OProgram? oldProgram,
    OProgram? newerProgram,
  }) {
    if (oldProgram == null || newerProgram == null) return;
    oHistoryBloc.add(AddOProgramToHistoryEvent(oldProgram, newerProgram));
  }

  static void addLastCheckedToHistory() =>
      oHistoryBloc.add(AddLastCheckedEvent());

  static void resetOProgramHistory() =>
      oHistoryBloc.add(OProgramHistoryResetEvent());

  static void resetSpecificOProgramHistory(OProgram program) =>
      oHistoryBloc.add(ResetSpecificOProgramHistoryEvent(program));

  static Future<void> updateAllSelectedPrograms() async {
    var programs = OUtil.getAllSelectedPrograms();
    if (programs == null) {
      return;
    }
    if (checkIfProgramUpdateIsRequired()) {
      debugPrint("[ALL PROGRAM UPDATE] Updating all programs...");
      for (var i = 0; i < programs.length; i++) {
        await updateSingleProgram(programs[i]);
      }
      debugPrint("[ALL PROGRAM UPDATE] All programs updated");
    }
    OUtil.removeOrphanedLessons();
    OUtil.isUpdating = false;
  }

  static bool checkIfProgramUpdateIsRequired() {
    debugPrint(
        "[PROGRAM UPDATE] Last checked: ${oHistoryBloc.state.lastUpdated}");
    try {
      // if (oHistoryBloc.state.lastUpdated != null) { // NNBD TEST BEHAVIOUR
      var oldDate = oHistoryBloc.state.lastUpdated;
      var newDate = DateTime.now();

      ///the const datedThreshold comes from organizer_constants
      if (oldDate.difference(newDate).inMilliseconds.abs() >
          datedProgramUpdatingThreshold) {
        debugPrint(
          "[PROGRAM UPDATE] because the current program data is old, "
          "$datedProgramUpdatingThreshold, reload",
        );
        addLastCheckedToHistory();
        return true;
        // } else {  // NNBD TEST BEHAVIOUR
        //   debugPrint( // NNBD TEST BEHAVIOUR
        //        // NNBD TEST BEHAVIOUR
        //       "[PROGRAM UPDATE] current data is relatively new,  // NNBD TEST BEHAVIOUR
        // $datedProgramUpdatingThreshold, do nothing");// NNBD TEST BEHAVIOUR
        //   return false; // NNBD TEST BEHAVIOUR
        // } // NNBD TEST BEHAVIOUR
      }
    } on Exception catch (e) {
      debugPrint(
        "[PROGRAM UPDATE] Something went wrong checking if "
        "programs need to be updated: $e",
      );
    }
    return true;
  }

  static void removeOrphanedLessons() {
    // get all available lesson
    var programs = OUtil.getAllSelectedPrograms();
    var lessonIds = <int>[];
    if (programs != null) {
      for (var i = 0; i < programs.length; i++) {
        var programLessons = programs[i].getDistinctLessonsInProgram();
        if (programLessons.isNotEmpty) {
          lessonIds.addAll(programs[i].getDistinctLessonsInProgram());
        }
      }
    }
    lessonIds.toSet().toList();
    // compare lessons to current selection
    var currentlySelectedLessons = OUtil.getAllSelectedLessonIds();
    var orphanLessonList = <int>[];
    if (currentlySelectedLessons != null) {
      for (var i = 0; i < currentlySelectedLessons.length; i++) {
        if (!lessonIds.contains(currentlySelectedLessons[i])) {
          orphanLessonList.add(currentlySelectedLessons[i]);
        }
      }
    }
    // if matches are found, remove them
    if (orphanLessonList.isNotEmpty) {
      deleteMulitpleLessons(orphanLessonList);
    }
  }

  static Future<OProgram?> updateSingleProgram(OProgram? oldProgram,
      {bool removeOrphanedLessons = false}) async {
    if (oldProgram == null) return null;
    var httpRepoObj = httpRepo;

    await OProgramFunctions.fillEntireProgramWithPoolsSubjectsLessonsUpdated(
            oldProgram.id, oldProgram.name, httpRepoObj)
        .then((newerProgram) {
      if (newerProgram == null) {
        return null;
      }
      addOProgram(newerProgram);
      debugPrint(
          "[SINGLE PROGRAM UPDATE] Program updated: ${newerProgram.name}");
      // OProgram updatedProgram;
      // updatedProgram = newerProgram;
      return newerProgram;
    }).catchError((e) {
      debugPrint(e.runtimeType.toString());
      debugPrint("[SINGLE PROGRAM UPDATE] Program update FAILED");
    });

    // because this method can be called when updating
    // all programs or just one program
    if (removeOrphanedLessons) {
      OUtil.removeOrphanedLessons();
    }
    return null;
    // return updatedProgram;
  }

  static List<int> getDeletedOElementIds() =>
      oSettingsBloc.state.deletedOElements
          .map((e) => e.id)
          .whereType<int>()
          .toList();

  static bool isDeleted(OElementWrapper? oElementWrapper) {
    if (oElementWrapper == null) return false;
    var id = oElementWrapper.ele.id ?? -1;

    try {
      if (oSettingsBloc.state.deletedOElements.isNotEmpty) {
        final val =
            oSettingsBloc.state.deletedOElements.map((e) => e.id).toList();
        return val.contains(id);
      }
    } on Exception catch (e) {
      debugPrint("$e - error in isDeleted()");
    }
    return false;
  }

  static List<OElement>? getDeletedOElements() =>
      oSettingsBloc.state.deletedOElements;

  static List<OProgram>? getAllSelectedPrograms() {
    final programState = oProgramBloc.state;
    if (programState is OProgramConfigurationEmptyState) {
      return null;
    } else if (programState is OProgramConfigurationReadyState) {
      if (programState.programs.isNotEmpty) {
        return programState.programs;
      } else {
        return null;
      }
    }
    return null;
  }

  static List<OSubject>? getAllSubjectsFromAllProgramsInferredByLesson(
      int lessonId) {
    var programs = OUtil.getAllSelectedPrograms();
    if (programs == null) return null;
    var referencedSubjects = <OSubject>[];
    for (var i = 0; i < programs.length; i++) {
      referencedSubjects.addAll(programs[i].findSubjectsByLessonId(lessonId));
    }
    return referencedSubjects;
  }

  static List<OSubject>? getSubjectsFromProgramInferredByLesson(
      OProgram? program, int lessonId) {
    if (program == null) return null;
    return program.findSubjectsByLessonId(lessonId);
  }

  static OProgram? getSpecificProgramIfExists(OProgram program) {
    final programState = oProgramBloc.state;
    if (programState is OProgramConfigurationEmptyState) {
      return null;
    } else if (programState is OProgramConfigurationReadyState) {
      if (programState.programs.isNotEmpty) {
        return programState.programs
            .firstWhereOrNull((e) => e.id == program.id);
      } else {
        return null;
      }
    }
    return null;
  }

  static List<int>? getAllSelectedLessonIds() {
    final state = oLessonBloc.state;
    if (oLessonBloc.state is OLessonSelectionReadyState) {
      if (state.lessons.isNotEmpty) {
        return state.lessons;
      } else {
        return null;
      }
    }
    return null;
  }

  static void deleteAllLessonsFromProgram(OProgram program) {
    var previousLessonList = oLessonBloc.state.lessons;
    oLessonBloc.add(
      RemoveOLessonsFromProgramEvent(
          previousLessonList: previousLessonList, program: program),
    );
  }

  static void deleteAllSelectedLessons() => oLessonBloc.add(
        OLessonSelectionResetEvent(),
      );

  static void deleteMulitpleLessons(List<int> toBeDeletedLessons) {
    var prevLessons = oLessonBloc.state.lessons;
    oLessonBloc.add(
      RemoveMultipleOLessonsEvent(
        lessons: toBeDeletedLessons,
        previousLessonList: prevLessons,
      ),
    );
  }

  static void deleteAllPrograms() {
    oProgramBloc.add(
      OProgramConfigurationResetEvent(),
    );
    var lessonSelectionBloc = oLessonBloc;
    lessonSelectionBloc.add(
      (OLessonSelectionResetEvent()),
    );
    OUtil.resetOProgramHistory();
    //TODO: CHECK: Delete related lessons and subjects aswell!
  }

  static void deleteSingleProgram(OProgram program) {
    OUtil.resetSpecificOProgramHistory(program);
    OUtil.deleteAllLessonsFromProgram(program);
    var state = oProgramBloc.state;
    if (state is OProgramConfigurationReadyState) {
      var programs = state.programs;
      oProgramBloc.add(
        DeleteSingleOProgram(program: program, previousProgramList: programs),
      );
    }
    OUtil.removeOrphanedLessons();
  }

  static void resetAllBlocs(BuildContext context) {
    final programConfigBloc =
        BlocProvider.of<OProgramConfigurationBloc>(context);
    programConfigBloc.clear();
    programConfigBloc.add(OProgramConfigurationResetEvent());
    final lessonsBloc = BlocProvider.of<OLessonSelectionBloc>(context);
    lessonsBloc.clear();
    lessonsBloc.add(OLessonSelectionResetEvent());
    final colorsBloc = BlocProvider.of<OColorMappingBloc>(context);
    colorsBloc.clear();
    colorsBloc.add(OColorMappingResetEvent());
    final calendarBloc = BlocProvider.of<OCalendarBloc>(context);
    calendarBloc.clear();
    calendarBloc.add(OCalendarResetEvent());
    final oSettingsBloc = BlocProvider.of<OSettingsBloc>(context);
    oSettingsBloc.clear();
    oSettingsBloc.add(OSettingsResetEvent());
    // TODO: why does drawer not work?
    // final drawerBloc = BlocProvider.of<ODrawerBloc>(context);
    // drawerBloc.add(ODeselectEvent());
    final historyBloc = BlocProvider.of<OProgramHistoryBloc>(context);
    historyBloc.add(OProgramHistoryResetEvent());
    historyBloc.clear();
  }

  /// If [initBlocs] is not called, almost every function will fail.
  static void initBlocs(BuildContext context) {
    oProgramBloc = BlocProvider.of<OProgramConfigurationBloc>(context);

    oLessonBloc = BlocProvider.of<OLessonSelectionBloc>(context);

    oCalendarBloc = BlocProvider.of<OCalendarBloc>(context);

    oColorMappingBloc = BlocProvider.of<OColorMappingBloc>(context);

    oHistoryBloc = BlocProvider.of<OProgramHistoryBloc>(context);

// this is different. use fresh context
    oDrawerBloc = BlocProvider.of<ODrawerBloc>(context);

// this is different. use fresh context
    oMessageBloc = BlocProvider.of<OMessageBloc>(context);

// this is different. use fresh context
    oAnimationBloc = BlocProvider.of<OAnimationBloc>(context);

// this is different. use fresh context
    oViewModeBloc = BlocProvider.of<OViewModeBloc>(context);
// this is different. use fresh context
    oLoadingBloc = BlocProvider.of<OLoadingBloc>(context);

    oSettingsBloc = BlocProvider.of<OSettingsBloc>(context);

    httpRepo = RepositoryProvider.of<HttpRepository>(context);
  }

  static bool organizerModuleDisposed = false;

  static void initPageValues() {
    ODataLoader.spreadInitiallyLoaded = false;
    organizerModuleDisposed = false;
  }

  static void disposePageValues() {
    organizerModuleDisposed = true;
  }

  static Future<List<OElement>> getAffectingLessons(
      {required List<int> oLessonId}) async {
    var lessons = await OrganizerInstanceFunctions.getOrganizerInstances(
        oLessonId, null, httpRepo);
    return lessons;
  }

  static late bool isUpdating = false;
  static late OProgramConfigurationBloc oProgramBloc;
  static late OLessonSelectionBloc oLessonBloc;
  static late OCalendarBloc oCalendarBloc;
  static late OColorMappingBloc oColorMappingBloc;
  static late OProgramHistoryBloc oHistoryBloc;
  static late ODrawerBloc oDrawerBloc;
  static late OAnimationBloc oAnimationBloc;
  static late OViewModeBloc oViewModeBloc;
  static late OLoadingBloc oLoadingBloc;
  static late OSettingsBloc oSettingsBloc;
  static late OMessageBloc oMessageBloc;
  static late HttpRepository httpRepo;

  static void resetOElementWrapperSelection() {
    oDrawerBloc.add(ODeselectEvent());
    oAnimationBloc
        .add(OSetAnimationStateEvent(OStopAllShakingAnimationState()));
  }

  static void switchToDailyViewMode(int weekday) {
    oViewModeBloc.add(ODailyViewEvent(weekday));
  }

  static void switchToWeeklyViewMode() {
    oViewModeBloc.add(OWeeklyViewEvent());
  }

  static void addODrawerEvent(ODrawerEvent event) {
    oDrawerBloc.add(event);
  }

  static void addOAnimationEvent(OAnimationEvent event) {
    oAnimationBloc.add(event);
  }

  static void setOAnimationState(OAnimationState state) {
    oAnimationBloc.add(OSetAnimationStateEvent(state));
  }

  static void addOViewModeEvent(OViewModeEvent event) {
    oViewModeBloc.add(event);
  }

  static void addOLoadingEvent(OLoadingEvent event) {
    oLoadingBloc.add(event);
  }

  static void addOMessageEvent(OMessageEvent event) {
    oMessageBloc.add(event);
  }

  static void addOHistoryBlocEvent(
    OProgramHistoryEvent event,
  ) {
    try {
      oHistoryBloc.add(event);
      // });
    } on Exception catch (e) {
      debugPrint(
        "Tried to remove a key from history bloc,"
        " but was not mounted? Error: $e",
      );
    }
  }

  static void addOSettingsEvent(
    OSettingsEvent event,
  ) {
    oSettingsBloc.add(event);
  }

  static void addOCalendarEvent(
    OCalendarEvent event,
  ) {
    oCalendarBloc.add(event);
  }

  static Color getColorWrapper(OElementWrapper? oElementWrapper) {
    var state = oColorMappingBloc.state;
    Color color;
    if (oElementWrapper != null) {
      if (state is OColorMappingReadyState) {
        color = state.getColor(subjectId: oElementWrapper.ele.subjectId ?? -1);
      } else {
        color = Colors.grey;
      }
    } else {
      color = Colors.grey;
    }
    return color;
  }

  static Color getColor(OElement? oElement) {
    var state = oColorMappingBloc.state;
    Color color;
    if (oElement != null) {
      if (state is OColorMappingReadyState) {
        color = state.getColor(subjectId: oElement.subjectId);
      } else {
        color = Colors.grey;
      }
    } else {
      color = Colors.grey;
    }
    return color;
  }

  static int getCurrentRelativeWeek() =>
      oViewModeBloc.state.currentRelativeWeek;

  static void setCurrentRelativeWeek(int i) =>
      addOViewModeEvent(OSetWeekEvent(i));

  static void setCurrentAbsoluteWeek(int i) {
    final calcValue = i - absoluteWeekSince1970(DateTime.now());
    addOViewModeEvent(OSetWeekEvent(calcValue));
  }

  static void switchToWeeklyViewModeIfNotAlready() {
    if (oViewModeBloc.state is! OWeeklyViewState) {
      addOViewModeEvent(OWeeklyViewEvent());
    }
  }

  static void deselectDrawerEventIfNotInitial() {
    if (oDrawerBloc.state is! ODrawerInitial) {
      addODrawerEvent(ODeselectEvent());
    }
  }

  static void stopAllShakingAnimationIfNotAlready() {
    if (oAnimationBloc.state is! OStopAllShakingAnimationState) {
      addOAnimationEvent(
          OSetAnimationStateEvent(OStopAllShakingAnimationState()));
    }
  }

  static int getCurrentSelectionAbsoluteWeek() =>
      absoluteWeekSince1970(DateTime.now().add(
        Duration(
          days: (7 * getCurrentRelativeWeek()),
        ),
      ));

  static int absoluteWeekSince1970(DateTime date) {
    var weekSince = date.millisecondsSinceEpoch.toDouble();
    // weekSince -= 345600000; //GMT
    weekSince -= 342000000; //germany
    // because unix timestamp starts at thursday,
    // remove everything until the followed monday
    // so our first week starts on monday
    weekSince -= date.timeZoneOffset.inMilliseconds;
    weekSince = ((weekSince / (1000 * 60 * 60 * 24 * 7)) + 1);
    // +1 because the initial week was removed by milliseconds
    // debugPrint(weekSince); // 2627,408... weeks since 1st of january 1970
    // example for 13.05.2020 ~20:00
    // assuming it can be + and -
    return weekSince.floor();
  }

  static DateTime getDateTimeFromWeekSince1970(
      int absoluteWeekSince1970, int weekday) {
    var date = DateTime.fromMillisecondsSinceEpoch(
        absoluteWeekSince1970 * (1000 * 60 * 60 * 24 * 7));
    date = date.add(Duration(days: -date.weekday + weekday));

    return date;
  }

  static double calculateBegin(
      DateTime start, double maxHeight, double extraSpace, OWeek oWeek) {
    // var a = Stopwatch();
    // a.start();
    // return 10.0;
    var startDay = DateTime.parse(DateFormat('yyyy-MM-dd').format(start));
    var startMinute = startDay.difference(start).inMinutes.abs().toDouble();

    // careful - we need to get the latest hour/minute of the week!
    startMinute = startMinute -
        oWeek.firstMinuteForEntireWeek; // now 8:00 o clock is zero
    startMinute = startMinute /
        (oWeek.latestMinuteForEntireWeek -
            oWeek.firstMinuteForEntireWeek +
            extraSpace) *
        maxHeight; //20:00 o clock (60*20)-480
    // a.stop();
    // debugPrint(a.elapsedMicroseconds.toString() +" ; begin");
    return startMinute.toDouble();
  }

  static bool areListsEqual(dynamic list1, dynamic list2) {
    // check if both are lists
    if (!(list1 is List && list2 is List)
        // check if both have same length
        ||
        list1.length != list2.length) {
      return false;
    }
    list1.sort();
    list2.sort();

    // check if elements are equal
    for (var i = 0; i < list1.length; i++) {
      if (list1[i] != list2[i]) {
        return false;
      }
    }

    return true;
  }

  static void resetEntireOrganizer() {
    oProgramBloc.clear();
    oProgramBloc.add(OProgramConfigurationResetEvent());
    oLessonBloc.clear();
    oLessonBloc.add(OLessonSelectionResetEvent());
    oColorMappingBloc.clear();
    oColorMappingBloc.add(OColorMappingResetEvent());
    oCalendarBloc.clear();
    oCalendarBloc.add(OCalendarResetEvent());
    oDrawerBloc.add(ODeselectEvent());
    oHistoryBloc.add(OProgramHistoryResetEvent());
    oHistoryBloc.clear();
    oSettingsBloc.add(OSettingsResetEvent());
    oSettingsBloc.clear();
    oLoadingBloc.add(OSetLoadingState(ONothingToLoadState()));
  }
}

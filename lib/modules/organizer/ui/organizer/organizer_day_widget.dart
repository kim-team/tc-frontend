/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:timeago/timeago.dart' as timeago;

import '../../../../common/tc_theme.dart';
import '../../blocs/o_calendar_bloc/o_calendar_bloc.dart';
import '../../blocs/o_color_mapping_bloc/o_color_mapping_bloc.dart';
import '../../blocs/o_view_mode_bloc/o_view_mode_bloc.dart';
import '../../model/o_element_wrapper.dart';
import '../../model/o_resource.dart';
import '../../model/o_week.dart';
import 'o_utility_functions.dart';
import 'organizer_constants.dart';

class OverviewDay extends StatefulWidget {
  final int weekday;
  final List<int> temporaryDeletedLessons;
  final OWeek oWeek;

  OverviewDay({
    Key? key,
    required this.weekday,
    required this.oWeek,
    required this.temporaryDeletedLessons,
  }) : super(key: key);

  @override
  _OverviewDayState createState() => _OverviewDayState();
}

class _OverviewDayState extends State<OverviewDay>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;
  late List<Choice> choices;

  @override
  void initState() {
    super.initState();

    // init choices
    choices = <Choice>[
      Choice(weekDay: 1),
      Choice(weekDay: 2),
      Choice(weekDay: 3),
      Choice(weekDay: 4),
      Choice(weekDay: 5),
    ];

    if (widget.oWeek.containsEntriesOn(DateTime.saturday) ||
        widget.oWeek.containsEntriesOn(DateTime.sunday)) {
      choices.add(Choice(weekDay: 6));
    }
    if (widget.oWeek.containsEntriesOn(DateTime.sunday)) {
      choices.add(Choice(weekDay: 7));
    }

    // find out what tab should be displayed
    var viewModeBlocState = BlocProvider.of<OViewModeBloc>(context).state;
    // if (viewModeBlocState is )
    if (viewModeBlocState is ODailyViewState) {
      _tabController = TabController(vsync: this, length: choices.length);
      // if (viewModeBlocState.weekday < 2) {
      //   _tabController.animateTo(choices.length - 1);
      // }
      // if (viewModeBlocState.weekday >= 2) {
      //   _tabController.animateTo(0);
      // }
      if (mounted) {
        _tabController.animateTo(widget.weekday - 1);
      }
    }
    // _tabController
    //     .addListener(_callBloc(ODailyViewEvent(weekday:
    //     _tabController.index)));
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final viewModeBloc = BlocProvider.of<OViewModeBloc>(context);
    return BlocListener(
      bloc: viewModeBloc,
      listener: (context, messageState) {
        if (messageState is ODailyViewState) {
          if (mounted) {
            _tabController.animateTo(messageState.weekday - 1);
          }
        }
      },
      child: DefaultTabController(
        length: choices.length,
        child: Scaffold(
          // backgroundColor: CorporateColors.passiveBackgroundLight,
          body: Stack(
            children: <Widget>[
              Positioned(
                top: 26,
                bottom: 0,
                left: 0,
                right: 0,
                child: NotificationListener(
                  // onNotification: (notification) {
                  //   if (notification is ScrollStartNotification) {
                  //     dragStartDetails = notification.dragDetails;
                  //   }
                  //   if (notification is OverscrollNotification) {
                  //     drag = widget.pageController.position
                  //         .drag(dragStartDetails, () {});
                  //     drag.update(notification.dragDetails);
                  //   }
                  //   if (notification is ScrollEndNotification) {
                  //     drag?.cancel();
                  //   }
                  //   return true;
                  // },
                  child: TabBarView(
                    // physics: NeverScrollableScrollPhysics(),
                    controller: _tabController,
                    children: choices
                        .map((choice) =>
                            _buildBody(choice: choice, context: context))
                        .toList(),
                  ),
                ),
              ),
              Positioned(
                top: 0,
                left: 0,
                right: 0,
                child: Container(
                  color: Colors.white,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Expanded(
                        flex: 2,
                        child: Container(),
                      ),
                      Expanded(
                        flex: choices.length == 5
                            ? 15
                            : choices.length == 6
                                ? 18
                                : 21,
                        child: SizedBox(
                          height: 36,
                          child: TabBar(
                            labelPadding: EdgeInsets.all(0.0),
                            indicatorSize: TabBarIndicatorSize.label,
                            indicatorPadding: EdgeInsets.all(0.0),
                            indicatorWeight: 2,
                            controller: _tabController,
                            // onTap: (value) {
                            // final oViewModeBloc =
                            //     BlocProvider.of<OViewModeBloc>(context);
                            // var state = oViewModeBloc.state;
                            // if (state is ODailyViewState) {
                            //   if (state.weekday == value + 1) {
                            //     oViewModeBloc.add(OWeeklyViewEvent());
                            //     return;
                            //   }
                            // }
                            // oViewModeBloc
                            //     .add(ODailyViewEvent(weekday: value + 1));
                            // },
                            tabs: choices
                                .map((choice) => Tab(
                                      iconMargin: EdgeInsets.all(0.0),
                                      child: Container(
                                        width: 60,
                                        height: 26,
                                        margin: EdgeInsets.symmetric(
                                            horizontal: 5.5),
                                        child: Wrap(
                                          // spacing: 2.0,
                                          // runSpacing: 2.0,
                                          alignment: WrapAlignment.center,
                                          children: List.generate(
                                            (widget.oWeek.days[choice.weekDay]
                                                        ?.length ??
                                                    0)
                                                .clamp(0, 10),
                                            (index) => buildSingleDot(
                                                context, choice, index),
                                          ),
                                        ),
                                      ),
                                    ))
                                .toList(),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              // Positioned(
              //   bottom: 0,
              //   left: 0,
              //   right: 0,
              //   child: Row(
              //     mainAxisAlignment: MainAxisAlignment.center,
              //     children: <Widget>[
              //       FlatButton(
              //         onPressed: () {
              //           _callBloc(OWeeklyViewEvent());
              //         },
              //         color:
              //             CorporateColors.tinyCampusIconGrey.
              //             withOpacity(0.95),
              //         shape: RoundedRectangleBorder(
              //             borderRadius: BorderRadius.circular(8.0)),
              //         child: Padding(
              //           padding: const EdgeInsets.all(16.0),
              //           child: Text(
              //             'Tagesansicht verlassen',
              //             style: TextStyle(
              //               color: Colors.white,
              //               fontSize: 16.0,
              //             ),
              //           ),
              //         ),
              //       ),
              //     ],
              //   ),
              // ),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildSingleDot(BuildContext context, Choice choice, int index) {
    if (OUtil.isDeleted(widget.oWeek.days[choice.weekDay]?[index])) {
      return SizedBox(width: 0, height: 0);
    } else if (widget.temporaryDeletedLessons
        .contains(widget.oWeek.days[choice.weekDay]?[index].ele.lessonId)) {
      return SizedBox(width: 0, height: 0);
    } else {
      const dotSize = 8.0;
      return Container(
        width: dotSize,
        height: dotSize,
        margin: EdgeInsets.all(1.0),
        padding: EdgeInsets.all(4.0),
        decoration: BoxDecoration(
            color:
                OUtil.getColor(widget.oWeek.days[choice.weekDay]?[index].ele),
            shape: BoxShape.circle),
      );
    }
  }

  Widget _buildBody({
    required Choice choice,
    required BuildContext context,
  }) =>
      BlocBuilder<OCalendarBloc, OCalendarState>(
          builder: (context, calendarState) {
        if (calendarState is OCalendarEmptyState) {
          return Center(
            child: FlatButton(
              onPressed: () {},
              child: I18nText('$i18nOKey.configure_organizer'),
            ),
          );
        } else if (calendarState is OCalendarReadyState) {
          return _buildOElementList(calendarState, choice.weekDay);
        } else {
          return Center(
              child: Column(
            children: <Widget>[
              I18nText(
                '$i18nOKey.error_messages.state_not_caught',
                translationParams: {"state": "$calendarState"},
              ),
            ],
          ));
        }
      });

  Widget _buildOElementList(OCalendarReadyState state, int weekday) {
    var weekMap = state.weekMap;
    if (weekMap[OUtil.getCurrentSelectionAbsoluteWeek()] == null) {
      return I18nText('$i18nOKey.error_messages.could_not_load_files');
    }
    return NotificationListener(
      onNotification: (notification) {
        if (notification is ScrollStartNotification) {
          // intercept, do nothing, this is important
        }
        if (notification is OverscrollNotification) {
          // intercept, do nothing, this is important
        }
        if (notification is ScrollEndNotification) {
          // intercept, do nothing, this is important
        }
        return true;
      },
      child: ListView.builder(
          physics: BouncingScrollPhysics(),
          itemCount: widget.oWeek.days[weekday]?.length ?? 0,
          itemBuilder: (context, index) {
            final oElementWrapper = widget.oWeek.days[weekday]?[index];
            // Filter
            if (OUtil.isDeleted(oElementWrapper)) {
              return Container();
            } else if (widget.temporaryDeletedLessons
                .contains(oElementWrapper?.ele.lessonId)) {
              return Container();
            } else {
              if (oElementWrapper != null) {
                return OElementDailyListTile(
                    context: context, oElementWrapper: oElementWrapper);
              } else {
                return Container();
              }
            }
          }),
    );
    // });
  }
}

class OElementDailyListTile extends StatelessWidget {
  const OElementDailyListTile({
    Key? key,
    required this.context,
    required this.oElementWrapper,
  }) : super(key: key);

  final BuildContext context;
  final OElementWrapper oElementWrapper;

  @override
  Widget build(BuildContext context) =>
      BlocBuilder<OColorMappingBloc, OColorMappingState>(
          builder: (context, colorState) {
        Color color;
        if (colorState is OColorMappingReadyState) {
          color = colorState.getColor(subjectId: oElementWrapper.ele.subjectId);
        } else {
          color = Colors.grey;
        }
        return GestureDetector(
          // onTap: () => selectElement(oElementWrapper, context),
          child: ODailyListTile(oElementWrapper: oElementWrapper, color: color),
        );
      });
}

class ODailyListTile extends StatelessWidget {
  ODailyListTile({
    Key? key,
    required this.oElementWrapper,
    required this.color,
  })  : start = oElementWrapper.ele.start ?? DateTime.now(),
        end = oElementWrapper.ele.end ?? DateTime.now(),
        super(key: key);

  final OElementWrapper oElementWrapper;
  final Color color;
  final DateTime start;
  final DateTime end;

  @override
  Widget build(BuildContext context) => Row(
        children: <Widget>[
          SizedBox(
            width: 50.0,
            child: Card(
              margin: EdgeInsets.all(0.0),
              // color: color,
              elevation: 0.0,
              child: Card(
                // margin: EdgeInsets.all(0.0),
                elevation: 0.0,
                color: Colors.white,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 2.0),
                      child: Text(
                        DateFormat('kk:mm').format(start),
                        style: TextStyle(
                          letterSpacing: -1.3,
                          fontSize: 18.0,
                        ),
                      ),
                    ),
                    // Padding(
                    //   padding: const EdgeInsets.symmetric(vertical: 2.0),
                    //   child: Text("-", style: TextStyle(letterSpacing: -1.0),
                    //   ),
                    // ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 2.0),
                      child: Text(DateFormat('kk:mm').format(end),
                          style: TextStyle(
                            letterSpacing: -1.3,
                            fontSize: 18.0,
                          )),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Expanded(
            // flex: 6,
            child: Container(
              margin: EdgeInsets.symmetric(
                vertical: 8.0,
              ),
              child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(12.0)),
                // elevation: 0.0,
                // margin: EdgeInsets.only(
                //   right: 4.0,
                //   top: 3.0,
                //   bottom: 3.0,
                // ),
                child: Container(
                  color: Colors.white,
                  child: AnimatedContainer(
                    duration: Duration(milliseconds: 240),
                    curve: Curves.elasticOut,
                    decoration: BoxDecoration(
                        border: Border(
                          left: BorderSide(width: 4.0, color: color),
                        ),
                        color: color.withOpacity(0.08)),
                    child: Container(
                      padding: EdgeInsets.symmetric(
                        horizontal: 8.0,
                        vertical: 8.0,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            oElementWrapper.ele.fullname ?? "",
                            // maxLines: 1,
                            // overflow: TextOverflow.ellipsis,
                            textAlign: TextAlign.start,
                            style: TextStyle(
                              color: CorporateColors.tinyCampusBlue,
                              letterSpacing: -1.0,
                              fontWeight: FontWeight.w400,
                              fontSize: 20.0,
                            ),
                          ),
                          Row(
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  oElementWrapper.ele.type != "?"
                                      ? "${oElementWrapper.ele.type} | "
                                          "${oElementWrapper.ele.typeLong}"
                                      : "${FlutterI18n.translate(
                                          context,
                                          'modules.organizer.data'
                                          '.unknown_short',
                                        )} | ${FlutterI18n.translate(
                                          context,
                                          'modules.organizer.data.unknown',
                                        )}",
                                  style: TextStyle(
                                    fontSize: 16,
                                    color: CorporateColors.tinyCampusBlue,
                                    fontWeight: FontWeight.w300,
                                    letterSpacing: -0.7,
                                  ),
                                ),
                              ),
                              Expanded(
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    Icon(
                                      Icons.access_time,
                                      size: 16,
                                      color: CorporateColors.tinyCampusIconGrey
                                          .withOpacity(0.5),
                                    ),
                                    Container(
                                      width: 4,
                                    ),
                                    I18nText(
                                      '$i18nOKey.duration',
                                      translationParams: {
                                        'minutes':
                                            (end.difference(start).inMinutes +
                                                    1)
                                                .toString()
                                      },
                                      child: Text(
                                        "Dauer:  min",
                                        style: TextStyle(
                                          fontSize: 16,
                                          color: CorporateColors.tinyCampusBlue,
                                          fontWeight: FontWeight.w300,
                                          letterSpacing: -0.7,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                          oElementWrapper.ele.comment != ""
                              ? Container(
                                  margin: EdgeInsets.symmetric(vertical: 4.0),
                                  child: SelectableText(
                                      oElementWrapper.ele.comment ?? "",
                                      scrollPhysics: BouncingScrollPhysics(),
                                      maxLines: 1,
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                          fontSize: 16.0,
                                          letterSpacing: -0.5,
                                          color: CorporateColors
                                              .tinyCampusOrange)),
                                )
                              : Container(),

                          Container(
                            margin: EdgeInsets.symmetric(
                              vertical: 4.0,
                            ),
                            child: Column(
                              children: List.generate(
                                  oElementWrapper.ele.resource.length,
                                  (index) => _buildDailyResourceRow(
                                      oElementWrapper.ele.resource[index])),
                            ),
                          ),
                          // Chip(
                          //   label: Text(
                          //     oElementWrapper.ele.typeLong,
                          //   ),
                          //   backgroundColor: color,
                          //   shadowColor: color,
                          //   labelStyle: TextStyle(
                          //     fontSize: 20,
                          //     color: CorporateColors.tinyCampusBlue,
                          //     fontWeight: FontWeight.w300,
                          //     letterSpacing: -1.5,
                          //   ),
                          //   // backgroundColor: color,
                          // ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              // Text(oElementWrapper.ele.abbr),
                              Text(
                                timeago.format(start,
                                    locale: FlutterI18n.currentLocale(context)
                                        ?.languageCode,
                                    allowFromNow: true),
                              ),
                              // Text(timeago.format(
                              //   start,
                              //   locale: FlutterI18n.currentLocale(context)
                              //       .languageCode,
                              //   allowFromNow: false,
                              // )),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      );
}

Row _buildDailyResourceRow(OResource res) => Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Expanded(
          // flex: 2,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                  margin: EdgeInsets.only(right: 4.0),
                  height: 16,
                  width: 16,
                  child: Image.asset(
                    "assets/images/organizer/organizer_teacher_room_pair.png",
                    fit: BoxFit.cover,
                  )

                  // Icon(
                  //   Icons.person_pin,
                  //   size: oDrawerIconHeight,
                  //   color: CorporateColors.tinyCampusBlue,
                  // ),
                  ),
              Expanded(
                child: Text(
                  "${res.role}: ${res.person}",
                  style: TextStyle(fontSize: 14.0),
                ),
              ),
            ],
          ),
        ),
        Expanded(
          // flex: 3,
          // child: Text(OUtil.concatRoomStrings(res)),
          // child: Text("f")
          child: Text("| ${res.rooms.join(", ")}"),
        )
      ],
    );

// String timeUntil(DateTime date) {
//   return timeago.format(date, locale: locale, allowFromNow: true);
// }

// void selectElement(OElementWrapper eW, BuildContext context) {
//   final drawerBloc = BlocProvider.of<ODrawerBloc>(context);
//   var state = drawerBloc.state;
//   if (state is ODrawerInitial) {
//     drawerBloc.add(OSelectEvent(oElementWrapper: eW));
//   } else if (state is ODrawerColorState) {
//     drawerBloc.add(OColorEvent(oElementWrapper: eW));
//   } else {
//     if (state.isDirectlySelected(eW)) {
//       drawerBloc.add(ODeselectEvent(oElementWrapper: eW));
//     } else {
//       drawerBloc.add(OSelectEvent(oElementWrapper: eW));
//       // final animationBloc = BlocProvider.of<OAnimationBloc>(context);
//       // animationBloc.add(OStopAllShakingAnimation());
//     }
//   }
// }

class Choice {
  const Choice({
    this.title = "",
    this.weekDay = 0,
    this.icon,
  });

  final String title;
  final int weekDay;
  final Icon? icon;
}

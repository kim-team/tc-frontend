/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../../../../../common/tc_theme.dart';
import '../../../model/o_element.dart';
import '../organizer_constants.dart'
    hide FlutterI18n, i18nOKey, I18nText, I18nPlural;
import 'i18n_common.dart';

class ODrawerInformationRow extends StatelessWidget {
  const ODrawerInformationRow({
    Key? key,
    required this.element,
  }) : super(key: key);
  final OElement element;

  @override
  Widget build(BuildContext context) {
    EdgeInsetsGeometry itemsMargin = EdgeInsets.only(
      right: 8.0,
      left: 0.0,
      bottom: 14.0,
    );
    return Scrollbar(
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        physics: BouncingScrollPhysics(),
        child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                width: 70,
                margin: itemsMargin,
                child: ODrawerTimePair(
                  element: element,
                ),
              ),
              Container(
                width: 80,
                margin: itemsMargin,
                child: ODrawerTypePair(
                  element: element,
                ),
              ),
              Container(
                // width: 80,
                margin: itemsMargin,
                child: ODrawerPersonRoomPairs(
                  element: element,
                ),
              ),
              element.organization != null
                  ? Container(
                      // width: 80,
                      margin: itemsMargin,
                      child: ODrawerOrganizationPair(
                        element: element,
                      ),
                    )
                  : Container(),
            ]),
      ),
    );
  }
}

class ODrawerOrganizationPair extends StatelessWidget {
  const ODrawerOrganizationPair({
    Key? key,
    required this.element,
  }) : super(key: key);
  final OElement element;

  @override
  Widget build(BuildContext context) => Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            margin: iconMargin,
            child: Icon(
              Icons.domain,
              size: oDrawerIconHeight,
              color: CorporateColors.tinyCampusBlue,
              // color: CorporateColors.tinyCampusIconGrey.withOpacity(0.5),
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              I18nText(
                '$i18nDrawerKey.organization',
                child: Text(
                  "Organisation",
                  softWrap: true,
                  style: TextStyle(fontSize: 12.0),
                ),
              ),
              Text(
                element.organization ?? "",
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 16,
                  color: CorporateColors.tinyCampusBlue,
                  letterSpacing: -0.5,
                ),
              ),
            ],
          ),
        ],
      );
}

class ODrawerTypePair extends StatelessWidget {
  const ODrawerTypePair({
    Key? key,
    required this.element,
  }) : super(key: key);
  final OElement element;

  @override
  Widget build(BuildContext context) => Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            margin: iconMargin,
            child: Stack(
              alignment: Alignment.center,
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                    color: Colors.transparent,
                    shape: BoxShape.circle,
                    border: Border.all(
                        width: 3,
                        color:
                            CorporateColors.tinyCampusIconGrey.withOpacity(0.5),
                        style: BorderStyle.solid),
                  ),
                  width: oDrawerIconHeight - 3,
                  height: oDrawerIconHeight - 3,
                ),
                Center(
                  child: Text(
                    (element.type) == "?"
                        ? FlutterI18n.translate(
                            context, 'modules.organizer.data.unknown_short')
                        : element.type, // TODO NNBD MIGRATION
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      letterSpacing: -1.0,
                      fontSize: 16,
                      fontWeight: FontWeight.w600,
                      color: CorporateColors.tinyCampusBlue,
                    ),
                  ),
                ),
              ],
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                FlutterI18n.translate(context, '$i18nODKey.type'),
                softWrap: true,
                style: TextStyle(fontSize: 12.0),
              ),
              Text(
                (element.type) == "?"
                    ? FlutterI18n.translate(
                        context, 'modules.organizer.data.unknown_short')
                    : element.typeLong, // TODO NNBD MIGRATION
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 16,
                  color: CorporateColors.tinyCampusBlue,
                  letterSpacing: -0.5,
                ),
              ),
            ],
          ),
        ],
      );
}

class ODrawerTimePair extends StatelessWidget {
  const ODrawerTimePair({
    Key? key,
    required this.element,
  }) : super(key: key);
  final OElement element;

  @override
  Widget build(BuildContext context) {
    // final isToday =
    //     DateTime.now().difference(element.start ?? DateTime.now()).inDays ==
    //             0 &&
    //         DateTime.now().day == element.start?.day;
    final sampleDate = DateTime.parse("1970-01-01");
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Container(
          margin: iconMargin,
          child: Icon(
            Icons.access_time,
            size: oDrawerIconHeight,
            color: CorporateColors.tinyCampusIconGrey.withOpacity(0.5),
          ),
        ),
        Column(
          children: <Widget>[
            I18nText(
              '$i18nDrawerKey.from_to',
              child: Text(
                "Von - Bis",
                softWrap: true,
                style: TextStyle(fontSize: 12.0),
              ),
            ),
            TweenAnimationBuilder<int>(
              tween: IntTween(
                begin: sampleDate
                    .add(Duration(
                        hours: element.start?.hour ?? 0,
                        minutes: element.start?.minute ?? 0))
                    .millisecondsSinceEpoch,
                end: sampleDate
                    .add(Duration(
                        hours: element.start?.hour ?? 0,
                        minutes: element.start?.minute ?? 0))
                    .millisecondsSinceEpoch,
              ),
              curve: Curves.easeOut,
              duration: Duration(milliseconds: 360),
              builder: (context, i, child) => Text(
                DateFormat('kk:mm')
                    .format(DateTime.fromMillisecondsSinceEpoch(i)),
                style: TextStyle(
                  fontSize: 16,
                  color: CorporateColors.tinyCampusBlue,
                  letterSpacing: -0.5,
                ),
              ),
            ),
            TweenAnimationBuilder<int>(
              tween: IntTween(
                begin: sampleDate
                    .add(Duration(
                        hours: element.end?.hour ?? 0,
                        minutes: element.end?.minute ?? 0))
                    .millisecondsSinceEpoch,
                end: sampleDate
                    .add(Duration(
                        hours: element.end?.hour ?? 0,
                        minutes: element.end?.minute ?? 0))
                    .millisecondsSinceEpoch,
              ),
              curve: Curves.easeOut,
              duration: Duration(milliseconds: 360),
              builder: (context, i, child) => Text(
                DateFormat('kk:mm')
                    .format(DateTime.fromMillisecondsSinceEpoch(i)),
                style: TextStyle(
                  fontSize: 16,
                  color: CorporateColors.tinyCampusBlue,
                  letterSpacing: -0.5,
                ),
              ),
            ),
          ],
        )
      ],
    );
  }
}

class ODrawerPersonRoomPairs extends StatelessWidget {
  const ODrawerPersonRoomPairs({
    Key? key,
    required this.element,
  }) : super(key: key);
  final OElement element;

  @override
  Widget build(BuildContext context) => Row(
        children: buildTeacherRoomArrayWidget(element),
      );

  List<Widget> buildTeacherRoomArrayWidget(OElement element) {
    var array = <Widget>[];
    var pairColumn = Container();
    if (element.resource.isEmpty) {
      array.add(Container());
      return array;
    }

    for (var i = 0; i < (element.resource.length); i++) {
      pairColumn = Container();
      var teacher = "";
      var rooms = "";
      var role = "";
      teacher = element.resource[i].person;
      role = element.resource[i].role;
      for (var j = 0; j < element.resource[i].rooms.length; j++) {
        rooms = rooms + element.resource[i].rooms[j].name;
        if (j >= 0 &&
            j < element.resource[i].rooms.length - 1 &&
            element.resource[i].rooms.length > 1) {
          rooms = "$rooms, ";
        }
      }
      pairColumn = Container(
        margin: EdgeInsets.only(left: 8.0),
        // width: 70,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
                margin: iconMargin,
                height: oDrawerIconHeight,
                width: oDrawerIconHeight,
                child: Image.asset(
                  "assets/images/organizer/organizer_teacher_room_pair.png",
                  fit: BoxFit.cover,
                )

                // Icon(
                //   Icons.person_pin,
                //   size: oDrawerIconHeight,
                //   color: CorporateColors.tinyCampusBlue,
                // ),
                ),
            Text(
              role,
              softWrap: true,
              style: TextStyle(
                fontSize: 12.0,
                letterSpacing: -0.5,
              ),
              overflow: TextOverflow.ellipsis,
            ),
            Text(
              teacher,
              softWrap: true,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                fontSize: 16.0,
                letterSpacing: -0.5,
                // fontWeight: FontWeight.w300,
                color: CorporateColors.tinyCampusBlue,
              ),
            ),
            SizedBox(
              width: 90,
              child: Text(
                rooms,
                // overflow: TextOverflow.ellipsis,
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 14.0,
                  letterSpacing: -0.5,
                ),
              ),
            ),
          ],
        ),
      );
      array.add(pairColumn);
      // return Text("test");
    }
    // return Row(
    //   crossAxisAlignment: CrossAxisAlignment.center,
    //   mainAxisAlignment: MainAxisAlignment.start,
    //   children: array,
    // );
    return array;
  }
}

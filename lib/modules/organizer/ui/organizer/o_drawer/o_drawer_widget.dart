/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../blocs/o_animation_bloc/o_animation_bloc.dart';
import '../../../blocs/o_drawer_bloc/o_drawer_bloc.dart';
import '../o_utility_functions.dart';
import 'o_drawer_m_color.dart';
import 'o_drawer_m_delete.dart';
import 'o_drawer_m_info.dart';
import 'o_drawer_menu.dart';

class ODrawerWidget extends StatefulWidget {
  const ODrawerWidget({Key? key}) : super(key: key);

  @override
  _ODrawerWidgetState createState() => _ODrawerWidgetState();
}

class _ODrawerWidgetState extends State<ODrawerWidget>
    with SingleTickerProviderStateMixin {
  double bottomValue = -150;
  @override
  Widget build(BuildContext context) => BlocListener<ODrawerBloc, ODrawerState>(
      listener: (context, state) {
        if (state is ODrawerInitial) {
          setState(() {
            bottomValue = -150;
          });
        } else if (state is ODrawerMenuState) {
          setState(() {
            bottomValue = -150;
          });
        }
      },
      child: AnimatedPositioned(
        duration: Duration(milliseconds: 260),
        curve: Curves.easeOut,
        bottom: bottomValue,
        left: 0,
        right: 0,
        child: determineContent(),
      ));

  Widget determineContent() => BlocBuilder<ODrawerBloc, ODrawerState>(
        builder: (context, drawerState) {
          Widget child = Container();
          final oElement = drawerState.oElementWrapper;

          if (oElement == null || drawerState is ODrawerInitial) {
            child = Container();
          } else if (drawerState is ODrawerMenuState) {
            child = ODrawerMenu(
              oElementWrapper: oElement,
              drawerMenuContent: ODrawerContent(
                oElementWrapper: oElement,
              ),
            );
          } else if (drawerState is ODrawerDeleteState) {
            child = ODrawerMenu(
              oElementWrapper: oElement,
              drawerMenuContent: ODrawerMDelete(
                oElementWrapper: oElement,
              ),
              showColorTopBar: true,
            );
          } else if (drawerState is ODrawerColorState) {
            child = ODrawerMenu(
              oElementWrapper: oElement,
              drawerMenuContent: ODrawerMColor(
                oElementWrapper: oElement,
              ),
              showColorTopBar: true,
            );
          } else if (drawerState is ODrawerInfoState) {
            child = ODrawerMenu(
              oElementWrapper: oElement,
              drawerMenuContent: ODrawerMInfo(
                oElementWrapper: oElement,
              ),
            );
          }

          return GestureDetector(
            onVerticalDragUpdate: (details) {
              // if (details.delta.dy > 0) {
              if (bottomValue == -450) return;
              bottomValue = bottomValue - details.delta.dy;
              if (bottomValue < -150) {
                setState(() {});
              }
              //  print(details.delta.dy.toString());
              // }
              if (details.localPosition.dy > 150) {
                shouldDisappear();
              }
              // print(details.localPosition.dy);
            },
            onVerticalDragEnd: (details) {
              if (bottomValue == -450) return;
              if (bottomValue < -200) {
                shouldDisappear();
              } else {
                setState(() {
                  bottomValue = -150;
                });
              }
            },
            child: Card(
              elevation: 4.0,
              margin: EdgeInsets.symmetric(horizontal: 4.0),
              clipBehavior: Clip.antiAlias,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(12)),
              ),
              child: Column(
                children: [
                  ODrawerColorTop(
                    oElementWrapper: drawerState.oElementWrapper,
                  ),
                  AnimatedSize(
                    curve: Curves.easeOut,
                    alignment: Alignment.bottomCenter,
                    duration: Duration(milliseconds: 260),
                    child: child,
                  ),
                ],
              ),
            ),
          );
        },
      );

  void shouldDisappear() {
    setState(() {
      bottomValue = -450;
    });
    Timer(Duration(milliseconds: 50), () {
      OUtil.addODrawerEvent(ODeselectEvent());
      OUtil.oAnimationBloc
          .add(OSetAnimationStateEvent(OStopAllShakingAnimationState()));
    });
  }
}

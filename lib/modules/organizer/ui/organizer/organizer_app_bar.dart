/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

import '../../../../common/constants/routing_constants.dart';
import '../../../../common/tc_theme.dart';
import '../../../../common/widgets/dialog/tc_dialog.dart' show TCDialog;
import '../../blocs/o_animation_bloc/o_animation_bloc.dart';
import '../../blocs/o_calendar_bloc/o_calendar_bloc.dart';
import '../../blocs/o_loading_bloc/o_loading_bloc.dart';
import '../../blocs/o_message_bloc/o_message_bloc.dart';
import '../../blocs/o_program_history_bloc/o_program_history_bloc.dart';
import '../../blocs/o_settings_bloc/o_settings_bloc.dart';
import '../../blocs/o_view_mode_bloc/o_view_mode_bloc.dart';
import '../../utility/organizer_data_loader.dart';
import '../configuration/organizer_configuration_page.dart';
import 'o_utility_functions.dart';
import 'organizer_constants.dart';

class OAppBar extends StatefulWidget implements PreferredSizeWidget {
  OAppBar({Key? key, required this.pageController})
      : preferredSize = Size.fromHeight(kToolbarHeight),
        super(key: key);

  @override
  final Size preferredSize; // default is 56.0
  final PageController pageController; // default is 56.0

  @override
  _OAppBarState createState() => _OAppBarState();
}

class _OAppBarState extends State<OAppBar> with SingleTickerProviderStateMixin {
  @override
  AppBar build(BuildContext context) => AppBar(
        elevation: 0.0,
        title: _buildAppBarTitle(context),
        actions: <Widget>[
          PopupMenuButton(
            icon: BlocBuilder<OProgramHistoryBloc, OProgramHistoryState>(
              builder: (context, state) => Stack(
                children: [
                  Positioned(
                    height: 8,
                    width: 8,
                    right: 0,
                    top: 0,
                    child: state.hasUncheckedChanges()
                        ? Container(
                            width: 8,
                            height: 8,
                            decoration: BoxDecoration(
                              color: CorporateColors.cafeteriaCautionRed,
                              shape: BoxShape.circle,
                            ),
                          )
                        : Container(),
                  ),
                  Icon(Icons.more_vert),
                ],
              ),
            ),
            itemBuilder: (context) => [
              PopupMenuItem(
                child: Row(
                  children: [
                    Expanded(
                      child: Padding(
                        padding: EdgeInsets.only(right: 48.0),
                        child: I18nText(
                          '$i18nOPKey.configure_organizer',
                          child: Text(
                            'Stundenplan konfigurieren',
                          ),
                        ),
                      ),
                    ),
                    BlocBuilder<OProgramHistoryBloc, OProgramHistoryState>(
                      builder: (context, state) => CountUncheckedDeltasWidget(
                        noDescription: true,
                      ),
                    ),
                  ],
                ),
                value: 0,
              ),
              PopupMenuItem(
                child: I18nText(
                  '$i18nOPKey.reset_entire_organizer',
                  child: Text(
                    'Stundenplan vollständig zurücksetzen',
                  ),
                ),
                value: 1,
              ),

              // PopupMenuItem(
              //   child: I18nText(
              //     'modules.organizer.ui.reset_colors',
              //     child: Text(
              //       'Farben zurÃ¼cksetzen',
              //     ),
              //   ),
              //   value: 2,
              // ),
              // PopupMenuItem(
              //   child: I18nText(
              //     'modules.organizer.ui.refresh',
              //     child: Text(
              //       'Aktualisieren',
              //     ),
              //   ),
              //   value: 3,
              // ),
              if ((OUtil.getDeletedOElements()?.length ?? 0) > 0)
                PopupMenuItem(
                  child: Text(
                    FlutterI18n.plural(
                        context,
                        '$i18nOPKey.clear_deleted_single_dates.count',
                        OUtil.getDeletedOElements()?.length ?? 0),
                  ),
                  value: 4,
                ),
            ],
            onSelected: (result) {
              if (result == 0) {
                // OUtil.addOAnimationEvent(OGoToInitialPage());
                OUtil.deselectDrawerEventIfNotInitial();
                Navigator.pushNamed(context, orgaConfigurationRoute)
                    .whenComplete(() {
                  ODataLoader.loadData(onlyCurrentWeek: true);
                  OUtil.setOAnimationState(OInitialPage());
                });
              }
              if (result == 1) {
                TCDialog.showCustomDialog(
                  context: context,
                  onConfirm: () {
                    OUtil.setOAnimationState(OInitialPage());
                    OUtil.resetEntireOrganizer();
                  },
                  headlineText: FlutterI18n.translate(
                      context, '$i18nOPKey.reset_entire_organizer_question'),
                  bodyText: FlutterI18n.translate(
                      context, '$i18nOPKey.reset_warning'),
                  functionActionText:
                      FlutterI18n.translate(context, '$i18nOPKey.reset'),
                  functionActionColor: CorporateColors.cafeteriaCautionRed,
                );
              }
              // if (result == 2) {
              //   final colorsBloc =
              // BlocProvider.of<OColorMappingBloc>(context);
              //   colorsBloc.add(ClearAllColorsEvent());
              //   colorsBloc.clear();
              //   setState(() {});
              // }
              // if (result == 3) {
              //   _loadData(
              //       context: context, forceReload: true,
              //  onlyCurrentWeek: true);
              // }
              if (result == 4) {
                OUtil.addOMessageEvent(OResolvedMessage());
                OUtil.addOAnimationEvent(OSetAnimationStateEvent(
                    ORevertDeleteMultiOElementsAnimationState(
                        OUtil.getDeletedOElementIds())));
                OUtil.addOSettingsEvent(ClearAllOElements());
              }
            },
          ),
        ],
      );

  Widget _buildLeaveDailyView() => Center(
        child: RaisedButton(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(12.0),
          ),
          onPressed: () {
            final viewModeBloc = BlocProvider.of<OViewModeBloc>(context);
            viewModeBloc.add(OWeeklyViewEvent());
          },
          elevation: 0.0,

          color: Colors.white,
          child: I18nText(
            '$i18nOKey.leave_day_view',
            child: Text(
              "Tagesansicht verlassen",
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.headline3,
            ),
          ),
          // child: _buildCurrentMonthSelectionWidget(),
        ),
      );

  Widget _buildAppBarTitle(BuildContext context) =>
      BlocBuilder<OLoadingBloc, OLoadingState>(
        builder: (context, state) => BlocBuilder<OCalendarBloc, OCalendarState>(
            builder: (context, calendarState) {
          if (calendarState is OCalendarEmptyState) {
            return I18nText('$i18nOKey.organizer');
          } else if (calendarState is OCalendarReadyState) {
            return BlocBuilder<OViewModeBloc, OViewModeState>(
              builder: (context, state) {
                if (state is OWeeklyViewState) {
                  return Container(child: _buildWeekHeaderWidget(context));
                } else if (state is ODailyViewState) {
                  return _buildLeaveDailyView();
                }
                return Container();
              },
            );
          }
          return I18nText('$i18nOKey.organizer');
        }),
      );

  Widget _buildWeekHeaderWidget(BuildContext context) => Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Ink(
              decoration: ShapeDecoration(
                shape: CircleBorder(),
              ),
              child: IconButton(
                tooltip:
                    FlutterI18n.translate(context, '$i18nOKey.preceding_week'),
                visualDensity: VisualDensity.compact,
                color: CurrentTheme().tcBlueFont,
                icon: Icon(Icons.chevron_left),
                onPressed: () {
                  OUtil.setOAnimationState(OPreviousPageState());
                },
              ),
            ),
            RaisedButton(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(12.0),
              ),
              padding: EdgeInsets.all(0.0),
              onPressed: () {
                OUtil.setOAnimationState(OInitialPage());
              },
              color: Theme.of(context).primaryColor,
              elevation: 0.0,
              child: _buildCurrentMonthSelectionWidget(),
            ),
            Ink(
              decoration: ShapeDecoration(
                shape: CircleBorder(),
              ),
              child: IconButton(
                tooltip: FlutterI18n.translate(context, '$i18nOKey.next_week'),
                visualDensity: VisualDensity.compact,
                color: CurrentTheme().tcBlueFont,
                icon: Icon(Icons.chevron_right),
                onPressed: () {
                  OUtil.setOAnimationState(ONextPageState());
                },
              ),
            ),
          ],
        ),
      );

  Widget _buildCurrentMonthSelectionWidget() => AnimatedBuilder(
        animation: widget.pageController,
        builder: (context, child) => Column(
          children: <Widget>[
            Text(
              DateFormat('MMMM yyyy',
                      FlutterI18n.currentLocale(context)?.languageCode)
                  .format(DateTime.now()
                      .add(Duration(days: OUtil.getCurrentRelativeWeek() * 7))),
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontSize: 18.0,
                  color: CurrentTheme().tcBlueFont,
                  fontWeight: FontWeight.w300,
                  letterSpacing: -1.3),
            ),
            BlocBuilder<OViewModeBloc, OViewModeState>(
              builder: (context, state) =>
                  Container(child: _buildWeekDateRowFromToDated()),
            ),
            // Transform.rotate(
            //     angle: OUtil.pageController.page -
            // OUtil.pageController.initialPage,
            //     child: Icon(Icons.access_time,size: 16.0,)),
          ],
        ),
        // child:
      );

  Widget _buildWeekDateRowFromToDated() {
    var leftSide = DateFormat('dd.MM.').format(DateTime.now()
        .add(Duration(days: OUtil.getCurrentRelativeWeek() * 7))
        .subtract(Duration(days: DateTime.now().weekday - 1)));
    var rightSide = DateFormat('dd.MM.').format(DateTime.now()
        .add(Duration(days: OUtil.getCurrentRelativeWeek() * 7))
        .add(Duration(days: 7 - DateTime.now().weekday)));

    return Row(
      children: <Widget>[
        Text(
          "$leftSide - $rightSide",
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 14.0,
            fontWeight: FontWeight.w500,
            letterSpacing: -0.4,
          ),
        ),
      ],
    );
  }
}

/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:math';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

import '../../../../../common/tc_theme.dart';
import '../../../../../common/widgets/dialog/tc_dialog.dart' show TCDialog;
import '../../../../../repositories/http/http_repository.dart';
import '../../../blocs/o_program_history_bloc/o_program_history_bloc.dart';
import '../../../model/o_change_record.dart';
import '../../../model/o_lesson.dart';
import '../../../model/o_pool.dart';
import '../../../model/o_program.dart';
import '../../../model/o_subject.dart';
import '../../../utility/organizer_program_functions.dart';
import '../o_utility_functions.dart';
import '../organizer_constants.dart';
import 'o_change_list_tile.dart';

class OrganizerProgramHistoryPageArguments {
  OrganizerProgramHistoryPageArguments(this.program);
  final OProgram program;
}

class OrganizerProgramHistoryPage extends StatelessWidget {
  const OrganizerProgramHistoryPage({Key? key, required this.program})
      : super(key: key);

  final OProgram program;
  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: Text(FlutterI18n.translate(
              context, 'modules.organizer.ui.changes.chip_label')),
        ),
        body: BlocBuilder<OProgramHistoryBloc, OProgramHistoryState>(
          builder: (context, historyState) {
            var amountChanges = historyState.countUncheckedDeltas(
                program.toContextString() + contextStringSeparator);
            return SingleChildScrollView(
              physics: BouncingScrollPhysics(),
              child: Column(
                children: [
                  if (kDebugMode || kProfileMode)
                    SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: Wrap(
                        children: [
                          FlatButton(
                            child: Text("Update Program Clean"),
                            onPressed: () =>
                                _reloadProgramClean(context, program),
                          ),
                          FlatButton(
                            child: Text("Reset History"),
                            onPressed: () =>
                                OUtil.resetSpecificOProgramHistory(program),
                          ),
                          FlatButton(
                            child: Text("Transfer Program to History"),
                            onPressed: () =>
                                _transferProgramToHistory(context, program),
                          ),
                          FlatButton(
                            child: Text("Add Random Pool"),
                            onPressed: () => _addRandomPool(context, program),
                          ),
                          FlatButton(
                            child: Text("Add Random Subjects"),
                            onPressed: () =>
                                _addRandomSubjects(context, program),
                          ),
                          FlatButton(
                            child: Text("Add Random Lessons"),
                            onPressed: () =>
                                _addRandomLessons(context, program),
                          ),
                          FlatButton(
                            child: Text("Rename Random Pool"),
                            onPressed: () =>
                                _renameRandomPools(context, program),
                          ),
                          FlatButton(
                            child: Text("Rename Random Subjects"),
                            onPressed: () =>
                                _renameRandomSubjects(context, program),
                          ),
                          FlatButton(
                            child: Text("Rename Random Lessons"),
                            onPressed: () =>
                                _renameRandomLessons(context, program),
                          ),
                          FlatButton(
                            child: Text("Delete Random Pool"),
                            onPressed: () =>
                                _deleteRandomPools(context, program),
                          ),
                          FlatButton(
                            child: Text("Delete Random Subjects"),
                            onPressed: () =>
                                _deleteRandomSubjects(context, program),
                          ),
                          FlatButton(
                            child: Text("Delete Random Lessons"),
                            onPressed: () =>
                                _deleteRandomLessons(context, program),
                          ),
                        ],
                      ),
                    ),
                  Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: 48.0, vertical: 12.0),
                      child: Center(
                          child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(program.name,
                              textAlign: TextAlign.center,
                              style: Theme.of(context).textTheme.headline2),
                          Text(
                            "${FlutterI18n.translate(
                              context,
                              'modules.organizer.ui.organizer'
                              '.week_widget.last_updated',
                            )}: ${DateFormat.yMd(
                              FlutterI18n.currentLocale(context)?.languageCode,
                            ).add_jm().format(historyState.lastUpdated)}",
                            textAlign: TextAlign.center,
                            style: Theme.of(context)
                                .textTheme
                                .bodyText2
                                ?.copyWith(
                                    color: CorporateColors.tinyCampusIconGrey),
                          ),
                        ],
                      ))),
                  Padding(
                    padding: EdgeInsets.all(16.0),
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        Padding(
                          padding: EdgeInsets.all(12.0),
                          child: AspectRatio(
                            aspectRatio: 1.0,
                            child: Container(
                              width: MediaQuery.of(context).size.width,
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle, color: Colors.white),
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.all(36.0),
                          child: AspectRatio(
                            aspectRatio: 1.0,
                            child: Container(
                              width: MediaQuery.of(context).size.width,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                border: Border.all(
                                    width: 4,
                                    color: amountChanges > 0
                                        ? CorporateColors.cafeteriaCautionRed
                                            .withOpacity(0.2)
                                        : CorporateColors.tinyCampusIconGrey
                                            .withOpacity(0.2)),
                              ),
                            ),
                          ),
                        ),
                        Column(
                          children: [
                            Text(
                              amountChanges.toString(),
                              style: TextStyle(
                                  fontSize: 72,
                                  color: amountChanges > 0
                                      ? CorporateColors.cafeteriaCautionRed
                                      : CorporateColors.tinyCampusIconGrey),
                            ),
                            Text(
                              FlutterI18n.translate(
                                  context,
                                  'modules.organizer.ui.'
                                  'changes.chip_label'),
                              style: Theme.of(context)
                                  .textTheme
                                  .headline3
                                  ?.copyWith(
                                      color:
                                          CorporateColors.tinyCampusIconGrey),
                            ),
                            if (amountChanges > 0)
                              Text(
                                FlutterI18n.translate(
                                    context,
                                    'modules.organizer.ui.'
                                    'changes.tap_to_read'),
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyText2
                                    ?.copyWith(
                                        color:
                                            CorporateColors.tinyCampusIconGrey),
                              ),
                          ],
                        ),
                        //   },
                        // ),
                        Padding(
                          padding: EdgeInsets.all(36.0),
                          child: AspectRatio(
                            aspectRatio: 1.0,
                            child: Material(
                              color: Colors.transparent,
                              child: InkWell(
                                customBorder: CircleBorder(),
                                onTap: () {
                                  if (amountChanges == 0) {
                                    TCDialog.showCustomDialog(
                                      context: context,
                                      onConfirm: () =>
                                          OUtil.addOHistoryBlocEvent(
                                        UserCheckedAllChangeForProgramEvent(
                                          program.toContextString() +
                                              contextStringSeparator,
                                        ),
                                      ),
                                      headlineText: FlutterI18n.translate(
                                        context,
                                        '$i18nOCHKey'
                                        '.clear_notifications_head',
                                      ),
                                      bodyText: FlutterI18n.translate(
                                        context,
                                        '$i18nOCHKey'
                                        '.clear_notifications_body',
                                      ),
                                    );
                                  }
                                },
                                child: Container(
                                  width: MediaQuery.of(context).size.width,
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Colors.transparent,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                      //   );
                      // },
                    ),
                  ),
                  if (amountChanges > 0)
                    OChangeList(
                        changeMap:
                            historyState.getChangesByProgram(program.id)),
                ],
              ),
            );
          },
        ),
      );

  void _addRandomSubjects(BuildContext context, OProgram remoteProgram) {
    var program = OProgram.fromJson(remoteProgram.toJson());
    for (var j = 0; j < 1; j++) {
      var poolRngLength = Random().nextInt(program.pools.length);
      var subjectRngLength =
          Random().nextInt(program.pools[poolRngLength].subjects.length);
      program.pools[poolRngLength].subjects.add(
        OSubject(
          id: program.pools[poolRngLength].subjects[subjectRngLength].id +
              Random().nextInt(800),
          name:
              "${program.pools[poolRngLength].subjects[subjectRngLength].name}"
              "CCC",
          lessons:
              program.pools[poolRngLength].subjects[subjectRngLength].lessons,
          enabled: false,
        ),
      );
    }
    _addOProgram(context, program);
  }

  void _addRandomLessons(BuildContext context, OProgram remoteProgram) {
    var program = OProgram.fromJson(remoteProgram.toJson());
    for (var j = 0; j < 1; j++) {
      var poolRngLength = Random().nextInt(program.pools.length);
      var subjectRngLength =
          Random().nextInt(program.pools[poolRngLength].subjects.length);
      program.pools[poolRngLength].subjects[subjectRngLength].lessons.add(
          OLesson(
              name: "TestLesson$j",
              id: Random().nextInt(99999),
              method: "FUN",
              comment: "created by testmethod",
              fullname: "Test Lesson Organizer"));
    }
    _addOProgram(context, program);
  }

  void _renameRandomPools(BuildContext context, OProgram remoteProgram) {
    var program = OProgram.fromJson(remoteProgram.toJson());
    for (var j = 0; j < 1; j++) {
      var poolRngLength = Random().nextInt(program.pools.length);
      program.pools[poolRngLength].name =
          "${program.pools[poolRngLength].name} (manipulated)";
    }
    _addOProgram(context, program);
  }

  void _deleteRandomPools(BuildContext context, OProgram remoteProgram) {
    var program = OProgram.fromJson(remoteProgram.toJson());
    for (var j = 0; j < 1; j++) {
      var poolRngLength = Random().nextInt(program.pools.length);
      program.pools.removeAt(poolRngLength);
    }
    _addOProgram(context, program);
  }

  void _renameRandomSubjects(BuildContext context, OProgram remoteProgram) {
    var program = OProgram.fromJson(remoteProgram.toJson());
    for (var j = 0; j < 1; j++) {
      var poolRngLength = Random().nextInt(program.pools.length);
      var subjectRngLength =
          Random().nextInt(program.pools[poolRngLength].subjects.length);
      program.pools[poolRngLength].subjects[subjectRngLength].name =
          "${program.pools[poolRngLength].subjects[subjectRngLength].name} "
          "(manipulated)";
    }
    _addOProgram(context, program);
    // _addOProgramHistory(context, program);
  }

  void _deleteRandomSubjects(BuildContext context, OProgram remoteProgram) {
    var program = OProgram.fromJson(remoteProgram.toJson());
    for (var j = 0; j < 1; j++) {
      var poolRngLength = Random().nextInt(program.pools.length);
      var subjectRngLength =
          Random().nextInt(program.pools[poolRngLength].subjects.length);
      program.pools[poolRngLength].subjects.removeAt(subjectRngLength);
    }
    _addOProgram(context, program);
  }

  void _renameRandomLessons(BuildContext context, OProgram remoteProgram) {
    var program = OProgram.fromJson(remoteProgram.toJson());
    for (var j = 0; j < 3; j++) {
      var poolRngLength = Random().nextInt(program.pools.length);
      var subjectRngLength =
          Random().nextInt(program.pools[poolRngLength].subjects.length);
      var lessonRngLength = Random().nextInt(program
          .pools[poolRngLength].subjects[subjectRngLength].lessons.length);

      var randomLesson = program.pools[poolRngLength].subjects[subjectRngLength]
          .lessons[lessonRngLength];

      randomLesson
        ..name = "${randomLesson.name ?? ""} (manipulated)"
        ..method = "FUN"
        ..comment = "${randomLesson.comment ?? ""} (manipulated)";
    }
    _addOProgram(context, program);
  }

  void _deleteRandomLessons(BuildContext context, OProgram remoteProgram) {
    var program = OProgram.fromJson(remoteProgram.toJson());
    for (var j = 0; j < 1; j++) {
      var poolRngLength = Random().nextInt(program.pools.length);
      var subjectRngLength =
          Random().nextInt(program.pools[poolRngLength].subjects.length);
      var lessonRngLength = Random().nextInt(program
          .pools[poolRngLength].subjects[subjectRngLength].lessons.length);
      program.pools[poolRngLength].subjects[subjectRngLength].lessons
          .removeAt(lessonRngLength);
    }
    _addOProgram(context, program);
  }

  void _addRandomPool(BuildContext context, OProgram remoteProgram) {
    var program = OProgram.fromJson(remoteProgram.toJson());
    var poolRngLength = Random().nextInt(program.pools.length);

    program.pools.add(
      OPool(
        id: program.pools[poolRngLength].id + Random().nextInt(200),
        name: "${program.pools[poolRngLength].name}copy",
        subjects: program.pools[poolRngLength].subjects,
      ),
    );
    _addOProgram(context, program);
  }

  void _addOProgram(BuildContext context, OProgram remoteProgram) {
    var program = OProgram.fromJson(remoteProgram.toJson());
    OUtil.addOProgram(program);
  }

  void _reloadProgramClean(BuildContext context, OProgram program) {
    OProgramFunctions.fillEntireProgramWithPoolsSubjectsLessonsUpdated(
            program.id,
            program.name,
            RepositoryProvider.of<HttpRepository>(context))
        .then((content) {
      if (content == null) {
        return;
      }

      _addOProgram(context, content);
    }).catchError((e) {
      debugPrint(e.runtimeType.toString());
    });
  }

  void _transferProgramToHistory(BuildContext context, OProgram program) {
    _addOProgram(context, program);
  }
}

class OChangeList extends StatelessWidget {
  const OChangeList({Key? key, required this.changeMap}) : super(key: key);
  final Map<String, OChangeRecord> changeMap;
  @override
  Widget build(BuildContext context) {
    // TODO: Optimize performance
    var listAdded = changeMap.entries
        .where((element) => element.value.delta.contains(ODelta.added));
    var listRemoved = changeMap.entries
        .where((element) => element.value.delta.contains(ODelta.removed));
    var listNameChanged = changeMap.entries
        .where((element) => element.value.delta.contains(ODelta.nameChanged));
    var listDescriptionChanged = changeMap.entries.where(
        (element) => element.value.delta.contains(ODelta.descriptionChanged));
    var listIncreased = changeMap.entries.where((element) =>
        element.value.delta.contains(ODelta.amountSubtreeIncreased));
    var listDecreased = changeMap.entries.where((element) =>
        element.value.delta.contains(ODelta.amountSubtreeDecreased));
    return Column(children: [
      buildListSegment(listAdded, ODelta.added, context),
      buildListSegment(listRemoved, ODelta.removed, context),
      buildListSegment(listNameChanged, ODelta.nameChanged, context),
      buildListSegment(
          listDescriptionChanged, ODelta.descriptionChanged, context),
      buildListSegment(listIncreased, ODelta.amountSubtreeIncreased, context),
      buildListSegment(listDecreased, ODelta.amountSubtreeDecreased, context),
    ]);
  }

  Widget buildListSegment(Iterable<MapEntry<String, OChangeRecord>> list,
      ODelta type, BuildContext context) {
    if (list.isEmpty) return Container();
    return Column(children: [
      Padding(
        padding: const EdgeInsets.only(bottom: 12.0, top: 32.0),
        child: segmentHeadline(type, context),
      ),
      ...list.map((entry) => OChangeListTile(entry, type)).toList()
    ]);
  }

  Widget segmentHeadline(ODelta type, BuildContext context) {
    var style = Theme.of(context).textTheme.headline3?.copyWith(
          color: CorporateColors.cafeteriaCautionRed,
        );
    var headlineText = "";
    switch (type) {
      case ODelta.added:
        headlineText = FlutterI18n.translate(
            context, 'modules.organizer.ui.changes.delta_added');
        break;
      case ODelta.nameChanged:
        headlineText = FlutterI18n.translate(
            context, 'modules.organizer.ui.changes.delta_name_changed');
        break;
      case ODelta.amountSubtreeIncreased:
        headlineText = FlutterI18n.translate(context,
            'modules.organizer.ui.changes.delta_amount_subtree_increased');
        break;
      case ODelta.amountSubtreeDecreased:
        headlineText = FlutterI18n.translate(context,
            'modules.organizer.ui.changes.delta_amount_subtree_decreased');
        break;
      case ODelta.removed:
        headlineText = FlutterI18n.translate(
            context, 'modules.organizer.ui.changes.delta_removed');
        break;
      case ODelta.descriptionChanged:
        headlineText = FlutterI18n.translate(
            context, 'modules.organizer.ui.changes.delta_description_changed');
        break;
    }
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 24.0),
      child: Text(headlineText.toUpperCase(),
          textAlign: TextAlign.center, style: style),
    );
  }
}

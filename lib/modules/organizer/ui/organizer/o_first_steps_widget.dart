/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../common/constants/routing_constants.dart';
import '../../../../common/tc_theme.dart';
import '../../blocs/o_drawer_bloc/o_drawer_bloc.dart';
import '../../blocs/o_loading_bloc/o_loading_bloc.dart';
import '../../blocs/o_program_configuration_bloc/o_program_configuration_bloc.dart';
import '../../utility/organizer_data_loader.dart';
import 'o_utility_functions.dart';
import 'organizer_constants.dart';

class OFirstStepsWidget extends StatelessWidget {
  const OFirstStepsWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) =>
      BlocBuilder<OProgramConfigurationBloc, OProgramConfigurationState>(
        builder: (context, programState) {
          var buttonText = "";
          var buttonSubText = "";
          late VoidCallback onPressedFunction;
          if (programState is OProgramConfigurationEmptyState) {
            buttonText = FlutterI18n.translate(
                context, '$i18nOPKey.search_study_course');
            buttonSubText =
                FlutterI18n.translate(context, '$i18nOPKey.beta_disclaimer');
            onPressedFunction = () {
              OUtil.addODrawerEvent(ODeselectEvent());
              Navigator.pushNamed(context, orgaConfigurationRoute)
                  .whenComplete(ODataLoader.loadData);
              Navigator.pushNamed(context, orgaSelectProgramRoute)
                  .whenComplete(() {
                final programBloc =
                    BlocProvider.of<OProgramConfigurationBloc>(context);
                final programState = programBloc.state;
                if (programState is OProgramConfigurationEmptyState) {
                  Navigator.pop(context);
                }
              });
            };
          } else if (programState is OProgramConfigurationReadyState) {
            //TODO: i know, redundancy detected, fix later
            if (programState.programs.isNotEmpty) {
              if (programState.programs.isEmpty) {
                buttonText = FlutterI18n.translate(
                    context, '$i18nOPKey.search_study_course');
                buttonSubText = FlutterI18n.translate(
                    context, '$i18nOPKey.beta_disclaimer');
                onPressedFunction = () {
                  OUtil.deselectDrawerEventIfNotInitial();
                  Navigator.pushNamed(context, orgaConfigurationRoute)
                      .whenComplete(ODataLoader.loadData);
                  Navigator.pushNamed(context, orgaSelectProgramRoute)
                      .whenComplete(() {
                    final programBloc =
                        BlocProvider.of<OProgramConfigurationBloc>(context);
                    final programState = programBloc.state;
                    if (programState is OProgramConfigurationEmptyState) {
                      Navigator.pop(context);
                    }
                  });
                };
              } else {
                buttonText =
                    FlutterI18n.translate(context, '$i18nOPKey.choose_modules');
                buttonSubText = FlutterI18n.translate(
                    context, '$i18nOPKey.beta_disclaimer');
                onPressedFunction = () {
                  OUtil.deselectDrawerEventIfNotInitial();
                  Navigator.pushNamed(context, orgaConfigurationRoute)
                      .whenComplete(ODataLoader.loadData);
                };
              }
            }
          } else {
            buttonText = "Neu Initialisieren";
            buttonText = "Fehler";

            onPressedFunction = OUtil.resetEntireOrganizer;
          }
          if (programState is OProgramConfigurationReadyState) {
            if (programState.programs.isEmpty) {
              buttonText = "Neu Initialisieren";
              buttonSubText = "Fehler";
              onPressedFunction = () {
                debugPrint("resetting");
                OUtil.resetEntireOrganizer();
              };
            }
          }
          return OrganizerFirstStepsWidget(
              onPressedFunction: onPressedFunction,
              buttonText: buttonText,
              buttonSubText: buttonSubText);
        },
      );
}

class OrganizerFirstStepsWidget extends StatelessWidget {
  const OrganizerFirstStepsWidget({
    Key? key,
    required this.onPressedFunction,
    required this.buttonText,
    required this.buttonSubText,
  }) : super(key: key);

  final VoidCallback onPressedFunction;
  final String buttonText;
  final String buttonSubText;

  @override
  Widget build(BuildContext context) =>
      BlocBuilder<OLoadingBloc, OLoadingState>(
        builder: (context, state) {
          if (state is! ONothingToLoadState) {
            return buildLoadingIndicator();
          } else if (state is OLoadMultipleState) {
            return CircularProgressIndicator();
          } else {
            return buildButton();
          }
        },
      );

  Center buildLoadingIndicator() => Center(
        child: I18nText(
          'common.messages.loading',
          child: Text(
            "",
            style: TextStyle(
              color: CorporateColors.tinyCampusIconGrey.withOpacity(0.5),
            ),
          ),
        ),
      );

  Center buildButton() => Center(
        child: RaisedButton(
          elevation: 4.0,
          color: Colors.white,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8.0),
          ),
          onPressed: onPressedFunction,
          child: SizedBox(
            width: 200,
            height: 300,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text(
                    buttonText,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 26,
                      letterSpacing: -1.1,
                      color: CorporateColors.tinyCampusBlue,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                  Container(height: 6),
                  SizedBox(
                    width: 150,
                    child: Text(
                      buttonSubText,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 16.0,
                        color: CorporateColors.tinyCampusOrange,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      );
}

/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';

import '../../../../common/tc_theme.dart';
import '../../utility/organizer_data_loader.dart';
import 'organizer_constants.dart';

class OrganizerReloadWidget extends StatelessWidget {
  const OrganizerReloadWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Center(
          child: Card(
        child: FlatButton(
          onPressed: () {
            ODataLoader.loadData(forceReload: false, onlyCurrentWeek: false);
          },
          child: SizedBox(
            width: 170,
            height: 110,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    I18nText(
                      '$i18nOKey.error_messages.connection_error',
                      child: Text(
                        "Verbindungsfehler",
                        textAlign: TextAlign.center,
                        style: TextStyle(fontWeight: FontWeight.w400),
                      ),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: I18nText(
                    'common.actions.reload',
                    child: Text(
                      "Neu laden",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontWeight: FontWeight.w400,
                          fontSize: 20,
                          color: CorporateColors.tinyCampusOrange),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ));
}

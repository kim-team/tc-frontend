/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../../../common/tc_theme.dart';
import '../../blocs/o_animation_bloc/o_animation_bloc.dart';
import '../../blocs/o_color_mapping_bloc/o_color_mapping_bloc.dart';
import '../../blocs/o_drawer_bloc/o_drawer_bloc.dart';
import '../../blocs/o_message_bloc/o_message_bloc.dart';
import '../../model/o_element_wrapper.dart';
import 'o_utility_functions.dart';

class OCalendarEntry extends StatefulWidget {
  const OCalendarEntry({
    Key? key,
    required this.eW,
    required this.maxHeight,
    required this.oElementColor,
    required this.isToday,
    this.directSelected = false,
    required this.width,
    required this.top,
    required this.height,
    this.selectedSubject,
    this.determineOpacity = 1.0, // TODO NNBD MIGRATION
  }) : super(key: key);

  final OElementWrapper eW;
  final double maxHeight;
  final Color oElementColor;
  final double width;
  final double top;
  final double height;
  final bool isToday;
  final bool directSelected;
  final double determineOpacity;
  final int? selectedSubject;

  @override
  _OCalendarEntryState createState() => _OCalendarEntryState();
}

// bool selected;  // TODO NNBD MIGRATION

class _OCalendarEntryState extends State<OCalendarEntry>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;
  late CurvedAnimation curve;
  late StagAnimation stagAnimation;
  String teachersString = ""; // TODO NNBD MIGRATION
  String roomsString = ""; // TODO NNBD MIGRATION
  final ValueNotifier<double> determineOpacity = ValueNotifier<double>(1.0);
  final ValueNotifier<bool> willBeDeleted = ValueNotifier<bool>(false);
  final ValueNotifier<bool> isShaking = ValueNotifier<bool>(false);
  final ValueNotifier<bool> directlySelected = ValueNotifier<bool>(false);
  final ValueNotifier<bool> subjectSelected = ValueNotifier<bool>(false);
  final ValueNotifier<bool> nothingSelected = ValueNotifier<bool>(true);

  @override
  void initState() {
    super.initState();
    initAnimationController();
    initAnimation();
    teachersString = OUtil.concatStringsWithSeparator(widget.eW.ele.resource);
    roomsString = OUtil.concatRoomStrings(widget.eW.ele.resource);
    checkIfDeleted();
  }

  void checkIfDeleted() {
    if (OUtil.isDeleted(widget.eW)) {
      instantShrinking();
    }
  }

  void initAnimation() {
    curve = CurvedAnimation(parent: _controller, curve: Curves.elasticIn);
    stagAnimation = StagAnimation(controller: _controller);
    _controller.addStatusListener(cyclingAnimationListener);
  }

  void initAnimationController() {
    _controller = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 600),
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  void selectElement(OElementWrapper eW) {
    final drawerBloc = BlocProvider.of<ODrawerBloc>(context);
    var state = drawerBloc.state;
    if (state is ODrawerInitial) {
      drawerBloc.add(OSelectEvent(oElementWrapper: eW));
    } else if (state is ODrawerColorState) {
      drawerBloc.add(OColorEvent(oElementWrapper: eW));
    } else {
      if (state.isDirectlySelected(eW)) {
        drawerBloc.add(ODeselectEvent(oElementWrapper: eW));
        OUtil.oAnimationBloc
            .add(OSetAnimationStateEvent(OStopAllShakingAnimationState()));
      } else {
        drawerBloc.add(OSelectEvent(oElementWrapper: eW));
        OUtil.oAnimationBloc
            .add(OSetAnimationStateEvent(OStopAllShakingAnimationState()));
      }
    }
  }

  void selectElementLongpress(OElementWrapper eW) {
    OUtil.addODrawerEvent(ODeselectEvent(oElementWrapper: eW));
    Timer(Duration(milliseconds: 150), () {
      if (mounted) {
        OUtil.addODrawerEvent(ODeleteEvent(oElementWrapper: eW));
      }
    });
    OUtil.oAnimationBloc
        .add(OSetAnimationStateEvent(OStopAllShakingAnimationState()));
    OUtil.addOAnimationEvent(OSetAnimationStateEvent(
        OStartSingleShakingAnimationState(oElementId: eW.ele.id ?? -1)));
  }

  void startShaking() {
    isShaking.value = true;
    removeAnimationListener();
    curve = CurvedAnimation(parent: _controller, curve: Curves.easeIn);
    _controller
      ..addStatusListener(cyclingAnimationListener)
      ..animateTo(0.001, duration: Duration(milliseconds: 200))
          .whenComplete(() => _controller.forward());
  }

  void startShrinking() {
    willBeDeleted.value = true;
    removeAnimationListener();
    _controller.animateTo(0.0, duration: Duration(milliseconds: 0));
    stagAnimation =
        StagAnimation(controller: _controller, beginScale: 1.0, endScale: 0.0);
    _controller.animateTo(1.0, duration: Duration(milliseconds: 600));
  }

  void instantShrinking() {
    willBeDeleted.value = true;
    removeAnimationListener();
    stagAnimation =
        StagAnimation(controller: _controller, beginScale: 1.0, endScale: 0.0);
    _controller.animateTo(1.0, duration: Duration(milliseconds: 0));
    _controller.forward();
  }

  void removeAnimationListener() {
    _controller.removeStatusListener(cyclingAnimationListener);
    _controller.removeListener(() {});
  }

  void cyclingAnimationListener(AnimationStatus status) {
    if (status == AnimationStatus.completed) {
      _controller.reverse();
    } else if (status == AnimationStatus.dismissed) {
      _controller.forward();
    }
  }

  bool checkIfReferenced(List<int> lessons) {
    if (lessons.contains(widget.eW.ele.lessonId)) {
      return true;
    }
    return false;
  }

  void stopShaking() {
    if (willBeDeleted.value == true) {
      return;
    }
    isShaking.value = false;
    removeAnimationListener();
    _controller.animateTo(0, duration: Duration(milliseconds: 900));
  }

  void revertShrinking() {
    isShaking.value = false;
    willBeDeleted.value = false;
    initAnimation();
    _controller.animateTo(0, duration: Duration(milliseconds: 600));
    removeAnimationListener();
  }

  @override
  Widget build(BuildContext context) => BlocListener<ODrawerBloc, ODrawerState>(
        listener: (context, state) {
          directlySelected.value = state.isDirectlySelected(widget.eW);
          subjectSelected.value = state.isSelectedBySubject(widget.eW);
          nothingSelected.value = state.nothingSelected();

          if (directlySelected.value) {
            determineOpacity.value = 1.0;
          } else if (subjectSelected.value) {
            determineOpacity.value = 1.0;
          } else if (nothingSelected.value) {
            determineOpacity.value = 1.0;
          } else if (!subjectSelected.value) {
            determineOpacity.value = 0.0;
          }
          // setState(() {});
        },
        child: BlocBuilder<OColorMappingBloc, OColorMappingState>(
          builder: (context, colorState) {
            Color color;
            if (colorState is OColorMappingReadyState) {
              color = colorState.getColor(subjectId: widget.eW.ele.subjectId);
            } else {
              color = Colors.grey;
            }
            final messageBloc = BlocProvider.of<OMessageBloc>(context);
            final animationBloc = BlocProvider.of<OAnimationBloc>(context);
            // if (messageState is OLessonsChangedState) {
            //   _controller.forward();
            // }

            return BlocListener<OAnimationBloc, OAnimationState>(
              bloc: animationBloc,
              listener: (context, animationState) {
                if (animationState is OStartShakingAnimationState) {
                  if (animationState.lessons.contains(widget.eW.ele.lessonId)) {
                    //switch animation to shaking
                    if (checkIfReferenced(animationState.lessons)) {
                      startShaking();
                    }
                  }
                }
                if (animationState is OStartSingleShakingAnimationState) {
                  if (animationState.oElementId == widget.eW.ele.id) {
                    //switch animation to shaking
                    startShaking();
                  }
                }
                if (animationState is ODeleteAnimationState) {
                  // if (isShaking.value) {
                  if (checkIfReferenced(animationState.lessons)) {
                    startShrinking();
                  }
                  // }
                }
                if (animationState is ODeleteSingleAnimationState) {
                  if (animationState.oElementId == widget.eW.ele.id) {
                    //switch animation to shaking
                    startShrinking();
                  }
                  // }
                }
                if (animationState is ORevertDeleteAnimationState) {
                  if (checkIfReferenced(animationState.lessons)) {
                    if (!OUtil.isDeleted(widget.eW)) {
                      revertShrinking();
                    }
                  }
                }
                if (animationState
                    is ORevertDeleteMultiOElementsAnimationState) {
                  if (animationState.oElements.contains(widget.eW.ele.id)) {
                    revertShrinking();
                  }
                }
                if (animationState is ORevertDeleteSingleAnimationState) {
                  if (animationState.oElementId == widget.eW.ele.id) {
                    revertShrinking();
                  }
                }
                if (animationState is OStopAllShakingAnimationState) {
                  if (isShaking.value) {
                    stopShaking();
                  }
                }
              },
              child: BlocListener<OMessageBloc, OMessageState>(
                bloc: messageBloc,
                listener: (context, messageState) {
                  if (messageState is OLessonsChangedState) {
                    if (messageState.lessons.contains(widget.eW.ele.lessonId)) {
                      //switch animation to shrinking
                      // startShrinking();
                    }
                  }
                  if (messageState is OResolvedMessage) {
                    // willBeDeleted.value = false;
                    // _controller.stop();
                    // stopShaking();
                    // revertShrinking();
                  }
                  if (messageState is OMessageInitial) {
                    // stopShaking();
                    // revertShrinking();
                  }
                },
                child: GestureDetector(
                  onTap: () => selectElement(widget.eW),
                  onLongPress: () => selectElementLongpress(widget.eW),
                  child: AnimatedSwitcher(
                    duration: Duration(milliseconds: 500),
                    child: !willBeDeleted.value
                        ? AnimatedBuilder(
                            animation: _controller,
                            builder: (context, child) => Transform.translate(
                              offset: Offset(
                                0,
                                isShaking.value
                                    ? sin(stagAnimation.yTranslate.value) * 2
                                    : 0,
                              ),
                              child: Transform.scale(
                                scale: stagAnimation.scale.value,
                                child: Transform.rotate(
                                    angle: !willBeDeleted.value
                                        ? sin(stagAnimation.rotation.value *
                                                pi *
                                                2 *
                                                6) /
                                            80
                                        : 0.0,
                                    child: child),
                              ),
                            ),
                            child: EntryBodyContentWidget(
                              widget: widget,
                              color: color,
                              directlySelected: directlySelected,
                              determineOpacity: determineOpacity,
                              isShaking: isShaking,
                              teachersString: teachersString,
                              roomsString: roomsString,
                            ),
                          )
                        : Container(),
                  ),
                ),
              ),
            );
          },
        ),
      );
}

class EntryBodyContentWidget extends StatelessWidget {
  const EntryBodyContentWidget({
    Key? key,
    required this.widget,
    required this.color,
    required this.directlySelected,
    required this.determineOpacity,
    required this.isShaking,
    required this.teachersString,
    required this.roomsString,
  }) : super(key: key);

  final OCalendarEntry widget;
  final Color color;
  final ValueNotifier<bool> directlySelected;
  final ValueNotifier<double> determineOpacity;
  final ValueNotifier<bool> isShaking;
  final String teachersString;
  final String roomsString;

  @override
  Widget build(BuildContext context) => ValueListenableBuilder(
        valueListenable: isShaking,
        builder: (context, value, child) => Stack(
          clipBehavior: Clip.none,
          children: <Widget>[
            ValueListenableBuilder(
              valueListenable: directlySelected,
              builder: (context, value, child) => AnimatedContainer(
                duration: Duration(milliseconds: 260),
                curve: Curves.easeOut,
                // alignment: FractionalOffset.center,
                // transform: (directlySelected.value
                //     ? (Matrix4.identity()
                //       ..translate(0.025 * 2,
                //           0.025 * 5) // translate towards right and down
                //       ..scale(
                //           0.95 * determineOpacity.value,
                //           0.95 *
                //               determineOpacity
                //                   .value)) // scale with to 95% anchorred at topleft of the Container
                //     : (Matrix4.identity()
                //       ..translate(0.025 * 2,
                //           0.025 * 5) // translate towards right and down
                //       ..scale(0.65 * determineOpacity.value,
                //           0.65 * determineOpacity.value))),
                height: widget.height,
                width: widget.width,
                decoration: BoxDecoration(
                  color: color.withAlpha(
                      CurrentTheme().theme == TCThemes.light ? 120 : 180),
                  borderRadius: BorderRadius.all(Radius.circular(4.0)),
                  border: Border.all(
                      width:
                          directlySelected.value || isShaking.value ? 2.0 : 0.0,
                      color: directlySelected.value || isShaking.value
                          ? color
                          : Colors.transparent,
                      style: BorderStyle.solid),
                ),
                child: ValueListenableBuilder(
                  valueListenable: directlySelected,
                  builder: (context, value, child) => Stack(
                    alignment: Alignment.center,
                    children: [
                      AnimatedPositioned(
                          duration: Duration(milliseconds: 360),
                          curve: Curves.easeOut,
                          top: directlySelected.value || isShaking.value
                              ? -widget.height / 3
                              : -widget.height * 4 + (widget.height / 2) - 10,
                          width: directlySelected.value || isShaking.value
                              ? widget.height * 2
                              : widget.height * 4,
                          height: directlySelected.value || isShaking.value
                              ? widget.height * 2
                              : widget.height * 4,
                          child: Container(
                            //  duration: Duration(
                            //     milliseconds: directlySelected
                            // ? 280 : 620),
                            decoration: BoxDecoration(
                              color: Theme.of(context).primaryColor.withAlpha(
                                  directlySelected.value || isShaking.value
                                      ? 50
                                      : 120),
                              shape: BoxShape.circle,
                            ),
                          )),
                      AnimatedPositioned(
                          duration: Duration(milliseconds: 180),
                          curve: Curves.easeInOut,
                          top: directlySelected.value || isShaking.value
                              ? -widget.height / 3
                              : -4,
                          width: directlySelected.value || isShaking.value
                              ? widget.height * 2
                              : 8,
                          height: directlySelected.value || isShaking.value
                              ? widget.height * 2
                              : 8,
                          child: Container(
                            // duration: Duration(
                            //     milliseconds:
                            // directlySelected ? 280 : 620),
                            decoration: BoxDecoration(
                              color: color.withAlpha(
                                  directlySelected.value || isShaking.value
                                      ? 40
                                      : 130),
                              shape: BoxShape.circle,
                            ),
                          )),
                      Container(
                        // color: Colors.white54,
                        child: (InnerContentCalendarEntry(
                            widget: widget,
                            teachersString: teachersString,
                            roomsString: roomsString)),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Positioned(
              top: 0,
              right: 0,
              left: 0,
              bottom: 0,
              child: AnimatedBuilder(
                animation: determineOpacity,
                builder: (context, child) => Transform.scale(
                  scale: isShaking.value ? 0 : 1 - determineOpacity.value,
                  child: Container(
                      color: Theme.of(context).primaryColor.withOpacity(0.7)),
                ),
              ),
            ),

            /// TODO: find out if users expect an [icon] similar to below
            // isShaking.value
            //     ? Positioned(
            //         top: -12,
            //         left: 0,
            //         right: 0,
            //         child: Container(
            //           width: 24,
            //           height: 24,
            //           decoration: BoxDecoration(
            //             color: Colors.white,
            //             // borderRadius: BorderRadius.circular(10),
            //             border: Border.all(
            //                 color: CorporateColors.tinyCampusIconGrey),
            //             shape: BoxShape.circle,
            //           ),
            //           child: Icon(
            //             Icons.delete,
            //             color: CorporateColors.tinyCampusIconGrey,
            //             size: 14.0,
            //           ),
            //         ))
            //     : Container(),
          ],
        ),
      );
}

class InnerContentCalendarEntry extends StatelessWidget {
  const InnerContentCalendarEntry({
    Key? key,
    required this.widget,
    required this.teachersString,
    required this.roomsString,
  }) : super(key: key);

  final OCalendarEntry widget;
  final String teachersString;
  final String roomsString;

  @override
  Widget build(BuildContext context) => Container(
        margin: EdgeInsets.only(top: 2),
        child: Wrap(
          direction: Axis.vertical,
          runAlignment: WrapAlignment.center,
          runSpacing: 0.0,
          alignment: WrapAlignment.start,
          crossAxisAlignment: WrapCrossAlignment.center,
          children: <Widget>[
            Container(
              width: widget.width - 4.0,
              margin: EdgeInsets.only(bottom: 8.0),
              // color: Colors.white54,
              child: Text(widget.eW.ele.abbr ?? "",
                  textAlign: TextAlign.center,
                  overflow: TextOverflow.clip,
                  maxLines: 1,
                  // minFontSize: 14.0,
                  // maxFontSize: 24.0,
                  style: TextStyle(
                      // color: Colors.black87,
                      fontSize: 14.0,
                      letterSpacing: -0.6,
                      fontWeight: FontWeight.w500)),
            ),
            SizedBox(
              width: widget.width - 4.0,
              child: Text(
                  ((widget.eW.ele.type) == "?"
                      ? FlutterI18n.translate(
                          context, 'modules.organizer.data.unknown_short')
                      : widget.eW.ele.type),
                  overflow: TextOverflow.clip,
                  maxLines: 1,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 11.0,
                      fontWeight: FontWeight.w400,
                      color: CurrentTheme().textSoftWhite,
                      letterSpacing: -0.4)),
            ),
            SizedBox(
              width: widget.width - 4.0,
              child: Text(teachersString,
                  //  "hi",
                  //  widget.eW.ele.teacher,
                  overflow: TextOverflow.clip,
                  maxLines: 1,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 11.0,
                      fontWeight: FontWeight.w400,
                      color: CurrentTheme().textSoftWhite,
                      letterSpacing: -0.8)),
            ),
            SizedBox(
              width: widget.width - 4.0,
              child: widget.eW.ele.comment != ""
                  ? Container(
                      margin: EdgeInsets.all(0.0),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8.0),
                        color: Theme.of(context).primaryColor,
                      ),
                      child: Text(widget.eW.ele.comment ?? "",
                          overflow: TextOverflow.ellipsis,
                          maxLines: 1,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 10.0,
                              fontWeight: FontWeight.w400,
                              letterSpacing: -0.8,
                              color: CorporateColors.tinyCampusOrange)),
                    )
                  : Text(roomsString,
                      overflow: TextOverflow.clip,
                      maxLines: 1,
                      textAlign: TextAlign.center,
                      // minFontSize: 9.0,
                      style: TextStyle(
                          fontSize: 10.0,
                          fontWeight: FontWeight.w400,
                          color: CurrentTheme().textSoftWhite,
                          letterSpacing: -0.4)),
            ),
          ],
        ),
      );
}

class StagAnimation {
  StagAnimation(
      {required this.controller,
      double beginRotation = 1,
      double endRotation = 1.10,
      double beginScale = 1.0,
      double endScale = 0.98})
      : rotation =
            Tween<double>(begin: beginRotation, end: endRotation).animate(
          CurvedAnimation(
            parent: controller,
            curve: Interval(0.0, 1.0, curve: Curves.linear),
          ),
        ),
        scale = Tween<double>(begin: beginScale, end: endScale).animate(
          CurvedAnimation(
            parent: controller,
            curve: Interval(0.3, 0.8, curve: Curves.easeIn),
          ),
        ),
        yTranslate = Tween<double>(begin: 0.0, end: 2 * pi).animate(
          CurvedAnimation(
            parent: controller,
            curve: Interval(0.3, 0.8, curve: Curves.linear),
          ),
        );

  final AnimationController controller;
  final Animation<double> rotation;
  final Animation<double> scale;
  final Animation<double> yTranslate;
}

/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../common/tc_theme.dart';
import '../../blocs/o_animation_bloc/o_animation_bloc.dart';
import '../../blocs/o_calendar_bloc/o_calendar_bloc.dart';
import '../../blocs/o_drawer_bloc/o_drawer_bloc.dart';
import '../../blocs/o_lesson_selection_bloc/o_lesson_selection_bloc.dart';
import '../../blocs/o_message_bloc/o_message_bloc.dart';
import '../../blocs/o_settings_bloc/o_settings_bloc.dart';
import '../../blocs/o_view_mode_bloc/o_view_mode_bloc.dart';
import '../../utility/organizer_data_loader.dart';
import 'o_drawer/bottom_sheet_drawer.dart';
import 'o_drawer/o_drawer_widget.dart';
import 'o_first_steps_widget.dart';
import 'o_utility_functions.dart';
import 'organizer_app_bar.dart';
import 'organizer_constants.dart';
import 'organizer_week_widget.dart';

class OPageView extends StatefulWidget {
  final PageController pageController = PageController(
      viewportFraction: 1.0,
      keepPage: false,
      initialPage: OUtil.absoluteWeekSince1970(DateTime.now()));
  OPageView({
    Key? key,
  }) : super(key: key);

  @override
  _OPageViewState createState() => _OPageViewState();
}

class _OPageViewState extends State<OPageView>
    with SingleTickerProviderStateMixin {
  final _scaffoldMessengerKey = GlobalKey<ScaffoldMessengerState>();

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
  }

  Widget _buildWeekWidget() => Column(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: _buildTransformableBody(),
          ),
        ],
      );
  void setPage(int page) {
    if (widget.pageController.hasClients) {
      try {
        widget.pageController.animateToPage(page,
            duration: Duration(milliseconds: 360), curve: Curves.easeOut);
      } on Exception catch (e) {
        debugPrint("Error setting the page: $e");
      }
    }
  }

  void goToInitialPageSafely() {
    if (widget.pageController.hasClients) {
      widget.pageController.animateToPage(widget.pageController.initialPage,
          duration: Duration(milliseconds: 360), curve: Curves.easeOut);
    }
  }

  void goToPreviousPage() {
    if (widget.pageController.hasClients) {
      widget.pageController.previousPage(
          duration: Duration(milliseconds: 360), curve: Curves.easeOut);
    }
  }

  void goToNextPage() {
    if (widget.pageController.hasClients) {
      widget.pageController.nextPage(
          duration: Duration(milliseconds: 360), curve: Curves.easeOut);
    }
  }

  @override
  Widget build(BuildContext context) {
    final dataBloc = BlocProvider.of<OMessageBloc>(context);
    return WillPopScope(
      onWillPop: () async {
        OUtil.stopAllShakingAnimationIfNotAlready();
        final viewMode = BlocProvider.of<OViewModeBloc>(context);
        if (viewMode.state is ODailyViewState) {
          viewMode.add(OWeeklyViewEvent());
        } else if (viewMode.state is OWeeklyViewState) {
          final drawer = BlocProvider.of<ODrawerBloc>(context);
          if (drawer.state is! ODrawerInitial) {
            if (drawer.state is ODrawerColorState) {
              drawer.add(
                  OSelectEvent(oElementWrapper: drawer.state.oElementWrapper));
            } else if (drawer.state is ODrawerDeleteState) {
              drawer.add(
                  OSelectEvent(oElementWrapper: drawer.state.oElementWrapper));
            } else {
              drawer.add(ODeselectEvent());
            }
          } else {
            Navigator.pop(context);
          }
        }
        return true; // TODO NNBD MIGRATION
      },
      child: ScaffoldMessenger(
        key: _scaffoldMessengerKey,
        child: Scaffold(
          backgroundColor: Theme.of(context).backgroundColor,
          appBar: OAppBar(
            pageController: widget.pageController,
          ),
          body: BlocListener<OAnimationBloc, OAnimationState>(
            listener: (context, state) {
              if (state is OPreviousPageState) {
                goToPreviousPage();
              }
              if (state is ONextPageState) {
                goToNextPage();
              }
              if (state is OSetPageState) {
                setPage(state.setPageValue);
              }
              if (state is OInitialPage) {
                goToInitialPageSafely();
              }
            },
            child: BlocListener<OViewModeBloc, OViewModeState>(
              listener: (context, state) {
                if (state is ODailyViewState) {
                  hideSnackbarSafely();
                }
              },
              child: BlocListener(
                  bloc: dataBloc,
                  listener: (context, messageState) {
                    hideSnackbarSafely();
                    if (messageState is OErrorState) {
                      ScaffoldMessenger.of(context).showSnackBar(
                        SnackBar(
                          behavior: SnackBarBehavior.floating,
                          backgroundColor: CorporateColors.snackbarBackground,
                          content:
                              I18nText('$i18nOKey.error_messages.try_again'),
                          action: SnackBarAction(
                            label: FlutterI18n.translate(
                                context, '$i18nOKey.load_caps'),
                            textColor: CorporateColors.tinyCampusOrange,
                            onPressed: () {
                              ODataLoader.loadData(
                                  forceReload: false, onlyCurrentWeek: false);
                            },
                          ),
                        ),
                      );
                    } else if (messageState is OLessonsChangedState) {
                      ScaffoldMessenger.of(context).showSnackBar(
                        SnackBar(
                          behavior: SnackBarBehavior.fixed,
                          backgroundColor: CorporateColors.snackbarBackground,
                          content: I18nText('$i18nOKey.entries_removed'),
                          action: SnackBarAction(
                            label: FlutterI18n.translate(
                                context, '$i18nOKey.revert_caps'),
                            textColor: CorporateColors.tinyCampusOrange,
                            onPressed: () {
                              // add multiple lessons again
                              var previousLessons =
                                  OUtil.oLessonBloc.state.lessons;
                              OUtil.oLessonBloc.add(
                                AddMultipleOLessonsEvent(
                                  lessons: messageState.lessons,
                                  previousLessonList: previousLessons,
                                ),
                              );
                              OUtil.addOAnimationEvent(
                                OSetAnimationStateEvent(
                                  ORevertDeleteAnimationState(
                                    lessons: messageState.lessons,
                                  ),
                                ),
                              );
                              // OUtil.addOAnimationEvent(
                              //   ORevertDeleteAnimation(
                              //     lessons: messageState.lessons,
                              //   ),
                              // );
                              OUtil.addOMessageEvent(OResolvedMessage());
                            },
                          ),
                        ),
                      );
                    } else if (messageState is OElementChangedState) {
                      ScaffoldMessenger.of(context).showSnackBar(
                        SnackBar(
                          behavior: SnackBarBehavior.fixed,
                          backgroundColor: CorporateColors.snackbarBackground,
                          content: I18nText('$i18nOKey.entries_removed'),
                          action: SnackBarAction(
                            label: FlutterI18n.translate(
                                context, '$i18nOKey.revert_caps'),
                            textColor: CorporateColors.tinyCampusOrange,
                            onPressed: () {
                              final oAnimationBloc =
                                  BlocProvider.of<OAnimationBloc>(context);
                              oAnimationBloc.add(
                                OSetAnimationStateEvent(
                                  ORevertDeleteSingleAnimationState(
                                      oElementId: messageState.oElementId),
                                ),
                                // ORevertDeleteSingleAnimation(
                                //     oElementId: messageState.oElementId),
                              );
                              dataBloc.add(
                                OResolvedMessage(),
                              );
                              OUtil.addOSettingsEvent(
                                  ClearSingleOLesson(messageState.oElementId));
                            },
                          ),
                        ),
                      );
                    }
                  },
                  child: BlocListener<ODrawerBloc, ODrawerState>(
                    listener: (context, state) {
                      hideSnackbarSafely();
                    },
                    child: Container(child: _buildFirstSteps(context)),
                  )),
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildFirstSteps(BuildContext context) =>
      BlocBuilder<OCalendarBloc, OCalendarState>(
        builder: (context, state) {
          if (state is OCalendarEmptyState) {
            return OFirstStepsWidget();
          } else if (state is OCalendarReadyState) {
            return _buildOrganizerStack(context);
          }
          return OFirstStepsWidget();
        },
      );

  Widget _buildOrganizerStack(BuildContext context) => BottomSheetDrawer(
        front: _buildWeekBody(),
        drawer: ODrawerWidget(),
        pageController: widget.pageController,
      );

  Widget _buildWeekBody() => Column(
        children: <Widget>[
          Expanded(
            flex: 7,
            child: _buildWeekWidget(),
          ),
        ],
      );

  void hideSnackbarSafely() {
    try {
      _scaffoldMessengerKey.currentState?.hideCurrentSnackBar();
    } on Exception catch (e) {
      debugPrint("[ORGANIZER PAGE VIEW] Could not hide current Snackbar: $e");
    }
  }

  Widget _buildTransformableBody() => PageView.builder(
      controller: widget.pageController,
      onPageChanged: (i) {
        hideSnackbarSafely();
        OUtil.setCurrentAbsoluteWeek(i);
        OUtil.deselectDrawerEventIfNotInitial();
        OUtil.stopAllShakingAnimationIfNotAlready();
        OUtil.switchToWeeklyViewModeIfNotAlready();
        // TODO: Improve method. The delay is required, because the bloc
        // needs to adapt to the "setCurrentAbsoluteWeek" change, which
        // takes some microseconds.
        Timer(Duration(milliseconds: 90), () {
          if (mounted) {
            ODataLoader.loadData();
          }
        });
      },
      itemCount: 5000,
      scrollDirection: Axis.horizontal,
      itemBuilder: (context, position) => OrganizerWeekWidget(
            position: position,
          ));
}

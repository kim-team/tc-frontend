/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../../../common/tc_theme.dart';

const String _i18nOEKey = 'modules.organizer.ui.explanations';

class OMultipleModulesExplanationPage extends StatelessWidget {
  const OMultipleModulesExplanationPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: I18nText(
            'common.actions.back',
          ),
        ),
        backgroundColor: CorporateColors.passiveBackgroundLight,
        body: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Container(
            padding: EdgeInsets.symmetric(
              horizontal: 16.0,
              vertical: 12.0,
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Card(
                  elevation: 0.0,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12.0)),
                  child: Container(
                    padding: EdgeInsets.symmetric(
                      horizontal: 24.0,
                      vertical: 24.0,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        I18nText(
                          '$_i18nOEKey.multiple_shown',
                          child: Text(
                            "Warum werden bestimmte Module mehrfach angezeigt?",
                            style: Theme.of(context)
                                .textTheme
                                .headline2
                                ?.copyWith(fontWeight: FontWeight.w600),
                          ),
                        ),
                        Container(height: 16),
                        Text(
                          FlutterI18n.translate(
                            context,
                            '$_i18nOEKey.interpretation_tc',
                          ),
                          style: Theme.of(context)
                              .textTheme
                              .bodyText2
                              ?.copyWith(
                                  color: CorporateColors.tinyCampusOrange),
                        ),
                        Container(height: 4),
                        Text(
                          FlutterI18n.translate(
                            context,
                            'modules.organizer.ui.organizer.'
                            'page.beta_disclaimer',
                          ),
                          style: Theme.of(context)
                              .textTheme
                              .bodyText2
                              ?.copyWith(
                                  color: CorporateColors.tinyCampusOrange),
                        ),
                        Container(height: 16),
                        I18nText(
                          '$_i18nOEKey.multiple_semesters',
                        ),
                        Container(height: 8),
                        I18nText(
                          '$_i18nOEKey.directly_connected',
                        ),
                        Container(height: 8),
                        I18nText('$_i18nOEKey.multiple_explanation'),
                        Container(height: 8),
                      ],
                    ),
                  ),
                ),
                Container(
                  height: 24.0,
                ),
                Image.asset(
                  "assets/images/organizer/organizer_multi_modules_example.png",
                  fit: BoxFit.cover,
                ),
              ],
            ),
          ),
        ),
      );
}

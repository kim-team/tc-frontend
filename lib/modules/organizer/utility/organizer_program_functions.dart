/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:async';
import 'dart:convert';

import 'package:flutter/foundation.dart';

import '../../../common/constants/api_constants.dart';
import '../../../common/tc_utility_functions.dart';
import '../../../repositories/http/http_repository.dart';
import '../model/o_lesson.dart';
import '../model/o_pool.dart';
import '../model/o_program.dart';
import '../model/o_subject.dart';
import 'organizer_common_classes.dart';
import 'organizer_common_functions.dart';

abstract class OProgramFunctions {
  static List<OProgram> _cachedPrograms = <OProgram>[];
  static const bool debug = false;
  static const String organizerBaseURL =
      "$thmUrl/organizer/index.php?option=com_thm_organizer&view=schedule_ajax&format=raw";

  static Future<List<OProgram>?> getAllProgramsUpdated(
      HttpRepository _httpRepo) async {
    var url =
        "$thmUrl/organizer/?option=com_organizer&view=categoryOptions&format=json";
    debugPrint(url);
    if (_cachedPrograms.isNotEmpty) {
      return _cachedPrograms;
    }
    var programs = <OProgram>[];

    /// TODO: This should be the proper expression: [secured: !kReleaseMode]
    var response = await (_httpRepo.read(url));
    var json = await jsonDecode(response);
    debugPrint(json.runtimeType.toString());
    if (json is Map<String, dynamic>) {
      // broken, no usable data
      return null; // data not usable
    } else if (json is List<dynamic>) {
      // valid!
      // TODO: wait for assumedDegree (M.Sc/B.Sc. etc)
      var assumedDegree = "TODO";

      programs = buildEntitiesFromList(
        json,
        (e) => OProgram(
          name: e["text"],
          id: int.parse(e["value"]),
          assumedDegree: assumedDegree,
        ),
      );
    } else {
      return null; // data not usable
    }
    if (programs.isNotEmpty) {
      _cachedPrograms = programs;
    }

    return programs;
  }

  static Future<List<OSubject>> getPoolSubjects(
      int pool, HttpRepository _httpRepo) async {
    debugPrint("Requesting subjects from pool: $pool");
    var url = "$organizerBaseURL&task=getPoolSubjects&poolIDs=$pool";

    var subjects = <OSubject>[];
    try {
      var response =
          await jsonDecode(await (_httpRepo.read(url))) as Map<String, dynamic>;
      response.forEach((key, value) {
        var a = OSubject(name: key, id: int.parse(value));
        subjects.add(a);
        debugPrint("Key: $key ### subjectID: $value");
      });
    } on Exception catch (e) {
      debugPrint(e.toString());
    }
    return subjects;
  }

  static Future<OProgram?> fillEntireProgramWithPoolsSubjectsLessonsUpdated(
      int programId, String programName, HttpRepository _httpRepo) async {
    var program = OProgram();
    program.id = programId;
    program.name = programName;
    program.pools = <OPool>[];
    var url =
        "$thmUrl/organizer/?option=com_organizer&view=category_units&format=json&categoryID=${programId.toString()}";
    debugPrint(url);
    var response = await jsonDecode(await (_httpRepo.read(url)));
    // check if the request is valid
    if (response is Map<String, dynamic>) {
      // valid!
    } else if (response is List<dynamic>) {
      // broken, no usable data
      throw EmptyDataException("no data found");
      // return null;
    } else {
      return null; // data not usable
    }

    response.forEach((poolKey, value) {
      // go through pools (updated-terminology: groups)
      // go through subjects (updated-terminology: events)
      // go through lessons (updated-terminology: units)
      final poolFromJson = getPoolFromJson(poolKey, value);
      if (poolFromJson != null) {
        program.pools.add(poolFromJson);
      }
    });
    // return the filled program (updated-terminology: category)
    return program;
  }

  static OPool? getPoolFromJson(String poolKey, dynamic input) {
    if (input == null) {
      return null;
    } else {
      if (input is Map<String, dynamic>) {
        return OPool(
          id: OCommonFunctions.forceInt(poolKey),
          name: input["name"],
          subjects: getSubjectsFromJson(input["events"]),
        );
      }
      return OPool();
    }
  }

  static List<OSubject> getSubjectsFromJson(dynamic input) =>
      buildEntitiesFromMap(
        input,
        (e) => OSubject(
          id: OCommonFunctions.forceInt(e.key),
          name: e.value["name"],
          lessons: getLessonsFromJson(e.value["units"], e.value["name"]),
        ),
      );

  static List<OLesson> getLessonsFromJson(dynamic input, String name) =>
      buildEntitiesFromMap(
        input,
        (e) => OLesson(
          id: OCommonFunctions.forceInt(e.key),
          name: name,
          comment: e.value["comment"],
          method: e.value["method"] ?? "?",
        ),
      );
}

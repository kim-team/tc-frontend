/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';

import '../../../repositories/http/http_repository.dart';
import '../blocs/o_calendar_bloc/o_calendar_bloc.dart';
import '../blocs/o_lesson_selection_bloc/o_lesson_selection_bloc.dart';
import '../blocs/o_loading_bloc/o_loading_bloc.dart';
import '../blocs/o_message_bloc/o_message_bloc.dart';
import '../blocs/o_view_mode_bloc/o_view_mode_bloc.dart';
import '../model/o_element.dart';
import '../model/o_week.dart';
import '../ui/organizer/o_utility_functions.dart';
import '../ui/organizer/organizer_constants.dart';
import '../ui/organizer/organizer_enums.dart';
import 'organizer_instance_functions.dart';

abstract class ODataLoader {
  static bool spreadInitiallyLoaded = false;

  static Future<void> loadMultiple({int spread = 5}) async {
    // determine request date
    OUtil.addOLoadingEvent(OSetLoadingState(OLoadMultipleState()));
    var requestDate = DateTime.now().add(
      Duration(
        days: (7 * OUtil.getCurrentRelativeWeek()),
      ),
    );

    // caution! if you request data on sunday, no data will be sent
    if (requestDate.weekday == 7) {
      requestDate = requestDate.subtract(Duration(days: 1));
    }
    debugPrint("[LOAD MULTIPLE CALENDAR DATA] "
        "Request Date: >>> ${requestDate.toString()} <<<");
    var lessons = OUtil.getAllSelectedLessonIds();

    var dates = <DateTime>[];
    var requestWeeks = <int>[];

    /// [spread] is a named parameter
    /// shift the spread, because newer data is more relevant
    /// Load week -3 to +7 (if spread is 5)
    for (var i = -spread + 2; i <= spread + 2; i++) {
      var date = requestDate.add(Duration(days: i * 7));
      requestWeeks.add(OUtil.absoluteWeekSince1970(date));
      dates.add(date);
    }

    var futureList = <Future<List<OElement>>>[];

    // check if the current data needs an update for each week
    var reset = checkIfLessonListsAreUnequal();
    if (reset) {
      debugPrint("[LOAD MULTIPLE CALENDAR DATA] "
          "Resetting map because the lessonIds are unequal");
    }
    var map = getMap(reset: reset);
    var shouldUpdate = <bool>[];
    // check: lessons
    for (var i = 0; i < dates.length; i++) {
      if (map[requestWeeks[i]] == null || reset) {
        shouldUpdate.add(true);
      } else {
        shouldUpdate.add(false);
      }
    }
    // check: time

    // now download the data if not old and already present
    var skipFuture = <bool>[];
    for (var i = 0; i < dates.length; i++) {
      if (shouldReload(requestWeeks[i]) || reset) {
        debugPrint("[LOAD MULTIPLE CALENDAR DATA UPDATING] "
            "Should reload week ${requestWeeks[i]}, reset: $reset");
        var future = OrganizerInstanceFunctions.getOrganizerInstances(
            lessons, dates[i], OUtil.httpRepo);
        futureList.add(future);
        skipFuture.add(false);
      } else {
        futureList.add(Future.delayed(Duration.zero));
        debugPrint(
          "[LOAD MULTIPLE CALENDAR DATA UPDATING] "
          "Should not reload week ${requestWeeks[i]}, data is relatively new",
        );
        skipFuture.add(true);
      }
    }

    // wait for all futures
    var error = false;
    var loadedElements =
        await Future.wait(futureList, eagerError: true).catchError((e) {
      OUtil.addOMessageEvent(OErrorMessage());
      if (e is SocketException) {
        debugPrint(
            "[LOAD CALENDAR DATA ERROR] Error while loading: SocketException");
        OUtil.addOViewModeEvent(OSetState(CurrentOrganizerState.noInternet));
        // return Future.delayed(Duration.zero);
      } else if (e is InternalServerErrorException) {
        debugPrint(
          "[LOAD CALENDAR DATA ERROR] Error while loading: "
          "InternalServerErrorException",
        );
        OUtil.addOViewModeEvent(OSetState(CurrentOrganizerState.serviceDown));

        // return Future.delayed(Duration.zero);
      } else if (e is ClientException) {
        debugPrint(
            "[LOAD CALENDAR DATA ERROR] Error while loading: ClientException");
        OUtil.addOViewModeEvent(OSetState(CurrentOrganizerState.error));
        // return Future.delayed(Duration.zero);
      } else {
        debugPrint(e.runtimeType.toString());
      }
      debugPrint(
          "[LOAD MULTIPLE CALENDAR DATA ERROR] Error loading multiple: $e");
      error = true;
      OUtil.addOLoadingEvent(OSetLoadingState(OErrorLoadMultipleState()));
    });
    if (error) return;

    debugPrint("[LOAD MULTIPLE CALENDAR DATA LISTS] "
        "${dates.length} | "
        "${requestWeeks.length} | "
        "${shouldUpdate.length} | "
        "${loadedElements.length} | "
        "${futureList.length}");

    for (var i = 0; i < dates.length; i++) {
      // var requestWeek = OUtil.absoluteWeekSince1970(dates[i]); // 2627
      if (!skipFuture[i]) {
        final intWeek = requestWeeks[i];
        final elements = loadedElements[i];
        // final elements = await futureList[i];
        map[intWeek] = prepareOWeek(intWeek, elements);
        debugPrint(
          "[LOAD MULTIPLE CALENDAR DATA] Request Week: "
          "${requestWeeks[i]} | ${map[requestWeeks[i]]?.containsData} | "
          "$shouldUpdate",
        );
      } else {
        debugPrint(
            "[LOAD MULTIPLE CALENDAR DATA] Skipped Week: ${requestWeeks[i]}");
      }
    }

    OUtil.addOCalendarEvent(
      OCalendarAddOWeek(
        lastUpdated: DateTime.now(),
        lessonIDs: List.from(lessons ?? <int>[]),
        weekMap: map,
      ),
    );
    if (skipFuture.contains(false)) {
      Future.delayed(Duration(milliseconds: 1200), () {
        OUtil.addOLoadingEvent(OSetLoadingState(OLoadedElementsPresentState()));
      });
    } else {
      OUtil.addOLoadingEvent(OSetLoadingState(OLoadedElementsPresentState()));
    }
    spreadInitiallyLoaded = true;
  }

  static Future<void> loadData(
      {bool forceReload = false, bool onlyCurrentWeek = false}) async {
    debugPrint("[LOAD CALENDAR DATA INIT] " "loadData fired");
    if (onlyCurrentWeek) {
      try {
        OUtil.setCurrentRelativeWeek(0);
        // goToInitialPage();
      } on Exception catch (e) {
        debugPrint(e.toString());
      }
    }
    var reload = false;
    var reset = false;
    var state = OUtil.oLessonBloc.state;
    List<int> lessons;
    if (state is OLessonSelectionReadyState) {
      lessons = state.lessons;
    } else {
      spreadInitiallyLoaded = false;
      OUtil.addOLoadingEvent(OSetLoadingState(ONothingToLoadState()));
      return; // exit
    }
    if (lessons.isEmpty) {
      _resetData();
      spreadInitiallyLoaded = false;
      OUtil.addOLoadingEvent(OSetLoadingState(ONothingToLoadState()));
      return;
    }
    if (lessons.isEmpty) {
      _resetData();
      spreadInitiallyLoaded = false;
      OUtil.addOLoadingEvent(OSetLoadingState(ONothingToLoadState()));
      return;
    }

    if (OUtil.oLoadingBloc.state is ONothingToLoadState ||
        checkIfLessonListsAreUnequal()) {
      // if (!spreadInitiallyLoaded) {
      loadMultiple();
      return;
      // }
    }

    // determine request date
    var requestDate = DateTime.now().add(
      Duration(
        days: (7 * OUtil.getCurrentRelativeWeek()),
      ),
    );
    if (onlyCurrentWeek) {
      requestDate = DateTime.now();
    }
    // caution! if you request data on sunday, no data will be sent
    if (requestDate.weekday == 7) {
      requestDate = requestDate.subtract(Duration(days: 1));
      debugPrint("[LOAD CALENDAR DATA] "
          "Request Date: ${requestDate.toString()}");
    }
    var requestWeek = OUtil.absoluteWeekSince1970(requestDate); // 2627
    debugPrint("[LOAD CALENDAR DATA] "
        "Relative Week: ${OUtil.getCurrentRelativeWeek()}");
    debugPrint("[LOAD CALENDAR DATA] " "REQUEST WEEK: $requestWeek");
    // find out if request already exists in bloc
    // 0 - check state
    var calendarState = OUtil.oCalendarBloc.state;
    if (calendarState is OCalendarEmptyState) {
      reload = true;
    }
    // 1 - check if lesson id's are same
    if (checkIfLessonListsAreUnequal()) {
      reset = true;
    }
    // 2 - check if entry exists e.g. [2627] <- weekNumber
    if (calendarState is OCalendarReadyState) {
      if (calendarState.weekMap[requestWeek] == null) {
        reload = true;
      }
    }
    // 3 - check if request is older than 10 minutes
    debugPrint("[LOAD CALENDAR DATA] "
        "check if dated");
    if (!forceReload) {
      if (shouldReload(requestWeek)) {
        reload = true;
      }
    }
    if (reset) {
      // debugPrint("resetting OCalendar");
      // _resetData();
      forceReload = true;
      OUtil.addOLoadingEvent(OSetLoadingState(OInitialFetchingState()));
      debugPrint("[LOAD CALENDAR DATA] "
          "Force Reload activated");
      // setState(() {});
      // setState(() {
      //   localState = CurrentOrganizerState.lessenSelectionChanged;
      // });
    }
    // (!) load data

    if (reload || forceReload) {
      try {
        OUtil.addOLoadingEvent(OSetLoadingState(OFetchingNewState()));
        // var lol = await Future.delayed(Duration(seconds: 2));
        debugPrint("[LOAD CALENDAR DATA] "
            "Starting to load");
        var remoteOElementList =
            await OrganizerInstanceFunctions.getOrganizerInstances(
                    lessons, requestDate, OUtil.httpRepo)
                .catchError((e) {
          OUtil.addOMessageEvent(OErrorMessage());

          var reason = "";

          switch (e.runtimeType) {
            case SocketException:
              reason = "SocketException";
              // state = CurrentOrganizerState.noInternet;
              break;
            case InternalServerErrorException:
              reason = "InternalServerErrorException";
              // state = CurrentOrganizerState.serviceDown;
              break;
            case ClientException:
              reason = "ClientException";
              // state = CurrentOrganizerState.error;
              break;
            default:
              debugPrint(e.runtimeType.toString());
          }

          //if (state != null) OUtil.addOViewModeEvent(OSetState(state));

          reason = reason.isNotEmpty ? ": $reason" : reason;
          debugPrint("[LOAD CALENDAR DATA ERROR] Error while loading$reason");
        });

        if (remoteOElementList.isEmpty) {
          debugPrint("[LOAD CALENDAR DATA] Loaded list is empty");
          // OUtil.addOViewModeEvent(OSetState(CurrentOrganizerState.error));
          return;
        }

        // 3 - fill the map with the new data
        var weekMap = getMap(reset: reset);
        weekMap[requestWeek] = prepareOWeek(requestWeek, remoteOElementList);

        // 4 - return everything to bloc
        debugPrint("[LOAD CALENDAR DATA] Adding to Bloc");
        OUtil.addOCalendarEvent(
          OCalendarAddOWeek(
            lastUpdated: DateTime.now(),
            lessonIDs: List.from(lessons),
            weekMap: weekMap,
          ),
        );

        OUtil.addOLoadingEvent(OSetLoadingState(OLoadedElementsPresentState()));
      } on Exception {
        //
      }
    } else {
      debugPrint("[LOAD CALENDAR DATA] Did not reload");
      OUtil.addOLoadingEvent(OSetLoadingState(ONothingNewState()));
    }
    OUtil.addOMessageEvent(OResolvedMessage());
    // OUtil.addOViewModeEvent(
    // OSetState(CurrentOrganizerState.loadedOElements));
  }

  static bool checkIfLessonListsAreUnequal() {
    final state = OUtil.oCalendarBloc.state;
    final lessons = OUtil.getAllSelectedLessonIds();

    if (state is OCalendarReadyState) {
      if (!OUtil.areListsEqual(state.lessonIDs, lessons)) {
        debugPrint("[LOAD CALENDAR DATA] " "Lists are UNEQUAL");
        return true;
      } else {
        debugPrint("[LOAD CALENDAR DATA] " "Lists are EQUAL");
        return false;
      }
    }
    debugPrint("[LOAD CALENDAR DATA ERROR] " "Lists could not be compared");
    return false;
  }

  static bool shouldReload(int requestWeek) {
    final calendarState = OUtil.oCalendarBloc.state;
    if (calendarState is OCalendarReadyState) {
      // if (calendarState.weekMap != null) {
      if (calendarState.weekMap[requestWeek] != null) {
        var oldDate =
            calendarState.weekMap[requestWeek]?.lastUpdated ?? DateTime.now();
        var newDate = DateTime.now();

        ///the const [datedThreshold] comes from organizer_constants
        if (oldDate.difference(newDate).inMilliseconds.abs() > datedThreshold) {
          debugPrint("[LOAD CALENDAR DATA] "
              "because the current data is old, $datedThreshold, reload");
          return true;
        } else {
          debugPrint("[LOAD CALENDAR DATA] "
              "current data is relatively new, $datedThreshold, do nothing");
          return false;
        }
      }
      // }
    }
    debugPrint("[LOAD CALENDAR DATA ERROR] "
        "Error: could not compute dated difference");
    return true;
  }

  static Map<int, OWeek> getMap({bool reset = false}) {
    Map<int, OWeek> weekMap;
    var calendarState = OUtil.oCalendarBloc.state;
    if (calendarState is OCalendarReadyState) {
      if (!reset) {
        weekMap = calendarState.weekMap;
      } else {
        weekMap = <int, OWeek>{};
      }
    } else {
      weekMap = <int, OWeek>{};
    }

    return weekMap;
  }

  static OWeek prepareOWeek(
    int requestWeek,
    List<OElement> remoteOElementList,
  ) {
    var week = OWeek(weekSinceEpoch1970: requestWeek);
    week.constructWeek(remoteOElementList);
    return week;
  }

  static void _resetData() {
    debugPrint("[LOAD CALENDAR DATA RESET CALENDARBLOC] Resetting...");
    OUtil.addOCalendarEvent(OCalendarResetEvent());
    OUtil.oCalendarBloc.clear();
  }
}

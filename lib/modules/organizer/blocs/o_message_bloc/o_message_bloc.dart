/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'o_message_event.dart';
part 'o_message_state.dart';

class OMessageBloc extends Bloc<OMessageEvent, OMessageState> {
  OMessageBloc() : super(OMessageInitial());

  @override
  Stream<OMessageState> mapEventToState(
    OMessageEvent event,
  ) async* {
    if (event is OErrorMessage) {
      yield OErrorState();
    } else if (event is OLessonsChanged) {
      yield OLessonsChangedState(event.lessons);
    } else if (event is OElementChanged) {
      yield OElementChangedState(event.oElementId);
    } else if (event is OResolvedMessage) {
      yield OMessageInitial();
    }
  }
}

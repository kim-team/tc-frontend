/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:core';

import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';

import '../../../../bloc/local_user_hyd_bloc/local_user_hyd_bloc.dart';
import '../../model/o_element.dart';
import '../../model/o_week.dart';
import '../../utility/organizer_state_converter.dart';

part 'o_calendar_bloc.g.dart';
part 'o_calendar_event.dart';
part 'o_calendar_state.dart';
part 'o_calendar_state_converter.dart';

class OCalendarBloc extends LocalUserHydBloc<OCalendarEvent, OCalendarState> {
  OCalendarBloc() : super(OCalendarEmptyState());

  @override
  OCalendarState fromJson(Map<String, dynamic> json) {
    try {
      return _OCalendarStateConverter().fromJson(json);
    } on Exception catch (exception) {
      // normal case
      debugPrint(exception.toString());
      debugPrint(
        "something went wrong in OCalendar at fromJson, Exception thrown",
      );
      return OCalendarEmptyState();
    }
  }

  @override
  Map<String, dynamic> toJson(OCalendarState state) =>
      _OCalendarStateConverter().toJson(state);

  @override
  Stream<OCalendarState> mapEventToState(
    OCalendarEvent event,
  ) async* {
    yield OCalendarEmptyState();
    if (event is OCalendarReplaceEntireDataEvent) {
      yield OCalendarReadyState(
        lastUpdated: DateTime.fromMillisecondsSinceEpoch(0),
        lessonIDs: <int>[],
        weekMap: <int, OWeek>{},
      );
    } else if (event is OCalendarAddOWeek) {
      yield OCalendarReadyState(
        lastUpdated: event.lastUpdated,
        lessonIDs: event.lessonIDs,
        weekMap: event.weekMap,
      );
    } else if (event is OCalendarResetEvent) {
      yield OCalendarEmptyState();
    }
  }
}

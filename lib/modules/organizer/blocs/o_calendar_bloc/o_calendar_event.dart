/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

part of 'o_calendar_bloc.dart';

abstract class OCalendarEvent extends Equatable {
  const OCalendarEvent();
}

class OCalendarReplaceEntireDataEvent extends OCalendarEvent {
  final DateTime timestampOfData;
  final List<OElement> oElementsList;
  OCalendarReplaceEntireDataEvent({
    required this.timestampOfData,
    required this.oElementsList,
  });

  @override
  List<Object> get props => [timestampOfData, oElementsList];
}

class OCalendarAddOWeek extends OCalendarEvent {
  final DateTime lastUpdated;
  final Map<int, OWeek> weekMap;
  final List<int> lessonIDs;

  OCalendarAddOWeek({
    required this.lastUpdated,
    required this.weekMap,
    required this.lessonIDs,
  });

  @override
  List<Object> get props => [lastUpdated, weekMap, lessonIDs];
}

class OCalendarResetEvent extends OCalendarEvent {
  // final List<int> subjects;
  // final Map<int, String> previousColorList;
  const OCalendarResetEvent();

  @override
  List<Object> get props => [];
}

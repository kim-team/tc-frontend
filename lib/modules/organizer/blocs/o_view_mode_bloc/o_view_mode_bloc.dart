/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:async';

import 'package:bloc/bloc.dart';

import '../../ui/organizer/organizer_enums.dart';

part 'o_view_mode_event.dart';
part 'o_view_mode_state.dart';

class OViewModeBloc extends Bloc<OViewModeEvent, OViewModeState> {
  OViewModeBloc() : super(OWeeklyViewState(1)); // TODO NNBD MIGRATION

  @override
  Stream<OViewModeState> mapEventToState(
    OViewModeEvent event,
  ) async* {
    yield event.executeAction(state);
  }
}

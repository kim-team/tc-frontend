/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

part of 'o_color_mapping_bloc.dart';

enum _OColorMappingStateType { base, ready }

class _OColorMappingStateConverter
    extends OStateConverter<OColorMappingState, _OColorMappingStateType> {
  const _OColorMappingStateConverter() : super();

  @override
  _OColorMappingStateType fromTypeJsonValueInternal(dynamic jsonValue) =>
      _OColorMappingStateType.values[jsonValue as int];

  @override
  int toTypeJsonValueInternal(OColorMappingState object) {
    _OColorMappingStateType type;

    switch (object.runtimeType) {
      case OColorMappingReadyState:
        type = _OColorMappingStateType.ready;
        break;
      default:
        type = _OColorMappingStateType.base;
    }

    return type.index;
  }

  @override
  OColorMappingState fromJsonInternal(
      _OColorMappingStateType type, Map<String, dynamic> json) {
    final base = OColorMappingState.fromJson(json);

    switch (type) {
      case _OColorMappingStateType.base:
        return base;
      case _OColorMappingStateType.ready:
        return OColorMappingReadyState(base.previousColorList);
    }
  }

  @override
  Map<String, dynamic> toJsonInternal(OColorMappingState object) =>
      object.toJson();

  @override
  OColorMappingState migrate(Map<String, dynamic> json) {
    final tmp = OColorMappingState.fromJson(json);
    return OColorMappingReadyState(tmp.previousColorList);
  }
}

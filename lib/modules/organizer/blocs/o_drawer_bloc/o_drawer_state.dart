/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

part of 'o_drawer_bloc.dart';

abstract class ODrawerState {
  ODrawerState({required this.oElementWrapper});
  OElementWrapper? oElementWrapper;

  bool nothingSelected() {
    if (oElementWrapper == null) {
      return true;
    } else {
      return false;
    }
  }

  bool isSelectedBySubject(OElementWrapper remoteWrapper) {
    if (oElementWrapper?.ele == null) {
      return false;
    }
    if (oElementWrapper?.ele.subjectId == null) {
      return false;
    }
    if (oElementWrapper?.ele.subjectId == (remoteWrapper.ele.subjectId ?? -1)) {
      return true;
    } else {
      return false;
    }
  }

  bool isDirectlySelected(OElementWrapper remoteWrapper) {
    if (oElementWrapper?.ele == null) {
      return false;
    }
    if ((oElementWrapper?.ele.id ?? -1) == (remoteWrapper.ele.id ?? -2)) {
      return true;
    } else {
      return false;
    }
  }
}

class ODrawerInitial extends ODrawerState {
  ODrawerInitial(OElementWrapper? oElementWrapper)
      : super(oElementWrapper: oElementWrapper);
}

class ODrawerMenuState extends ODrawerState {
  ODrawerMenuState(OElementWrapper? oElementWrapper)
      : super(oElementWrapper: oElementWrapper);
}

class ODrawerDeleteState extends ODrawerState {
  ODrawerDeleteState(OElementWrapper? oElementWrapper)
      : super(oElementWrapper: oElementWrapper);
}

class ODrawerColorState extends ODrawerState {
  ODrawerColorState(OElementWrapper? oElementWrapper)
      : super(oElementWrapper: oElementWrapper);
}

class ODrawerInfoState extends ODrawerState {
  ODrawerInfoState(OElementWrapper? oElementWrapper)
      : super(oElementWrapper: oElementWrapper);
}

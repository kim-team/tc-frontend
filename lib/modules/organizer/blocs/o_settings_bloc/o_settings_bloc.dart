/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:async';

import 'package:copy_with_extension/copy_with_extension.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';

import '../../../../bloc/local_user_hyd_bloc/local_user_hyd_bloc.dart';
import '../../model/o_block.dart';
import '../../model/o_block_intervals.dart';
import '../../model/o_element.dart';

part 'o_settings_bloc.g.dart';
part 'o_settings_event.dart';
part 'o_settings_state.dart';

class OSettingsBloc extends LocalUserHydBloc<OSettingsEvent, OSettingsState> {
  OSettingsBloc() : super(OSettingsState.initial());

  @override
  Stream<OSettingsState> mapEventToState(
    OSettingsEvent event,
  ) async* {
    yield OSettingsState.fromJson(event.executeAction(state).toJson());
  }

  @override
  OSettingsState fromJson(Map<String, dynamic> json) {
    try {
      return OSettingsState.fromJson(json);
    } on Exception catch (e) {
      debugPrint(e.toString());
      return OSettingsState.initial();
    }
  }

  @override
  Map<String, dynamic> toJson(OSettingsState state) => state.toJson();
}

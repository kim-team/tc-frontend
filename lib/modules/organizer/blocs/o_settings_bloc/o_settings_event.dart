/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

part of 'o_settings_bloc.dart';

abstract class OSettingsEvent extends Equatable {
  const OSettingsEvent();

  @override
  List<Object> get props => [];

  OSettingsState executeAction(OSettingsState state) =>
      OSettingsState.initial();
}

class OSettingsResetEvent extends OSettingsEvent {
  const OSettingsResetEvent();

  @override
  List<Object> get props => [];

  @override
  OSettingsState executeAction(OSettingsState state) =>
      OSettingsState.initial();
}

class SetOBlockIntervalEvent extends OSettingsEvent {
  final int newSelection;

  SetOBlockIntervalEvent(this.newSelection);

  @override
  List<Object> get props => [newSelection];

  @override
  OSettingsState executeAction(OSettingsState state) {
    if (state.blockIntervals.length < newSelection) {
      return state;
    } else {
      return state.copyWith(selectedInterval: newSelection);
    }
  }
}

class CycleOBlockIntervalEvent extends OSettingsEvent {
  CycleOBlockIntervalEvent();

  @override
  List<Object> get props => [];

  @override
  OSettingsState executeAction(OSettingsState state) {
    var newSelection =
        (state.selectedInterval + 1) % (state.blockIntervals.length);
    return state.copyWith(selectedInterval: newSelection);
  }
}

class DeleteSingleOLesson extends OSettingsEvent {
  DeleteSingleOLesson(this.element);

  final OElement element;

  @override
  List<Object> get props => [element];

  @override
  OSettingsState executeAction(OSettingsState state) {
    try {
      state.deletedOElements.add(element);
    } on Exception catch (e) {
      debugPrint(
          "${e.toString()} Error deleting single oElement in event execution");
    }
    return state;
  }
}

class ClearSingleOLesson extends OSettingsEvent {
  ClearSingleOLesson(this.oElementId);

  final int oElementId;

  @override
  List<Object> get props => [oElementId];

  @override
  OSettingsState executeAction(OSettingsState state) {
    try {
      state.deletedOElements.removeWhere((element) => element.id == oElementId);
    } on Exception catch (e) {
      debugPrint(
          "${e.toString()} Error clearing single oElement in event execution");
    }
    return state;
  }
}

class ClearAllOElements extends OSettingsEvent {
  ClearAllOElements();

  @override
  OSettingsState executeAction(OSettingsState state) {
    OSettingsState newState;
    try {
      newState = state.copyWith(deletedOElements: <OElement>[]);
      return newState;
    } on Exception catch (e) {
      debugPrint(
          "${e.toString()} Error clearing all oElements in event execution");
    }
    return state;
  }
}

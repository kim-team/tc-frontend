/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

part of 'o_settings_bloc.dart';

@CopyWith()
@JsonSerializable(anyMap: true, explicitToJson: true)
class OSettingsState extends Equatable {
  final int selectedInterval;
  final List<OBlockIntervals> blockIntervals;
  final List<OElement> editedOElements;
  final List<OElement> deletedOElements;
  final List<String> programBlacklist;

  OSettingsState({
    this.selectedInterval = 0,
    List<OBlockIntervals>? blockIntervals,
    List<OElement>? editedOElements,
    List<OElement>? deletedOElements,
    List<String>? programBlacklist,
  })  : blockIntervals = blockIntervals ?? <OBlockIntervals>[],
        editedOElements = editedOElements ?? <OElement>[],
        deletedOElements = deletedOElements ?? <OElement>[],
        programBlacklist = programBlacklist ?? <String>[];

  OBlockIntervals getCurrentIntervals() {
    if (selectedInterval < blockIntervals.length) {
      return blockIntervals[selectedInterval];
    } else {
      return initBlocks()[0];
    }
  }

  OSettingsState.initial()
      : selectedInterval = 0,
        blockIntervals = initBlocks(),
        editedOElements = <OElement>[],
        deletedOElements = <OElement>[],
        programBlacklist = programBlackList;

  static const List<String> programBlackList = [
    "Baumaßnahmen",
    "Diverse Campus Friedberg Veranstaltungen",
    "Diverse FB BAU Veranstaltungen",
    "Diverse FB EI Veranstaltungen",
    "Diverse FB IEM Veranstaltungen",
    "Diverse FB LSE Veranstaltungen",
    "Diverse FB M Veranstaltungen",
    "Diverse FB ME Veranstaltungen",
    "Diverse FB MND Veranstaltungen",
    "Diverse FB MNI Veranstaltungen",
    "Diverse FB MuK Veranstaltungen",
    "Diverse FB W Veranstaltungen",
    "Diverse FB WI Veranstaltungen",
    "Diverse StK Veranstaltungen",
    "Diverse ZDH Veranstaltungen",
    "Grundstudium LSE Wiederholungsveranstaltungen (B.Sc.)",
    "Grundstudium Maschinenbau und Energiesysteme Studiengänge (B.Eng.)",
    "Hochschulsport",
    "International Office",
    "Studienberatung",
    "Sprachenzentrum",
    "StK - SP Gesellschaftswissenschaft",
    "StK - SP Medizin",
    "StK - SP Technik",
    "StK - SP Wirtschaft",
    "Zentrale Veranstaltungen"
  ];

  static List<OBlockIntervals> initBlocks() {
    var map = <OBlockIntervals>[];
    var s = DateTime.parse("2020-01-01");
    map.add(OBlockIntervals("GI", "Gießen", [
      OBlock("1", s.add(Duration(hours: 8, minutes: 00)),
          s.add(Duration(hours: 9, minutes: 30))),
      OBlock("2", s.add(Duration(hours: 9, minutes: 50)),
          s.add(Duration(hours: 11, minutes: 20))),
      OBlock("3", s.add(Duration(hours: 11, minutes: 30)),
          s.add(Duration(hours: 13, minutes: 00))),
      OBlock("4", s.add(Duration(hours: 14, minutes: 00)),
          s.add(Duration(hours: 15, minutes: 30))),
      OBlock("5", s.add(Duration(hours: 15, minutes: 45)),
          s.add(Duration(hours: 17, minutes: 15))),
      OBlock("6", s.add(Duration(hours: 17, minutes: 30)),
          s.add(Duration(hours: 19, minutes: 00))),
      OBlock("7", s.add(Duration(hours: 19, minutes: 10)),
          s.add(Duration(hours: 20, minutes: 40))),
      OBlock("8", s.add(Duration(hours: 20, minutes: 50)),
          s.add(Duration(hours: 22, minutes: 20))),
      OBlock("9", s.add(Duration(hours: 22, minutes: 30)),
          s.add(Duration(hours: 23, minutes: 59))),
    ]));
    map.add(OBlockIntervals("FB", "Friedberg", [
      OBlock("1", s.add(Duration(hours: 8, minutes: 00)),
          s.add(Duration(hours: 9, minutes: 30))),
      OBlock("2", s.add(Duration(hours: 9, minutes: 45)),
          s.add(Duration(hours: 11, minutes: 15))),
      OBlock("3", s.add(Duration(hours: 11, minutes: 30)),
          s.add(Duration(hours: 13, minutes: 00))),
      OBlock("4", s.add(Duration(hours: 14, minutes: 00)),
          s.add(Duration(hours: 15, minutes: 30))),
      OBlock("5", s.add(Duration(hours: 15, minutes: 40)),
          s.add(Duration(hours: 17, minutes: 10))),
      OBlock("6", s.add(Duration(hours: 17, minutes: 20)),
          s.add(Duration(hours: 18, minutes: 50))),
      OBlock("7", s.add(Duration(hours: 19, minutes: 00)),
          s.add(Duration(hours: 20, minutes: 30))),
      OBlock("8", s.add(Duration(hours: 20, minutes: 50)),
          s.add(Duration(hours: 22, minutes: 20))),
      OBlock("9", s.add(Duration(hours: 22, minutes: 30)),
          s.add(Duration(hours: 23, minutes: 59))),
    ]));

    return map;
  }

  @override
  List<Object> get props => [
        selectedInterval,
        blockIntervals,
        editedOElements,
        deletedOElements,
        programBlacklist
      ];

  factory OSettingsState.fromJson(Map<String, dynamic> json) =>
      _$OSettingsStateFromJson(json);

  Map<String, dynamic> toJson() => _$OSettingsStateToJson(this);
}

/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/foundation.dart';

import '../../model/o_change_record.dart';
import '../../model/o_program.dart';
import 'o_program_history_bloc.dart';

abstract class OProgramHistoryAction {
  OProgramHistoryState executeAction() =>
      OProgramHistoryState.initialSettings();
}

class AddOProgramToHistoryAction extends OProgramHistoryAction {
  AddOProgramToHistoryAction(this.program, this.newerProgram, this.state);

  final OProgram? program;
  final OProgram? newerProgram;
  final OProgramHistoryState state;

  @override
  OProgramHistoryState executeAction() {
    if (program == null) return state;
    if (newerProgram == null) return state;
    program!.changeReport(newerProgram!).forEach((key, value) {
      // if there is an archived version already
      if (state.changeRecordMap[key] != null) {
        var oldDelta = state.changeRecordMap[key]?.delta ?? <ODelta>[];
        oldDelta = oldDelta.toSet().toList();
        var newDelta = value.delta;
        newDelta = newDelta.toSet().toList();
        if (newDelta.contains(ODelta.amountSubtreeDecreased)) {
          oldDelta.remove(ODelta.amountSubtreeIncreased);
        }
        if (newDelta.contains(ODelta.amountSubtreeIncreased)) {
          oldDelta.remove(ODelta.amountSubtreeDecreased);
        }
        if (newDelta.contains(ODelta.added)) {
          oldDelta.remove(ODelta.removed);
          oldDelta.add(ODelta.added);
          state.changeRecordMap[key]?.newRef = value.newRef;
        }
        if (newDelta.contains(ODelta.removed)) {
          oldDelta.remove(ODelta.added);
          oldDelta.add(ODelta.removed);
          state.changeRecordMap[key]?.oldRef = value.oldRef;
        }
        if (newDelta.isNotEmpty) {
          state.changeRecordMap[key]?.userChecked = false;
        }
        state.changeRecordMap[key]?.delta.addAll(value.delta);
        state.changeRecordMap[key]?.delta = oldDelta.toSet().toList();
      } else {
        state.changeRecordMap[key] = value;
      }
    });
    return OProgramHistoryState.fromJson(state.toJson())
        .copyWith(lastUpdated: DateTime.now());
  }
}

class UserCheckedChangeAction extends OProgramHistoryAction {
  UserCheckedChangeAction(this.contextId, this.state);

  final OProgramHistoryState state;
  final String contextId;

  @override
  OProgramHistoryState executeAction() {
    if (state.changeRecordMap.isEmpty) return state;
    if (state.changeRecordMap.containsKey(contextId)) {
      state.changeRecordMap.remove(contextId);
      // TODO: maybe add back "read" functionality in future
      // state.changeRecordMap[contextId].userChecked = true;
    }
    return OProgramHistoryState.fromJson(state.toJson())
        .copyWith(lastUpdated: DateTime.now());
  }
}

class UserCheckedAllChangeForProgramAction extends OProgramHistoryAction {
  UserCheckedAllChangeForProgramAction(this.contextId, this.state);

  final OProgramHistoryState state;
  final String contextId;

  @override
  OProgramHistoryState executeAction() {
    var recordMap = state.changeRecordMap;
    if (recordMap.isEmpty) return state;
    // TODO: maybe add back "read" functionality in future
    // recordMap.forEach((key, value) {
    //   if (key.startsWith(contextId)) {
    //     recordMap[key].userChecked = true;
    //   }
    // });
    recordMap.removeWhere((key, value) => key.startsWith(contextId));
    return OProgramHistoryState.fromJson(state.toJson())
        .copyWith(lastUpdated: DateTime.now());
  }
}

class ResetOProgramHistoryAction extends OProgramHistoryAction {
  @override
  OProgramHistoryState executeAction() =>
      OProgramHistoryState.initialSettings();
}

class ResetSpecificOProgramHistoryAction extends OProgramHistoryAction {
  ResetSpecificOProgramHistoryAction(this.state, this.program);

  final OProgramHistoryState state;
  final OProgram program;

  @override
  OProgramHistoryState executeAction() {
    var recordMap = state.changeRecordMap;
    if (recordMap.isEmpty) return state;
    try {
      recordMap
          .removeWhere((key, value) => key.startsWith(program.id.toString()));
    } on Exception catch (e) {
      debugPrint("Error while deleting specific OProgramHistory: $e");
    }
    return OProgramHistoryState.fromJson(state.toJson())
        .copyWith(lastUpdated: DateTime.now());
  }
}

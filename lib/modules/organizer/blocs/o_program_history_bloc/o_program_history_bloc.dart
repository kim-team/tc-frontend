/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:async';

import 'package:copy_with_extension/copy_with_extension.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';

import '../../../../bloc/local_user_hyd_bloc/local_user_hyd_bloc.dart';
import '../../model/o_change_record.dart';
import '../../model/o_program.dart';
import 'o_program_history_action.dart';

part 'o_program_history_bloc.g.dart';
part 'o_program_history_event.dart';
part 'o_program_history_state.dart';

class OProgramHistoryBloc
    extends LocalUserHydBloc<OProgramHistoryEvent, OProgramHistoryState> {
  OProgramHistoryBloc() : super(OProgramHistoryState.initialSettings());

  @override
  Stream<OProgramHistoryState> mapEventToState(
    OProgramHistoryEvent event,
  ) async* {
    if (event is AddOProgramToHistoryEvent) {
      yield AddOProgramToHistoryAction(
              event.oldProgram, event.newerProgram, state)
          .executeAction();
    }
    if (event is OProgramHistoryResetEvent) {
      yield ResetOProgramHistoryAction().executeAction();
    }
    if (event is ResetSpecificOProgramHistoryEvent) {
      yield ResetSpecificOProgramHistoryAction(state, event.program)
          .executeAction();
    }
    if (event is UserCheckedChangeEvent) {
      yield UserCheckedChangeAction(event.contextId, state).executeAction();
    }
    if (event is AddLastCheckedEvent) {
      yield state.copyWith(lastUpdated: DateTime.now());
    }
    if (event is UserCheckedAllChangeForProgramEvent) {
      yield UserCheckedAllChangeForProgramAction(event.contextId, state)
          .executeAction();
    }
  }

  @override
  OProgramHistoryState fromJson(Map<String, dynamic> json) =>
      OProgramHistoryState.fromJson(json);

  @override
  Map<String, dynamic> toJson(OProgramHistoryState state) => state.toJson();
}

/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../../common/tc_theme.dart';
import '../../../../common/tinycampus_icons.dart';
import '../../model/zs_item.dart';

class LinkTile extends StatelessWidget {
  final ZsItem item;

  const LinkTile({Key? key, required this.item}) : super(key: key);

  @override
  Widget build(BuildContext context) => ListTile(
        contentPadding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
        title: Row(
          children: [
            Container(
              width: 64,
              height: 64,
              padding: EdgeInsets.all(10),
              child: _determineLeading(),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 4.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      item.title,
                      style: Theme.of(context)
                          .textTheme
                          .headline3
                          ?.copyWith(color: CurrentTheme().textSoftWhite),
                    ),
                    if (item.subtitle.isNotEmpty)
                      Text(
                        item.subtitle,
                        style: Theme.of(context)
                            .textTheme
                            .bodyText2
                            ?.copyWith(color: CurrentTheme().textSoftWhite),
                      ),
                  ],
                ),
              ),
            ),
            Container(
              width: 64,
              padding: EdgeInsets.all(12.0),
              child: Icon(
                Icons.arrow_forward_ios_rounded,
                size: 18,
              ),
            )
          ],
        ),
        onTap: () => _launchUrl(item.url),
      );

  Future<void> _launchUrl(String url) async {
    if (await canLaunch(url)) {
      launch(url);
    }
  }

  Widget _determineLeading() {
    switch (item.url.split(':')[0]) {
      case 'tel':
        return Icon(Icons.phone);
      case 'mailto':
        return Icon(Icons.mail_rounded);
      case 'http':
      case 'https':
        if (item.url.contains('chat/php/app')) {
          return Icon(TinyCampusIcons.message);
        }
        return validImageUrl(item.imageUrl)
            ? FutureBuilder(
                future: Future.delayed(Duration(milliseconds: 120)),
                builder: (context, snapshot) => ClipOval(
                  clipBehavior: Clip.antiAlias,
                  child: CachedNetworkImage(
                    key: Key(item.imageUrl),
                    imageUrl: item.imageUrl,
                    fit: BoxFit.cover,
                    errorWidget: (context, url, error) => Center(
                      child: _getTextLeading(item.title),
                    ),
                  ),
                ),
              )
            : _getTextLeading(item.title);
      default:
        return _getTextLeading(item.title);
    }
  }

  Text _getTextLeading(String s) =>
      Text(s.length >= 2 ? s.substring(0, 2).toUpperCase() : "?");

  bool validImageUrl(String url) {
    if (url.isEmpty) return false;
    if (url.contains(RegExp(r'\.(gif|jpe?g|tiff?|png|webp|bmp)$'))) {
      if (Uri.parse(url).isAbsolute) {
        return true;
      }
    }
    return false;
  }
}

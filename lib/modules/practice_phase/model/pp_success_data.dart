/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:copy_with_extension/copy_with_extension.dart';
import 'package:json_annotation/json_annotation.dart';

part 'pp_success_data.g.dart';

/// [PpSuccessData] contains the relevant data from a user
/// to be sent when a phase is finished
///
/// It contains [matNumber], [lastName]
@CopyWith()
@JsonSerializable(explicitToJson: true, anyMap: true)
class PpSuccessData {
  /// [matNumber] is the matriculation number of the user
  final int matNumber;

  /// [lastName] is the last name of the user
  final String lastName;

  /// The constructor of [PpSuccessData]
  PpSuccessData({
    this.matNumber = -1,
    this.lastName = "",
  });

  factory PpSuccessData.fromJson(Map<String, dynamic> json) =>
      _$PpSuccessDataFromJson(json);

  Map<String, dynamic> toJson() => _$PpSuccessDataToJson(this);
}

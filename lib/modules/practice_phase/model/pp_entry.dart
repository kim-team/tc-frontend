/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:copy_with_extension/copy_with_extension.dart';
import 'package:json_annotation/json_annotation.dart';

part 'pp_entry.g.dart';

/// A [PpEntry] is one entry in a [PpUnit]
///
/// It contains [entryId], [order], [title], [body]
@CopyWith()
@JsonSerializable(explicitToJson: true, anyMap: true)
class PpEntry {
  /// [entryId] is an unique id for [PpEntry]
  final int entryId;

  /// [order] specifies the order of the entries
  final int order;

  /// [title] is a map that contains the titles in different languages
  final Map<String, String> title;

  /// [body] is a map that contains the content in different languages
  final Map<String, String> body;

  /// The constructor of [PpEntry]
  PpEntry({
    this.entryId = -1,
    this.order = -1,
    this.title = const {},
    this.body = const {},
  });

  factory PpEntry.fromJson(Map<String, dynamic> json) =>
      _$PpEntryFromJson(json);

  Map<String, dynamic> toJson() => _$PpEntryToJson(this);
}

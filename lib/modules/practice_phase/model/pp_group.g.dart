// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pp_group.dart';

// **************************************************************************
// CopyWithGenerator
// **************************************************************************

extension PpGroupCopyWith on PpGroup {
  PpGroup copyWith({
    int? dependsOn,
    int? groupId,
    int? order,
    Map<String, String>? title,
    List<PpUnit>? units,
  }) {
    return PpGroup(
      dependsOn: dependsOn ?? this.dependsOn,
      groupId: groupId ?? this.groupId,
      order: order ?? this.order,
      title: title ?? this.title,
      units: units ?? this.units,
    );
  }
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PpGroup _$PpGroupFromJson(Map json) => PpGroup(
      groupId: json['groupId'] as int? ?? -1,
      order: json['order'] as int? ?? -1,
      dependsOn: json['dependsOn'] as int? ?? -1,
      title: (json['title'] as Map?)?.map(
            (k, e) => MapEntry(k as String, e as String),
          ) ??
          const {},
      units: (json['units'] as List<dynamic>?)
              ?.map((e) => PpUnit.fromJson(Map<String, dynamic>.from(e as Map)))
              .toList() ??
          const [],
    );

Map<String, dynamic> _$PpGroupToJson(PpGroup instance) => <String, dynamic>{
      'groupId': instance.groupId,
      'order': instance.order,
      'dependsOn': instance.dependsOn,
      'title': instance.title,
      'units': instance.units.map((e) => e.toJson()).toList(),
    };

// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pp_phase.dart';

// **************************************************************************
// CopyWithGenerator
// **************************************************************************

extension PpPhaseCopyWith on PpPhase {
  PpPhase copyWith({
    int? categoryId,
    Map<String, String>? description,
    String? faqUrl,
    List<PpGroup>? groups,
    List<PpPerson>? persons,
    int? phaseId,
    Map<String, String>? title,
  }) {
    return PpPhase(
      categoryId: categoryId ?? this.categoryId,
      description: description ?? this.description,
      faqUrl: faqUrl ?? this.faqUrl,
      groups: groups ?? this.groups,
      persons: persons ?? this.persons,
      phaseId: phaseId ?? this.phaseId,
      title: title ?? this.title,
    );
  }
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PpPhase _$PpPhaseFromJson(Map json) => PpPhase(
      phaseId: json['phaseId'] as int? ?? -1,
      title: (json['title'] as Map?)?.map(
            (k, e) => MapEntry(k as String, e as String),
          ) ??
          const {},
      groups: (json['groups'] as List<dynamic>?)
              ?.map(
                  (e) => PpGroup.fromJson(Map<String, dynamic>.from(e as Map)))
              .toList() ??
          const [],
      description: json['description'] == null
          ? const {}
          : PpPhase.getMap(json['description']),
      faqUrl: json['faqUrl'] as String? ?? "",
      categoryId: json['categoryId'] as int? ?? -1,
      persons: (json['persons'] as List<dynamic>?)
              ?.map(
                  (e) => PpPerson.fromJson(Map<String, dynamic>.from(e as Map)))
              .toList() ??
          const [],
    );

Map<String, dynamic> _$PpPhaseToJson(PpPhase instance) => <String, dynamic>{
      'phaseId': instance.phaseId,
      'title': instance.title,
      'groups': instance.groups.map((e) => e.toJson()).toList(),
      'description': instance.description,
      'faqUrl': instance.faqUrl,
      'categoryId': instance.categoryId,
      'persons': instance.persons.map((e) => e.toJson()).toList(),
    };

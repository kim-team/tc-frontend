// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pp_answer.dart';

// **************************************************************************
// CopyWithGenerator
// **************************************************************************

extension PpAnswerCopyWith on PpAnswer {
  PpAnswer copyWith({
    int? answerId,
    Map<String, String>? body,
  }) {
    return PpAnswer(
      answerId: answerId ?? this.answerId,
      body: body ?? this.body,
    );
  }
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PpAnswer _$PpAnswerFromJson(Map json) => PpAnswer(
      answerId: json['answerId'] as int? ?? -1,
      body: (json['body'] as Map?)?.map(
            (k, e) => MapEntry(k as String, e as String),
          ) ??
          const {},
    );

Map<String, dynamic> _$PpAnswerToJson(PpAnswer instance) => <String, dynamic>{
      'answerId': instance.answerId,
      'body': instance.body,
    };

/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:copy_with_extension/copy_with_extension.dart';
import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

import 'pp_unit.dart';

part 'pp_group.g.dart';

/// A [PpGroup] is one group of a practice phase
///
/// It contains a [groupId], [order], [dependsOn], [title], [units]
@CopyWith()
@JsonSerializable(explicitToJson: true, anyMap: true)
class PpGroup extends Equatable {
  /// [groupId] is a unique id for a group
  final int groupId;

  /// [order] specifies the order of all the groups
  final int order;

  /// [dependsOn] specifies if a group depends on another group to be active
  final int dependsOn;

  /// [title] is a map that contains the titles in different languages
  final Map<String, String> title;

  /// [units] contains all [PpUnit] of a group
  final List<PpUnit> units;

  /// The constructor of [PpGroup]
  const PpGroup({
    this.groupId = -1,
    this.order = -1,
    this.dependsOn = -1,
    this.title = const {},
    this.units = const [],
  });

  factory PpGroup.fromJson(Map<String, dynamic> json) =>
      _$PpGroupFromJson(json);

  Map<String, dynamic> toJson() => _$PpGroupToJson(this);

  @override
  List<Object?> get props => [
        groupId,
        order,
        dependsOn,
        title,
        units,
      ];
}

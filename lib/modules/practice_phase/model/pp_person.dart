/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:copy_with_extension/copy_with_extension.dart';
import 'package:json_annotation/json_annotation.dart';

import 'pp_address.dart';

part 'pp_person.g.dart';

/// [PpPerson] is a person who supervises a practice phase
///
/// It contains a [fullName], [image], [imageAlt], [description], [addresses]
@CopyWith()
@JsonSerializable(explicitToJson: true, anyMap: true)
class PpPerson {
  /// [fullName] is a map which contains the name of
  /// the [PpPerson] in different languages
  final Map<String, String> fullName;

  /// [image] is the image of a [PpPerson]
  final String image;

  /// [imageAlt] the description of the image in different languages
  final Map<String, String> imageAlt;

  /// [description] is map which contains a description
  /// of the person in different languages
  final Map<String, String> description;

  /// [addresses] contains 0-n location and contact information of a [PpPerson]
  final List<PpAddress> addresses;

  /// The constructor of [PpPerson]
  PpPerson({
    this.fullName = const {},
    this.image = "",
    this.imageAlt = const {},
    this.description = const {},
    this.addresses = const [],
  });

  factory PpPerson.fromJson(Map<String, dynamic> json) =>
      _$PpPersonFromJson(json);

  Map<String, dynamic> toJson() => _$PpPersonToJson(this);
}

/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:copy_with_extension/copy_with_extension.dart';
import 'package:json_annotation/json_annotation.dart';

import 'pp_entry.dart';
import 'pp_media.dart';
import 'pp_question.dart';

part 'pp_unit.g.dart';

/// [PpUnit] is a specific unit in a [PpGroup]
///
/// It contains [unitId], [title], [subtitle], [markdown],
/// [image], [imageAlt], [medias], [entries], [questions]
@CopyWith()
@JsonSerializable(explicitToJson: true, anyMap: true)
class PpUnit {
  /// [unitId] is a unique id for an [PpUnit]
  final int unitId;

  /// [title] is the title of the unit in different languages
  final Map<String, String> title;

  /// [subtitle] is the subtitle of the unit in different languages
  final Map<String, String> subtitle;

  /// [markdown] contains the learning material in different languages
  /// and with markdown elements
  final Map<String, String> markdown;

  /// [image] the url to the image
  final String image;

  /// [imageAlt] the description to the image in different languages
  final Map<String, String> imageAlt;

  /// [medias] all medias in a [PpUnit]
  final List<PpMedia> medias;

  /// [entries] contains all entries of a [PpUnit]
  final List<PpEntry> entries;

  /// [questions] contains all questions of the quiz of a [PpUnit]
  final List<PpQuestion> questions;

  /// The constructor of [PpUnit]
  PpUnit({
    this.unitId = -1,
    this.title = const {},
    this.subtitle = const {},
    this.markdown = const {},
    this.image = "",
    this.imageAlt = const {},
    this.medias = const [],
    this.entries = const [],
    this.questions = const [],
  });

  factory PpUnit.fromJson(Map<String, dynamic> json) => _$PpUnitFromJson(json);

  Map<String, dynamic> toJson() => _$PpUnitToJson(this);
}

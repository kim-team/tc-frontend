/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:copy_with_extension/copy_with_extension.dart';
import 'package:json_annotation/json_annotation.dart';

part 'pp_media.g.dart';

/// [PpMedia] is a media, like a document, a video or an url, in a [PpUnit]
///
/// It contains [mediaId], [order], [type], [url], [title]
@CopyWith()
@JsonSerializable(explicitToJson: true, anyMap: true)
class PpMedia {
  /// [mediaId] is a unique Id for [PpMedia]
  final int mediaId;

  /// [order] specifies the order in which the medias are displayed
  final int order;

  /// [type] specifies the type of the media
  /// 0 = link, 1 = document, 2 = video, default = error
  final int type;

  /// [url] is the Url where to find the media
  final String url;

  /// [title] is a map that contains the titles in different languages
  final Map<String, String> title;

  /// The constructor of [PpMedia]
  PpMedia({
    this.mediaId = -1,
    this.order = -1,
    this.type = -1,
    this.url = "",
    this.title = const {},
  });

  factory PpMedia.fromJson(Map<String, dynamic> json) =>
      _$PpMediaFromJson(json);

  Map<String, dynamic> toJson() => _$PpMediaToJson(this);
}

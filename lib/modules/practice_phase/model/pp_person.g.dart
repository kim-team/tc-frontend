// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pp_person.dart';

// **************************************************************************
// CopyWithGenerator
// **************************************************************************

extension PpPersonCopyWith on PpPerson {
  PpPerson copyWith({
    List<PpAddress>? addresses,
    Map<String, String>? description,
    Map<String, String>? fullName,
    String? image,
    Map<String, String>? imageAlt,
  }) {
    return PpPerson(
      addresses: addresses ?? this.addresses,
      description: description ?? this.description,
      fullName: fullName ?? this.fullName,
      image: image ?? this.image,
      imageAlt: imageAlt ?? this.imageAlt,
    );
  }
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PpPerson _$PpPersonFromJson(Map json) => PpPerson(
      fullName: (json['fullName'] as Map?)?.map(
            (k, e) => MapEntry(k as String, e as String),
          ) ??
          const {},
      image: json['image'] as String? ?? "",
      imageAlt: (json['imageAlt'] as Map?)?.map(
            (k, e) => MapEntry(k as String, e as String),
          ) ??
          const {},
      description: (json['description'] as Map?)?.map(
            (k, e) => MapEntry(k as String, e as String),
          ) ??
          const {},
      addresses: (json['addresses'] as List<dynamic>?)
              ?.map((e) =>
                  PpAddress.fromJson(Map<String, dynamic>.from(e as Map)))
              .toList() ??
          const [],
    );

Map<String, dynamic> _$PpPersonToJson(PpPerson instance) => <String, dynamic>{
      'fullName': instance.fullName,
      'image': instance.image,
      'imageAlt': instance.imageAlt,
      'description': instance.description,
      'addresses': instance.addresses.map((e) => e.toJson()).toList(),
    };

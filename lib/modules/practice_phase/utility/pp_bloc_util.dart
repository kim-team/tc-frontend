/*
 * Copyright 2020-2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:async';
import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../common/constants/api_constants.dart';
import '../../../common/styled_print.dart';
import '../../../repositories/http/http_repository.dart';
import '../bloc/pp_model/pp_model_bloc.dart';
import '../bloc/pp_user_progress/pp_user_progress_bloc.dart';
import '../model/pp_group.dart';
import '../model/pp_phase.dart';
import '../model/pp_unit.dart';

abstract class PpBlocUtil {
  static late PpUserProgressBloc ppUserProgressBloc;
  static late PpModelBloc ppModelBloc;
  static late HttpRepository httpRepo;

  static bool saving = false;

  static void addProgressEvent(PpUserProgressEvent event) {
    sprint("$event added", caller: PpBlocUtil, style: PrintStyle.praxisPhase);
    PpBlocUtil.ppUserProgressBloc.add(event);
    saveProgressToBackend();
  }

  static Future<void> saveProgressToBackend() async {
    sprint("trying to save");
    if (!PpBlocUtil.saving) {
      PpBlocUtil.saving = true;
      await Future.delayed(Duration(milliseconds: 180), () {
        sprint("SAVING", style: PrintStyle.attention);
        PpBlocUtil.ppUserProgressBloc.add(PpSaveProgressToBackend());
        PpBlocUtil.saving = false;
      });
    }
  }

  static void addModelEvent(PpModelEvent event) {
    sprint("$event added", caller: PpBlocUtil, style: PrintStyle.praxisPhase);
    PpBlocUtil.ppModelBloc.add(event);
  }

  static UnitState getDynamicUnitState(PpUnit unit) {
    final entryProgression = ppUserProgressBloc.state.entryCheckedSet;
    final questionProgression = ppUserProgressBloc.state.entryQuestionMap;
    var unitState = UnitState();
    if (getUserSelectedPhase() == const PpPhase()) return unitState;

    /// check [PpEntry]s "If there's at least one [PpEntry] unchecked"
    unitState.amountUncheckedEntries =
        unit.entries.where((e) => !entryProgression.contains(e.entryId)).length;

    if (unitState.amountUncheckedEntries == 0 || unit.entries.isEmpty) {
      unitState.state = UnitEnum.level1Complete;
    }

    // TODO: if [entryMap] should only hold 0 or 1 Why not use bool?
    /// check [PpQuestion]s "If there's at least one [PpQuestion] incomplete"
    unit.questions
        .where((e) => questionProgression.containsKey(e.questionId))
        .map((e) => questionProgression[e.questionId] == 1)
        .forEach(
          (e) => e
              ? unitState.amountCorrectAnswers++
              : unitState.amountWrongAnswers++,
        );

    if (unitState.amountWrongAnswers >= 1) {
      unitState.state = UnitEnum.level2Failed;
    }

    if (unit.questions.isEmpty && unitState.state == UnitEnum.level1Complete) {
      unitState.state = UnitEnum.level2Complete;
    }
    if (unit.questions.isNotEmpty &&
        unitState.amountCorrectAnswers == unit.questions.length) {
      unitState.state = UnitEnum.level2Complete;
    }
    return unitState;
  }

  /// If [initBlocs] is not called, almost every function will fail.
  static void initBlocs(BuildContext context) {
    sprint(
      "Initializing Blocs",
      style: PrintStyle.praxisPhase,
      caller: PpBlocUtil,
    );

    ppUserProgressBloc = BlocProvider.of<PpUserProgressBloc>(context);
    ppModelBloc = BlocProvider.of<PpModelBloc>(context);
    httpRepo = RepositoryProvider.of<HttpRepository>(context);

    // Use [debug] webserver or [live] backend, depending on execution mode
    PpBlocUtil.addModelEvent(
      kDebugMode
          ? PpFetchDebugModelFromRemoteEvent()
          : PpFetchAndReplaceModelFromRemoteEvent(),
    );
    PpBlocUtil.addProgressEvent(PpFetchUserProgress());
  }

  static GroupState getDynamicGroupState(PpGroup group, PpPhase phase) {
    var groupState = GroupState();

    groupState.amountCompletedUnits = group.units
        .map((e) => getDynamicUnitState(e).state)
        .where(
          (e) => e == UnitEnum.level2Failed || e == UnitEnum.level2Complete,
        )
        .length;

    if (group.units.length == groupState.amountCompletedUnits) {
      groupState.state = GroupEnum.complete;
    } else if (groupState.amountCompletedUnits > 0) {
      groupState.state = GroupEnum.inProgress;
    } else if (groupState.amountCompletedUnits == 0) {
      groupState.state = GroupEnum.initial;
    }

    if (group.dependsOn != -1) {
      groupState.dependingGroup = phase.groups.firstWhere(
        (element) => group.dependsOn == element.groupId,
        orElse: () => const PpGroup(),
      );
      if (groupState.dependingGroup == const PpGroup()) {
        sprint("Could not find depending group, unlocked by default now",
            style: PrintStyle.praxisPhase, caller: PpBlocUtil);
        groupState.isLocked = false;
      } else {
        if (getDynamicGroupState(groupState.dependingGroup, phase).state !=
            GroupEnum.complete) {
          if (!ppUserProgressBloc.state.skippedGroups.contains(group.groupId)) {
            groupState.isLocked = true;
          }
        }
      }
    }
    return groupState;
  }

  static bool userHasSelectedPhase() =>
      ppUserProgressBloc.state.phaseSelection != -1;

  static Future<bool> userSendData(
    String matNumber,
    String lastName,
    int phaseId,
  ) async {
    try {
      var map = {
        'lastName': lastName,
        'matNumber': matNumber,
        'phaseId': '$phaseId',
      };
      debugPrint(map.toString());
      await httpRepo
          .post('$ppUrl/completions', body: utf8.encode(jsonEncode(map)))
          .then((value) {
        debugPrint(value.statusCode.toString());
        if (value.statusCode == 201) {
          PpBlocUtil.ppUserProgressBloc.add(PpSuccessfulSendEvent(phaseId));
        }
      });
      return Future.value(true);
    } on Exception catch (e) {
      debugPrint(e.toString());
      return Future.value(false);
    }
  }

  static bool successFullSend(int phaseId) =>
      PpBlocUtil.ppUserProgressBloc.state.successfulSent.containsKey(phaseId);

  static PpPhase getUserSelectedPhase() => ppModelBloc.state.phases.firstWhere(
        (element) => element.phaseId == ppUserProgressBloc.state.phaseSelection,
        orElse: () => const PpPhase(),
      );

  static dynamic timeFunction(VoidCallback function) {
    var sw = Stopwatch()..start();
    var functionReturn = function();
    sw.stop();
    var stringLog = ""
        "{$function} "
        "Millisekunden: ${sw.elapsedMilliseconds.toString()} "
        "Mikrosekunden: ${sw.elapsedMicroseconds.toString()},";
    sprint(stringLog, style: PrintStyle.attention, prefix: "[TIME]");
    return functionReturn;
  }
}

class UnitState {
  UnitEnum state = UnitEnum.initial;
  int amountCorrectAnswers = 0;
  int amountWrongAnswers = 0;
  int amountUncheckedEntries = 0;
}

enum UnitEnum {
  initial,
  level1Complete,
  level2Failed,
  level2Complete,
}

class GroupState {
  GroupEnum state = GroupEnum.initial;
  int amountCompletedUnits = 0;
  bool isLocked = false;
  PpGroup dependingGroup = const PpGroup();
}

enum GroupEnum {
  initial,
  inProgress,
  complete,
  locked,
}

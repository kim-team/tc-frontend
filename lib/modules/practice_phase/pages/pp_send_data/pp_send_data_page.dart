/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../model/pp_phase.dart';
import 'pp_send_data_button.dart';
import 'pp_send_form.dart';

class PpSendDataPageArguments {
  PpSendDataPageArguments(this.selectedPhase);
  final PpPhase selectedPhase;
}

class PpSendDataPage extends StatelessWidget {
  final _matController = TextEditingController();
  final _nameController = TextEditingController();

  final PpPhase selectedPhase;

  PpSendDataPage({
    Key? key,
    required this.selectedPhase,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
        resizeToAvoidBottomInset: true,
        appBar: AppBar(
          title: Text(
            FlutterI18n.translate(
              context,
              'modules.practice_phase.pp_finished',
            ),
          ),
        ),
        body: SafeArea(
          bottom: true,
          child: Column(
            children: [
              Expanded(
                child: SingleChildScrollView(
                  physics: const BouncingScrollPhysics(),
                  child: Container(
                    margin: const EdgeInsets.only(top: 10),
                    child: PpSendForm(
                      matController: _matController,
                      nameController: _nameController,
                      selectedPhase: selectedPhase,
                    ),
                  ),
                ),
              ),
              PpSendDataButton(
                matController: _matController,
                nameController: _nameController,
                selectedPhase: selectedPhase,
              ),
            ],
          ),
        ),
      );
}

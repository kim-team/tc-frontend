/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../../../common/constants/routing_constants.dart';
import '../../../../common/tc_theme.dart';
import '../../../../common/widgets/tc_button.dart';
import '../../model/pp_phase.dart';
import 'pp_verify_data_page.dart';

class PpSendDataButton extends StatefulWidget {
  final TextEditingController matController;
  final TextEditingController nameController;
  final PpPhase selectedPhase;

  PpSendDataButton({
    Key? key,
    required this.matController,
    required this.nameController,
    required this.selectedPhase,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => PpSendDataState();
}

class PpSendDataState extends State<PpSendDataButton> {
  ValueNotifier<bool> valid = ValueNotifier<bool>(false);

  @override
  void initState() {
    super.initState();
    widget.matController.addListener(_hasValidInput);
    widget.nameController.addListener(_hasValidInput);
  }

  @override
  Widget build(BuildContext context) => Container(
        margin: const EdgeInsets.all(12),
        child: ValueListenableBuilder<bool>(
          valueListenable: valid,
          builder: (context, value, child) => TCButton(
            onPressedCallback: value
                ? () {
                    FocusScope.of(context).unfocus();
                    Navigator.pushNamed(
                      context,
                      ppVerifyDataRoute,
                      arguments: PpVerifyDataArguments(
                        lastName: widget.nameController.text,
                        matNumber: widget.matController.text,
                        selectedPhase: widget.selectedPhase,
                      ),
                    );
                  }
                : null,
            elevation: 5,
            buttonLabel: FlutterI18n.translate(
              context,
              'modules.practice_phase.continue',
            ),
            backgroundColor: CurrentTheme().tcBlue,
          ),
        ),
      );

  void _hasValidInput() {
    if (widget.matController.text.trim().length >= 4 &&
        widget.nameController.text.trim().isNotEmpty) {
      valid.value = true;
      return;
    }
    valid.value = false;
  }
}

/*
 * Copyright 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';

import '../../bloc/pp_user_progress/pp_user_progress_bloc.dart';
import '../../model/pp_phase.dart';
import '../../utility/pp_bloc_util.dart';

class PpGroupBodyDebugOptions extends StatelessWidget {
  final ValueNotifier<bool> showDebug;
  final PpPhase selectedPhase;

  const PpGroupBodyDebugOptions({
    Key? key,
    required this.showDebug,
    required this.selectedPhase,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          ValueListenableBuilder<bool>(
            valueListenable: showDebug,
            builder: (context, value, child) => !value
                ? Container()
                : TextButton(
                    child: Text("Alles abschließen"),
                    onPressed: () => selectedPhase.groups
                        .expand((e) => e.units)
                        .expand((e) => [
                              PpCheckAllUnitEntriesEvent(e),
                              PpSetAllQuestionsToCorrectEvent(e),
                            ])
                        .forEach(PpBlocUtil.addProgressEvent),
                  ),
          ),
          ValueListenableBuilder<bool>(
            valueListenable: showDebug,
            builder: (context, value, child) => !value
                ? Container()
                : TextButton(
                    child: Text("Alles zurücksetzen"),
                    onPressed: () => selectedPhase.groups
                        .expand((e) => e.units)
                        .expand((e) => [
                              PpRemoveQuestionResultsEvent(e),
                              PpUncheckAllUnitEntriesEvent(e),
                            ])
                        .forEach(PpBlocUtil.addProgressEvent),
                  ),
          ),
        ],
      );
}

/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:math';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../../../common/tc_theme.dart';
import '../../../../common/tc_utility_functions.dart' as tc_util;
import '../../../../common/widgets/tc_button.dart';
import '../../../../common/widgets/tc_cached_image.dart';
import '../../../../common/widgets/tc_popup_widgets.dart';
import '../../model/pp_answer.dart';
import 'pp_quiz_logic.dart';
import 'pp_quiz_page.dart';

extension QuizWidgets on PpQuizPageState {
  Center buildUnableToDisplayQuestionWidget(BuildContext context) => Center(
        child: Container(
          margin: EdgeInsets.all(16),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                FlutterI18n.translate(
                  context,
                  'modules.practice_phase.questions_problem',
                  translationParams: {
                    "faulty_question": isFaultyQuestion().toString(),
                  },
                ),
              ),
              TCButton(
                buttonLabel: FlutterI18n.translate(
                  context,
                  'common.actions.continue',
                ),
                onPressedCallback: chooseAnswer(null),
              ),
            ],
          ),
        ),
      );

  ValueListenableBuilder<double> buildQuizContent() => ValueListenableBuilder(
        valueListenable: translateAnimation,
        child: ValueListenableBuilder<bool>(
          valueListenable: answerChosen,
          builder: (context, value, child) => GestureDetector(
            onTap: value
                ? () {
                    if (translateController.velocity == 0.0) {
                      translateController.animateTo(1.0,
                          duration: translateDuration);
                    }
                  }
                : null,
            child: Column(
              children: [
                Container(height: 16),
                Expanded(
                  child: buildQuestionBlock(context),
                ),
                Container(height: 12),
                buildAnswers(),
              ],
            ),
          ),
        ),
        builder: (context, value, child) => Container(
          margin: EdgeInsets.symmetric(horizontal: 16),
          child: Transform.translate(
            offset: Offset(value * 500, 0),
            child: child,
          ),
        ),
      );

  Center buildNoQuestionsFound(BuildContext context) => Center(
          child: Text(
        FlutterI18n.translate(
          context,
          'modules.practice_phase.no_questions_found',
          translationParams: {"error_string": errors.toString()},
        ),
        textAlign: TextAlign.center,
      ));

  Widget buildQuizPopup(BuildContext context) =>
      PopupMenuButton<QuizPopUpSelection>(
        onSelected: (result) {
          switch (result) {
            case QuizPopUpSelection.animationSpeed:
              ppSettingsFastAnimations.value = !ppSettingsFastAnimations.value;
              updateAnimationSettings();
              setSettings();
              break;
            case QuizPopUpSelection.vibration:
              ppSettingsEnableVibrations.value =
                  !ppSettingsEnableVibrations.value;
              setSettings();
              break;
            case QuizPopUpSelection.reset:
              reset();
              break;
          }
        },
        itemBuilder: (_) => [
          TcPopupWidgets.buildCheckedPopupItem(
            ppSettingsFastAnimations,
            QuizPopUpSelection.animationSpeed,
            FlutterI18n.translate(
              context,
              'modules.practice_phase.fast_animation',
            ),
          ),
          PopupMenuDivider(),
          TcPopupWidgets.buildCheckedPopupItem(
            ppSettingsEnableVibrations,
            QuizPopUpSelection.vibration,
            FlutterI18n.translate(
              context,
              'modules.practice_phase.vibration',
            ),
          ),
          if (!kReleaseMode) PopupMenuDivider(),
          if (!kReleaseMode)
            TcPopupWidgets.buildTextPopupItem(
              value: QuizPopUpSelection.reset,
              title: "Reset",
              subtitle: "Debug Only",
            ),
          if (!kReleaseMode) PopupMenuDivider(),
          if (!kReleaseMode)
            PopupMenuItem(
              value: QuizPopUpSelection.reset,
              child: ValueListenableBuilder<double>(
                valueListenable: animController,
                builder: (context, value, child) => Column(
                  children: [
                    Slider(
                      onChanged: (value) {
                        animController.value = value;
                      },
                      value: animController.value,
                    ),
                  ],
                ),
              ),
            ),
          if (!kReleaseMode) PopupMenuDivider(),
          if (!kReleaseMode)
            PopupMenuItem(
              value: QuizPopUpSelection.reset,
              child: ValueListenableBuilder<double>(
                valueListenable: quizFlipController,
                builder: (context, value, child) => Column(
                  children: [
                    Slider(
                      onChanged: (value) {
                        quizFlipController.value = value;
                      },
                      value: quizFlipController.value,
                    ),
                  ],
                ),
              ),
            ),
          if (!kReleaseMode) PopupMenuDivider(),
          if (!kReleaseMode)
            PopupMenuItem(
              value: QuizPopUpSelection.reset,
              child: ValueListenableBuilder<double>(
                valueListenable: translateController,
                builder: (context, value, child) => Column(
                  children: [
                    Slider(
                      onChanged: (value) {
                        translateController.value = value;
                      },
                      value: translateController.value,
                    ),
                  ],
                ),
              ),
            ),
        ],
      );

  Column buildAnswers() => Column(
        children: [
          Row(
            children: [
              buildAnswerWidget(
                  copiedUnit.questions[selectedQuestion].answers[0], 1),
              Container(width: 12),
              buildAnswerWidget(
                  copiedUnit.questions[selectedQuestion].answers[1], 2),
              if (copiedUnit.questions[selectedQuestion].answers.length == 3)
                Container(width: 12),
              if (copiedUnit.questions[selectedQuestion].answers.length == 3)
                buildAnswerWidget(
                    copiedUnit.questions[selectedQuestion].answers[2], 3),
            ],
          ),
          if (copiedUnit.questions[selectedQuestion].answers.length == 4)
            Container(height: 12),
          if (copiedUnit.questions[selectedQuestion].answers.length == 4)
            Row(
              children: [
                buildAnswerWidget(
                    copiedUnit.questions[selectedQuestion].answers[2], 3),
                Container(width: 12),
                buildAnswerWidget(
                    copiedUnit.questions[selectedQuestion].answers[3], 4),
              ],
            ),
          Container(height: 16),
        ],
      );

  Widget buildAnswerWidget(PpAnswer answer, int answerNumber) => Expanded(
        child: AnimatedBuilder(
          animation: animController,
          builder: (context, child) {
            final correct =
                isCorrectAnswer(copiedUnit.questions[selectedQuestion], answer);
            final isChosen =
                answerIdChosen.value == answer.answerId ? true : false;
            var answerTranslateAnimation = animation.translateAnswer1;
            switch (answerNumber) {
              case 1:
                answerTranslateAnimation = animation.translateAnswer1;
                break;
              case 2:
                answerTranslateAnimation = animation.translateAnswer2;
                break;
              case 3:
                answerTranslateAnimation = animation.translateAnswer3;
                break;
              case 4:
                answerTranslateAnimation = animation.translateAnswer4;
                break;
              default:
                answerTranslateAnimation = animation.translateAnswer1;
            }
            return Transform.scale(
              scale: isChosen || correct
                  ? animation.chosenAnswerScale.value
                  : 1 / animation.chosenAnswerScale.value,
              child: Transform.translate(
                offset: Offset(0, answerTranslateAnimation.value * 200),
                child: AbsorbPointer(
                  absorbing: answerChosen.value || animController.value < 0.45,
                  child: child,
                ),
              ),
            );
          },
          child: ValueListenableBuilder<bool>(
            valueListenable: answerChosen,
            builder: (context, value, child) {
              final correct = isCorrectAnswer(
                  copiedUnit.questions[selectedQuestion], answer);
              final isChosen =
                  answerIdChosen.value == answer.answerId ? true : false;
              return TCButton(
                buttonLabel: tc_util.read(answer.body, context),
                onPressedCallback: chooseAnswer(answer),
                elevation: 4.0,
                borderColor: !value
                    ? CurrentTheme().themeData.primaryColor
                    : correct
                        ? CorporateColors.ppAnswerCorrectGreen
                        : answerIdChosen.value == answer.answerId
                            ? CorporateColors.ppAnswerWrongRed
                            : Colors.transparent,
                borderThickness:
                    answerIdChosen.value == answer.answerId ? 4 : 0,
                backgroundColor: !value
                    ? CurrentTheme().themeData.primaryColor
                    : correct
                        ? CorporateColors.ppAnswerCorrectGreen
                        : isChosen
                            ? CorporateColors.ppAnswerWrongRed
                            : CurrentTheme().themeData.primaryColor,
                noAnimationDuration: false,
                customPadding:
                    EdgeInsets.symmetric(horizontal: 4.0, vertical: 4),
                customHeight: 64,
                useAutoSizeText: true,
                customFontStyle:
                    CurrentTheme().themeData.textTheme.bodyText1?.copyWith(
                          color: !value
                              ? null
                              : correct
                                  ? CurrentTheme().themeData.primaryColor
                                  : isChosen
                                      ? CurrentTheme().themeData.primaryColor
                                      : null,
                        ),
              );
            },
          ),
        ),
      );

  Widget buildQuestionBlock(BuildContext context) => AnimatedBuilder(
        animation: quizFlipController,
        builder: (context, child) => Transform(
          alignment: FractionalOffset.center,
          transform: perspective.scaled(1.0, 1.0, 1.0)
            ..rotateY(pi - quizFlipAnimation.flipRotation.value * pi / 180)
            ..rotateX(0.0)
            ..rotateZ(0.0),
          child: Transform.scale(
            scale: 0.9 + (quizFlipAnimation.questionBodyOpacity.value / 10),
            child: child,
          ),
        ),
        child: AnimatedBuilder(
          animation: animController,
          builder: (context, child) => Transform.translate(
            offset: correctAnswerChosen.value
                ? Offset(0, 0)
                : Offset(animation.wrongShakeHead.value * 2, 0),
            child: Material(
              elevation: 4,
              shape: RoundedRectangleBorder(
                side: BorderSide(
                    color: correctAnswerChosen.value
                        ? animation.correctBorderColor.value
                        : animation.wrongBorderColor.value!,
                    width: 4),
                borderRadius: BorderRadius.all(Radius.circular(8.0)),
              ),
              color: CurrentTheme().themeData.primaryColor,
              clipBehavior: Clip.antiAlias,
              child: child,
            ),
          ),
          child: Center(
            child: Stack(
              clipBehavior: Clip.none,
              alignment: Alignment.center,
              children: [
                buildUnitImage(),
                ValueListenableBuilder<bool>(
                  valueListenable: questionHasImage,
                  builder: (context, value, child) =>
                      value ? buildQuestionImage() : Container(),
                ),
                buildQuestionNumber(context),
                buildQuestionAndNoticeBlock(context),
                buildResultIndicator(),
              ],
            ),
          ),
        ),
      );

  Widget buildResultIndicator() => Positioned(
        top: 12,
        child: ValueListenableBuilder<double>(
          valueListenable: animController,
          builder: (context, value, child) => Transform.scale(
            scale: animation.iconScale.value,
            child: child,
          ),
          child: ValueListenableBuilder<bool>(
            valueListenable: correctAnswerChosen,
            builder: (context, value, child) => Container(
                width: 64,
                height: 64,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.white, // always white, on purpose
                  boxShadow: [
                    BoxShadow(
                      color: CurrentTheme().themeData.primaryColor,
                      blurRadius: 4,
                    )
                  ],
                ),
                padding: EdgeInsets.all(12.0),
                child: correctAnswerChosen.value
                    ? Image.asset(
                        // TODO: Move to asset adapter
                        "assets/images/praxis_phase/correct.png",
                        fit: BoxFit.contain,
                      )
                    : Image.asset(
                        // TODO: Move to asset adapter
                        "assets/images/praxis_phase/wrong.png",
                        fit: BoxFit.contain,
                      )),
          ),
        ),
      );

  Widget buildCorrectOrWrongBodyText() => AnimatedSize(
        alignment: Alignment.topCenter,
        curve: Curves.easeOut,
        duration: animatedSizeDuration,
        child: ValueListenableBuilder<double>(
          valueListenable: animController,
          builder: (context, value, child) => (!answerChosen.value &&
                  animController.value <= 0.50)
              ? Container()
              : !tc_util.hasContent(
                      correctAnswerChosen.value
                          ? copiedUnit.questions[selectedQuestion].bodyCorrect
                          : copiedUnit.questions[selectedQuestion].bodyWrong,
                      context)
                  ? Container()
                  : Column(
                      children: [
                        Container(height: 6),
                        Text(
                          tc_util.read(
                              correctAnswerChosen.value
                                  ? copiedUnit
                                      .questions[selectedQuestion].bodyCorrect
                                  : copiedUnit
                                      .questions[selectedQuestion].bodyWrong,
                              context),
                          style: CurrentTheme()
                              .themeData
                              .textTheme
                              .bodyText1
                              ?.copyWith(
                                  color: correctAnswerChosen.value
                                      ? CorporateColors.ppAnswerCorrectGreen
                                      : CorporateColors.ppAnswerWrongRed),
                          textAlign: TextAlign.center,
                        ),
                        Container(height: 6),
                      ],
                    ),
        ),
      );

  Positioned buildQuestionAndNoticeBlock(BuildContext context) =>
      questionHasImage.value
          ? Positioned(
              bottom: 0,
              left: 0,
              right: 0,
              child: AnimatedBuilder(
                  animation: quizFlipController,
                  builder: (context, child) => Transform.scale(
                        scale: quizFlipAnimation.questionBodyOpacity.value,
                        child: Container(
                          color: CurrentTheme()
                              .themeData
                              .primaryColor
                              .withOpacity(0.95),
                          child: Container(
                            margin: EdgeInsets.only(
                                left: 42, right: 42, bottom: 12, top: 8),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  tc_util.read(
                                      copiedUnit
                                          .questions[selectedQuestion].body,
                                      context),
                                  style: CurrentTheme()
                                      .themeData
                                      .textTheme
                                      .headline4,
                                  textAlign: TextAlign.center,
                                ),
                                buildCorrectOrWrongBodyText(),
                              ],
                            ),
                          ),
                        ),
                      )),
            )
          : Positioned.fill(
              child: AnimatedBuilder(
                animation: quizFlipController,
                builder: (context, child) => Transform.scale(
                    scale: quizFlipAnimation.questionBodyOpacity.value,
                    child: child),
                child: Container(
                  margin: EdgeInsets.symmetric(horizontal: 42.0),
                  child: Center(
                    child: Wrap(
                      alignment: WrapAlignment.center,
                      children: [
                        Text(
                          tc_util.read(
                              copiedUnit.questions[selectedQuestion].body,
                              context),
                          style: CurrentTheme().themeData.textTheme.headline2,
                          textAlign: TextAlign.center,
                        ),
                        buildCorrectOrWrongBodyText(),
                      ],
                    ),
                  ),
                ),
              ),
            );

  Positioned buildQuestionNumber(BuildContext context) => Positioned(
        bottom: 0,
        left: 0,
        right: 0,
        child: ValueListenableBuilder<double>(
          valueListenable: quizFlipAnimation.questionNumberOpacity,
          builder: (context, value, child) => Transform.scale(
            scale: value,
            child: child,
          ),
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 16.0),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(8),
                  bottomRight: Radius.circular(8)),
              color: CurrentTheme().themeData.colorScheme.secondary,
            ),
            child: Text(
              FlutterI18n.translate(
                context,
                'modules.practice_phase.question_number',
                translationParams: {
                  "number": (selectedQuestion + 1).toString(),
                },
              ),
              style: CurrentTheme()
                  .themeData
                  .textTheme
                  .headline3
                  ?.copyWith(color: CurrentTheme().themeData.primaryColor),
              textAlign: TextAlign.center,
            ),
          ),
        ),
      );

  Positioned buildQuestionImage() => Positioned.fill(
        child: ValueListenableBuilder<double>(
          valueListenable: quizFlipAnimation.questionNumberOpacity,
          child: TcCachedImage(
            isRounded: true,
            url: copiedUnit.image,
          ),
          builder: (context, value, child) => Transform.scale(
            scale: 1,
            child: child,
          ),
        ),
      );

  Positioned buildUnitImage() => Positioned.fill(
        child: AnimatedBuilder(
          animation: quizFlipController,
          child: TcCachedImage(
            isRounded: true,
            url: copiedUnit.image,
          ),
          builder: (context, child) => Transform.scale(
            scale: quizFlipAnimation.questionNumberOpacity.value,
            child: child,
          ),
        ),
      );
}

/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';

import '../../../../common/tc_theme.dart';

class QuizAnimation {
  QuizAnimation({
    required this.controller,
  })  : translateQuestion = TweenSequence(<TweenSequenceItem<double>>[
          TweenSequenceItem<double>(
            tween: Tween<double>(begin: 1.0, end: 0.0)
                .chain(CurveTween(curve: Curves.easeOutExpo)),
            weight: 5.0,
          ),
          TweenSequenceItem<double>(
            tween: ConstantTween<double>(0.0),
            weight: 90.0,
          ),
          TweenSequenceItem<double>(
            tween: Tween<double>(begin: 0.0, end: -1.0)
                .chain(CurveTween(curve: Curves.easeInExpo)),
            weight: 5.0,
          ),
        ]).animate(
          CurvedAnimation(
            parent: controller,
            curve: Interval(0.0, 1.0, curve: Curves.linear),
          ),
        ),
        translateAnswer1 = TweenSequence(<TweenSequenceItem<double>>[
          TweenSequenceItem<double>(
            tween: Tween<double>(begin: 1, end: 0.2)
                .chain(CurveTween(curve: Curves.easeIn)),
            weight: 5.0,
          ),
          TweenSequenceItem<double>(
            tween: Tween<double>(begin: 0.2, end: -0.025)
                .chain(CurveTween(curve: Curves.easeOut)),
            weight: 2.5,
          ),
          TweenSequenceItem<double>(
            tween: Tween<double>(begin: -0.025, end: 0)
                .chain(CurveTween(curve: Curves.easeIn)),
            weight: 2.5,
          ),
        ]).animate(
          CurvedAnimation(
            parent: controller,
            curve: Interval(0.35, 0.42, curve: Curves.linear),
          ),
        ),
        translateAnswer2 = TweenSequence(<TweenSequenceItem<double>>[
          TweenSequenceItem<double>(
            tween: Tween<double>(begin: 1, end: 0.2)
                .chain(CurveTween(curve: Curves.easeIn)),
            weight: 5.0,
          ),
          TweenSequenceItem<double>(
            tween: Tween<double>(begin: 0.2, end: -0.025)
                .chain(CurveTween(curve: Curves.easeOut)),
            weight: 2.5,
          ),
          TweenSequenceItem<double>(
            tween: Tween<double>(begin: -0.025, end: 0)
                .chain(CurveTween(curve: Curves.easeIn)),
            weight: 2.5,
          ),
        ]).animate(
          CurvedAnimation(
            parent: controller,
            curve: Interval(0.36, 0.43, curve: Curves.linear),
          ),
        ),
        translateAnswer3 = TweenSequence(<TweenSequenceItem<double>>[
          TweenSequenceItem<double>(
            tween: Tween<double>(begin: 1, end: 0.2)
                .chain(CurveTween(curve: Curves.easeIn)),
            weight: 5.0,
          ),
          TweenSequenceItem<double>(
            tween: Tween<double>(begin: 0.2, end: -0.025)
                .chain(CurveTween(curve: Curves.easeOut)),
            weight: 2.5,
          ),
          TweenSequenceItem<double>(
            tween: Tween<double>(begin: -0.025, end: 0)
                .chain(CurveTween(curve: Curves.easeIn)),
            weight: 2.5,
          ),
        ]).animate(
          CurvedAnimation(
            parent: controller,
            curve: Interval(0.37, 0.44, curve: Curves.linear),
          ),
        ),
        translateAnswer4 = TweenSequence(<TweenSequenceItem<double>>[
          TweenSequenceItem<double>(
            tween: Tween<double>(begin: 1, end: 0.2)
                .chain(CurveTween(curve: Curves.easeIn)),
            weight: 5.0,
          ),
          TweenSequenceItem<double>(
            tween: Tween<double>(begin: 0.2, end: -0.025)
                .chain(CurveTween(curve: Curves.easeOut)),
            weight: 2.5,
          ),
          TweenSequenceItem<double>(
            tween: Tween<double>(begin: -0.025, end: 0)
                .chain(CurveTween(curve: Curves.easeIn)),
            weight: 2.5,
          ),
        ]).animate(
          CurvedAnimation(
            parent: controller,
            curve: Interval(0.38, 0.45, curve: Curves.linear),
          ),
        ),
        correctBorderColor = TweenSequence<Color>(
          [
            TweenSequenceItem(
              tween: ConstantTween<Color>(Colors.transparent),
              weight: 1.0,
            ),
            TweenSequenceItem(
              tween: ConstantTween<Color>(CorporateColors.ppAnswerCorrectGreen),
              weight: 99.0,
            ),
          ],
        ).animate(
          CurvedAnimation(
            parent: controller,
            curve: Interval(0.5, 1.0, curve: Curves.easeOut),
          ),
        ),
        correctFillColor = TweenSequence<Color?>(
          [
            TweenSequenceItem(
              tween: ColorTween(
                begin: CurrentTheme().themeData.primaryColor,
                end: CorporateColors.ppAnswerCorrectGreen,
              ).chain(CurveTween(curve: Curves.easeOut)),
              weight: 100.0,
            ),
          ],
        ).animate(
          CurvedAnimation(
            parent: controller,
            curve: Interval(0.5, 0.51, curve: Curves.linear),
          ),
        ),
        correctFontColor = TweenSequence<Color?>(
          [
            TweenSequenceItem(
              tween: ColorTween(
                begin: CurrentTheme().textSoftWhite,
                end: Colors.white,
              ).chain(CurveTween(curve: Curves.easeOut)),
              weight: 100.0,
            ),
          ],
        ).animate(
          CurvedAnimation(
            parent: controller,
            curve: Interval(0.5, 0.51, curve: Curves.linear),
          ),
        ),
        wrongFontColor = TweenSequence<Color?>(
          [
            TweenSequenceItem(
              tween: ColorTween(
                      begin: CurrentTheme().textSoftWhite, end: Colors.white)
                  .chain(CurveTween(curve: Curves.easeOut)),
              weight: 100.0,
            ),
          ],
        ).animate(
          CurvedAnimation(
            parent: controller,
            curve: Interval(0.5, 0.51, curve: Curves.linear),
          ),
        ),
        wrongBorderColor = TweenSequence<Color?>(
          [
            TweenSequenceItem(
              tween: ColorTween(
                begin: Colors.transparent,
                end: CorporateColors.ppAnswerWrongRed,
              ),
              weight: 100.0,
            ),
          ],
        ).animate(
          CurvedAnimation(
            parent: controller,
            curve: Interval(0.5, 0.51, curve: Curves.easeOut),
          ),
        ),
        wrongFillColor = TweenSequence<Color?>(
          [
            TweenSequenceItem(
              tween: ColorTween(
                begin: CurrentTheme().themeData.primaryColor,
                end: CorporateColors.ppAnswerWrongRed,
              ).chain(CurveTween(curve: Curves.easeOut)),
              weight: 10.0,
            ),
          ],
        ).animate(
          CurvedAnimation(
            parent: controller,
            curve: Interval(0.5, 0.51, curve: Curves.linear),
          ),
        ),
        wrongFillColorLFO = TweenSequence<Color?>(
          [
            TweenSequenceItem(
              tween: ColorTween(
                begin: CurrentTheme().themeData.primaryColor,
                end: CorporateColors.ppAnswerWrongRed,
              ).chain(CurveTween(curve: Curves.easeOut)),
              weight: 10.0,
            ),
          ],
        ).animate(
          CurvedAnimation(
            parent: controller,
            curve: Interval(0.5, 0.6, curve: Curves.linear),
          ),
        ),
        wrongShakeHead = TweenSequence(<TweenSequenceItem<double>>[
          TweenSequenceItem<double>(
            tween: Tween<double>(begin: 0.0, end: -1.0)
                .chain(CurveTween(curve: Curves.easeOut)),
            weight: 20.0,
          ),
          TweenSequenceItem<double>(
            tween: Tween<double>(begin: -1.0, end: 1.0)
                .chain(CurveTween(curve: Curves.easeOut)),
            weight: 20.0,
          ),
          TweenSequenceItem<double>(
            tween: Tween<double>(begin: 1.0, end: -1.0)
                .chain(CurveTween(curve: Curves.easeOut)),
            weight: 20.0,
          ),
          TweenSequenceItem<double>(
            tween: Tween<double>(begin: -1.0, end: 1.0)
                .chain(CurveTween(curve: Curves.easeOut)),
            weight: 20.0,
          ),
          TweenSequenceItem<double>(
            tween: Tween<double>(begin: 1.0, end: 0.0)
                .chain(CurveTween(curve: Curves.easeOut)),
            weight: 20.0,
          ),
        ]).animate(
          CurvedAnimation(
            parent: controller,
            curve: Interval(0.5, 0.575, curve: Curves.linear),
          ),
        ),
        selectedAnswerScale = TweenSequence(<TweenSequenceItem<double>>[
          TweenSequenceItem<double>(
            tween: Tween<double>(begin: 1.0, end: 0.9)
                .chain(CurveTween(curve: Curves.easeOut)),
            weight: 20.0,
          ),
          TweenSequenceItem<double>(
            tween: ConstantTween<double>(0.9),
            weight: 60.0,
          ),
          TweenSequenceItem<double>(
            tween: Tween<double>(begin: 0.9, end: 1.0)
                .chain(CurveTween(curve: Curves.easeOut)),
            weight: 20.0,
          ),
        ]).animate(
          CurvedAnimation(
            parent: controller,
            curve: Interval(0.0, 1.0, curve: Curves.easeOut),
          ),
        ),
        borderThickness = TweenSequence(<TweenSequenceItem<double>>[
          TweenSequenceItem<double>(
            tween: ConstantTween<double>(0.0),
            weight: 50.0,
          ),
          TweenSequenceItem<double>(
            tween: Tween<double>(begin: 0.0, end: 4.0)
                .chain(CurveTween(curve: Curves.easeOut)),
            weight: 7.5,
          ),
          TweenSequenceItem<double>(
            tween: ConstantTween<double>(4.0),
            weight: 42.5,
          ),
        ]).animate(
          CurvedAnimation(
            parent: controller,
            curve: Interval(0.0, 1.0, curve: Curves.linear),
          ),
        ),
        iconOpacity = TweenSequence(<TweenSequenceItem<double>>[
          TweenSequenceItem<double>(
            tween: Tween<double>(begin: 0.0, end: 1.0)
                .chain(CurveTween(curve: Curves.easeOut)),
            weight: 100.0,
          ),
        ]).animate(
          CurvedAnimation(
            parent: controller,
            curve: Interval(0.5, 0.6, curve: Curves.linear),
          ),
        ),
        iconScale = TweenSequence(<TweenSequenceItem<double>>[
          TweenSequenceItem<double>(
            tween: Tween<double>(begin: 0.0, end: 1.6)
                .chain(CurveTween(curve: Curves.easeIn)),
            weight: 5.0,
          ),
          TweenSequenceItem<double>(
            tween: Tween<double>(begin: 1.6, end: 1.0)
                .chain(CurveTween(curve: Curves.elasticOut)),
            weight: 95.0,
          ),
        ]).animate(
          CurvedAnimation(
            parent: controller,
            curve: Interval(0.575, 0.85, curve: Curves.linear),
          ),
        ),
        chosenAnswerScale = TweenSequence(<TweenSequenceItem<double>>[
          TweenSequenceItem<double>(
            tween: Tween<double>(begin: 1.0, end: 1.04)
                .chain(CurveTween(curve: Curves.easeOut)),
            weight: 10.0,
          ),
        ]).animate(
          CurvedAnimation(
            parent: controller,
            curve: Interval(0.5, 0.525, curve: Curves.linear),
          ),
        );

  final AnimationController controller;
  final Animation<double> translateQuestion;
  final Animation<double> translateAnswer1;
  final Animation<double> translateAnswer2;
  final Animation<double> translateAnswer3;
  final Animation<double> translateAnswer4;
  final Animation<Color> correctBorderColor;
  final Animation<Color?> correctFontColor;
  final Animation<Color?> correctFillColor;
  final Animation<Color?> wrongFontColor;
  final Animation<Color?> wrongBorderColor;
  final Animation<Color?> wrongFillColor;
  final Animation<Color?> wrongFillColorLFO;
  final Animation<double> wrongShakeHead;
  final Animation<double> selectedAnswerScale;
  final Animation<double> borderThickness;
  final Animation<double> iconOpacity;
  final Animation<double> iconScale;
  final Animation<double> chosenAnswerScale;
}

class QuizRotationAnimation {
  QuizRotationAnimation({
    required this.controller,
  })  : questionNumberOpacity = TweenSequence(<TweenSequenceItem<double>>[
          TweenSequenceItem<double>(
            tween: ConstantTween<double>(1),
            weight: 50.0,
          ),
          TweenSequenceItem<double>(
            tween: ConstantTween<double>(0),
            weight: 50.0,
          ),
        ]).animate(
          CurvedAnimation(
            parent: controller,
            curve: Interval(0.0, 0.4, curve: Curves.linear),
          ),
        ),
        questionBodyOpacity = TweenSequence(<TweenSequenceItem<double>>[
          TweenSequenceItem<double>(
            tween: ConstantTween<double>(0),
            weight: 50.0,
          ),
          TweenSequenceItem<double>(
            tween: ConstantTween<double>(1),
            weight: 50.0,
          ),
        ]).animate(
          CurvedAnimation(
            parent: controller,
            curve: Interval(0.2, 0.201, curve: Curves.linear),
          ),
        ),
        flipRotation = TweenSequence(<TweenSequenceItem<double>>[
          TweenSequenceItem<double>(
            tween: Tween<double>(begin: -180, end: -90)
                .chain(CurveTween(curve: Curves.easeIn)),
            weight: 0.2,
          ),
          TweenSequenceItem<double>(
            tween: Tween<double>(begin: 90, end: 180)
                .chain(CurveTween(curve: Curves.elasticOut)),
            weight: 0.8,
          ),
        ]).animate(
          CurvedAnimation(
            parent: controller,
            curve: Interval(0.0, 1.0, curve: Curves.linear),
          ),
        ),
        yFlipRotation = TweenSequence(<TweenSequenceItem<double>>[
          TweenSequenceItem<double>(
            tween: Tween<double>(begin: 0, end: 0)
                .chain(CurveTween(curve: Curves.linear)),
            weight: 0.2,
          ),
          TweenSequenceItem<double>(
            tween: Tween<double>(begin: 0, end: -2.5)
                .chain(CurveTween(curve: Curves.easeIn)),
            weight: 0.1,
          ),
          TweenSequenceItem<double>(
            tween: Tween<double>(begin: -2.5, end: 0)
                .chain(CurveTween(curve: Curves.easeOut)),
            weight: 0.1,
          ),
          TweenSequenceItem<double>(
            tween: ConstantTween<double>(1),
            weight: 0.6,
          ),
        ]).animate(
          CurvedAnimation(
            parent: controller,
            curve: Interval(0.0, 1.0, curve: Curves.linear),
          ),
        );

  final AnimationController controller;

  final Animation<double> questionNumberOpacity;
  final Animation<double> questionBodyOpacity;
  final Animation<double> flipRotation;
  final Animation<double> yFlipRotation;
}

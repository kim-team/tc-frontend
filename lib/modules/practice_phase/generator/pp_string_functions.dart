/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

// ignore_for_file: lines_longer_than_80_chars

import 'dart:math';

// TODO: Cleanup
class PpStr {
  static String randomImageUrl() =>
      imageUrls[Random().nextInt(imageUrls.length)];

  static String randomWords(
    int amount,
    Language language,
  ) {
    var words = <String>[];
    for (var i = 0; i < amount; i++) {
      final wordRef = language == Language.german ? germanWords : englishWords;
      words.add(wordRef[Random().nextInt(wordRef.length)]);
    }
    return words.join(" ");
  }

  static String markdownText() => markdownExample;

  static String randomText(Language language) {
    var reference = <String>[];
    switch (language) {
      case Language.german:
        reference = germanText;
        break;
      case Language.english:
        reference = englishText;
        break;
      case Language.lorem:
        reference = loremText;
        break;
    }
    return reference[Random().nextInt(reference.length)];
  }
}

const markdownExample =
    "Lorem ipsum dolor sit amet, consetetur sadipscing [tinyCampus Webseite](https://tinycampus.de), sed diam nonumy eirmod tempor invidunt ut labore et dolore magna `aliquyam` erat.\nAt vero eos et accusam et justo duo dolores et ea rebum.\n# Überschrift 1\nLorem ipsum **dolor** sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna ~~aliquyam~~ erat\n## Überschrift 2\nLorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat";

const loremText = [
  "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.",
];
const englishText = [
  "Evening of a hot day started the little wind to moving among the leaves. The shade climbed up the hills toward the top. On the sand banks the rabbits sat as quietly as little gray sculptured stones. And then from the direction of the state highway came the sound of footsteps on crisp sycamore leaves. The rabbits hurried noiselessly for cover.",
  "Either the well was very deep, or she was falling very slowly, because she had plenty of time while she was going down to look around and wonder what was going to happen next. At first she tried to look down and see what was coming, but it was too dark to see anything.",
  "The mouse looked at him, but did not move. Hazel spoke again and the mouse began suddenly to run toward him as the kestrel turned and slid sideways and downward. Hazel hastened back to the hole. Looking out, he saw the mouse following him. When it had almost reached the foot of the bank it scuttered over a fallen twig with two or three green leaves. The twig turned, one of the leaves caught the sunlight slanting through the trees and Hazel saw it flash for an instant.",
];
const germanText = [
  "Entweder war der Brunnen sehr tief, oder sie fiel sehr langsam, denn sie hatte viel Zeit, während sie hinunterging, um sich umzusehen und sich zu fragen, was als nächstes passieren würde. Zuerst versuchte sie, nach unten zu schauen und zu erkennen, was auf sie zukam, aber es war zu dunkel, um etwas zu sehen.",
  "Am Abend eines heißen Tages begann der kleine Wind, sich zwischen den Blättern zu bewegen. Der Schatten kletterte die Hügel hinauf zum Gipfel. Auf den Sandbänken saßen die Kaninchen so ruhig wie kleine graue Skulpturensteine. Und dann kam aus der Richtung des State Highways das Geräusch von Schritten auf knackigen Platanenblättern. Die Kaninchen eilten geräuschlos in Deckung.",
  "Die Maus sah ihn an, bewegte sich aber nicht. Hazel sprach wieder, und die Maus begann plötzlich, auf ihn zuzulaufen, während der Turmfalke sich umdrehte und zur Seite und nach unten. Hazel eilte zurück zum Loch. Als er herausschaute, sah er die Maus die ihm folgte. Als sie fast den Fuß des Ufers erreicht hatte, huschte sie über einen einen heruntergefallenen Zweig mit zwei oder drei grünen Blättern. Der Zweig drehte sich, eines der Blätter fängt das Sonnenlicht ein, das durch die Bäume fällt, und Hazel sieht es für einen Augenblick aufblitzen.",
];

const imageUrls = [
  "https://tinycampus.de/img/app/pp/annika.png",
  "https://tinycampus.de/img/app/pp/hr.png",
  "https://tinycampus.de/img/app/pp/prof.png",
  "https://tinycampus.de/img/team/FabianRudzinski.jpg",
  "https://tinycampus.de/img/team/MauriceBlattmann.jpg",
  "https://tinycampus.de/img/team/AnthonySchuster.jpg",
  "https://tinycampus.de/img/team/MarwinLebensky.jpg",
  "https://tinycampus.de/img/team/JustinSauer.jpg",
  "https://tinycampus.de/img/team/ChristianEpping.jpg",
  "https://tinycampus.de/img/team/AnastasiaRuppel.jpg",
  "https://tinycampus.de/img/team/DustinBecker.jpg",
  "https://tinycampus.de/img/team/DanielKerkmann.jpg",
  "https://tinycampus.de/img/team/FelicitasNeyer.jpg",
  "https://tinycampus.de/img/team/SteffenBock.jpg",
  "https://tinycampus.de/img/team/MarcelMehlmann.jpg",
  "https://tinycampus.de/img/team/LukasWalter.jpg",
  "https://tinycampus.de/img/team/LauraWeber.jpg",
];
const englishWords = [
  "The",
  "Analysis",
  "of",
  "Business",
  "Processes",
  "typically",
  "includes",
  "the",
  "Mapping",
  "or",
  "Modeling",
  "of",
  "Processes",
  "and",
  "Sub-processes",
  "Down",
  "to",
  "Activity",
  "Task",
  "Level",
  "Processes",
  "can",
  "be",
  "Modeled",
  "through",
  "a",
  "Large",
  "Number",
  "Of",
  "Methods",
  "And",
  "Techniques",
  "For",
  "Instance",
  "The",
  "Business",
  "Process",
  "Modeling",
  "Notation",
  "is",
  "a",
  "Business",
  "Process",
  "Modeling",
];
const germanWords = [
  "Business",
  "Prozess",
  "Praxisphase",
  "Studium",
  "Gießen",
  "Friedberg",
  "Wetzlar",
  "Wirtschaftsingenieurwesen",
  "Abschlussphase",
  "Davor",
  "Danach",
  "Mittendrin",
  "Meike",
  "Annika",
  "Praxiesreferat",
  "Unterstützung",
];

enum Language {
  german,
  english,
  lorem,
}

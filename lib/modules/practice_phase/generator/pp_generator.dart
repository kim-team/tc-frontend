/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:math';

import '../model/pp_address.dart';
import '../model/pp_answer.dart';
import '../model/pp_entry.dart';
import '../model/pp_group.dart';
import '../model/pp_media.dart';
import '../model/pp_person.dart';
import '../model/pp_phase.dart';
import '../model/pp_question.dart';
import '../model/pp_unit.dart';
import 'pp_string_functions.dart';

const int randomizedSpread = 3;
const int randomizedGroupSpread = 1;

class PpGenerator {
  static late Set<int> uniqueGroupIds;
  static late Set<int> uniqueUnitIds;
  static late Set<int> uniqueQuestionIds;
  static late Set<int> uniqueAnswerIds;
  static late Set<int> uniqueEntryIds;
  static late Set<int> uniqueMediaIds;
  static late Set<int> uniquePhaseIds;

  static Set<int> uniqueSetOfInts() =>
      List<int>.generate(5000, (var index) => index).toSet();

  static void initLists() {
    uniquePhaseIds = uniqueSetOfInts();
    uniqueGroupIds = uniqueSetOfInts();
    uniqueUnitIds = uniqueSetOfInts();
    uniqueMediaIds = uniqueSetOfInts();
    uniqueEntryIds = uniqueSetOfInts();
    uniqueQuestionIds = uniqueSetOfInts();
    uniqueAnswerIds = uniqueSetOfInts();
  }

  static PpAnswer rAnswer({required int answerId}) =>
      PpAnswer(answerId: answerId, body: randomMediumText());

  static List<PpPhase> rDeepPhases() {
    initLists();
    var phases = <PpPhase>[];
    for (var i = 0; i < 2; i++) {
      // for (var i = 0; i <= 2 + Random().nextInt(randomizedSpread); i++) {
      phases.add(PpPhase(
        phaseId: getFirstAndPop(uniquePhaseIds),
        groups: rulGroups(),
        title: randomMediumText(),
        persons: contacts(),
        description: randomMarkdown(),
        faqUrl: "https://thm.de",
        categoryId: Random().nextInt(180),
      ));
    }
    return phases;
  }

  static List<PpPerson> contacts() => [
        PpPerson(
          fullName: {"de": "Frau Meike Zimmer"},
          description: randomMarkdown(),
          image: "https://tinycampus.de/img/app/pp/annika.png",
          imageAlt: randomMediumText(),
          addresses: [
            PpAddress(
              city: "Friedberg",
              email: "mailto:noreply@tinycampus.de",
              latitude: "50.3303101",
              longitude: "8.7591954",
              phone: "tel:491234566789",
              postal: "12345",
              street: "Teststraße 1",
            ),
            PpAddress(
              city: "Gießen",
              email: "mailto:noreply@tinycampus.de",
              phone: "tel:491234566789",
              postal: "12345",
              street: "Teststraße 1",
            ),
          ],
        ),
        PpPerson(
            fullName: {"de": "Frau Annika Schön"},
            description: randomMarkdown(),
            image: "https://tinycampus.de/img/app/pp/annika.png",
            imageAlt: randomMediumText(),
            addresses: [
              PpAddress(
                city: "Friedberg",
                email: "mailto:noreply@tinycampus.de",
                latitude: "50.3303101",
                longitude: "8.7591954",
                phone: "tel:491234566789",
                postal: "12345",
                street: "Teststraße 1",
              )
            ]),
      ];

  static List<PpGroup> rulGroups() {
    final groups = <PpGroup>[];
    final amountGroups = 3 + Random().nextInt(randomizedGroupSpread);

    for (var i = 0; i <= amountGroups; i++) {
      groups.add(PpGroup(
        title: randomShortText(),
        order: i,
        groupId: getFirstAndPop(uniqueGroupIds),
        dependsOn: groups.isEmpty ? -1 : groups.last.groupId,
        units: rulUnits(),
      ));
    }
    return groups;
  }

  static List<PpUnit> rulUnits() {
    final units = <PpUnit>[];
    final amountUnits = 3 + Random().nextInt(randomizedSpread);
    for (var i = 0; i <= amountUnits; i++) {
      units.add(PpUnit(
        entries: rulEntries(),
        image: randomImage(),
        imageAlt: randomText(),
        markdown: randomMarkdown(),
        medias: rulMedias(),
        questions: rulQuestions(),
        subtitle: randomText(),
        title: randomMediumText(),
        unitId: getFirstAndPop(uniqueUnitIds),
      ));
    }
    return units;
  }

  static List<PpMedia> rulMedias() {
    final medias = <PpMedia>[];
    final amountMedias = 0 + Random().nextInt(3);

    for (var i = 0; i <= amountMedias; i++) {
      medias.add(PpMedia(
        mediaId: getFirstAndPop(uniqueMediaIds),
        order: Random().nextInt(50),
        title: randomText(),
        type: Random().nextInt(3),
        url: "https://tinycampus.de",
      ));
    }
    return medias;
  }

  static int getFirstAndPop(Set<int> value) {
    var result = value.first;
    value.remove(result);
    return result;
  }

  static List<PpEntry> rulEntries() {
    final entries = <PpEntry>[];
    if (Random().nextInt(4) == 1) {
      return entries;
    }
    final amountEntries = 1 + Random().nextInt(randomizedSpread);

    for (var i = 0; i <= amountEntries; i++) {
      entries.add(PpEntry(
        entryId: getFirstAndPop(uniqueEntryIds),
        body: randomMarkdown(),
        title: randomText(),
        order: Random().nextInt(50),
      ));
    }
    return entries;
  }

  static bool roll(double percent) => Random().nextDouble() < percent;

  static List<PpQuestion> rulQuestions() {
    final questions = <PpQuestion>[];

    /// return empty questions with [n] chance to test
    if (roll(0.5)) {
      return questions;
    }
    final amountQuestions = 2 + Random().nextInt(randomizedSpread);

    for (var i = 0; i <= amountQuestions; i++) {
      final randomType = roll(0.50) ? QuestionType.image : QuestionType.noImage;
      // final randomType = QuestionType.values[Random().nextInt(2)];
      final randomLength = QuestionLength.values[Random().nextInt(3)];
      // final randomLength = QuestionLength.values[Random().nextInt(3)];
      final answers = rulAnswers(randomLength);
      late PpAnswer correctAnswer;
      if (answers.isNotEmpty) {
        correctAnswer = answers[Random().nextInt(answers.length)];
        correctAnswer.body.clear();
        correctAnswer.body.addAll(randomCorrectAnswerText());
      }

      questions.add(
        PpQuestion(
          questionId: getFirstAndPop(uniqueQuestionIds),
          answers: answers,
          correctAnswerId: answers.isEmpty ? -1 : correctAnswer.answerId,
          body: randomQuestionText(),
          bodyCorrect:
              Random().nextBool() ? <String, String>{} : randomMediumText(),
          bodyWrong:
              Random().nextBool() ? {"de": "", "en": ""} : randomMediumText(),
          imageAlt: randomType == QuestionType.image
              ? randomMediumText()
              : {"de": "", "en": ""},
          image: randomType == QuestionType.image ? randomImage() : "",
        ),
      );
    }
    return questions;
  }

  static List<PpAnswer> rulAnswers(QuestionLength length) {
    final answers = <PpAnswer>[];
    var amountAnswers = 2;
    switch (length) {
      case QuestionLength.two:
        amountAnswers = 2;
        break;
      case QuestionLength.four:
        amountAnswers = 4;
        break;
      case QuestionLength.faulty:
        final faultyList = [0, 1, 3, 5, 6, 7, 8, 9, 10];
        final randomFaultyNumber = Random().nextInt(faultyList.length);
        amountAnswers = faultyList[randomFaultyNumber];
        break;
    }
    for (var i = 0; i < amountAnswers; i++) {
      answers.add(PpAnswer(
          answerId: getFirstAndPop(uniqueAnswerIds), body: randomMediumText()));
    }
    return answers;
  }

  static String randomImage() => PpStr.randomImageUrl();

  static Map<String, String> randomText() => {
        "de": PpStr.randomText(Language.german),
        "en": PpStr.randomText(Language.english),
      };
  static Map<String, String> randomMarkdown() => {
        "de": PpStr.markdownText(),
        "en": PpStr.markdownText(),
      };
  static Map<String, String> randomMediumText() => {
        "de": PpStr.randomWords(
            3 + (5 * Random().nextDouble()).floor(), Language.german),
        "en": PpStr.randomWords(
            3 + (5 * Random().nextDouble()).floor(), Language.english),
      };
  static Map<String, String> randomQuestionText() => {
        "de": "${PpStr.randomWords(6, Language.german)}?",
        "en": "${PpStr.randomWords(6, Language.english)}?",
      };
  static Map<String, String> randomShortText() => {
        "de": PpStr.randomWords(1, Language.german),
        "en": PpStr.randomWords(1, Language.english),
      };

  static Map<String, String> randomCorrectAnswerText() => {
        "de": "${PpStr.randomWords(3, Language.german)} (C)",
        "en": "${PpStr.randomWords(3, Language.english)} (C)",
      };
}

enum QuestionType {
  noImage,
  image,
}
enum QuestionLength {
  two,
  four,
  faulty,
}

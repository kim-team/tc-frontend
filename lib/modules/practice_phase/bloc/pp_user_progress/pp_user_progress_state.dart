/*
 * Copyright 2020-2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
part of 'pp_user_progress_bloc.dart';

@CopyWith()
@JsonSerializable(anyMap: true, explicitToJson: true)
class PpUserProgressState extends Equatable
    implements Comparable<PpUserProgressState> {
  final int phaseSelection;
  final Set<int> skippedGroups;
  final Set<int> entryCheckedSet;
  final Map<int, int> entryQuestionMap;
  final Map<int, DateTime> successfulSent;

  const PpUserProgressState({
    this.phaseSelection = -1,
    this.skippedGroups = const {},
    this.entryCheckedSet = const {},
    this.entryQuestionMap = const {},
    this.successfulSent = const {},
  });

  @override
  List<Object> get props => [
        phaseSelection,
        skippedGroups,
        entryCheckedSet,
        entryQuestionMap,
        successfulSent,
      ];

  factory PpUserProgressState.initial() => const PpUserProgressState();

  factory PpUserProgressState.fromJson(Map<String, dynamic> json) =>
      _$PpUserProgressStateFromJson(json);

  Map<String, dynamic> toJson() => _$PpUserProgressStateToJson(this);

  @override
  int compareTo(PpUserProgressState other) {
    if (entryCheckedSet.length != other.entryCheckedSet.length) {
      if (entryCheckedSet.length > other.entryCheckedSet.length) {
        return 1;
      } else {
        return -1;
      }
    }

    if (entryQuestionMap.length != other.entryQuestionMap.length) {
      if (entryQuestionMap.length > other.entryQuestionMap.length) {
        return 1;
      } else {
        return -1;
      }
    }

    for (var key in entryQuestionMap.keys) {
      /// if a question is not in remote state
      if (!other.entryQuestionMap.containsKey(key)) {
        return 1;
      }

      /// if a remote question has a higher value than the current state
      /// 1 = [correct answer chosen]
      /// 0 = [incorrect answer chosen]
      if (other.entryQuestionMap[key]! > entryQuestionMap[key]!) {
        return -1;
      }
    }

    for (var key in successfulSent.keys) {
      if (!other.successfulSent.containsKey(key)) {
        return 1;
      }

      if (successfulSent[key]!.isBefore(other.successfulSent[key]!)) {
        return -1;
      }
    }

    return 0;
  }
}

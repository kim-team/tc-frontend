/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

part of 'pp_user_progress_bloc.dart';

abstract class PpUserProgressEvent extends Equatable {
  const PpUserProgressEvent();

  @override
  List<Object> get props => [];

  Future<PpUserProgressState> executeAction(PpUserProgressState state) async =>
      PpUserProgressState.initial();
}

class PpInitialUserProgressEvent extends PpUserProgressEvent {
  PpInitialUserProgressEvent();

  @override
  List<Object> get props => [];

  @override
  Future<PpUserProgressState> executeAction(PpUserProgressState state) async =>
      PpUserProgressState.initial();
}

class UpbSelectPhaseEvent extends PpUserProgressEvent {
  final int phaseSelection;

  UpbSelectPhaseEvent(this.phaseSelection);

  @override
  List<Object> get props => [phaseSelection];

  @override
  Future<PpUserProgressState> executeAction(PpUserProgressState state) async =>
      state.copyWith(phaseSelection: phaseSelection);
}

class UpbDeselectPhaseEvent extends PpUserProgressEvent {
  UpbDeselectPhaseEvent();

  @override
  List<Object> get props => [];

  @override
  Future<PpUserProgressState> executeAction(PpUserProgressState state) async =>
      state.copyWith(phaseSelection: -1);
}

class UpbEntryCheckedEvent extends PpUserProgressEvent {
  final int entryId;

  UpbEntryCheckedEvent(this.entryId);

  @override
  List<Object> get props => [entryId];

  @override
  Future<PpUserProgressState> executeAction(PpUserProgressState state) async {
    var newList = {...state.entryCheckedSet};
    newList.add(entryId);
    return state.copyWith(entryCheckedSet: newList);
  }
}

class UpbUnitQuestionSetEvent extends PpUserProgressEvent {
  final int questionId;
  final int result;

  UpbUnitQuestionSetEvent(this.questionId, this.result);

  @override
  List<Object> get props => [questionId, result];

  @override
  Future<PpUserProgressState> executeAction(PpUserProgressState state) async {
    var newMap = {...state.entryQuestionMap};
    newMap[questionId] = result;
    return state.copyWith(entryQuestionMap: newMap);
  }
}

class UpbResetEvent extends PpUserProgressEvent {
  const UpbResetEvent();

  @override
  List<Object> get props => [];

  @override
  Future<PpUserProgressState> executeAction(PpUserProgressState state) async =>
      PpUserProgressState.initial();
}

class PpCheckAllUnitEntriesEvent extends PpUserProgressEvent {
  PpCheckAllUnitEntriesEvent(this.unit);
  final PpUnit unit;

  @override
  List<Object> get props => [unit];

  @override
  Future<PpUserProgressState> executeAction(PpUserProgressState state) async {
    var newList = {...state.entryCheckedSet};
    for (var entry in unit.entries) {
      newList.add(entry.entryId);
    }
    return state.copyWith(entryCheckedSet: newList);
  }
}

class PpResetUnitProgressionEvent extends PpUserProgressEvent {
  PpResetUnitProgressionEvent(this.unit);
  final PpUnit unit;

  @override
  List<Object> get props => [unit];

  @override
  Future<PpUserProgressState> executeAction(PpUserProgressState state) async {
    var state1 = await PpUncheckAllUnitEntriesEvent(unit).executeAction(state);
    var state2 = PpRemoveQuestionResultsEvent(unit).executeAction(state1);
    return state2;
  }
}

class PpUncheckAllUnitEntriesEvent extends PpUserProgressEvent {
  PpUncheckAllUnitEntriesEvent(this.unit);
  final PpUnit unit;

  @override
  List<Object> get props => [unit];

  @override
  Future<PpUserProgressState> executeAction(PpUserProgressState state) async {
    var newList = {...state.entryCheckedSet};
    for (var entry in unit.entries) {
      newList.remove(entry.entryId);
    }
    return state.copyWith(entryCheckedSet: newList);
  }
}

class PpUpdateQuestionResultsEvent extends PpUserProgressEvent {
  PpUpdateQuestionResultsEvent(this.resultMap);
  final Map<int, int> resultMap;

  @override
  List<Object> get props => [resultMap];

  @override
  Future<PpUserProgressState> executeAction(PpUserProgressState state) async {
    var newMap = {...state.entryQuestionMap};
    newMap.addEntries(resultMap.entries);

    try {
      // check if resultMap is worse than previous result
      // users may not lose previous "better percentages" due
      // to repeating a quiz
      var percentOld = 0.0;
      var percentNew = 0.0;
      var index = 0;
      for (var entry in resultMap.entries) {
        percentOld += state.entryQuestionMap[entry.key] ?? 0;
        percentNew += entry.value;
        index++;
      }
      percentOld /= index;
      percentNew /= index;
      if (percentNew >= percentOld) {
        sprint(
            "Updated Questions: "
            "newPercent: $percentNew and percentOld: $percentOld",
            caller: PpUpdateQuestionResultsEvent,
            style: PrintStyle.praxisPhase);
        return state.copyWith(entryQuestionMap: newMap);
      } else {
        sprint(
            "Not updated because"
            " newPercent: $percentNew and percentOld: $percentOld",
            style: PrintStyle.attention,
            caller: PpUpdateQuestionResultsEvent,
            prefix: "[PP]");
        return state;
      }
    } on Exception catch (e) {
      debugPrint("Error in PpUpdateQuestionResultsEvent: $e");
    }
    return state.copyWith(entryQuestionMap: newMap);
  }
}

class PpRandomQuestionResultsEvent extends PpUserProgressEvent {
  PpRandomQuestionResultsEvent(this.unit);
  final PpUnit unit;

  @override
  List<Object> get props => [unit];

  @override
  Future<PpUserProgressState> executeAction(PpUserProgressState state) async {
    var newMap = {...state.entryQuestionMap};
    for (var question in unit.questions) {
      newMap[question.questionId] = Random().nextInt(2);
    }
    return state.copyWith(entryQuestionMap: newMap);
  }
}

class PpRemoveQuestionResultsEvent extends PpUserProgressEvent {
  PpRemoveQuestionResultsEvent(this.unit);
  final PpUnit unit;

  @override
  List<Object> get props => [unit];

  @override
  Future<PpUserProgressState> executeAction(PpUserProgressState state) async {
    var newMap = {...state.entryQuestionMap};
    for (var question in unit.questions) {
      newMap.removeWhere((key, value) => key == question.questionId);
    }
    return state.copyWith(entryQuestionMap: newMap);
  }
}

class PpSetAllQuestionsToCorrectEvent extends PpUserProgressEvent {
  PpSetAllQuestionsToCorrectEvent(this.unit);
  final PpUnit unit;

  @override
  List<Object> get props => [unit];

  @override
  Future<PpUserProgressState> executeAction(PpUserProgressState state) async {
    var newMap = {...state.entryQuestionMap};
    for (var question in unit.questions) {
      newMap[question.questionId] = 1;
    }
    return state.copyWith(entryQuestionMap: newMap);
  }
}

class PpUnlockGroupEvent extends PpUserProgressEvent {
  PpUnlockGroupEvent(this.groupId);
  final int groupId;

  @override
  List<Object> get props => [groupId];

  @override
  Future<PpUserProgressState> executeAction(PpUserProgressState state) async {
    var newList = {...state.skippedGroups};
    newList.add(groupId);
    return state.copyWith(skippedGroups: newList);
  }
}

class PpSuccessfulSendEvent extends PpUserProgressEvent {
  final int phaseId;
  PpSuccessfulSendEvent(this.phaseId);

  @override
  List<Object> get props => [phaseId];

  @override
  Future<PpUserProgressState> executeAction(PpUserProgressState state) async {
    var sus = {...state.successfulSent};
    sus[phaseId] = DateTime.now();
    return state.copyWith(successfulSent: sus);
  }
}

class PpFetchUserProgress extends PpUserProgressEvent {
  PpFetchUserProgress({this.forceReload = false});

  final bool forceReload;
  @override
  List<Object> get props => [forceReload];

  final updateThreshold = Duration(seconds: 10);

  @override
  Future<PpUserProgressState> executeAction(PpUserProgressState state) async {
    try {
      var overviewRes = await HttpRepository().get("$baseUrl/praxis/progress",
          secured: true, headers: {'Accept': 'application/json'});

      final remoteState = overviewRes.bodyBytes.isEmpty
          ? PpUserProgressState.initial()
          : PpUserProgressState.fromJson(
              jsonDecode(utf8.decode(overviewRes.bodyBytes)));
      sprint("${remoteState.compareTo(state)} << COMPARISON");
      if (remoteState.compareTo(state) == 1) {
        /// Only replace if remote state is of more value
        return remoteState;
      }
    } on Exception catch (e) {
      eprint(e.toString());
    }

    return state;
  }
}

class PpSaveProgressToBackend extends PpUserProgressEvent {
  PpSaveProgressToBackend({this.forceReload = false});

  final bool forceReload;
  @override
  List<Object> get props => [forceReload];

  final updateThreshold = Duration(seconds: 10);

  @override
  Future<PpUserProgressState> executeAction(PpUserProgressState state) async {
    try {
      var overviewRes =
          await HttpRepository().post("$baseUrl/praxis/progress", // /phases/id
              secured: true,
              headers: {'Accept': 'application/json'},
              body: jsonEncode(state));

      var jsonString = utf8.decode(overviewRes.bodyBytes);
      var overviewJson = jsonDecode(jsonString);
      sprint(overviewJson.runtimeType.toString(),
          prefix: "[JSON TYPE]", style: PrintStyle.attention);
    } on Exception catch (e) {
      eprint(e.toString());
    }
    return state;
  }
}

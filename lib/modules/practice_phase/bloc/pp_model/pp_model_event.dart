/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

part of 'pp_model_bloc.dart';

abstract class PpModelEvent extends Equatable {
  const PpModelEvent();

  @override
  List<Object> get props => [];

  Future<PpModelState> executeAction(PpModelState state) async =>
      PpModelState.initial();
}

class PpInitialEvent extends PpModelEvent {
  PpInitialEvent();

  @override
  List<Object> get props => [];

  @override
  Future<PpModelState> executeAction(PpModelState state) async =>
      PpModelState.initial();
}

class PpModelResetEvent extends PpModelEvent {
  const PpModelResetEvent();

  @override
  List<Object> get props => [];

  @override
  Future<PpModelState> executeAction(PpModelState state) async =>
      PpModelState.initial();
}

class PpFetchAndReplaceModelFromRemoteEvent extends PpModelEvent {
  PpFetchAndReplaceModelFromRemoteEvent({this.forceReload = false});

  final bool forceReload;
  @override
  List<Object> get props => [forceReload];
  Future<void> setLastUpdate() async {
    var sp = await SharedPreferences.getInstance();
    const keyLastUpdated = 'ppModel.lastUpdated';
    await sp.setString(keyLastUpdated, DateTime.now().toIso8601String());
  }

  Future<DateTime> getLastUpdate() async {
    var sp = await SharedPreferences.getInstance();
    const keyLastUpdated = 'ppModel.lastUpdated';
    var res = sp.getString(keyLastUpdated);
    return res != null ? DateTime.parse(res) : DateTime(2021, 04, 07);
  }

  final updateThreshold = Duration(seconds: 10);

  Future<bool> shouldUpdate() async {
    final now = DateTime.now();
    final lastUpdate = await getLastUpdate();
    final until = lastUpdate.add(updateThreshold);
    final difference = now.difference(lastUpdate);
    final remaining = until.difference(now);
    final a = updateThreshold.inMilliseconds;
    final b = difference.inMilliseconds;
    final result = ((b / a) * 10).floor();
    var progressString = "[";
    for (var i = 0; i < 10; i++) {
      progressString += (result < i ? "_" : "#");
    }
    progressString += "]";

    if (difference.compareTo(updateThreshold) < 0) {
      sprint(
          "[PP][PP_MODEL_EVENT][DO_NOT_UPDATE]"
          " Progress: $progressString | "
          "${difference.inSeconds.toString()} seconds passed | "
          "${remaining.inSeconds.toString()} seconds "
          "left until update gets triggered | ",
          style: PrintStyle.attention,
          prefix: "[PP]");
      return false;
    } else {
      sprint(
          "[PP][PP_MODEL_EVENT][UPDATING_MODEL]"
          " Progress: $progressString | "
          "${difference.inSeconds.toString()} seconds passed | "
          "${remaining.inSeconds.toString()} seconds "
          "left until update gets triggered | ",
          style: PrintStyle.attention,
          prefix: "[PP]");
      return true;
    }
  }

  @override
  Future<PpModelState> executeAction(PpModelState state) async {
    if (!forceReload) {
      if (!await shouldUpdate()) {
        return state;
      }
    } else {
      sprint("[PP_MODEL_EVENT] Force reload", style: PrintStyle.praxisPhase);
    }
    try {
      var overviewRes = await HttpRepository().get(
        "$baseUrl/praxis", // /phases/id
        secured: true,
        headers: {'Accept': 'application/json'},
      );
      var jsonString = utf8.decode(overviewRes.bodyBytes);
      final overviewJson = jsonDecode(jsonString);
      sprint(overviewJson.runtimeType.toString(),
          prefix: "[JSON TYPE]", style: PrintStyle.attention);

      final phaseIdSet = <int>{};

      debugPrint(overviewJson.runtimeType.toString());
      if ((overviewJson is List<dynamic>)) {
        // list
        phaseIdSet.addAll(
          buildEntitiesFromList(overviewJson, (e) => e["phaseId"]),
        );
      } else if (overviewJson is Map<String, dynamic>) {
        // single object

        phaseIdSet.add(overviewJson["phaseId"]);
      } else {
        return PpModelState.initial(); // no entries found, return initial
      }

      final phases = <PpPhase>[];
      for (var i in phaseIdSet) {
        var res = await HttpRepository().get(
          "$baseUrl/praxis/${i.toString()}", // /phases/id
          secured: true,
          headers: {'Accept': 'application/json'},
        );
        final utf8Res = utf8.decode(res.bodyBytes);
        final json = jsonDecode(utf8Res);
        phases.add(PpPhase.fromJson(json));
      }

      final newState =
          PpModelState(phases: phases, lastUpdated: DateTime.now());
      setLastUpdate();
      return newState;
    } on Exception catch (e) {
      eprint(e.toString());
      return state;
    }
  }
}

class PpFetchDebugModelFromRemoteEvent extends PpModelEvent {
  PpFetchDebugModelFromRemoteEvent({this.forceReload = false});

  final bool forceReload;
  static const String keyLastUpdated = 'ppModel.lastUpdated';

  @override
  List<Object> get props => [forceReload];
  Future<void> setLastUpdate() async {
    var sp = await SharedPreferences.getInstance();
    await sp.setString(keyLastUpdated, DateTime.now().toIso8601String());
  }

  Future<DateTime> getLastUpdate() async {
    var sp = await SharedPreferences.getInstance();
    var res = sp.getString(keyLastUpdated);
    return res != null
        ? DateTime.parse(res)
        : DateTime.fromMicrosecondsSinceEpoch(0);
  }

  final updateThreshold = Duration(seconds: 60);

  Future<bool> shouldUpdate() async {
    final now = DateTime.now();
    final lastUpdate = await getLastUpdate();
    final until = lastUpdate.add(updateThreshold);
    final difference = now.difference(lastUpdate);
    final remaining = until.difference(now);

    final a = updateThreshold.inMilliseconds;
    final b = difference.inMilliseconds;
    final result = ((b / a) * 10).floor();
    var progressString = "[";
    for (var i = 0; i < 10; i++) {
      progressString += (result < i ? "_" : "#");
    }
    progressString += "]";

    if (difference.compareTo(updateThreshold) < 0) {
      sprint(
          "[PP][PP_MODEL_EVENT][DO_NOT_UPDATE]"
          " Progress: $progressString | "
          "${difference.inSeconds.toString()} seconds passed | "
          "${remaining.inSeconds.toString()} seconds "
          "left until update gets triggered | ",
          style: PrintStyle.attention,
          prefix: "[PP]");
      return false;
    } else {
      sprint(
          "[PP][PP_MODEL_EVENT][UPDATING_MODEL]"
          " Progress: $progressString | "
          "${difference.inSeconds.toString()} seconds passed | "
          "${remaining.inSeconds.toString()} seconds "
          "left until update gets triggered | ",
          style: PrintStyle.attention,
          prefix: "[PP]");
      return true;
    }
  }

  @override
  Future<PpModelState> executeAction(PpModelState state) async {
    if (!forceReload) {
      if (!await shouldUpdate()) {
        return state;
      }
    } else {
      sprint("[PP_MODEL_EVENT] Force reload", style: PrintStyle.praxisPhase);
    }
    try {
      var res = await HttpRepository().get(
        "https://tinycampus.de/i18n/2021-04-07_pp_test_model.json",
        secured: false,
        headers: {'Accept': 'application/json'},
      );
      final json =
          jsonDecode(utf8.decode(res.bodyBytes)) as Map<String, dynamic>;
      final newState = PpModelState.fromJson(json);

      setLastUpdate();
      return newState;
    } on Exception catch (e) {
      eprint(e.toString());
      return state;
    }
  }
}

class PmbGenerateModelsEvent extends PpModelEvent {
  @override
  List<Object> get props => [];

  @override
  Future<PpModelState> executeAction(PpModelState state) async {
    var newList = <PpPhase>[];
    try {
      newList.addAll(state.phases);
      newList.addAll(PpGenerator.rDeepPhases());
      return state.copyWith(phases: newList);
    } on Exception catch (e) {
      debugPrint("[Pp][pp_model_event]"
          "Error: $e | Could not generate debug models, return previous state");
      return state;
    }
  }
}

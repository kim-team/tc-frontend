/*
 * Copyright 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../../../../common/tc_theme.dart';
import '../../../bloc/settings/exercise_settings_bloc.dart';
import '../../../i18n_keys.dart';
import '../../../model/exercise_enums.dart';

// part 'exercise_category_entry.dart';

typedef ProgramSelectionCallback = void Function(ExerciseCategory cat);

class ProgramSelector extends StatefulWidget {
  final ProgramSelectionCallback onPressed;

  ProgramSelector({
    Key? key,
    required this.onPressed,
  }) : super(key: key);

  @override
  _ProgramSelectorState createState() => _ProgramSelectorState();
}

class _ProgramSelectorState extends State<ProgramSelector> {
  ValueNotifier<ExerciseCategory> selectedCategory =
      ValueNotifier<ExerciseCategory>(ExerciseCategory.strength);
  final Duration animDur = Duration(milliseconds: 120);

  @override
  void initState() {
    super.initState();
    selectedCategory.value = BlocProvider.of<ExerciseSettingsBloc>(context)
        .state
        .selectedExerciseCategory;
  }

  @override
  Widget build(BuildContext context) => ValueListenableBuilder(
        valueListenable: selectedCategory,
        builder: (context, value, child) => Row(
          children: [
            Expanded(child: buildItem(ExerciseCategory.strength)),
            Container(width: 12),
            Expanded(child: buildItem(ExerciseCategory.mobility)),
            Container(width: 12),
            Expanded(child: buildItem(ExerciseCategory.stretch)),
          ],
        ),
      );

  Widget buildItem(ExerciseCategory category) => Card(
        elevation: 4,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        child: InkWell(
          borderRadius: BorderRadius.circular(10),
          onTap: () {
            if (selectedCategory.value != category) {
              selectedCategory.value = category;
              widget.onPressed(category);
            }
          },
          child: Stack(
            children: [
              buildAnimatedBody(category),
              animatedBorder(category, top: true),
              animatedBorder(category, top: false),
            ],
          ),
        ),
      );

  AnimatedContainer buildAnimatedBody(ExerciseCategory category) {
    final selected = selectedCategory.value == category;
    return AnimatedContainer(
        duration: animDur,
        height: 128,
        // padding: EdgeInsets.only(top: !selected ? 2.0 : 0.0),
        decoration: BoxDecoration(
          border: Border.all(
              color: selected
                  ? CurrentTheme().themeData.colorScheme.secondary
                  : Colors.transparent,
              width: selected ? 2 : 2), // added
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Center(
          child: AutoSizeText(
            FlutterI18n.translate(context,
                '$sportSport.exercises.${category.toString().split('.').last}'),
            maxFontSize: 16,
            maxLines: 1,
            minFontSize: 12,
            style: TextStyle(
                fontSize: 16,
                fontWeight: selected ? FontWeight.w700 : FontWeight.w400,
                color: selected
                    ? CurrentTheme().tcBlue
                    : CurrentTheme().textSoftWhite.withOpacity(0.75)),
          ),
        ));
  }

  Positioned animatedBorder(ExerciseCategory category, {bool top = true}) {
    final selected = selectedCategory.value == category;
    const size = 6.0;
    return Positioned(
        top: top ? 0 : null,
        bottom: !top ? 0 : null,
        left: 16,
        right: 16,
        child: AnimatedContainer(
          duration: animDur,
          curve: Curves.easeOut,
          height: selected ? size : 0,
          decoration: BoxDecoration(
            color: selectedCategory.value == category
                ? CurrentTheme().themeData.colorScheme.secondary
                : Colors.transparent,
            borderRadius: BorderRadius.only(
                topLeft: top ? Radius.circular(0) : Radius.circular(size),
                topRight: top ? Radius.circular(0) : Radius.circular(size),
                bottomLeft: top ? Radius.circular(size) : Radius.circular(0),
                bottomRight: top ? Radius.circular(size) : Radius.circular(0)),
          ),
        ));
  }
}

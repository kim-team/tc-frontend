/*
 * Copyright 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../../../../common/tc_theme.dart';
import '../../../i18n_keys.dart';

typedef DurationCallback = void Function(Duration duration);

class TimingWidget extends StatefulWidget {
  final bool minutes;
  final int min;
  final int max;
  final int initialValue;
  final int divisions;
  final DurationCallback durationCallback;

  final String i18nKey;

  TimingWidget.minutes({
    Key? key,
    required this.initialValue,
    required this.durationCallback,
    required this.min,
    required this.max,
    required this.divisions,
  })  : minutes = true,
        i18nKey = 'total_duration',
        super(key: key);

  TimingWidget.seconds({
    Key? key,
    required this.initialValue,
    required this.durationCallback,
    required this.min,
    required this.max,
    required this.divisions,
  })  : minutes = false,
        i18nKey = 'pauses',
        super(key: key);

  @override
  _TimingWidgetState createState() => _TimingWidgetState();
}

class _TimingWidgetState extends State<TimingWidget> {
  late int duration;

  @override
  void initState() {
    super.initState();
    duration = widget.initialValue;
  }

  @override
  Widget build(BuildContext context) => Column(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 16,
              vertical: 16,
            ),
            child: Row(
              children: [
                Expanded(
                  child: I18nText(
                    '$sportSport.${widget.i18nKey}',
                    child: Text("", style: CurrentTheme().sportTextLabel),
                  ),
                ),
                Expanded(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      const Icon(Icons.av_timer),
                      _timeText(),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Slider(
            value: duration.toDouble(),
            min: widget.min.toDouble(),
            max: widget.max.toDouble(),
            activeColor: CurrentTheme().themeData.colorScheme.secondary,
            divisions: widget.divisions,
            onChanged: (value) {
              setState(() {
                duration = value.toInt();
              });
              widget.durationCallback(widget.minutes
                  ? Duration(minutes: value.toInt())
                  : Duration(seconds: value.toInt()));
            },
          ),
        ],
      );

  Text _timeText() {
    String unit;
    if (widget.minutes) {
      unit = 'min';
    } else if (FlutterI18n.currentLocale(context)?.languageCode == 'de') {
      unit = 'sek';
    } else {
      unit = 'sec';
    }
    return Text(
      '$duration $unit',
      style: CurrentTheme().sportInputIndicator,
    );
  }
}

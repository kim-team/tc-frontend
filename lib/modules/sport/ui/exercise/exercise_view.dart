/*
 * Copyright 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:math';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:wakelock/wakelock.dart';

import '../../../../common/styled_print.dart';
import '../../../../common/tc_theme.dart';
import '../../../../common/tc_utility_functions.dart';
import '../../../../common/widgets/dialog/tc_dialog.dart';
import '../../../../common/widgets/tc_cached_image.dart';
import '../../bloc/settings/exercise_settings_bloc.dart';
import '../../i18n_keys.dart';
import '../../model/exercise.dart';
import '../../singletons/audio_player.dart';
import '../../singletons/tts.dart';
import 'exercise_constants.dart';
import 'exercise_end_card.dart';
import 'utility/sport_animation_helper.dart';

class ExerciseView extends StatefulWidget {
  final List<Exercise> exercises;

  const ExerciseView({
    Key? key,
    required this.exercises,
  }) : super(key: key);

  @override
  _ExerciseViewState createState() => _ExerciseViewState();
}

class _ExerciseViewState extends State<ExerciseView>
    with
        // Ignored because:
        // "When used as a mixin, provides no-op method implementations."
        // ignore: prefer_mixin
        WidgetsBindingObserver,
        TickerProviderStateMixin {
  int selectedExercise = 0;
  EInstruction selectedInstruction = EInstruction.effect;
  EPhase currentPhase = EPhase.preparation;
  Stopwatch watch = Stopwatch();

  late ExerciseSettingsState settings;

  /// [anim] is the main animation for every step inbetween
  late SportAnimationHelper anim;

  /// [transition] is the main animation for transitioning from
  /// [Exercise] A to [Exercise] B or vice versa (both directions work)
  late SportAnimationHelper transition;

  /// [pauseAnimation] is the main animation for scaling the main body
  /// if the user pauses
  late SportAnimationHelper pauseAnimation;

  int exerciseTransitionFlag = 0;

// int amountExercises = 0;
  int amountInstructions = 0;
  ValueNotifier<bool> forceDebug = ValueNotifier<bool>(false);
  ValueNotifier<bool> pauseRequested = ValueNotifier<bool>(false);
  ValueNotifier<bool> searchingForInstruction = ValueNotifier<bool>(false);

  /// [EMode.alternating], [EMode.switching] or [EMode.none]
  EMode mode = EMode.none;

  /// Relevant for modes: [EMode.alternating] and [EMode.switching]
  bool leftSide = true;

  /// Current number of executed repetitions
  int currentRepetition = 0;

  int currentInstruction = 0;

  /// Current number of executed sets
  int currentSet = 0;

  /// Determines if a timer or counter is shown
  bool showDuration = true;

  int maxRepetitions = 1;
  int maxSets = 1;

  Duration computedExerciseDuration = Duration(seconds: 10);

  @override
  void initState() {
    super.initState();
    Wakelock.enable();
    initAnimation();
    initExercise();
    initMusic();
    WidgetsBinding.instance?.addObserver(this);
    Future.delayed(transitionDuration, nextStep);
  }

  void initMusic() {
    if (BlocProvider.of<ExerciseSettingsBloc>(context).state.showMusic) {
      AudioPlayer().playOrResume();
    } else {
      AudioPlayer().stop();
    }
  }

  @override
  void dispose() {
    anim.dispose();
    transition.dispose();
    Tts().clearOnEnd();
    WidgetsBinding.instance?.removeObserver(this);
    Wakelock.disable();
    super.dispose();
  }

  void initAnimation() {
    transition = SportAnimationHelper(this);
    transition.resetWithValue(1.0, customDuration: transitionDuration);
    transition.addAfterEnd(nextStep);

    anim = SportAnimationHelper(this);
    // anim.addListener(() {
    //   sprint(anim.ctrl);
    // });
    anim.addAfterEnd(() async {
      if (!transition.ctrl.isAnimating) {
        // sprint(anim.ctrl);
        var sw = Stopwatch()..start();
        while (settings.enableTextToSpeech &&
            Tts().isPlaying &&
            currentPhase == EPhase.preparation) {
          await Future.delayed(Duration(milliseconds: 500), () {
            sprint(
              "nextStep will be called when TTS is complete,"
              " waiting for ${sw.elapsedMilliseconds} milliseconds",
              prefix: '[SPORT]',
            );
          });
          if (sw.elapsedMilliseconds > 9000) {
            break;
          }
        }
        nextStep();
        exerciseTransitionFlag = 0;
      }
    });
    pauseAnimation = SportAnimationHelper(this);
    pauseAnimation.initWith(1.0, 0.0, millisecondsDuration: 160);
    pauseRequested.addListener(() {
      if (pauseRequested.value) {
        pauseAnimation.ctrl.forward();
      } else {
        pauseAnimation.ctrl.reverse();
      }
    });
  }

  void initExercise() {
    settings = BlocProvider.of<ExerciseSettingsBloc>(context).state;
    sprint(
      "${settings.showEffectDesc} settings.showEffectDesc",
      prefix: '[SPORT]',
    );
    sprint(
      "${settings.showStartingPositionDesc} settings.showStartingPositionDesc",
      prefix: '[SPORT]',
    );
    sprint(
      "${settings.showMotionSequenceDesc} settings.showMotionSequenceDesc",
      prefix: '[SPORT]',
    );
    sprint(
      "${settings.showRepetitionDesc} settings.showRepetitionDesc",
      prefix: '[SPORT]',
    );
    sprint(
      "${settings.showVariantDesc} settings.showVariantDesc",
      prefix: '[SPORT]',
    );
    selectedInstruction = EInstruction.effect;
    if (settings.enableInstruction) {
      searchingForInstruction.value = true;
      currentPhase = EPhase.preparation;
      selectedInstruction = determineFirstInstruction();
      sprint(
        selectedInstruction,
        prefix: '[SPORT]',
      );
      searchingForInstruction.value = true;
    }
    if (!settings.enableInstruction) {
      setCurrentAnimToCurrentExerciseDuration();
      currentPhase = EPhase.execution;
    }
    // amountExercises = getCurrentExercise().calculateAmountInstructions();
    currentInstruction = 0;
    amountInstructions = determineAmountInstructions();
    if (amountInstructions == 0) {
      currentPhase = EPhase.execution;
    }
    initExerciseMode();
    currentRepetition = 0;
    currentSet = 0;
    sprint(
      getCurrentExercise().computeTotalDuration,
      prefix: '[SPORT]',
    );

    /// [TODO] set reps to 0
    /// [TODO] set alternates to 0 [L/R]?
    /// [TODO] set sets to 0
    /// [TODO] set current / max reps
    /// [TODO] set current / max alternates?
    /// [TODO] set current / max sets
  }

  void initExerciseMode() {
    final exercise = getCurrentExercise();
    mode = exercise.getExerciseMode;
    leftSide = true;
    currentRepetition = 1;
    currentSet = 0;
    showDuration = exercise.showDuration;
    maxRepetitions = 1;
    maxSets = 1;
    computedExerciseDuration = exercise.computeTotalDuration;
    switch (mode) {
      case EMode.switching:
        maxRepetitions = getCurrentExercise().repetition;
        maxSets = getCurrentExercise().sets * 2;
        break;
      case EMode.alternating:
        maxRepetitions = getCurrentExercise().repetition * 2;
        maxSets = getCurrentExercise().sets;
        break;
      case EMode.none:
        maxRepetitions = getCurrentExercise().repetition;
        maxSets = getCurrentExercise().sets;
        break;
    }
  }

  //TODO: OnWillPopScope + DialogFeld
  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance?.addPostFrameCallback((_) {
      // executes after build
      Tts().setLanguage(FlutterI18n.currentLocale(context)?.languageCode);
    });
    return WillPopScope(
      onWillPop: () async => showDialog(),
      child: Scaffold(
        appBar: AppBar(
          title: ValueListenableBuilder(
            valueListenable: forceDebug,
            builder: (context, value, child) => !forceDebug.value
                ? GestureDetector(
                    onTap: kReleaseMode
                        ? null
                        : () {
                            forceDebug.value = !forceDebug.value;
                          },
                    // TODO: i18n
                    child: Text("Sportprogramm"))
                : AnimatedBuilder(
                    animation: Listenable.merge(
                        [anim.ctrl, transition.ctrl, pauseRequested]),
                    builder: (context, child) => Row(
                      children: [
                        Column(
                          children: [
                            SizedBox(
                              width: 180,
                              child: Row(
                                children: [
                                  IconButton(
                                    icon: Icon(Icons.chevron_left_rounded),
                                    onPressed: () {
                                      durationFactor.value /= 1.5;
                                      // anim.ctrl.reset();
                                      anim.forwardWithDurationFactor(
                                          durationFactor.value);
                                      setState(() {});
                                    },
                                  ),
                                  Expanded(
                                    child: Column(
                                      children: [
                                        Text(
                                          durationFactor.value
                                              .toStringAsPrecision(1)
                                              .toString(),
                                          textAlign: TextAlign.center,
                                        ),
                                        Text(
                                          "anim factor",
                                          style: TextStyle(fontSize: 8),
                                        ),
                                      ],
                                    ),
                                  ),
                                  IconButton(
                                    icon: Icon(Icons.chevron_right_rounded),
                                    onPressed: () {
                                      durationFactor.value =
                                          (durationFactor.value * 1.5)
                                              .clamp(0.0, 1.0);

                                      // anim.ctrl.reset();
                                      anim.forwardWithDurationFactor(
                                          durationFactor.value);
                                      setState(() {});
                                    },
                                  ),
                                  IconButton(
                                    visualDensity: VisualDensity.compact,
                                    icon: Icon(Icons.forward),
                                    onPressed: navigateToResultPage,
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
          ),
          actions: [
            BlocBuilder<ExerciseSettingsBloc, ExerciseSettingsState>(
              builder: (context, state) => IconButton(
                icon: Icon(state.enableTextToSpeech
                    ? Icons.record_voice_over
                    : Icons.record_voice_over_outlined),
                // icon: Icon(state.enableTextToSpeech
                //     ? Icons.mic_sharp
                //     : Icons.mic_off_outlined),
                color: state.enableTextToSpeech
                    // color: state.enableTextToSpeech
                    ? CurrentTheme().textSoftWhite
                    : CurrentTheme().textPassive,
                onPressed: () async {
                  await Tts().stop();
                  Tts().toggleVolume().then((value) => setState(() {
                        BlocProvider.of<ExerciseSettingsBloc>(context).add(
                            ExerciseSettingsEvent.fromState(state.copyWith(
                                enableTextToSpeech:
                                    !state.enableTextToSpeech)));
                      }));
                },
              ),
            ),
            BlocBuilder<ExerciseSettingsBloc, ExerciseSettingsState>(
              builder: (context, state) => IconButton(
                icon: BlocProvider.of<ExerciseSettingsBloc>(context)
                        .state
                        .showMusic
                    ? const Icon(Icons.audiotrack)
                    : const Icon(Icons.music_off_outlined),
                color: BlocProvider.of<ExerciseSettingsBloc>(context)
                        .state
                        .showMusic
                    // color: state.enableTextToSpeech
                    ? CurrentTheme().textSoftWhite
                    : CurrentTheme().textPassive,
                onPressed: () async {
                  final sBloc = BlocProvider.of<ExerciseSettingsBloc>(context);
                  final newMusicEnabledState =
                      !BlocProvider.of<ExerciseSettingsBloc>(context)
                          .state
                          .showMusic;
                  if (newMusicEnabledState) {
                    AudioPlayer().playOrResume();
                  } else {
                    AudioPlayer().pause();
                  }
                  sBloc.add(ExerciseSettingsEvent.fromState(
                      sBloc.state.copyWith(showMusic: newMusicEnabledState)));
                },
              ),
            ),
          ],
        ),
        body: SafeArea(
          bottom: true,
          top: true,
          child: Column(
            children: [
              Expanded(
                child: AnimatedBuilder(
                  animation:
                      Listenable.merge([pauseRequested, pauseAnimation.ctrl]),
                  builder: (context, child) => Transform.scale(
                    scale: 1 + (pauseAnimation.gameLoop.value - 1) / 8,
                    child: AnimatedBuilder(
                      animation: transition.ctrl,
                      builder: (context, child) => Opacity(
                        opacity: 1 + (pauseAnimation.gameLoop.value - 1) / 4,
                        child: Transform.translate(
                            offset: Offset(transition.gameLoop.value * 500, 0),
                            child: child),
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Expanded(child: buildProgression()),
                          Expanded(
                              flex: 1,
                              child: TcCachedImage(
                                boxFit: BoxFit.fitHeight,
                                url: getCurrentExercise().imageUrl,
                              )),
                          Expanded(child: buildBottomProgression(context)),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              buildInteractions(),
            ],
          ),
        ),
      ),
    );
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.detached ||
        state == AppLifecycleState.paused) {
      Tts().stop();
      AudioPlayer().pause();
    }
    if (state == AppLifecycleState.resumed) {
      AudioPlayer().resume();
    }
  }

  Future<bool> showDialog() async {
    pause();
    TCDialog.showCustomDialog(
      context: context,
      onConfirm: () {
        Tts().stop();
        AudioPlayer().stop();
        Navigator.of(context).pop();
      },
      onCancel: () {
        if (!pauseRequested.value) forward();
      },
      functionActionText:
          FlutterI18n.translate(context, 'common.actions.confirm')
              .toUpperCase(),
      headlineText: FlutterI18n.translate(
          context, '$sportExercise.alert_dialog.headline'),
      alternativeWidget: Container(height: 32),
      // TODO: Muss das so?
      bodyText: '<Hier etwas einfügen>',
    );
    return false;
  }

  Widget buildInstructions(BuildContext context) => Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: Column(
              children: [
                Expanded(
                  child: Center(
                    child: Container(
                      margin: EdgeInsets.symmetric(horizontal: 24),
                      child: AutoSizeText(
                        read(getCurrentInstruction(), context),
                        style: CurrentTheme()
                            .themeData
                            .textTheme
                            .bodyText1
                            ?.copyWith(
                              fontSize: 24,
                            ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ),
                FlatButton(
                  child: Text("Überspringen"),
                  onPressed: () {
                    onExercise(forceContinue: true);
                    pauseRequested.value = false;
                    setState(() {});
                  },
                ),
              ],
            ),
          ),
        ],
      );

  Widget buildPause(BuildContext context) => Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 24),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    "Verschnaufpause",
                    style:
                        CurrentTheme().themeData.textTheme.bodyText1?.copyWith(
                              fontSize: 24,
                            ),
                    textAlign: TextAlign.center,
                  ),
                  if (pauseRequested.value)
                    Text(
                      FlutterI18n.translate(
                          context, 'modules.sport.exercises.tap_to_continue'),
                      textAlign: TextAlign.center,
                      style: CurrentTheme()
                          .themeData
                          .textTheme
                          .bodyText1
                          ?.copyWith(
                            fontSize: 32,
                          ),
                    ),
                  if (!pauseRequested.value)
                    AnimatedBuilder(
                        animation: anim.ctrl,
                        builder: (context, child) => Text(
                              calcProgressTime(),
                              textAlign: TextAlign.center,
                              style: CurrentTheme()
                                  .themeData
                                  .textTheme
                                  .bodyText1
                                  ?.copyWith(
                                    fontSize: 36,
                                  ),
                            )),
                ],
              ),
            ),
          ),
        ],
      );

  Widget buildBottomProgression(BuildContext context) {
    // if (pauseRequested.value){
    //   return Text("Paused");
    // }
    switch (currentPhase) {
      case EPhase.preparation:
        return buildInstructions(context);
      case EPhase.execution:
        return buildBottomTimer(context);
      case EPhase.pause:
        return buildPause(context);
      default:
        return CircularProgressIndicator();
    }
  }

  String calcProgressTime() {
    final duration = anim.ctrl.duration;
    if (duration == null) {
      return "...";
    }

    final time = (duration.inMilliseconds - anim.gameLoop.value) / 1000;
    return "${time.toStringAsFixed(1).replaceFirst(".", ",")}s";
  }

  Widget buildBottomTimer(BuildContext context) => Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          if (mode != EMode.none && !pauseRequested.value)
            Expanded(
                child: AnimatedOpacity(
              duration: Duration(milliseconds: 240),
              opacity: leftSide ? 1 : 0.12,
              child: TweenAnimationBuilder<double>(
                tween: Tween<double>(begin: 52, end: !leftSide ? 52 : 72),
                curve: Curves.elasticOut,
                duration: Duration(milliseconds: 1600),
                builder: (context, value, child) => Text(
                  "L",
                  style: TextStyle(
                      fontSize: value,
                      fontWeight: leftSide ? FontWeight.w800 : FontWeight.w800),
                  textAlign: TextAlign.center,
                ),
              ),
            )),
          Expanded(
            flex: 2,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                if (!pauseRequested.value)
                  Text(
                    "Übung läuft",
                    style:
                        CurrentTheme().themeData.textTheme.bodyText1?.copyWith(
                              fontSize: 24,
                            ),
                    textAlign: TextAlign.center,
                  ),
                if (pauseRequested.value)
                  Text(
                    FlutterI18n.translate(
                        context, 'modules.sport.exercises.tap_to_continue'),
                    textAlign: TextAlign.center,
                    style:
                        CurrentTheme().themeData.textTheme.bodyText1?.copyWith(
                              fontSize: 24,
                            ),
                  ),
                if (!pauseRequested.value)
                  AnimatedBuilder(
                      animation: anim.ctrl,
                      builder: (context, child) => Text(
                            calcProgressTime(),
                            style: CurrentTheme()
                                .themeData
                                .textTheme
                                .bodyText1
                                ?.copyWith(
                                  fontSize: 48,
                                ),
                          )),
                Container(height: 12),
                if (!pauseRequested.value) buildSet(),
              ],
            ),
          ),
          if (mode != EMode.none && !pauseRequested.value)
            Expanded(
                child: AnimatedOpacity(
              duration: Duration(milliseconds: 240),
              opacity: leftSide ? 0.12 : 1,
              child: TweenAnimationBuilder<double>(
                tween: Tween<double>(begin: 52, end: leftSide ? 52 : 72),
                curve: Curves.elasticOut,
                duration: Duration(milliseconds: 1600),
                builder: (context, value, child) => Text(
                  "R",
                  style: TextStyle(
                      fontSize: value,
                      fontWeight:
                          !leftSide ? FontWeight.w800 : FontWeight.w800),
                  textAlign: TextAlign.center,
                ),
              ),
            )),
        ],
      );

  Widget buildSet() => Column(
        children: [
          // Text(
          //     currentRepetition.toString() + " / " + maxRepetitions.toString()),
          // Text(currentSet.toString() + " / " + maxSets.toString()),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [...List.generate(maxSets, buildSingleSetProgress)],
          ),
          // Text(mode.toString()),
        ],
      );

  Widget buildSingleSetProgress(int count) {
    const dim = 16.0;
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 4.0),
      padding: EdgeInsets.all(2),
      child: Stack(
        children: [
          // Container(
          //   height: dim,
          //   width: dim,
          //   decoration: BoxDecoration(
          //       shape: BoxShape.circle, color: CurrentTheme().textPassive),
          // ),
          SizedBox(
              height: dim,
              width: dim,
              child: CircularProgressIndicator(
                // backgroundColor: CurrentTheme().passiveIconColor,
                valueColor: AlwaysStoppedAnimation<Color>(
                    CurrentTheme().textPassive.withOpacity(0.5)),
                strokeWidth: 2,
                value: 1.0,
              )),
          SizedBox(
              height: dim,
              width: dim,
              child: CircularProgressIndicator(
                // backgroundColor: CurrentTheme().passiveIconColor,
                strokeWidth: 2,
                value: currentSet > count
                    ? 1.0
                    : currentSet == count
                        ? ((currentRepetition - 1) / maxRepetitions)
                        : 0.0,
              )),
          AnimatedContainer(
            duration: transitionDuration * 0.5,
            height: dim,
            width: dim,
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: currentSet > count
                    ? CurrentTheme().tcBlue
                    : currentSet == count
                        ? CurrentTheme().textPassive.withOpacity(0.1)
                        : Colors.transparent),
          ),
        ],
      ),
    );
  }

  void setCurrentAnimToCurrentExerciseDuration() {
    anim.ctrl.duration = currentSingleDuration;
  }

  void replayExerciseAnimation() {
    setCurrentAnimToCurrentExerciseDuration();
    anim.replayWithDuration(currentSingleDuration);
  }

  Widget buildProgression() => Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(height: 8),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(
                      child: Text(
                        read(getCurrentExercise().name, context),
                        textAlign: TextAlign.center,
                        style: CurrentTheme()
                            .themeData
                            .textTheme
                            .headline2
                            ?.copyWith(
                                fontWeight: FontWeight.w900,
                                letterSpacing: -2,
                                fontSize: 28,
                                color: CurrentTheme().textSoftWhite),
                      ),
                    ),
                  ],
                ),
                Container(height: 8),
                AnimatedBuilder(
                  animation: anim.ctrl,
                  builder: (context, child) => RepaintBoundary(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        if (settings.enableInstruction)
                          progressBarContainer(
                              width: 30,
                              percent: calculatePercent(EPhase.preparation)),
                        progressBarContainer(
                            width: (!settings.enableInstruction &&
                                    !settings.enablePauses)
                                ? 160
                                : 120,
                            percent: calculatePercent(EPhase.execution)),
                        if (settings.enablePauses)
                          progressBarContainer(
                              width: 30,
                              percent: calculatePercent(EPhase.pause)),
                      ],
                    ),
                  ),
                ),
                Container(height: 8),
                Text(
                  FlutterI18n.translate(
                    context,
                    "modules.sport.program.phases."
                    "${currentPhase.toString().split(".")[1]}",
                  ).toUpperCase(),
                  style: CurrentTheme()
                      .themeData
                      .textTheme
                      .bodyText1
                      ?.copyWith(letterSpacing: -1.0),
                ),
              ],
            ),
          ),
        ],
      );

  double calculatePercent(EPhase phase) {
    switch (phase) {
      case EPhase.preparation:
        if (currentPhase != EPhase.preparation) {
          return 1.0;
        }
        // var modifier = 1 / amountInstructions - 1;
        // if (amountExercises != 0) {
        // modifier = amountInstructions / (currentInstruction + 1);
        final part = 1 / amountInstructions;
        final addition = part * (currentInstruction);
        // }
        // switch (selectedInstruction) {
        return addition + (part * anim.ctrl.value);
      // return preValue + (modifier * anim.ctrl.value);
      // case EInstruction.effect:
      //   return 0.0 + (modifier * anim.ctrl.value);
      //   break;
      // case EInstruction.startingPosition:
      //   return modifier + (modifier * anim.ctrl.value);
      //   break;
      // case EInstruction.motionSequence:
      //   return modifier * 2 + (modifier * anim.ctrl.value);
      //   break;
      // case EInstruction.repetitions:
      //   return modifier * 3 + (modifier * anim.ctrl.value);
      //   break;
      // case EInstruction.variant:
      //   return modifier * 4 + (modifier * anim.ctrl.value);
      //   break;
      // }
      case EPhase.execution:
        if (currentPhase == EPhase.preparation) {
          return 0;
        }
        if (currentPhase == EPhase.pause) {
          return 1;
        }
        final singleDur = currentSingleDuration.inMilliseconds.toDouble();
        var dur = anim.ctrl.value * singleDur;
        dur += (currentRepetition - 1) * singleDur;
        dur += currentSet * singleDur * (maxRepetitions);
        dur = dur / currentMaximumDuration.inMilliseconds;
        dur = dur.clamp(0.0, 1.0);

        /// it kinda over- and undershoots. [TODO] Find out why
        /// 25th of August: looks fine now, keep an eye on the algorithm
        return dur;
      case EPhase.pause:
        if (currentPhase != EPhase.pause) {
          return 0;
        }
        return anim.ctrl.value;
    }
  }

  Widget progressBarContainer({
    double width = 10,
    double percent = 1,
  }) =>
      Stack(
        children: [
          Container(
            margin: EdgeInsets.symmetric(horizontal: 2),
            height: 4,
            width: width,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(2),
                color: CurrentTheme().textPassive.withOpacity(0.5)),
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 2),
            height: 4,
            width: width * percent,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(2),
                color: CurrentTheme().tcBlue),
          ),
        ],
      );

  bool get isPaused => pauseRequested.value;

  Widget buildInteractions() => AnimatedBuilder(
        animation: Listenable.merge([transition.ctrl, pauseRequested]),
        builder: (context, child) => Container(
          margin: EdgeInsets.symmetric(vertical: 16),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Spacer(),
              IconButton(
                icon: Icon(Icons.chevron_left),
                onPressed:
                    transition.ctrl.isAnimating ? () {} : previousExercise,
                iconSize: 42,
              ),
              Spacer(),
              IconButton(
                icon:
                    Icon(pauseRequested.value ? Icons.play_arrow : Icons.pause),
                onPressed: pauseRequested.value ? forward : pause,
                iconSize: 42,
              ),
              Spacer(),
              IconButton(
                icon: Icon(Icons.chevron_right),
                onPressed: transition.ctrl.isAnimating ? () {} : nextExercise,
                iconSize: 42,
              ),
              Spacer(),
            ],
          ),
        ),
      );

  SizedBox debugPrepareWidget(
          Map<String, String> i18nMap, String name, BuildContext context) =>
      SizedBox(
        width: 70,
        child: Column(
          children: [
            Text(name, style: CurrentTheme().themeData.textTheme.headline2),
            Text(read(i18nMap, context)),
          ],
        ),
      );

  Exercise getCurrentExercise() => widget.exercises[selectedExercise];

  Map<String, String> getCurrentInstruction() {
    switch (selectedInstruction) {
      case EInstruction.effect:
        return widget.exercises[selectedExercise].instructions.effect;
      case EInstruction.startingPosition:
        return widget.exercises[selectedExercise].instructions.startingPosition;
      case EInstruction.motionSequence:
        return widget.exercises[selectedExercise].instructions.motionSequence;
      case EInstruction.repetitions:
        return widget.exercises[selectedExercise].instructions.repetitions;
      case EInstruction.variant:

        /// TODO: Fix [variant] vs. [variants]
        return widget.exercises[selectedExercise].instructions.variant;
    }
  }

  void nextExercise() {
    navigateExercise(1);
  }

  void previousExercise() {
    navigateExercise(-1);
  }

  void navigateExercise(double val) {
    if (transition.ctrl.isAnimating) {
      sprint(
        "[EXERCISE][TRANSITION] You're already navigating",
        prefix: '[SPORT]',
      );
      return;
    }
    if (selectedExercise == 0 && val == -1) {
      sprint(
        "[EXERCISE] You can't go back further",
        prefix: '[SPORT]',
      );
      transition.stop();
      return;
    }
    if (((selectedExercise + 1) == (widget.exercises.length)) && val == 1) {
      sprint(
        "[EXERCISE] You reached the end, there's nothing left",
        prefix: '[SPORT]',
      );
      navigateToResultPage();
      return;
    }
    transition
      ..resetWithValue(val, customDuration: transitionDuration)
      ..forward();
    anim.ctrl.stop();

    Future.delayed(transitionDuration * 0.5, () {
      selectedExercise =
          (selectedExercise + val.toInt()) % widget.exercises.length;
      initExercise();
      anim.ctrl.reset();
      if (mounted) {
        setState(() {});
      }
    });
  }

  void navigateToResultPage() {
    Tts().stop();
    AudioPlayer().stop();
    Navigator.of(context).pushReplacement(MaterialPageRoute(
        builder: (context) => ExerciseEndCard(
              completedExercises: widget.exercises,
            )));
  }

  bool hasInstruction(EInstruction instruction) =>
      getCurrentExercise().hasInstructionByExercise(instruction);

  int determineAmountInstructions() {
    var amount = 0;
    if (hasInstruction(EInstruction.effect) && settings.showEffectDesc) {
      amount++;
    }
    if (hasInstruction(EInstruction.startingPosition) &&
        settings.showStartingPositionDesc) {
      amount++;
    }
    if (hasInstruction(EInstruction.motionSequence) &&
        settings.showMotionSequenceDesc) {
      amount++;
    }
    if (hasInstruction(EInstruction.repetitions) &&
        settings.showRepetitionDesc) {
      amount++;
    }
    if (hasInstruction(EInstruction.variant) && settings.showVariantDesc) {
      amount++;
    }
    return amount;
  }

  EInstruction determineFirstInstruction() {
    if (hasInstruction(EInstruction.effect) && settings.showEffectDesc) {
      searchingForInstruction.value = false;
      return EInstruction.effect;
    }
    if (hasInstruction(EInstruction.startingPosition) &&
        settings.showStartingPositionDesc) {
      return EInstruction.startingPosition;
    }
    if (hasInstruction(EInstruction.motionSequence) &&
        settings.showMotionSequenceDesc) {
      return EInstruction.motionSequence;
    }

    if (hasInstruction(EInstruction.repetitions) &&
        settings.showRepetitionDesc) {
      return EInstruction.repetitions;
    }

    if (hasInstruction(EInstruction.variant) && settings.showVariantDesc) {
      return EInstruction.variant;
    }
    return EInstruction.effect;
  }

  EInstruction getNextInstruction(EInstruction instruction) {
    if (searchingForInstruction.value) {
      searchingForInstruction.value = false;
      return selectedInstruction;
    }
    switch (instruction) {
      case EInstruction.effect:

        /// Because it's always initialized as [effect],
        /// check flag: [searchingForInstruction]

        if (hasInstruction(EInstruction.startingPosition) &&
            settings.showStartingPositionDesc) {
          // searchingForInstruction.value = false;
          currentInstruction++;
          return EInstruction.startingPosition;
          // }

          // // }
          // if (hasInstruction(EInstruction.startingPosition) &&
          //     settings.showStartingPositionDesc) {
          //   return EInstruction.startingPosition;
        } else {
          eprint("no startingPosition");
          return getNextInstruction(EInstruction.startingPosition);
        }
      case EInstruction.startingPosition:
        if (hasInstruction(EInstruction.motionSequence) &&
            settings.showMotionSequenceDesc) {
          currentInstruction++;
          return EInstruction.motionSequence;
        } else {
          eprint("no motionSequence");
          return getNextInstruction(EInstruction.motionSequence);
        }
      case EInstruction.motionSequence:
        if (hasInstruction(EInstruction.repetitions) &&
            settings.showRepetitionDesc) {
          currentInstruction++;
          return EInstruction.repetitions;
        } else {
          eprint("no repetitions");
          return getNextInstruction(EInstruction.repetitions);
        }
      case EInstruction.repetitions:
        if (hasInstruction(EInstruction.variant) && settings.showVariantDesc) {
          currentInstruction++;
          return EInstruction.variant;
        } else {
          eprint("no variant");
          return getNextInstruction(EInstruction.variant);
        }
      case EInstruction.variant:
        throw NoNextInstructionException();
      default:
        return EInstruction.variant;
    }
  }

  bool nextInstruction() {
    /// TODO: Determine [count mode]
    /// calculate total [duration]
    /// further animation controller handling
    try {
      selectedInstruction = getNextInstruction(selectedInstruction);
      onNextInstruction();
    } on NoNextInstructionException catch (e) {
      eprint(e);
      return false;
    }
    anim.replay();
    return true;
  }

  void toggleLeftRight() {
    leftSide = !leftSide;
  }

  bool exerciseIsFinished() {
    sprint(
      "[exerciseStep()] called: $currentRepetition / $maxRepetitions Max-Reps || $currentSet / $maxSets Max-Sets",
      style: PrintStyle.sport,
      prefix: '[SPORT]',
    );

    // if (currentRepetition == 0 && currentSet == 0) {
    //   speakTts(FlutterI18n.currentLocale(context).languageCode == "de"
    //       ? [
    //           "Los geht's",
    //           "Und los geht's",
    //           "Beginne mit der Übung"
    //         ][Random().nextInt(3)]
    //       : [
    //           "Let's go",
    //           "Let's begin",
    //           "Starting the exercise"
    //         ][Random().nextInt(3)]);
    // }

    if (currentRepetition == (maxRepetitions ~/ 2) &&
        currentSet == maxSets ~/ 2) {
      speakTts(FlutterI18n.currentLocale(context)?.languageCode == "de"
          ? [
              "Weiter so",
              "Hälfte geschafft",
              "Weiter durchhalten"
            ][Random().nextInt(3)]
          : [
              "Almost done",
              "Halfway through",
              "You can do it"
            ][Random().nextInt(3)]);
    }
    if (currentRepetition == maxRepetitions && currentSet < maxSets) {
      currentRepetition = 0;
      currentSet++;
      if (currentSet != maxSets) {
        speakTts(FlutterI18n.currentLocale(context)?.languageCode == "de"
            ? [
                "Nächstes Set",
                "Set $currentSet geschafft.",
              ][Random().nextInt(2)]
            : [
                "Next Set",
                "Set $currentSet completed.",
              ][Random().nextInt(2)]);
      }

      if (mode == EMode.switching) toggleLeftRight();
    }

    if (currentRepetition < maxRepetitions) {
      currentRepetition++;
      if (mode == EMode.alternating) toggleLeftRight();
    }
    if (currentSet == maxSets) {
      /// [Exercise] finished
      return true;
    }
    return false;
  }

  void setAnimationAndDurationForExecution() {
    anim.replayWithDuration(currentSingleDuration);
  }

  void speakTts(String tts) {
    if (!settings.enableTextToSpeech) {
      return;
    }
    if (tts.isNotEmpty) {
      // if (tts.length > 30) {
      //   Tts().rate = 3;
      // } else if (tts.length <= 30 && tts.length > 15) {
      //   Tts().rate = 2;
      // } else {
      //   if (Platform.isIos){

      // Tts().rate = 1;
      //   }
      // }
      Tts().stop();
      Tts().speak(tts);
    }
  }

  void onNextInstruction() {
    final tts = read(getCurrentInstruction(), context);
    speakTts(tts);
  }

  void onExercise({bool forceContinue = false}) {
    currentPhase = EPhase.execution;
    // sprint(FlutterI18n.currentLocale(context).toLanguageTag());
    // if (!pauseRequested.value) {
    //   speakTts(FlutterI18n.currentLocale(context).languageCode == "de"
    //       ? [
    //           "Los geht's",
    //           "Und los geht's",
    //           "Beginne mit der Übung"
    //         ][Random().nextInt(3)]
    //       : [
    //           "Let's go",
    //           "Let's begin",
    //           "Starting the exercise"
    //         ][Random().nextInt(3)]);
    // }

    initExerciseMode();
    anim.resetAnimationDuration(currentSingleDuration);
    if (!settings.automaticInstruction && !forceContinue) {
      pause();
    } else {
      replayExerciseAnimation();
      speakTts(FlutterI18n.currentLocale(context)?.languageCode == "de"
          ? [
              "Los geht's",
              "Und los geht's",
              "Beginne mit der Übung"
            ][Random().nextInt(3)]
          : [
              "Let's go",
              "Let's begin",
              "Starting the exercise"
            ][Random().nextInt(3)]);
    }
  }

  void onPause() {
    currentPhase = EPhase.pause;
    speakTts(FlutterI18n.currentLocale(context)?.languageCode == "de"
        ? "Geschafft, jetzt eine kurze Pause"
        : "Well done, now a short pause");
  }

  void nextStep() {
    sprint(
      "nextStep called",
      prefix: '[SPORT]',
    );
    if (pauseRequested.value) {
      sprint(
        "pause request received, stopping main animation",
        prefix: '[SPORT]',
      );
      anim.stop();
      return;
    }
    if (anim.ctrl.isAnimating) {
      sprint(
        "Called, but still running",
        prefix: '[SPORT]',
      );
      return;
    }
    if (mounted) {
      settings = BlocProvider.of<ExerciseSettingsBloc>(context).state;
      // if (!settings.enableInstruction) {
      /// set for [Exercise]!
      // if (!exerciseIsFinished()) {
      //   setState(() {});
      //   return;
      // }
      // }

      switch (currentPhase) {
        case EPhase.preparation:
          anim.replayWithDuration(instructionDuration);
          if (!nextInstruction()) {
            /// set for [Execution]!
            // if (!settings.automaticInstruction) {
            // pause();
            // } else {
            onExercise();
            // }
          }
          eprint(currentInstruction);
          setState(() {});
          break;
        case EPhase.execution:
          final exerciseFinished = exerciseIsFinished();
          setState(() {});
          if (!exerciseFinished) {
            sprint(
              "Exercise is not finished!",
              prefix: '[SPORT]',
            );
            // setAnimationAndDurationForExecution();
            replayExerciseAnimation();
            return;
          } else {
            sprint(
              "Exercise is FINISHED!",
              prefix: '[SPORT]',
            );

            if (settings.enablePauses) {
              onPause();
              if (settings.automaticPauses) {
                anim.replayWithDuration(pauseDuration);
              } else {
                anim.resetAnimationDuration(pauseDuration);
                pause();
              }
              setState(() {});
            } else if (!settings.enablePauses) {
              nextExercise();
              exerciseTransitionFlag = 1;
            }
          }

          break;
        case EPhase.pause:
          // TODO: Handle this case.
          nextExercise();
          exerciseTransitionFlag = 1;
          // initExercise();
          // setState(() {});
          break;
      }
    }
  }

  void previousStep() {
    /// TODO: [discuss with M.L. wether this feature is really required]
  }

  void pause() {
    if (mounted) {
      AudioPlayer().pause();
      if (anim.ctrl.isAnimating) {
        anim.stop();
        pauseRequested.value = true;
      } else {
        sprint(
          "Pause requested",
          style: PrintStyle.sport,
          prefix: '[SPORT]',
        );
        pauseRequested.value = true;
      }
    }
  }

  void forward() {
    AudioPlayer().resume();
    pauseRequested.value = false;
    sprint(
      "Forward requested",
      style: PrintStyle.sport,
      prefix: '[SPORT]',
    );
    if (currentPhase == EPhase.execution) {
      if (currentRepetition == 0 && currentSet == 0) {
        speakTts(FlutterI18n.currentLocale(context)?.languageCode == "de"
            ? [
                "Los geht's",
                "Und los geht's",
                "Beginne mit der Übung"
              ][Random().nextInt(3)]
            : [
                "Let's go",
                "Let's begin",
                "Starting the exercise"
              ][Random().nextInt(3)]);
      } else {
        speakTts(FlutterI18n.currentLocale(context)?.languageCode == "de"
            ? ["Weiter geht's", "Fahre fort", "Und los."][Random().nextInt(3)]
            : [
                "Alright, continuing.",
                "Going on",
                "Let's go again"
              ][Random().nextInt(3)]);
      }
    }
    if (searchingForInstruction.value) {
      nextStep();
    }
    if (!transition.ctrl.isAnimating) {
      anim.forward();
    }
    // setState(() {});
  }

  Duration get currentSingleDuration =>
      Duration(milliseconds: getCurrentExercise().singleDuration) *
      durationFactor.value;

  Duration get currentMaximumDuration =>
      Duration(
          milliseconds:
              getCurrentExercise().singleDuration * maxRepetitions * maxSets) *
      durationFactor.value;

  Duration get instructionDuration =>
      Duration(milliseconds: ExerciseConstants.instructionDuration) *
      durationFactor.value;

  Duration get transitionDuration =>
      Duration(milliseconds: ExerciseConstants.transitionDuration) *
      durationFactor.value;

  Duration get pauseDuration => (settings.pauseDuration) * durationFactor.value;

  ValueNotifier<double> durationFactor = ValueNotifier<double>(1.0);
}

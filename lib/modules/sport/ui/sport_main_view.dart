/*
 * Copyright 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../../common/tc_theme.dart';
import '../../../common/tc_utility_functions.dart';
import '../../../repositories/sport/exercises/exercises_repository.dart';
import '../i18n_keys.dart';
import '../model/sport_model.dart';
import 'tabs/exercises_tab.dart';
import 'tabs/external_tab.dart';

class SportMainView extends StatelessWidget {
  const SportMainView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => RepositoryProvider<ExerciseRepository>(
        create: (context) => ExerciseRepository(),
        child: FutureBuilder<SportModel>(
          future: ExerciseRepository().getExercises(),
          builder: (context, snapshot) {
            final data = snapshot.data;
            return data == null
                ? Scaffold(
                    appBar: AppBar(
                      title: Text(
                        FlutterI18n.translate(
                            context, "common.messages.loading"),
                      ),
                    ),
                    body: Center(child: CircularProgressIndicator()))
                : DefaultTabController(
                    length: data.tabs.length + 1,
                    child: Scaffold(
                      appBar: AppBar(
                        title: I18nText('$sportKey.title'),
                        bottom: TabBar(
                          isScrollable: true,
                          labelStyle: CurrentTheme().sportTextLabelSubtitle,
                          tabs: [
                            Tab(
                              child: Text(
                                FlutterI18n.translate(
                                    context, '$sportSport.tab_title'),
                              ),
                            ),
                            for (final tab in data.tabs)
                              Tab(
                                child: Text(
                                  read(tab.tab, context),
                                ),
                              ),
                          ],
                        ),
                      ),
                      body: TabBarView(
                        children: [
                          ExercisesTab(
                            data.exercises,
                            key: ValueKey('sport_tab_exercise'),
                          ),
                          for (final tab in data.tabs)
                            ExternalTab(
                              tabModel: tab,
                              segment: tab.segment,
                            ),
                        ],
                      ),
                    ),
                  );
          },
        ),
      );
}

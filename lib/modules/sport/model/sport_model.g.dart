// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sport_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SportModel _$SportModelFromJson(Map<String, dynamic> json) => SportModel(
      tabs: (json['tabs'] as List<dynamic>)
          .map((e) => SportTabModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      exercises: (json['exercises'] as List<dynamic>)
          .map((e) => Exercise.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$SportModelToJson(SportModel instance) =>
    <String, dynamic>{
      'tabs': instance.tabs,
      'exercises': instance.exercises,
    };

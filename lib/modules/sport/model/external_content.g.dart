// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'external_content.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ExternalContent _$ExternalContentFromJson(Map<String, dynamic> json) =>
    ExternalContent(
      imageUrl: json['imageUrl'] as String,
      text: Map<String, String>.from(json['text'] as Map),
      link: Map<String, String>.from(json['link'] as Map),
    );

Map<String, dynamic> _$ExternalContentToJson(ExternalContent instance) =>
    <String, dynamic>{
      'imageUrl': instance.imageUrl,
      'text': instance.text,
      'link': instance.link,
    };

/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../../common/tc_theme.dart';
import '../bloc/bookmark_links/bookmark_links_bloc.dart';
import 'bookmarked_link_page.dart';
import 'important_links_page.dart';

class ImportantLinksStartPage extends StatefulWidget {
  const ImportantLinksStartPage({Key? key}) : super(key: key);

  @override
  _ImportantLinksStartPageState createState() =>
      _ImportantLinksStartPageState();
}

class _ImportantLinksStartPageState extends State<ImportantLinksStartPage>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(
      vsync: this,
      length: 2,
      initialIndex:
          BlocProvider.of<BookmarkLinksBloc>(context).state.startIndex,
    );
    _tabController.addListener(_handleTabSelection);
  }

  void _handleTabSelection() {
    BlocProvider.of<BookmarkLinksBloc>(context)
        .add(SetStartIndexEvent(startIndex: _tabController.index));
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        backgroundColor: Theme.of(context).backgroundColor,
        appBar: AppBar(
          backgroundColor: Theme.of(context).primaryColor,
          title: Text(
              FlutterI18n.translate(context, 'modules.important_links.title')),
          bottom: PreferredSize(
            preferredSize: Size.fromHeight(60.0),
            child: Container(
              constraints: BoxConstraints(minWidth: 140, maxWidth: 320),
              child: TabBar(
                controller: _tabController,
                indicatorColor: CurrentTheme().tcBlue,
                indicatorPadding: EdgeInsets.symmetric(horizontal: 12.0),
                unselectedLabelColor: CurrentTheme().textPassive,
                labelColor: CurrentTheme().tcBlue,
                labelStyle: Theme.of(context).textTheme.headline4,
                tabs: [
                  Tab(
                    child: I18nText(
                      "modules.important_links.all",
                      child: Text(
                        'ALL',
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                  Tab(
                    child: I18nText(
                      "modules.important_links.bookmark",
                      child: Text(
                        'BOOKMARKS',
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        body: TabBarView(
          controller: _tabController,
          children: [
            ImportantLinksPage(),
            BookmarkLinkPage(),
          ],
        ),
      );
}

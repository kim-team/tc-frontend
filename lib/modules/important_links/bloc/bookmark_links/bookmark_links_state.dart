/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:meta/meta.dart';

import '../../model/item.dart';

part 'bookmark_links_state.g.dart';

@immutable
@JsonSerializable(anyMap: true, explicitToJson: true)
class BookmarkLinksState {
  final List<Item> bookmarkedItems;
  final int startIndex;

  @mustCallSuper
  BookmarkLinksState({required this.bookmarkedItems, required this.startIndex});

  factory BookmarkLinksState.fromJson(Map<String, dynamic> json) =>
      _$BookmarkLinksStateFromJson(json);

  Map<String, dynamic> toJson() => _$BookmarkLinksStateToJson(this);
}

class InitialBookmarkLinksState extends BookmarkLinksState {
  InitialBookmarkLinksState() : super(bookmarkedItems: [], startIndex: 0);
}

class AddBookmarkLinksState extends BookmarkLinksState {
  AddBookmarkLinksState(List<Item> bookmarkedItems, int startIndex)
      : super(bookmarkedItems: bookmarkedItems, startIndex: startIndex);
}

class RemoveBookmarkLinksState extends BookmarkLinksState {
  RemoveBookmarkLinksState(List<Item> bookmarkedItems, int startIndex)
      : super(bookmarkedItems: bookmarkedItems, startIndex: startIndex);
}

class SwapStartIndexState extends BookmarkLinksState {
  SwapStartIndexState(List<Item> bookmarkedItems, int startIndex)
      : super(bookmarkedItems: bookmarkedItems, startIndex: startIndex);
}

// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'bookmark_links_state.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BookmarkLinksState _$BookmarkLinksStateFromJson(Map json) => BookmarkLinksState(
      bookmarkedItems: (json['bookmarkedItems'] as List<dynamic>)
          .map((e) => Item.fromJson(Map<String, dynamic>.from(e as Map)))
          .toList(),
      startIndex: json['startIndex'] as int,
    );

Map<String, dynamic> _$BookmarkLinksStateToJson(BookmarkLinksState instance) =>
    <String, dynamic>{
      'bookmarkedItems':
          instance.bookmarkedItems.map((e) => e.toJson()).toList(),
      'startIndex': instance.startIndex,
    };

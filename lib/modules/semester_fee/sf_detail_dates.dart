/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:timeline_tile/timeline_tile.dart';

import '../../common/tc_theme.dart';
import 'sf_custom_timeline_tile.dart';
import 'sf_interval.dart';

class DetailDatesWidget extends StatefulWidget {
  const DetailDatesWidget({
    Key? key,
    required this.nearestInterval,
    required this.now,
    required this.state,
  }) : super(key: key);

  final SFInterval nearestInterval;
  final DateTime now;
  final SFState state;

  @override
  _DetailDatesWidgetState createState() => _DetailDatesWidgetState();
}

class _DetailDatesWidgetState extends State<DetailDatesWidget>
    with SingleTickerProviderStateMixin {
  final ValueNotifier<bool> expanded = ValueNotifier<bool>(false);

  @override
  Widget build(BuildContext context) => ValueListenableBuilder(
        valueListenable: expanded,
        builder: (context, value, child) => InkWell(
          borderRadius: BorderRadius.circular(12),
          child: Center(
            child: SizedBox(
              width: 270,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  CustomTimelineTile(
                    timelineAlign: TimelineAlign.start,
                    date: widget.nearestInterval.endPayment,
                    labelI18nKey: "modules.semester_fee.timeline.end_payment",
                    position: CTTPosition.isStart,
                  ),
                  AnimatedContainer(
                    duration: Duration(milliseconds: 160),
                    curve: Curves.easeInOut,
                    height: expanded.value ? 68 : 0,
                    margin: EdgeInsets.only(left: 10),
                    child: CustomTimelineTile(
                      date: widget.now,
                      hasIndicator: false,
                      timelineAlign: TimelineAlign.start,
                      customWidget: Padding(
                        padding: EdgeInsets.symmetric(
                            vertical: 4.0, horizontal: 9.0),
                        child: AnimatedOpacity(
                          opacity: expanded.value ? 1.0 : 0.0,
                          duration: Duration(milliseconds: 160),
                          child: Builder(
                            builder: (context) {
                              final calcDaysBetween = widget
                                      .nearestInterval.endPayment
                                      .difference(widget.now.isAfter(widget
                                              .nearestInterval.beginPayment)
                                          ? widget.now
                                          : widget.nearestInterval.beginPayment)
                                      .inDays +
                                  1;
                              return Stack(
                                clipBehavior: Clip.none,
                                children: [
                                  AnimatedPositioned(
                                    duration: Duration(milliseconds: 360),
                                    curve: Curves.easeInBack.flipped,
                                    left: expanded.value ? 0 : 60,
                                    child: Text(
                                      "$calcDaysBetween ${FlutterI18n.plural(
                                        context,
                                        'modules.semester_fee'
                                        '.count_days_generic.day',
                                        calcDaysBetween,
                                      )}",
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodyText1
                                          ?.copyWith(
                                              color: CorporateColors
                                                  .tinyCampusIconGrey,
                                              fontSize: 24),
                                    ),
                                  ),
                                ],
                              );
                            },
                          ),
                        ),
                      ),
                    ),
                  ),
                  CustomTimelineTile(
                    timelineAlign: TimelineAlign.start,
                    isToday:
                        widget.now.isAfter(widget.nearestInterval.beginPayment)
                            ? true
                            : false,
                    date:
                        widget.now.isAfter(widget.nearestInterval.beginPayment)
                            ? widget.now
                            : widget.nearestInterval.beginPayment,
                    customTextLabelColor: widget.state != SFState.urgentPayment
                        ? null
                        : CorporateColors.cafeteriaCautionRed,
                    labelI18nKey:
                        widget.now.isAfter(widget.nearestInterval.beginPayment)
                            ? "modules.semester_fee.timeline.today"
                            : "modules.semester_fee.timeline.begin_payment",
                  ),
                  AnimatedContainer(
                    duration: Duration(milliseconds: 160),
                    curve: Curves.easeInOut,
                    height: expanded.value ? 68 : 0,
                    margin: EdgeInsets.only(left: 10),
                    child: CustomTimelineTile(
                      date: widget.now,
                      hasIndicator: false,
                      timelineAlign: TimelineAlign.start,
                      customWidget: Padding(
                        padding: EdgeInsets.symmetric(
                            vertical: 4.0, horizontal: 9.0),
                        child: AnimatedOpacity(
                          opacity: expanded.value ? 1.0 : 0.0,
                          duration: Duration(milliseconds: 90),
                          child: Builder(builder: (context) {
                            var calcDaysBetween = widget.now
                                    .difference(
                                        widget.nearestInterval.beginPayment)
                                    .inDays
                                    .abs() +
                                1;
                            if (widget.now
                                        .difference(
                                            widget.nearestInterval.beginPayment)
                                        .inDays <=
                                    1 &&
                                widget.now.day ==
                                    widget.nearestInterval.beginPayment.day) {
                              calcDaysBetween = 0;
                            }
                            return Stack(
                              clipBehavior: Clip.none,
                              children: [
                                AnimatedPositioned(
                                  duration: Duration(milliseconds: 460),
                                  curve: Curves.easeInBack.flipped,
                                  left: expanded.value ? 0 : 60,
                                  child: Text(
                                    "$calcDaysBetween ${FlutterI18n.plural(
                                      context,
                                      'modules.semester_fee'
                                      '.count_days_generic.day',
                                      calcDaysBetween,
                                    )}",
                                    style: Theme.of(context)
                                        .textTheme
                                        .bodyText1
                                        ?.copyWith(
                                            color: CorporateColors
                                                .tinyCampusIconGrey,
                                            fontSize: 24),
                                  ),
                                ),
                              ],
                            );
                          }),
                        ),
                      ),
                    ),
                  ),
                  CustomTimelineTile(
                    timelineAlign: TimelineAlign.start,
                    isToday:
                        widget.now.isAfter(widget.nearestInterval.beginPayment)
                            ? false
                            : true,
                    date:
                        widget.now.isAfter(widget.nearestInterval.beginPayment)
                            ? widget.nearestInterval.beginPayment
                            : widget.now,
                    labelI18nKey:
                        widget.now.isAfter(widget.nearestInterval.beginPayment)
                            ? "modules.semester_fee.timeline.begin_payment"
                            : "modules.semester_fee.timeline.today",
                    position: CTTPosition.isEnd,
                  ),
                ],
              ),
            ),
          ),
        ),
      );
}

/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:intl/intl.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../common/tc_theme.dart';
import 'sf_animation.dart';
import 'sf_button.dart';
import 'sf_debug_column.dart';
import 'sf_detail_dates.dart';
import 'sf_detail_view_icon.dart';
import 'sf_helper_functions.dart';
import 'sf_inner_circle.dart';
import 'sf_interval.dart';

class SemesterFeeStartScreen extends StatefulWidget {
  const SemesterFeeStartScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => SemesterFeeState();
}

class SemesterFeeState extends State<SemesterFeeStartScreen>
    with TickerProviderStateMixin {
  late AnimationController controller;
  late CurvedAnimation curve;
  late SFAnimation animation;

  late List<SFInterval> intervals;
  late SFInterval nearestInterval;
  late SFState state;
  DateTime now = DateTime.now();
  final DateTime initialNow = DateTime.now();
  double sliderValue = 0.0;
  bool showDebug = !kReleaseMode;
  bool showDebugText = false;
  late SharedPreferences prefs;
  EdgeInsets stackMargin = const EdgeInsets.symmetric(horizontal: 8.0);

  final ValueNotifier<bool> hasPaidForNearest = ValueNotifier<bool>(false);
  final ValueNotifier<bool> showButtons = ValueNotifier<bool>(true);
  final ValueNotifier<bool> showRadial = ValueNotifier<bool>(true);
  final ValueNotifier<bool> showHint = ValueNotifier<bool>(true);

  void initAnimation() {
    animation = SFAnimation(
      controller: controller,
      startPercent: 0.0,
      endPercent: calculateEnd(),
    );
  }

  void initAnimationController() {
    controller = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 900),
    );
  }

  void startAnimation() {
    curve = CurvedAnimation(parent: controller, curve: Curves.linear);
    controller.forward();
  }

  @override
  void initState() {
    super.initState();
    initDates();
    initAnimationController();
    initAnimation();
    startAnimation();
    initSharedPreferences();
  }

  Future<void> initSharedPreferences() async {
    prefs = await SharedPreferences.getInstance();
    initHasPaidValue();
    showHint.value = await SFUtil.isHideHint();
  }

  Future<void> initHasPaidValue() async {
    if (state == SFState.waitingForBegin) {
      hasPaidForNearest.value = false;
    } else {
      hasPaidForNearest.value =
          prefs.getBool(constructPaidIdentifierString()) ?? false;
    }
  }

  Future<void> togglePaidForNearest() async {
    try {
      await prefs.setBool(
          constructPaidIdentifierString(), !hasPaidForNearest.value);
      hasPaidForNearest.value = !hasPaidForNearest.value;
    } on Exception catch (e) {
      debugPrint(e.toString());
    }
  }

  String constructPaidIdentifierString() =>
      'sf-paid-${DateFormat('yyyy-MM-dd').format(
        nearestInterval.startInterval,
      )}';

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  double calculateEnd() {
    const threshold = 140;
    if (state != SFState.error) {
      var calcDays = 0;
      final paymentIntervalLength = nearestInterval.beginPayment
          .difference(nearestInterval.endPayment)
          .inDays
          .abs();
      switch (state) {
        case SFState.waitingForBegin:
          calcDays = nearestInterval.beginPayment.difference(now).inDays;
          if (calcDays > threshold) {
            calcDays = threshold;
          }
          calcDays++;
          return 1 - (calcDays / threshold);
        case SFState.beginPayment:
          calcDays = nearestInterval.endPayment.difference(now).inDays;
          calcDays++;
          return 1 - (calcDays / paymentIntervalLength);
        case SFState.urgentPayment:
          calcDays = nearestInterval.endPayment.difference(now).inDays;
          calcDays++;
          return 1 - (calcDays / paymentIntervalLength);
        case SFState.error:
          return 0.0;
      }
    }
    return 0.0;
  }

  void initDates() {
    final currentYear = now.year;
    intervals = <SFInterval>[];
    for (var year = currentYear - 1; year < currentYear + 2; year++) {
      intervals.add(
        SFInterval(
          startInterval: DateTime(year, 2, 1),
          beginPayment: DateTime(year, 6, 15),
          endPayment: DateTime(year, 7, 31, 23, 59, 59),
          semester: Semester.forWinter,
        ),
      );
      intervals.add(
        SFInterval(
          startInterval: DateTime(year, 8, 1),
          beginPayment: DateTime(year, 12, 15),
          endPayment: DateTime(year + 1, 1, 31, 23, 59, 59),
          semester: Semester.forSummer,
        ),
      );
    }
    nearestInterval = findNearestInterval(intervals);
    state = findState(nearestInterval);
  }

  SFState findState(SFInterval interval) {
    if (now.isAfter(interval.startInterval) &&
        now.isBefore(interval.beginPayment)) {
      return SFState.waitingForBegin;
    }

    if (now.isAfter(interval.beginPayment) &&
        now.isBefore(interval.endPayment)) {
      if (now.difference(interval.endPayment).inDays.abs() > 15) {
        return SFState.beginPayment;
      } else {
        return SFState.urgentPayment;
      }
    }

    return SFState.error;
  }

  SFInterval findNearestInterval(List<SFInterval> intervals) {
    var result = intervals.first;

    for (final interval in intervals) {
      final isBefore = now.isAfter(interval.startInterval);
      final isAfter = now.isBefore(interval.endPayment);

      if (isBefore && isAfter) {
        result = interval;
        break;
      }
    }

    // This should never happen
    return result;
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        backgroundColor: Theme.of(context).backgroundColor,
        appBar: AppBar(
          title: I18nText('modules.semester_fee.title'),
          actions: [
            if (!kReleaseMode)
              Switch(
                onChanged: (value) {
                  setState(() {
                    showDebugText = !showDebugText;
                  });
                },
                value: showDebugText,
              ),
            if (!kReleaseMode)
              Switch(
                onChanged: (value) {
                  setState(() {
                    showDebug = !showDebug;
                  });
                },
                value: showDebug,
              ),
            ValueListenableBuilder(
              valueListenable: showRadial,
              builder: (context, value, child) => IconButton(
                onPressed: () {
                  showRadial.value = !showRadial.value;
                },
                tooltip: showRadial.value
                    ? FlutterI18n.translate(
                        context, 'modules.semester_fee.view_detail')
                    : FlutterI18n.translate(
                        context, 'modules.semester_fee.view_main'),
                icon: showRadial.value
                    ? SFDetailViewIcon()
                    : SizedBox(
                        width: 16,
                        height: 16,
                        child: AnimatedBuilder(
                          animation: controller,
                          builder: (context, child) =>
                              CircularProgressIndicator(
                            value: animation.percent.value,
                            strokeWidth: 2,
                            backgroundColor:
                                CorporateColors.tinyCampusBlue.withOpacity(0.3),
                            valueColor: AlwaysStoppedAnimation<Color>(
                                CurrentTheme().tcBlue),
                          ),
                        ),
                      ),
              ),
            )
          ],
        ),
        body: Stack(
          clipBehavior: Clip.none,
          fit: StackFit.passthrough,
          children: [
            Positioned(
                bottom: 0,
                left: 0.0,
                right: 0.0,
                top: 0,
                child: SingleChildScrollView(
                    physics: BouncingScrollPhysics(),
                    child: Container(
                      margin: stackMargin,
                      child: Column(
                        children: [
                          if (showDebug)
                            Slider(
                              label: "debug-slider",
                              onChanged: (value) {
                                now = initialNow
                                    .add(Duration(days: value.toInt()));
                                sliderValue = value;
                                initDates();
                                initAnimation();
                                initHasPaidValue();
                                setState(() {});
                              },
                              value: sliderValue,
                              min: -240,
                              max: 240,
                            ),
                          buildCircleStack(),
                          Container(
                            margin: EdgeInsets.symmetric(vertical: 12.0),
                            padding: EdgeInsets.all(12.0),
                            child: I18nText(
                              'modules.semester_fee.disclaimer',
                              child: Text('Die hier dargestellten Daten...',
                                  textAlign: TextAlign.center,
                                  style: Theme.of(context)
                                      .textTheme
                                      .headline4
                                      ?.copyWith(
                                          color: CorporateColors
                                              .tinyCampusIconGrey)),
                            ),
                          ),
                          Container(height: 72),
                        ],
                      ),
                    ))),
            AnimatedPositioned(
              duration: Duration(milliseconds: 160),
              bottom: 16,
              left: 0.0,
              right: 0.0,
              child: Container(
                margin: stackMargin,
                child: buildButtons(context),
              ),
            ),
          ],
        ),
      );

  Column buildButtons(BuildContext context) => Column(
        children: [
          if (state != SFState.waitingForBegin)
            ViewPaymentDataButton(
                label: FlutterI18n.translate(
                    context, 'modules.semester_fee.bank_transfer_data')),
          if (state == SFState.waitingForBegin)
            ViewPaymentDataButton(
              label: FlutterI18n.translate(
                  context, 'modules.semester_fee.thm_official_site'),
              iconData: Icons.info_outline,
            ),
        ],
      );

  Widget buildCircleStack() {
    if (state == SFState.error) {
      return Container();
    }
    var color = CorporateColors.tinyCampusBlue;
    switch (state) {
      case SFState.waitingForBegin:
        color = CorporateColors.tinyCampusBlue;
        break;
      case SFState.beginPayment:
        color = CorporateColors.cafeteriaVeganGreen;
        break;
      case SFState.urgentPayment:
        color = CorporateColors.cafeteriaCautionRed;
        break;
      case SFState.error:
        color = CorporateColors.cafeteriaCautionRed;
        break;
    }

    return buildRadialCard(color);
  }

  Widget buildRadialCard(Color color) => Card(
        margin: EdgeInsets.only(top: 8.0),
        elevation: 0,
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
        child: Column(
          children: [
            if (showDebugText)
              DebugTextColumn(
                state: state,
                nearestInterval: nearestInterval,
                now: now,
              ),
            Padding(
              padding: const EdgeInsets.only(top: 24.0, bottom: 8.0),
              child: Column(
                children: [
                  Text(
                      FlutterI18n.translate(
                          context, 'modules.semester_fee.fee_period'),
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.headline1),
                  Text(
                    "${FlutterI18n.translate(
                      context,
                      nearestInterval.semester == Semester.forWinter
                          ? "modules.semester_fee.for_winter_semester"
                          : "modules.semester_fee.for_summer_semester",
                    )} ${nearestInterval.endPayment.year}",
                    style: Theme.of(context)
                        .textTheme
                        .bodyText1
                        ?.copyWith(color: CorporateColors.tinyCampusIconGrey),
                  )
                ],
              ),
            ),
            ValueListenableBuilder(
              valueListenable: hasPaidForNearest,
              builder: (context, value, child) => AnimatedSize(
                duration: Duration(milliseconds: 160),
                curve: Curves.easeInOutQuad,
                child: ValueListenableBuilder(
                  valueListenable: showRadial,
                  builder: (context, value, child) => AnimatedSwitcher(
                    switchInCurve: Curves.easeInOutQuad,
                    switchOutCurve: Curves.easeInOutQuad,
                    duration: Duration(milliseconds: 64),
                    child: !showRadial.value
                        ? DetailDatesWidget(
                            nearestInterval: nearestInterval,
                            now: now,
                            state: state,
                          )
                        : Stack(
                            alignment: Alignment.center,
                            // fit: StackFit.expand,
                            children: [
                              Padding(
                                padding: EdgeInsets.all(12.0),
                                child: AnimatedBuilder(
                                  animation: controller,
                                  builder: (context, child) => Opacity(
                                    opacity: kReleaseMode
                                        ? 1.0
                                        : animation.opacity.value,
                                    child: Transform.scale(
                                      scale: animation.scale.value,
                                      child: CircularPercentIndicator(
                                        lineWidth: 5,
                                        progressColor: hasPaidForNearest.value
                                            ? CorporateColors
                                                .cafeteriaVeganGreen
                                            : color,
                                        radius: 260,
                                        backgroundColor: CorporateColors
                                            .tinyCampusBlue
                                            .withOpacity(0.3),
                                        circularStrokeCap:
                                            CircularStrokeCap.round,
                                        percent: animation.percent.value % 1,
                                        animation: false,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 48.0, vertical: 12.0),
                                child: Center(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      AnimatedSwitcher(
                                        duration: Duration(milliseconds: 160),
                                        child: hasPaidForNearest.value
                                            ? TweenAnimationBuilder<double>(
                                                duration:
                                                    Duration(milliseconds: 560),
                                                curve: Curves.elasticOut,
                                                tween: Tween<double>(
                                                    begin: 1.75, end: 1.0),
                                                builder:
                                                    (context, value, child) =>
                                                        Icon(
                                                  Icons.check_circle_rounded,
                                                  size: value * 128,
                                                  color: CorporateColors
                                                      .cafeteriaVeganGreen,
                                                ),
                                              )
                                            : InnerCircleWidget(
                                                hideHint: showHint,
                                                nearestInterval:
                                                    nearestInterval,
                                                now: now,
                                                context: context,
                                                color: color,
                                                state: state,
                                              ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
            ),
          ],
        ),
      );
}

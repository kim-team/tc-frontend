/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../../common/constants/routing_constants.dart';
import '../../../common/tc_theme.dart';
import '../v_card/v_card_bloc.dart';
import 'vcard_contact_note.dart';
import 'vcard_sharing.dart';

/// This class is used in the router to give arguments to a screen
class VCardContactScreenArguments {
  /// The constructor of [VCardContactScreenArguments]
  VCardContactScreenArguments(this.pos);

  /// The position of [vCard] in the contact list
  int pos;
}

/// The screen where you can see the information of a contact
class VCardContactScreen extends StatelessWidget {
  /// The position of [vCard] in the contact list
  final int pos;

  /// The constructor of [VCardContactScreen]
  VCardContactScreen({
    Key? key,
    required this.pos,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => BlocBuilder<VCardBloc, VCardState>(
        builder: (context, state) => Scaffold(
          backgroundColor: CorporateColors.passiveBackgroundLight,
          appBar: AppBar(
            title: Text(
              _getValue(state.contacts.elementAt(pos).formattedName),
            ),
            actions: <Widget>[
              IconButton(
                icon: Icon(
                  Icons.share,
                  color: Color.fromARGB(255, 25, 140, 178),
                ),
                onPressed: () =>
                    VCardSharing().share(state.contacts.elementAt(pos)),
              ),
            ],
          ),
          body: ListView(children: <Widget>[
            Container(
              color: Colors.white,
              margin: EdgeInsets.symmetric(vertical: 20.0),
              padding: EdgeInsets.only(top: 40, bottom: 10),
              alignment: Alignment.center,
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(top: 12, bottom: 20.0),
                    child: Text(
                      _getValue(state.contacts.elementAt(pos).formattedName),
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 24,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      state.contacts.elementAt(pos).note == ''
                          ? Container(
                              decoration: BoxDecoration(
                                color: Theme.of(context).colorScheme.secondary,
                                borderRadius: BorderRadius.all(
                                  Radius.circular(18),
                                ),
                              ),
                              alignment: Alignment.center,
                              child: FlatButton(
                                onPressed: () {
                                  Navigator.pushNamed(
                                      context, vCardContactNoteRoute,
                                      arguments:
                                          VCardContactNoteArguments(pos));
                                },
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Row(
                                    children: <Widget>[
                                      I18nText(
                                        'modules.business_cards.'
                                        'vcard.contacts.write_note',
                                        child: Text(
                                          "schreibe eine Notiz ",
                                          style: TextStyle(
                                            fontSize: 20,
                                            color: Colors.white,
                                          ),
                                        ),
                                      ),
                                      Icon(
                                        Icons.edit,
                                        color: Colors.white,
                                        size: 40,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            )
                          : Column(
                              mainAxisSize: MainAxisSize.max,
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: <Widget>[
                                SizedBox(
                                  width: MediaQuery.of(context).size.width - 60,
                                  child: Text(
                                    state.contacts.elementAt(pos).note,
                                    maxLines: 20,
                                    textAlign: TextAlign.center,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                      fontSize: 20,
                                    ),
                                  ),
                                ),
                                IconButton(
                                  alignment: Alignment.bottomRight,
                                  icon: Icon(Icons.edit),
                                  color: Colors.grey[300],
                                  iconSize: 40,
                                  onPressed: () {
                                    Navigator.pushNamed(
                                        context, vCardContactNoteRoute,
                                        arguments:
                                            VCardContactNoteArguments(pos));
                                  },
                                ),
                              ],
                            ),
                    ],
                  ),
                ],
              ),
            ),
            Container(
              color: Colors.white,
              padding: EdgeInsets.only(left: 15, top: 20, bottom: 7),
              alignment: Alignment.centerLeft,
              child: I18nText(
                'modules.business_cards.vcard.contacts.email',
                child: Text(
                  "E-Mail",
                  textAlign: TextAlign.start,
                  style: TextStyle(
                    fontSize: 20,
                    color: Theme.of(context).colorScheme.secondary,
                  ),
                ),
              ),
            ),
            Container(
              color: Colors.white,
              padding: EdgeInsets.only(left: 15),
              alignment: Alignment.centerLeft,
              child: Text(
                _getValue(state.contacts.elementAt(pos).email),
                style: TextStyle(
                  fontSize: 18,
                ),
              ),
            ),
            Container(
              color: Colors.white,
              padding: EdgeInsets.only(left: 15, top: 20, bottom: 7),
              alignment: Alignment.centerLeft,
              child: I18nText(
                'modules.business_cards.vcard.contacts.url',
                child: Text(
                  "URL",
                  textAlign: TextAlign.start,
                  style: TextStyle(
                    fontSize: 20,
                    color: Theme.of(context).colorScheme.secondary,
                  ),
                ),
              ),
            ),
            Container(
              color: Colors.white,
              padding: EdgeInsets.only(left: 15),
              alignment: Alignment.centerLeft,
              child: Text(
                _getValue(state.contacts.elementAt(pos).url),
                style: TextStyle(
                  fontSize: 18,
                ),
              ),
            ),
            Container(
              color: Colors.white,
              padding: EdgeInsets.only(left: 15, top: 20, bottom: 7),
              alignment: Alignment.centerLeft,
              child: I18nText(
                'modules.business_cards.vcard.contacts.organisation',
                child: Text(
                  "Organisation",
                  textAlign: TextAlign.start,
                  style: TextStyle(
                    fontSize: 20,
                    color: Theme.of(context).colorScheme.secondary,
                  ),
                ),
              ),
            ),
            Container(
              color: Colors.white,
              padding: EdgeInsets.only(left: 15),
              alignment: Alignment.centerLeft,
              child: Text(
                _getValue(state.contacts.elementAt(pos).organization),
                style: TextStyle(
                  fontSize: 18,
                ),
              ),
            ),
            Container(
              color: Colors.white,
              padding: EdgeInsets.only(left: 15, top: 20, bottom: 7),
              alignment: Alignment.centerLeft,
              child: I18nText(
                'modules.business_cards.vcard.contacts.interests',
                child: Text(
                  "Interessen",
                  textAlign: TextAlign.start,
                  style: TextStyle(
                    fontSize: 20,
                    color: Theme.of(context).colorScheme.secondary,
                  ),
                ),
              ),
            ),
            Container(
              color: Colors.white,
              padding: EdgeInsets.only(left: 15),
              alignment: Alignment.centerLeft,
              child: Text(
                _getValue(state.contacts.elementAt(pos).categories),
                style: TextStyle(
                  fontSize: 18,
                ),
              ),
            ),
            Container(
              color: Colors.white,
              padding: EdgeInsets.only(left: 15, top: 20, bottom: 7),
              alignment: Alignment.centerLeft,
              child: I18nText(
                'modules.business_cards.vcard.contacts.role',
                child: Text(
                  "Rolle",
                  textAlign: TextAlign.start,
                  style: TextStyle(
                    fontSize: 20,
                    color: Theme.of(context).colorScheme.secondary,
                  ),
                ),
              ),
            ),
            Container(
              color: Colors.white,
              padding: EdgeInsets.only(left: 15),
              alignment: Alignment.centerLeft,
              child: Text(
                _getValue(state.contacts.elementAt(pos).role),
                style: TextStyle(
                  fontSize: 18,
                ),
              ),
            ),
            Container(
              color: Colors.white,
              padding: EdgeInsets.only(left: 15, top: 20, bottom: 7),
              alignment: Alignment.centerLeft,
              child: I18nText(
                'modules.business_cards.vcard.contacts.title',
                child: Text(
                  "Titel",
                  textAlign: TextAlign.start,
                  style: TextStyle(
                    fontSize: 20,
                    color: Theme.of(context).colorScheme.secondary,
                  ),
                ),
              ),
            ),
            Container(
              color: Colors.white,
              padding: EdgeInsets.only(left: 15),
              alignment: Alignment.centerLeft,
              child: Text(
                _getValue(state.contacts.elementAt(pos).title),
                style: TextStyle(
                  fontSize: 18,
                ),
              ),
            ),
            Container(
              color: Colors.white,
              padding: EdgeInsets.only(left: 15, top: 20, bottom: 7),
              alignment: Alignment.centerLeft,
              child: I18nText(
                'modules.business_cards.vcard.contacts.phone',
                child: Text(
                  '',
                  textAlign: TextAlign.start,
                  style: TextStyle(
                    fontSize: 20,
                    color: Theme.of(context).colorScheme.secondary,
                  ),
                ),
              ),
            ),
            Container(
              color: Colors.white,
              padding: EdgeInsets.only(left: 15),
              alignment: Alignment.centerLeft,
              child: Text(
                _getValue(state.contacts.elementAt(pos).otherPhone),
                style: TextStyle(
                  fontSize: 18,
                ),
              ),
            ),
            Container(
              color: Colors.white,
              padding: EdgeInsets.only(left: 15, top: 20, bottom: 7),
              alignment: Alignment.centerLeft,
              child: I18nText(
                'modules.business_cards.vcard.contacts.added_on',
                child: Text(
                  '',
                  textAlign: TextAlign.start,
                  style: TextStyle(
                    fontSize: 20,
                    color: Theme.of(context).colorScheme.secondary,
                  ),
                ),
              ),
            ),
            Container(
              color: Colors.white,
              padding: EdgeInsets.only(left: 15, bottom: 15),
              margin: EdgeInsets.only(bottom: 20),
              alignment: Alignment.centerLeft,
              child: I18nText(
                'modules.business_cards.vcard.contacts.time',
                translationParams: _getDate(state.contacts.elementAt(pos).date),
                child: Text(
                  '',
                  style: TextStyle(
                    fontSize: 18,
                  ),
                ),
              ),
            ),
          ]),
        ),
      );

  String _getValue(String value) {
    if (value.isNotEmpty) return value;
    return "...";
  }

  Map<String, String> _getDate(String value) {
    var date = DateTime.parse(value);
    return {
      'day': '${date.day}',
      'month': '${date.month}',
      'year': '${date.year}',
      'hour': '${date.hour}',
      'minute': '${date.minute}',
    };
  }
}

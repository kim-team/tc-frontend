/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:json_annotation/json_annotation.dart';
import 'package:meta/meta.dart';

import '../contacts/vcard/vcard.dart';

part 'v_card_state.g.dart';

/// The possible states of the BusinessCard Module
@immutable
@JsonSerializable(anyMap: true, explicitToJson: true)
class VCardState {
  /// The user's personal [VCard]
  final VCard personal;

  /// The user's contact list
  final List<VCard> contacts;

  /// How the user sorted the contact list
  final String sort;

  /// Constructor of [VCardState]
  ///
  /// @param cant be null
  @mustCallSuper
  VCardState({
    required this.personal,
    required this.contacts,
    required this.sort,
  });

  /// Support for [JsonSerializable]
  factory VCardState.fromJson(Map<String, dynamic> json) =>
      _$VCardStateFromJson(json);

  /// Support for [JsonSerializable]
  Map<String, dynamic> toJson() => _$VCardStateToJson(this);
}

/// Init state of the parameters
class InitialVCardState extends VCardState {
  /// Constructor of [InitialVCardState]
  InitialVCardState() : super(personal: VCard(), contacts: [], sort: "newest");
}

/// State changes when the user edits his personal [VCard]
class PersonalVCardChangeState extends VCardState {
  /// Constructor of [PersonalVCardChangeState]
  PersonalVCardChangeState(VCard personal, List<VCard> contacts, String sort)
      : super(personal: personal, contacts: contacts, sort: sort);
}

/// State changes when the user adds a new contact
class ContactListAddState extends VCardState {
  /// Constructor of [ContactListAddState]
  ContactListAddState(VCard personal, List<VCard> contacts, String sort)
      : super(personal: personal, contacts: contacts, sort: sort);
}

/// State changes when the user deletes a contact
class ContactListDeleteState extends VCardState {
  /// Constructor of [ContactListDeleteState]
  ContactListDeleteState(VCard personal, List<VCard> contacts, String sort)
      : super(personal: personal, contacts: contacts, sort: sort);
}

/// State changes when the user edits the note from a contact
class ContactListEditState extends VCardState {
  /// Constructor of [ContactListEditState]
  ContactListEditState(VCard personal, List<VCard> contacts, String sort)
      : super(personal: personal, contacts: contacts, sort: sort);
}

/// State changes when the user changes the way the contact list is sorted
class ContactListSortState extends VCardState {
  /// Constructor of [ContactListSortState]
  ContactListSortState(VCard personal, List<VCard> contacts, String sort)
      : super(personal: personal, contacts: contacts, sort: sort);
}

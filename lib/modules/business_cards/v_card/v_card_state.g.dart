// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'v_card_state.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

VCardState _$VCardStateFromJson(Map json) => VCardState(
      personal:
          VCard.fromJson(Map<String, dynamic>.from(json['personal'] as Map)),
      contacts: (json['contacts'] as List<dynamic>)
          .map((e) => VCard.fromJson(Map<String, dynamic>.from(e as Map)))
          .toList(),
      sort: json['sort'] as String,
    );

Map<String, dynamic> _$VCardStateToJson(VCardState instance) =>
    <String, dynamic>{
      'personal': instance.personal.toJson(),
      'contacts': instance.contacts.map((e) => e.toJson()).toList(),
      'sort': instance.sort,
    };

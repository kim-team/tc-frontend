/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:validators/validators.dart';

import '../../../common/widgets/tc_form_field/tc_form_field.dart';
import '../../../common/widgets/tc_form_field/tc_form_field_model.dart';
import '../contacts/vcard/vcard.dart';
import '../v_card/v_card_bloc.dart';

// TODO: Cleanup
///Contains all TCFormField for the Business Card Module
class VCardFormField {
  /// @return [TCFormField] for formattedName
  static TCFormField getName(VCardState state, BuildContext context) =>
      TCFormField(
        formFieldModel: TCFormFieldModel(
          initialValue: state.personal.formattedName,
          title: FlutterI18n.translate(
              context, 'modules.business_cards.vcard.form.name'),
          hintText: FlutterI18n.translate(
              context, 'modules.business_cards.vcard.form.name_hint'),
          maxLength: 40,
          isPersistent: true,
          textCapitalization: TextCapitalization.words,
          persistent: (value) {
            // Ist keine finale Lösung
            BlocProvider.of<VCardBloc>(context).add(PersonalVCardChangeEvent(
              personal: VCard(
                formattedName: value,
                categories: state.personal.categories,
                title: state.personal.title,
                role: state.personal.role,
                organization: state.personal.organization,
                email: state.personal.email,
                url: state.personal.url,
                otherPhone: state.personal.otherPhone,
              ),
            ));
          },
        ),
      );

  /// @return [TCFormField] for categories
  static TCFormField getCategories(VCardState state, BuildContext context) =>
      TCFormField(
        formFieldModel: TCFormFieldModel(
          initialValue: state.personal.categories,
          title: FlutterI18n.translate(
              context, 'modules.business_cards.vcard.form.interests'),
          hintText: FlutterI18n.translate(
              context, 'modules.business_cards.vcard.form.interests_hint'),
          maxLength: 40,
          isPersistent: true,
          textCapitalization: TextCapitalization.words,
          persistent: (value) {
            BlocProvider.of<VCardBloc>(context).add(PersonalVCardChangeEvent(
              personal: VCard(
                  formattedName: state.personal.formattedName,
                  categories: value,
                  title: state.personal.title,
                  role: state.personal.role,
                  organization: state.personal.organization,
                  email: state.personal.email,
                  url: state.personal.url,
                  otherPhone: state.personal.otherPhone),
            ));
          },
        ),
      );

  /// @return [TCFormField] for for job title
  static TCFormField getTitle(VCardState state, BuildContext context) =>
      TCFormField(
        formFieldModel: TCFormFieldModel(
          initialValue: state.personal.title,
          title: FlutterI18n.translate(
              context, 'modules.business_cards.vcard.form.job_title'),
          hintText: FlutterI18n.translate(
              context, 'modules.business_cards.vcard.form.job_title_hint'),
          maxLength: 30,
          isPersistent: true,
          textCapitalization: TextCapitalization.words,
          persistent: (value) {
            BlocProvider.of<VCardBloc>(context).add(PersonalVCardChangeEvent(
              personal: VCard(
                  formattedName: state.personal.formattedName,
                  categories: state.personal.categories,
                  title: value,
                  role: state.personal.role,
                  organization: state.personal.organization,
                  email: state.personal.email,
                  url: state.personal.url,
                  otherPhone: state.personal.otherPhone),
            ));
          },
        ),
      );

  /// @return [TCFormField] for job role
  static TCFormField getRole(VCardState state, BuildContext context) =>
      TCFormField(
        formFieldModel: TCFormFieldModel(
          initialValue: state.personal.role,
          title: FlutterI18n.translate(
              context, 'modules.business_cards.vcard.form.occupation'),
          hintText: FlutterI18n.translate(
              context, 'modules.business_cards.vcard.form.occupation_hint'),
          maxLength: 40,
          isPersistent: true,
          textCapitalization: TextCapitalization.words,
          persistent: (value) {
            BlocProvider.of<VCardBloc>(context).add(PersonalVCardChangeEvent(
              personal: VCard(
                  formattedName: state.personal.formattedName,
                  categories: state.personal.categories,
                  title: state.personal.title,
                  role: value,
                  organization: state.personal.organization,
                  email: state.personal.email,
                  url: state.personal.url,
                  otherPhone: state.personal.otherPhone),
            ));
          },
        ),
      );

  /// @return [TCFormField] for organisation
  static TCFormField getOrganisation(VCardState state, BuildContext context) =>
      TCFormField(
        formFieldModel: TCFormFieldModel(
          initialValue: state.personal.organization,
          title: FlutterI18n.translate(
              context, 'modules.business_cards.vcard.form.organisation'),
          hintText: FlutterI18n.translate(
              context, 'modules.business_cards.vcard.form.organisation_hint'),
          maxLength: 40,
          isPersistent: true,
          textCapitalization: TextCapitalization.words,
          persistent: (value) {
            BlocProvider.of<VCardBloc>(context).add(PersonalVCardChangeEvent(
              personal: VCard(
                  formattedName: state.personal.formattedName,
                  categories: state.personal.categories,
                  title: state.personal.title,
                  role: state.personal.role,
                  organization: value,
                  email: state.personal.email,
                  url: state.personal.url,
                  otherPhone: state.personal.otherPhone),
            ));
          },
        ),
      );

  /// @return [TCFormField] for email
  static TCFormField getEmail(VCardState state, BuildContext context) =>
      TCFormField(
        formFieldModel: TCFormFieldModel(
          initialValue: state.personal.email,
          title: FlutterI18n.translate(
              context, 'modules.business_cards.vcard.form.email'),
          hintText: FlutterI18n.translate(
              context, 'modules.business_cards.vcard.form.email_hint'),
          maxLength: 50,
          isPersistent: true,
          valid: [
            (value) {
              if (!isEmail(value.trim())) {
                return FlutterI18n.translate(
                    context, 'modules.business_cards.vcard.form.email_error');
              }
              return null;
            }
          ],
          textCapitalization: TextCapitalization.words,
          textInputType: TextInputType.emailAddress,
          persistent: (value) {
            BlocProvider.of<VCardBloc>(context).add(PersonalVCardChangeEvent(
              personal: VCard(
                  formattedName: state.personal.formattedName,
                  categories: state.personal.categories,
                  title: state.personal.title,
                  role: state.personal.role,
                  organization: state.personal.organization,
                  email: value,
                  url: state.personal.url,
                  otherPhone: state.personal.otherPhone),
            ));
          },
        ),
      );

  /// @return [TCFormField] for url
  static TCFormField getUrl(VCardState state, BuildContext context) =>
      TCFormField(
        formFieldModel: TCFormFieldModel(
          initialValue: state.personal.url,
          title: FlutterI18n.translate(
              context, 'modules.business_cards.vcard.form.web_page'),
          hintText: FlutterI18n.translate(
              context, 'modules.business_cards.vcard.form.web_page_hint'),
          maxLength: 50,
          isPersistent: true,
          valid: [
            (value) {
              if (!isURL(value.trim())) {
                return FlutterI18n.translate(context,
                    'modules.business_cards.vcard.form.web_page_error');
              }
              return null;
            }
          ],
          textCapitalization: TextCapitalization.none,
          textInputType: TextInputType.url,
          persistent: (value) {
            BlocProvider.of<VCardBloc>(context).add(PersonalVCardChangeEvent(
              personal: VCard(
                  formattedName: state.personal.formattedName,
                  categories: state.personal.categories,
                  title: state.personal.title,
                  role: state.personal.role,
                  organization: state.personal.organization,
                  email: state.personal.email,
                  url: value,
                  otherPhone: state.personal.otherPhone),
            ));
          },
        ),
      );

  /// @return [TCFormField] for phone number
  static TCFormField getPhone(VCardState state, BuildContext context) =>
      TCFormField(
        formFieldModel: TCFormFieldModel(
          initialValue: state.personal.otherPhone,
          title: FlutterI18n.translate(
              context, 'modules.business_cards.vcard.form.phone_number'),
          hintText: FlutterI18n.translate(
              context, 'modules.business_cards.vcard.form.phone_number_hint'),
          maxLength: 20,
          isPersistent: true,
          valid: [],
          textCapitalization: TextCapitalization.words,
          textInputType: TextInputType.phone,
          persistent: (value) {
            BlocProvider.of<VCardBloc>(context).add(PersonalVCardChangeEvent(
              personal: VCard(
                  formattedName: state.personal.formattedName,
                  categories: state.personal.categories,
                  title: state.personal.title,
                  role: state.personal.role,
                  organization: state.personal.organization,
                  email: state.personal.email,
                  url: state.personal.url,
                  otherPhone: value),
            ));
          },
        ),
      );

  /// @return [TCFormField] for phone note
  static TCFormField getNote(VCardState state, BuildContext context, int pos) =>
      TCFormField(
        formFieldModel: TCFormFieldModel(
            initialValue: state.contacts.elementAt(pos).note,
            title: FlutterI18n.translate(
                context, 'modules.business_cards.vcard.form.comment'),
            hintText: FlutterI18n.translate(
                context, 'modules.business_cards.vcard.form.comment_hint'),
            maxLength: 255,
            isPersistent: true,
            persistent: (value) {
              var vCard = VCard(
                formattedName: state.contacts.elementAt(pos).formattedName,
                categories: state.contacts.elementAt(pos).categories,
                title: state.contacts.elementAt(pos).title,
                role: state.contacts.elementAt(pos).role,
                organization: state.contacts.elementAt(pos).organization,
                email: state.contacts.elementAt(pos).email,
                url: state.contacts.elementAt(pos).url,
                otherPhone: state.contacts.elementAt(pos).otherPhone,
                date: state.contacts.elementAt(pos).date,
                note: value,
              );
              BlocProvider.of<VCardBloc>(context).add(ContactListEditEvent(
                contact: vCard,
                pos: pos,
              ));
            }),
      );
}

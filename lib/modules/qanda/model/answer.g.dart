// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'answer.dart';

// **************************************************************************
// CopyWithGenerator
// **************************************************************************

extension AnswerCopyWith on Answer {
  Answer copyWith({
    Author? author,
    String? body,
    bool? canEdit,
    DateTime? createdAt,
    int? id,
    bool? isHelpful,
    bool? isLiked,
    bool? isReported,
    int? likes,
    DateTime? modifiedAt,
    int? questionId,
  }) {
    return Answer(
      author: author ?? this.author,
      body: body ?? this.body,
      canEdit: canEdit ?? this.canEdit,
      createdAt: createdAt ?? this.createdAt,
      id: id ?? this.id,
      isHelpful: isHelpful ?? this.isHelpful,
      isLiked: isLiked ?? this.isLiked,
      isReported: isReported ?? this.isReported,
      likes: likes ?? this.likes,
      modifiedAt: modifiedAt ?? this.modifiedAt,
      questionId: questionId ?? this.questionId,
    );
  }
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Answer _$AnswerFromJson(Map json) => Answer(
      id: json['answerId'] as int,
      author: Author.fromJson(Map<String, dynamic>.from(json['author'] as Map)),
      body: json['text'] as String,
      createdAt: DateTime.parse(json['createdAt'] as String),
      modifiedAt: DateTime.parse(json['modifiedAt'] as String),
      isLiked: json['liked'] as bool,
      likes: json['likes'] as int,
      questionId: json['questionId'] as int,
      isReported: json['reported'] as bool? ?? false,
      canEdit: json['canEdit'] as bool? ?? false,
      isHelpful: json['helpful'] as bool? ?? false,
    );

Map<String, dynamic> _$AnswerToJson(Answer instance) => <String, dynamic>{
      'answerId': instance.id,
      'questionId': instance.questionId,
      'author': instance.author.toJson(),
      'text': instance.body,
      'createdAt': instance.createdAt.toIso8601String(),
      'modifiedAt': instance.modifiedAt.toIso8601String(),
      'liked': instance.isLiked,
      'likes': instance.likes,
      'canEdit': instance.canEdit,
      'helpful': instance.isHelpful,
      'reported': instance.isReported,
    };

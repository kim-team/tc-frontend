/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:copy_with_extension/copy_with_extension.dart';
import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'flair.g.dart';

@CopyWith()
@JsonSerializable(explicitToJson: true, anyMap: true)
class Flair extends Equatable {
  final int id;
  final String title;

  Flair({required this.id, required this.title});

  factory Flair.fromJson(Map<String, dynamic> json) => _$FlairFromJson(json);

  Map<String, dynamic> toJson() => _$FlairToJson(this);

  @override
  List<Object> get props => [id, title];
}

const List<String> verified = [
  "profile.flairs.tinycampus_editor",
  "profile.flairs.tinycampus_team",
  "profile.flairs.tinycampus_moderator",
  "profile.flairs.kim_staff",
  "profile.flairs.thm_staff"
];

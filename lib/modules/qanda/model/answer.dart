/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:copy_with_extension/copy_with_extension.dart';
import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

import '../../../reward/model/likable.dart';
import 'author.dart';

part 'answer.g.dart';

///Model of an answer
@CopyWith()
@JsonSerializable(explicitToJson: true, anyMap: true)
class Answer extends Equatable implements Likable {
  @JsonKey(name: 'answerId')
  final int id;
  final int questionId;
  final Author author;
  @JsonKey(name: 'text')
  final String body;

  final DateTime createdAt;
  final DateTime modifiedAt;

  @override
  @JsonKey(name: 'liked')
  final bool isLiked;
  @override
  final int likes;
  final String likableType = 'answer';

  final bool canEdit;
  @JsonKey(name: 'helpful')
  final bool isHelpful;

  @JsonKey(name: 'reported')
  final bool isReported;

  Answer({
    required this.id,
    required this.author,
    required this.body,
    required this.createdAt,
    required this.modifiedAt,
    required this.isLiked,
    required this.likes,
    required this.questionId,
    this.isReported = false,
    this.canEdit = false,
    this.isHelpful = false,
  });

  factory Answer.fromJson(Map<String, dynamic> json) => _$AnswerFromJson(json);

  Map<String, dynamic> toJson() => _$AnswerToJson(this);

  @override
  List<Object> get props => [
        id,
        author,
        body,
        createdAt,
        modifiedAt,
        isLiked,
        likes,
        questionId,
        isReported,
        canEdit,
        isHelpful,
      ];

  @override
  bool get stringify => true;

  bool get isEdited => createdAt.difference(modifiedAt).inSeconds < -3;
}

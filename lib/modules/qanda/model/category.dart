/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';

part 'category.g.dart';

///Model of a category
@immutable
@JsonSerializable(anyMap: true, explicitToJson: true)
class Category extends Equatable {
  final int id;
  final String title;
  final String description;
  final int countActivities;
  final String logo;
  final bool subscribed;
  final List<Category> subcategories;

  const Category({
    required this.id,
    required this.title,
    required this.description,
    required this.countActivities,
    required this.logo,
    required this.subscribed,
    this.subcategories = const [],
  });

  const Category.placeholder()
      : id = -1,
        title = '',
        description = '',
        countActivities = 0,
        logo = '',
        subscribed = false,
        subcategories = const [];

  factory Category.fromJson(Map<String, dynamic> json) =>
      _$CategoryFromJson(json);

  Map<String, dynamic> toJson() => _$CategoryToJson(this);

  @override
  List<Object> get props => [
        id,
        title,
        description,
        countActivities,
        logo,
        subscribed,
        subcategories,
      ];
}

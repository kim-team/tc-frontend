/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:jdenticon_dart/jdenticon_dart.dart';
import 'package:timeago/timeago.dart' as timeago;

import '../../../common/tc_theme.dart';
import '../../../common/tinycampus_icons.dart';
import '../../../common/widgets/reporting/popup_report_button.dart';
import '../../../common/widgets/reporting/reported_blur_wrapper.dart';
import '../../../common/widgets/tc_link_text.dart';
import '../../../reward/widgets/heart_widget.dart';
import '../api_helper.dart';
import '../bloc/answer/answer_bloc.dart';
import '../bloc/edit/edit_bloc.dart';
import '../model/answer.dart';
import 'edit_answer_widget.dart';
import 'flair_widget.dart';
import 'helpful_widget.dart';
import 'question_and_answer_delete_button.dart';

///Display for a single Answer
class AnswerWidget extends StatefulWidget {
  final bool canMarkAsHelpful;
  final Answer _answer;
  final int index;

  const AnswerWidget(
    this._answer,
    this.index, {
    this.canMarkAsHelpful = false,
    Key? key,
  }) : super(key: key);

  @override
  _AnswerWidgetState createState() => _AnswerWidgetState();
}

class _AnswerWidgetState extends State<AnswerWidget>
    with SingleTickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    var textController = TextEditingController(text: widget._answer.body);
    return ReportedBlurWrapper(
      shouldBlur: widget._answer.isReported,
      child: AnimatedSize(
        duration: Duration(milliseconds: 260),
        curve: Curves.easeInOut,
        alignment: Alignment.topCenter,
        child: BlocBuilder<EditBloc, EditState>(
          builder: (context, state) => !(state.editing[widget.index] ?? false)
              ? Card(
                  margin: EdgeInsets.zero,
                  // shape: Border(),
                  elevation: 0,
                  child: InkWell(
                    child: Padding(
                      padding: const EdgeInsets.all(12.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Expanded(
                                child: Row(
                                  children: [
                                    ClipOval(
                                      child: SvgPicture.string(
                                        Jdenticon.toSvg(
                                          widget._answer.author.displayName,
                                          size: 20,
                                        ),
                                        height: 20,
                                        width: 20,
                                        fit: BoxFit.fill,
                                        semanticsLabel: FlutterI18n.translate(
                                            context,
                                            'profile.semantics.avatar'),
                                      ),
                                    ),
                                    Container(
                                      width: 8.0,
                                    ),
                                    Expanded(
                                      child: Padding(
                                        padding: const EdgeInsets.symmetric(
                                            vertical: 2.0),
                                        child: Row(
                                          children: [
                                            Expanded(
                                              child: Text(
                                                widget
                                                    ._answer.author.displayName,
                                                textAlign: TextAlign.start,
                                                maxLines: 1,
                                                overflow: TextOverflow.ellipsis,
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .headline4,
                                              ),
                                            ),
                                            Container(
                                              width: 5,
                                            ),
                                            // _verifiedWidget(),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              if (widget._answer.canEdit) ...[
                                Container(
                                  width: 24,
                                  height: 24,
                                  margin: EdgeInsets.only(right: 8.0),
                                  child: IconButton(
                                    onPressed: () {
                                      BlocProvider.of<EditBloc>(context)
                                          .add(OpenEditEvent(widget.index));
                                    },
                                    padding: EdgeInsets.all(0),
                                    visualDensity: VisualDensity.compact,
                                    tooltip: FlutterI18n.translate(
                                        context, 'common.actions.edit'),
                                    icon: Icon(
                                      TinyCampusIcons.edit,
                                      color: CorporateColors.tinyCampusIconGrey
                                          .withOpacity(0.5),
                                      size: 16,
                                    ),
                                  ),
                                ),
                                Container(
                                  width: 10,
                                ),
                                QuestionAndAnswerDeleteButton(
                                  isQuestion: false,
                                  id: widget._answer.id,
                                  questionId: widget._answer.questionId,
                                ),
                              ],
                              if (!widget._answer.canEdit) ...[
                                PopupReportButton(
                                  reportPostRoute:
                                      reportAnswerByIdUrl(widget._answer.id),
                                  userId: widget._answer.author.authorId,
                                ),
                              ],
                            ],
                          ),
                          FlairWidget(flairs: widget._answer.author.flairs),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 8.0),
                            child: Row(
                              children: [
                                HelpfulWidget(
                                  canMarkAsHelpful: widget.canMarkAsHelpful,
                                  answer: widget._answer,
                                ),
                                Expanded(
                                  child: TweenAnimationBuilder<Color?>(
                                    duration: Duration(milliseconds: 260),
                                    curve: Curves.easeOut,
                                    tween: ColorTween(
                                        begin:
                                            CorporateColors.tinyCampusIconGrey,
                                        end: widget._answer.isHelpful
                                            ? CorporateColors.tinyCampusOrange
                                            : CurrentTheme().textSoftWhite),
                                    builder: (context, value, child) => Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          FlutterI18n.translate(
                                            context,
                                            widget._answer.isHelpful
                                                ? 'modules.q_and_a.helpful'
                                                    '.marked_helpful'
                                                : 'modules.q_and_a.answer',
                                          ).toUpperCase(),
                                          style: Theme.of(context)
                                              .textTheme
                                              .bodyText1
                                              ?.copyWith(
                                                color: value?.withOpacity(0.7),
                                                fontSize: 12.0,
                                              ),
                                        ),
                                        TCLinkText(
                                          text: widget._answer.body,
                                          style: Theme.of(context)
                                              .textTheme
                                              .bodyText1
                                              ?.copyWith(color: value),
                                          linkStyle: Theme.of(context)
                                              .textTheme
                                              .bodyText1
                                              ?.copyWith(
                                                color: value,
                                                decoration:
                                                    TextDecoration.underline,
                                                decorationColor: value,
                                              ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                "${timeago.format(
                                  widget._answer.createdAt,
                                  locale: FlutterI18n.currentLocale(context)
                                          ?.languageCode ??
                                      "de",
                                )} ${_edited(context)}",
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyText2
                                    ?.copyWith(color: Colors.grey),
                              ),
                              HeartWidget(
                                likable: widget._answer,
                                toggleLike: () =>
                                    BlocProvider.of<AnswerBloc>(context).add(
                                  ToggleLikeEvent(widget._answer),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                )
              : EditAnswerWidget(widget._answer, textController, () {
                  BlocProvider.of<EditBloc>(context)
                      .add(CloseEditEvent(widget.index));
                }, () {
                  if (textController.text.trim().isNotEmpty) {
                    BlocProvider.of<AnswerBloc>(context).add(EditAnswerEvent(
                        widget._answer.id,
                        textController.text.trim(),
                        widget._answer.questionId));
                    BlocProvider.of<EditBloc>(context)
                        .add(CloseEditEvent(widget.index));
                  }
                }),
        ),
      ),
    );
  }

  // TODO: Use when requirement is clarified
  // Widget _verifiedWidget() => widget._answer.author.flairs.any(
  //         (element) => flair.verified.contains(element.title) ? true : false)
  //     ? Container(
  //         child: Icon(
  //           Icons.check_circle,
  //           size: 20,
  //           color: CurrentTheme().tcDarkBlue,
  //         ),
  //       )
  //     : Container();

  String _edited(BuildContext context) => widget._answer.isEdited
      ? FlutterI18n.translate(context, "modules.q_and_a.edited")
      : "";
}

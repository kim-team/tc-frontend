/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../../common/tc_theme.dart';
import '../../../common/widgets/code_of_conduct/coc_info_text.dart';
import '../../../common/widgets/tc_form_field/tc_form_field.dart';
import '../../../common/widgets/tc_form_field/tc_form_field_model.dart';
import '../bloc/category/category_bloc.dart';
import '../bloc/question/question_bloc.dart';
import '../model/category.dart';

///Widget that let's the user post a question to a category
class AddQuestionWidget extends StatefulWidget {
  final Category category;
  final bool openAskQuestion;
  final String initialText;

  AddQuestionWidget({
    Key? key,
    required this.category,
    this.openAskQuestion = false,
    this.initialText = "",
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => AddQuestionWidgetState();
}

class AddQuestionWidgetState extends State<AddQuestionWidget>
    with SingleTickerProviderStateMixin {
  final _textController = TextEditingController();

  var _subcategoryThere = false;
  var _askQuestion = false;
  var _dropDownBorderColorCategory = Color.fromARGB(255, 203, 217, 220);
  var _dropDownBorderColorSubcategory = Color.fromARGB(255, 203, 217, 220);

  Category? _categoryValue;
  Category? _subcategoryValue;

  @override
  void initState() {
    super.initState();
    _askQuestion = widget.openAskQuestion;
    _textController.text = widget.initialText;
  }

  @override
  Widget build(BuildContext context) {
    var categoryBloc = BlocProvider.of<CategoryBloc>(context);
    return AnimatedSize(
      duration: Duration(milliseconds: 260),
      alignment: Alignment.topCenter,
      curve: Curves.easeInOut,
      child: Card(
        margin: EdgeInsets.zero,
        elevation: 0,
        child: _askQuestion
            ? Container(
                margin: EdgeInsets.only(top: 10),
                padding: EdgeInsets.symmetric(vertical: 20, horizontal: 10),
                color: Theme.of(context).primaryColor,
                child: Column(
                  children: <Widget>[
                    if (widget.category.id == -1)
                      Container(
                        margin: EdgeInsets.only(bottom: 10, left: 5, right: 10),
                        padding: EdgeInsets.symmetric(horizontal: 10),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          border: Border.all(
                            color: _dropDownBorderColorCategory,
                            style: BorderStyle.solid,
                            width: 1.5,
                          ),
                        ),
                        child: DropdownButtonHideUnderline(
                          child: DropdownButton<Category>(
                            focusColor: CorporateColors.tinyCampusBlue,
                            elevation: 6,
                            items: buildItems(categoryBloc.state.all),
                            onChanged: (val) {
                              setState(() {
                                _categoryValue = val;
                                _subcategoryValue = null;
                                _subcategoryThere =
                                    val!.subcategories.isNotEmpty;
                              });
                            },
                            style: TextStyle(
                              color: CorporateColors.tinyCampusBlue,
                              fontSize: 15,
                            ),
                            isExpanded: true,
                            hint: I18nText("modules.q_and_a.choose_category"),
                            value: _categoryValue,
                          ),
                        ),
                      ),
                    if (_subcategoryThere &&
                        widget.category.id == -1 &&
                        _categoryValue != null)
                      Container(
                        margin: EdgeInsets.only(bottom: 10, left: 5, right: 10),
                        padding: EdgeInsets.symmetric(horizontal: 10),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          border: Border.all(
                            color: _dropDownBorderColorSubcategory,
                            style: BorderStyle.solid,
                            width: 1.5,
                          ),
                        ),
                        child: DropdownButtonHideUnderline(
                          child: DropdownButton<Category>(
                            items: buildItems(_categoryValue!.subcategories),
                            onChanged: (val) => setState(
                              () => _subcategoryValue = val,
                            ),
                            style: TextStyle(
                              color: CorporateColors.tinyCampusBlue,
                              fontSize: 15,
                            ),
                            isExpanded: true,
                            hint: I18nText(
                              'modules.q_and_a.choose_subcategory',
                            ),
                            value: _subcategoryValue,
                          ),
                        ),
                      ),
                    TCFormField(
                      formFieldModel: TCFormFieldModel(
                        title: FlutterI18n.translate(
                          context,
                          'modules.q_and_a.your_question',
                        ),
                        hintText: FlutterI18n.translate(
                          context,
                          'modules.q_and_a.enter_question',
                        ),
                        textInputType: TextInputType.multiline,
                        maxLength: 1024,
                        valid: [
                          (value) => value.trim().isEmpty
                              ? FlutterI18n.translate(
                                  context,
                                  'modules.q_and_a.number_of_characters',
                                )
                              : null
                        ],
                        controller: _textController,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(bottom: 24, left: 5, right: 5),
                      child: CocInfoTextWidget(),
                    ),
                    Container(
                      margin: EdgeInsets.only(bottom: 10, left: 5, right: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Expanded(
                            child: RaisedButton(
                              onPressed: () => setState(_closeAddQuestion),
                              padding: EdgeInsets.symmetric(vertical: 16.0),
                              elevation: 0.0,
                              color: Theme.of(context).primaryColor,
                              textColor: CorporateColors.tinyCampusBlue,
                              clipBehavior: Clip.antiAlias,
                              child: Text(
                                FlutterI18n.translate(
                                  context,
                                  'common.actions.cancel',
                                ),
                                style: Theme.of(context).textTheme.headline4,
                              ),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8),
                                side: BorderSide(
                                  color: CorporateColors.tinyCampusBlue,
                                ),
                              ),
                            ),
                          ),
                          Container(
                            width: 20,
                          ),
                          Expanded(
                            child: RaisedButton(
                              padding: EdgeInsets.symmetric(vertical: 16.0),
                              elevation: 0.0,
                              onPressed: () {
                                final subVal = _subcategoryValue;
                                if (_textController.text.trim().isNotEmpty) {
                                  if (_subcategoryThere) {
                                    if (subVal != null) {
                                      _addQuestion(
                                        context,
                                        subVal.id,
                                        widget.category.id,
                                      );
                                      setState(_closeAddQuestion);
                                    } else {
                                      setState(() {
                                        _dropDownBorderColorCategory =
                                            Color.fromARGB(255, 203, 217, 220);
                                        _dropDownBorderColorSubcategory =
                                            Colors.red[500] ?? Colors.red;
                                      });
                                    }
                                  } else {
                                    if (_categoryValue != null ||
                                        widget.category.id != -1) {
                                      _addQuestion(
                                          context,
                                          widget.category.id == -1
                                              ? _categoryValue!.id
                                              : widget.category.id,
                                          widget.category.id);
                                      setState(_closeAddQuestion);
                                    } else {
                                      setState(() {
                                        _dropDownBorderColorCategory =
                                            Colors.red[500] ?? Colors.red;
                                      });
                                    }
                                  }
                                }
                              },
                              color: CorporateColors.tinyCampusBlue,
                              textColor: Colors.white,
                              clipBehavior: Clip.antiAlias,
                              child: Text(
                                FlutterI18n.translate(
                                  context,
                                  'common.actions.confirm',
                                ),
                                style: Theme.of(context)
                                    .textTheme
                                    .headline4
                                    ?.copyWith(color: Colors.white),
                              ),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8),
                                side: BorderSide(
                                  color: CorporateColors.tinyCampusBlue,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              )
            : InkWell(
                onTap: () => setState(() => _askQuestion = true),
                child: Ink(
                  child: Container(
                    margin: EdgeInsets.only(top: 10),
                    padding: EdgeInsets.symmetric(
                      vertical: 20,
                      horizontal: 10,
                    ),
                    // color: Colors.white,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        CircleAvatar(
                          backgroundColor: Color.fromARGB(255, 240, 240, 240),
                          foregroundColor: CorporateColors.tinyCampusIconGrey,
                          child: Icon(Icons.add),
                        ),
                        Container(
                          width: 15,
                        ),
                        Text(
                          FlutterI18n.translate(context, 'modules.q_and_a.ask'),
                          style: Theme.of(context).textTheme.headline2,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
      ),
    );
  }

  void _closeAddQuestion() {
    _askQuestion = false;
    _categoryValue = null;
    _subcategoryThere = false;
    _subcategoryValue = null;
    _textController.text = "";
    _dropDownBorderColorCategory = Color.fromARGB(255, 203, 217, 220);
    _dropDownBorderColorSubcategory = Color.fromARGB(255, 203, 217, 220);
  }

  void _addQuestion(BuildContext context, int categoryID, int currentId) {
    BlocProvider.of<QuestionBloc>(context).add(
        PostQuestionEvent(categoryID, _textController.text.trim(), currentId));
  }

  List<DropdownMenuItem<Category>> buildItems(List<Category> categories) =>
      categories
          .map((e) => DropdownMenuItem(value: e, child: Text(e.title)))
          .toList();
}

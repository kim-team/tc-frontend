/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../bloc/question/question_bloc.dart';
import 'qanda_loading_indicator.dart';
import 'question_widget.dart';

///Tab View for all the questions which the user has posted
class MyQuestionsTab extends StatelessWidget {
  const MyQuestionsTab({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<QuestionBloc>(context).add(
      LoadQuestionsEvent(-1),
    );
    return BlocBuilder<QuestionBloc, QuestionState>(
      builder: (context, state) {
        switch (state.runtimeType) {
          case LoadingQuestionsState:
            return QAndALoadingIndicator();
          case EmptyQuestionState:
          case QuestionToggledState:
          case QuestionsLoadedState:
            return state.myQuestions.isNotEmpty
                ? ListView.separated(
                    physics: BouncingScrollPhysics(),
                    // Add one entry, as items and separators are swapped
                    itemCount: state.myQuestions.length + 1,
                    itemBuilder: (context, index) => Container(height: 12),
                    separatorBuilder: (context, index) => QuestionWidget(
                      state.myQuestions[index],
                      canNavigate: true,
                      showAuthor: false,
                      showHeart: false,
                    ),
                  )
                : Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: Center(
                      child: I18nText(
                        "modules.q_and_a.no_questions",
                        child: Text("You have not asked any questions"),
                      ),
                    ),
                  );
          default:
            return Padding(
              padding: const EdgeInsets.all(12.0),
              child: Center(
                child: Text("Something went wrong"),
              ),
            );
        }
      },
    );
  }
}

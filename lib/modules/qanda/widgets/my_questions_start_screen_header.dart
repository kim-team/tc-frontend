/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../../common/constants/routing_constants.dart';
import '../../../common/tc_theme.dart';
import '../../../common/tinycampus_icons.dart';
import '../bloc/question/question_bloc.dart';

///Header widget which routes to the screen showing the users own contributions
class MyQuestionsStartScreenHeader extends StatelessWidget {
  final int newReplies;

  const MyQuestionsStartScreenHeader({Key? key, required this.newReplies})
      : super(key: key);

  @override
  Widget build(BuildContext context) => GestureDetector(
        onTap: () {
          BlocProvider.of<QuestionBloc>(context).add(LoadQuestionsEvent(-1));
          Navigator.of(context).pushNamed(
            qaMyQuestionsRoute,
          );
        },
        child: Card(
          margin: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(8.0))),
          elevation: 2.0,
          clipBehavior: Clip.antiAlias,
          child: Padding(
            padding: const EdgeInsets.symmetric(
              vertical: 20,
              horizontal: 8,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  width: 30,
                ),
                Column(
                  children: <Widget>[
                    I18nText(
                      'modules.q_and_a.my_contributions',
                      child: Text(
                        '',
                        style: Theme.of(context).textTheme.headline2,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 8.0),
                      child: Row(
                        children: <Widget>[
                          Icon(
                            TinyCampusIcons.message,
                            color: newReplies > 0
                                ? CorporateColors.qAndAActivityTextGreen
                                : CorporateColors.tinyCampusIconGrey
                                    .withOpacity(0.5),
                            size: 16,
                          ),
                          Container(
                            margin: EdgeInsets.only(left: 10),
                            child: I18nPlural(
                              'modules.q_and_a.new_answers.times',
                              newReplies,
                              child: Text(
                                '',
                                style: TextStyle(
                                  color: newReplies > 0
                                      ? CorporateColors.qAndAActivityTextGreen
                                      : CorporateColors.tinyCampusIconGrey
                                          .withOpacity(0.5),
                                  fontSize: 15,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                Icon(
                  Icons.chevron_right,
                  size: 30,
                ),
              ],
            ),
          ),
          color: Theme.of(context).primaryColor,
        ),
      );
}

/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/widgets/I18nPlural.dart';

import '../../../common/constants/routing_constants.dart';
import '../../../common/tc_theme.dart';
import '../bloc/category/category_bloc.dart';
import '../model/category.dart';
import '../qanda_subcategory_screen.dart';

class MainCategoryWidget extends StatelessWidget {
  final Category _category;

  MainCategoryWidget(
    this._category, {
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => Card(
        margin: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(6.0))),
        elevation: 2.0,
        clipBehavior: Clip.antiAlias,
        child: InkWell(
          onTap: () {
            if (_category.subcategories.isNotEmpty) {
              Navigator.pushNamed(context, qaAddSubcategories,
                  arguments: QAndASubcategoryScreenArguments(_category));
            } else {
              BlocProvider.of<CategoryBloc>(context).add(SubscribeCategoryEvent(
                categoryID: _category.id,
              ));
              Navigator.pop(context);
            }
          },
          child: Padding(
            padding: const EdgeInsets.only(right: 8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Container(
                  height: 140,
                  width: 110,
                  margin: EdgeInsets.only(right: 10),
                  color: CorporateColors.passiveBackgroundLightBlue,
                  child: _category.logo.isNotEmpty
                      ? Image.asset(
                          // TODO: Change to right path when clear which path
                          _category.logo,
                          fit: BoxFit.fill,
                        )
                      : Center(
                          child: Text(
                            _category.title
                                .substring(0, min(_category.title.length, 3))
                                .toUpperCase(),
                            style: TextStyle(
                              color: CorporateColors.tinyCampusBlue
                                  .withOpacity(0.2),
                              fontWeight: FontWeight.w400,
                              fontSize: 36,
                              letterSpacing: -1.0,
                            ),
                          ),
                        ),
                ),
                Flexible(
                  child: SizedBox(
                    height: 120,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(_category.title,
                            style: Theme.of(context).textTheme.headline2),
                        if (_category.subcategories.isNotEmpty)
                          Row(
                            children: <Widget>[
                              if (_category.subcategories.isNotEmpty)
                                Text(
                                  "${_category.subcategories.length} ",
                                  style: TextStyle(
                                    color: CorporateColors.tinyCampusOrange,
                                    fontSize: 18,
                                  ),
                                ),
                              I18nPlural(
                                'modules.q_and_a.subcategories.times',
                                _category.subcategories.length,
                                child: Text(
                                  '',
                                  style: TextStyle(
                                    color: _category.subcategories.isNotEmpty
                                        ? CorporateColors.tinyCampusDarkBlue
                                        : CorporateColors.tinyCampusIconGrey,
                                    fontSize: 18,
                                    fontWeight: FontWeight.w300,
                                  ),
                                ),
                              )
                            ],
                          ),
                      ],
                    ),
                  ),
                ),
                if (_category.subcategories.isNotEmpty)
                  Icon(Icons.chevron_right, size: 24),
              ],
            ),
          ),
        ),
      );
}

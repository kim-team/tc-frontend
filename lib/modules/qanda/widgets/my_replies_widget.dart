/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:timeago/timeago.dart' as timeago;

import '../../../common/constants/routing_constants.dart';
import '../../../common/tc_theme.dart';
import '../model/answer.dart';
import '../model/question.dart';
import 'helpful_answer_display.dart';

///Widget used in the MyAnswers Tab to display all answers a user has given
///including the related question
class MyRepliesWidget extends StatelessWidget {
  final Question _answeredQuestion;
  final Answer _answer;

  const MyRepliesWidget(
    this._answeredQuestion,
    this._answer, {
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => Padding(
        padding: const EdgeInsets.only(top: 12.0),
        child: GestureDetector(
          onTap: () => Navigator.of(context).pushNamed(
            qaViewQuestionRoute,
            arguments: _answeredQuestion.id,
          ),
          child: Card(
            margin: EdgeInsets.zero,
            shape: Border(),
            elevation: 0,
            child: Padding(
              padding: const EdgeInsets.all(12.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(bottom: 2.0),
                    child: Text(
                      _answeredQuestion.category,
                      style: Theme.of(context).textTheme.bodyText1?.copyWith(
                            color: CorporateColors.passiveFontColor
                                .withOpacity(0.5),
                          ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                      top: 2.0,
                      bottom: 2.0,
                      right: 16.0,
                    ),
                    child: Text(
                      _answeredQuestion.body,
                      textAlign: TextAlign.start,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: Theme.of(context).textTheme.bodyText1?.copyWith(
                            color: CorporateColors.tinyCampusIconGrey
                                .withOpacity(0.5),
                          ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 8.0),
                    child: InlineAnswerDisplay(_answer),
                  ),
                  SizedBox(
                    height: 26.0,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                            "${timeago.format(
                              _answeredQuestion.createdAt,
                              locale: FlutterI18n.currentLocale(context)
                                      ?.languageCode ??
                                  "de",
                            )} ${_edited(context)}",
                            style: Theme.of(context)
                                .textTheme
                                .bodyText2
                                ?.copyWith(color: Colors.grey)),

                        // TODO: Implement later if glitch is resolved
                        // HeartWidget(
                        //   likable: _answer,
                        //   toggleLike: () async {
                        //     BlocProvider.of<AnswerBloc>(context)
                        //       ..add(ToggleLikeEvent(_answer))
                        //       ..add(LoadMyAnswersEvent());
                        //   },
                        // ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      );

  String _edited(BuildContext context) => _answeredQuestion.isEdited
      ? FlutterI18n.translate(context, "modules.q_and_a.edited")
      : "";
}

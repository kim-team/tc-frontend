/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

part of 'question_bloc.dart';

///Abstract class which defines the basis for all other Question Events.
abstract class QuestionEvent extends Equatable {
  ///The ID of the Question
  final int questionId;

  ///ID of the related Category
  final int categoryId;

  ///The text content of the Question
  final String text;

  QuestionEvent({
    this.categoryId = -1,
    this.text = "",
    this.questionId = -1,
  });

  @mustCallSuper
  Stream<QuestionState> _performAction(QuestionBloc bloc) async* {
    if (bloc._eventQueue.isNotEmpty) {
      yield* bloc._eventQueue.removeFirst()._performAction(bloc);
    }
  }

  @override
  List<Object> get props => [questionId, categoryId, text];
}

// ///Event which indicated loading has been completed.
// ///It yields a new State, which has to be created before adding the event and
// ///does not perform any actions.
// class LoadingCompleteEvent extends QuestionEvent {
//   final QuestionState nextState;
//
//   LoadingCompleteEvent(this.nextState);
//
//   @override
//   QuestionState _nextState(QuestionBloc bloc) => nextState;
//
//   @override
//   Future<void> _performAction(QuestionBloc bloc) async => null;
// }

///Event which prompts loading all questions
///(for a particular Category, if specified) from the backend
class LoadQuestionsEvent extends QuestionEvent {
  LoadQuestionsEvent(int categoryId) : super(categoryId: categoryId);

  @override
  Stream<QuestionState> _performAction(QuestionBloc bloc) async* {
    yield LoadingQuestionsState(
      currentQuestion: bloc.state.currentQuestion,
      allQuestions: bloc.state.allQuestions,
    );

    try {
      var body = categoryId == -1
          ? jsonDecode(await HttpRepository().read(api.qAndABaseUrl))
          : jsonDecode(await HttpRepository()
              .read('${api.questionsCategoriesUrl}/$categoryId'));

      final questions = buildEntitiesFromList(body, Question.fromJson);

      yield QuestionsLoadedState(
        allQuestions: questions,
        currentQuestion: bloc.state.currentQuestion,
      );
    } on Exception catch (_) {
      yield bloc.state;
    }

    super._performAction(bloc);
  }
}

///Event which posts a new Question (to a particular Category, if specified)
class PostQuestionEvent extends QuestionEvent {
  final int currentId;
  PostQuestionEvent(int categoryId, String questionText, this.currentId)
      : super(text: questionText, categoryId: categoryId);

  @override
  Stream<QuestionState> _performAction(QuestionBloc bloc) async* {
    yield LoadingQuestionsState(
      allQuestions: bloc.state.allQuestions,
      currentQuestion: bloc.state.currentQuestion,
    );

    var map = {
      'categoryId': '$categoryId',
      'text': text,
    };
    await HttpRepository().post(
      api.qAndABaseUrl,
      body: utf8.encode(jsonEncode(map)),
    );
    yield* LoadQuestionsEvent(currentId)._performAction(bloc);

    super._performAction(bloc);
  }
}

///Event which edits a Question (to a particular Category, if specified)
class EditQuestionEvent extends QuestionEvent {
  final int currentId;
  EditQuestionEvent(String questionText, int questionId, this.currentId)
      : super(text: questionText, questionId: questionId);

  @override
  Stream<QuestionState> _performAction(QuestionBloc bloc) async* {
    yield LoadingQuestionsState(
      allQuestions: bloc.state.allQuestions,
      currentQuestion: bloc.state.currentQuestion,
    );

    var map = {
      'text': text,
    };
    await HttpRepository().patch(
      "${api.qAndABaseUrl}/$questionId",
      body: utf8.encode(jsonEncode(map)),
    );
    yield* LoadQuestionsEvent(currentId)._performAction(bloc);

    super._performAction(bloc);
  }
}

///Event which deletes an already posted question
class DeleteQuestionEvent extends QuestionEvent {
  DeleteQuestionEvent(int questionId, int categoryId)
      : super(questionId: questionId, categoryId: categoryId);

  @override
  Stream<QuestionState> _performAction(QuestionBloc bloc) async* {
    yield LoadingQuestionsState(
      allQuestions: bloc.state.allQuestions,
      currentQuestion: bloc.state.currentQuestion,
    );

    try {
      await HttpRepository().delete(api.questionByIdUrl(questionId));
      yield* LoadQuestionsEvent(categoryId)._performAction(bloc);
    } on Exception catch (e) {
      yield QuestionsLoadedState(
        allQuestions: bloc.state.allQuestions
          ..removeWhere((que) => que.id == questionId),
        currentQuestion: bloc.state.currentQuestion,
      );
      debugPrint(e.toString());
    }

    super._performAction(bloc);
  }
}

///Events which triggers reloading of a single Question
class UpdateSingleQuestionEvent extends QuestionEvent {
  UpdateSingleQuestionEvent(int questionId)
      : super(questionId: questionId, categoryId: -1);

  @override
  Stream<QuestionState> _performAction(QuestionBloc bloc) async* {
    yield LoadingQuestionsState(
      currentQuestion: bloc.state.questionById(questionId),
      allQuestions: bloc.state.allQuestions,
    );

    try {
      var body = jsonDecode(
        await HttpRepository().read(api.questionByIdUrl(questionId)),
      );
      var question = Question.fromJson(body);

      ///Replace the newly loaded Question by hand
      var index = bloc.state.allQuestions
          .indexWhere((element) => element.id == question.id);

      if (index == -1) {
        yield* LoadQuestionsEvent(categoryId)._performAction(bloc);
        index = bloc.state.allQuestions
            .indexWhere((element) => element.id == question.id);
      }
      final newQuestions = <Question>[
        ...bloc.state.allQuestions..removeAt(index)
      ]..insert(index, question);

      yield QuestionsLoadedState(
        currentQuestion: question,
        allQuestions: newQuestions,
      );
    } on Exception catch (_) {
      yield EmptyQuestionState();
    }

    super._performAction(bloc);
  }
}

///Event which toggles the Like on a question
class ToggleLikeEvent extends QuestionEvent {
  final Question question;

  ToggleLikeEvent(this.question, {int categoryId = -1})
      : super(questionId: question.id, categoryId: categoryId);

  @override
  Stream<QuestionState> _performAction(QuestionBloc bloc) async* {
    var index = bloc.state.allQuestions.indexOf(question);
    var newQuestions = <Question>[...bloc.state.allQuestions..remove(question)]
      ..insert(
        index,
        question.copyWith(
          likes: question.isLiked ? question.likes - 1 : question.likes + 1,
          isLiked: !question.isLiked,
        ),
      );
    yield QuestionToggledState(allQuestions: newQuestions);

    try {
      if (question.isLiked) {
        await HttpRepository().post(api.unlikeQuestionByIdUrl(question.id));
      } else {
        await HttpRepository().post(api.likeQuestionByIdUrl(question.id));
      }
    } on Exception catch (e) {
      debugPrint(e.toString());
    }

    super._performAction(bloc);
  }
}

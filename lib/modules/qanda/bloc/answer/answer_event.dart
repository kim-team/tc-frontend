/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

part of 'answer_bloc.dart';

///Base class for all answer Events
abstract class AnswerEvent extends Equatable {
  ///ID of the question to which an AnswerEvent belongs
  final int questionId;

  ///ID of the Answer which is to be modified
  final int answerId;

  ///The text content of the Answer
  final String text;

  AnswerEvent({
    this.questionId = -1,
    this.answerId = -1,
    this.text = "",
  });

  Future<void> _performAction(AnswerBloc bloc);

  ///If not further specified all inheriting Events yield a LoadingState with
  ///all data being copied over.
  AnswerState _nextState(AnswerBloc bloc) => LoadingAnswersState(
        answers: bloc.state.answers,
        myReplies: bloc.state.myReplies,
        questionId: bloc.state.questionId,
      );

  @override
  List<Object> get props => [questionId, answerId, text];
}

///Event which indicated loading has been completed.
///It yields a new State, which has to be created before adding the event and
///does not perform any actions.
class LoadingCompleteEvent extends AnswerEvent {
  final AnswerState nextState;

  LoadingCompleteEvent(this.nextState);

  @override
  AnswerState _nextState(AnswerBloc bloc) => nextState;

  @override
  Future<void> _performAction(AnswerBloc bloc) async {}
}

///Event which prompts loading answers for a specific question
class LoadAnswersEvent extends AnswerEvent {
  LoadAnswersEvent(int questionId) : super(questionId: questionId);

  @override
  Future<void> _performAction(AnswerBloc bloc) async {
    try {
      var body = jsonDecode(
        await HttpRepository().read(
          api.answersByQuestionIdUrl(questionId),
        ),
      );
      final answers = buildEntitiesFromList(body, Answer.fromJson);

      bloc.add(
        LoadingCompleteEvent(
          AnswersLoadedState(
            answers: answers,
            questionId: questionId,
            myReplies: bloc.state.myReplies,
          ),
        ),
      );
    } on Exception catch (e) {
      debugPrint("EXCEPTION CAUGHT IN LOAD EVENT: $e");
      bloc.add(
        LoadingCompleteEvent(
          AnswersLoadedState(
            questionId: bloc.state.questionId,
            myReplies: bloc.state.myReplies,
            answers: bloc.state.answers,
          ),
        ),
      );
    }
  }
}

///Event which posts a new Answer to a specific question.
class PostAnswerEvent extends AnswerEvent {
  PostAnswerEvent(int questionId, String answerText)
      : super(text: answerText, questionId: questionId);

  @override
  Future<void> _performAction(AnswerBloc bloc) async {
    var map = {'text': text};
    await HttpRepository().post(
      api.answersByQuestionIdUrl(questionId),
      body: utf8.encode(jsonEncode(map)),
    );
    bloc.add(LoadMyAnswersEvent());
    bloc.add(LoadAnswersEvent(questionId));
  }
}

///Event which edits an existing Answer from a specific question.
class EditAnswerEvent extends AnswerEvent {
  EditAnswerEvent(int answerId, String answerText, int questionId)
      : super(text: answerText, answerId: answerId, questionId: questionId);

  @override
  Future<void> _performAction(AnswerBloc bloc) async {
    var map = {'text': text};
    await HttpRepository().patch(
      api.answerByAnswerIdUrl(answerId),
      body: utf8.encode(jsonEncode(map)),
    );
    bloc.add(LoadMyAnswersEvent());
    bloc.add(LoadAnswersEvent(questionId));
  }
}

///Event which deletes an already given answer.
class DeleteAnswerEvent extends AnswerEvent {
  DeleteAnswerEvent(int questionId, int answerId)
      : super(questionId: questionId, answerId: answerId);

  @override
  Future<void> _performAction(AnswerBloc bloc) async {
    try {
      await HttpRepository().delete(api.answerByAnswerIdUrl(answerId));
      bloc.add(LoadMyAnswersEvent());
      bloc.add(LoadAnswersEvent(questionId));
    } on Exception catch (e) {
      debugPrint('EXCEPTION CAUGHT IN DELETE ANSWER: $e');
      bloc.add(
        LoadingCompleteEvent(
          AnswersLoadedState(
            questionId: bloc.state.questionId,
            answers: bloc.state.answers
              ..removeWhere((element) => element.id == answerId),
            myReplies: bloc.state.myReplies,
          ),
        ),
      );
    }
  }

  @override
  AnswerState _nextState(AnswerBloc bloc) => AnswersLoadedState(
        questionId: bloc.state.questionId,
        answers: bloc.state.answers
          ..removeWhere((element) => element.id == answerId),
        myReplies: bloc.state.myReplies,
      );
}

///Event which loads all the answers of the current user including their
///respective questions for the [MyAnswersTab]
class LoadMyAnswersEvent extends AnswerEvent {
  @override
  Future<void> _performAction(AnswerBloc bloc) async {
    try {
      final body = List<Map<String, dynamic>?>.from(jsonDecode(
        await HttpRepository().read(api.myRepliesUrl),
      ));

      var allReplies = <Question, List<Answer>>{};

      for (var entry in body.whereType<Map<String, dynamic>>()) {
        final repliesPerEntry = buildEntitiesFromList(
          entry['answers'],
          Answer.fromJson,
        );

        final question = Question.fromJson(entry);

        if (allReplies.keys.any((element) => element.id == question.id)) {
          allReplies[question]!.add(
            repliesPerEntry.elementAt(0),
          );
        } else {
          allReplies.putIfAbsent(
              Question.fromJson(entry), () => repliesPerEntry);
        }
      }

      bloc.add(
        LoadingCompleteEvent(
          AnswersLoadedState(
            answers: bloc.state.answers,
            questionId: bloc.state.questionId,
            myReplies: allReplies,
          ),
        ),
      );
    } on Exception catch (e) {
      debugPrint('EXCEPTION CAUGHT IN LOADING OWN ASWERS: $e');
      bloc.add(
        LoadingCompleteEvent(
          AnswersLoadedState(
            answers: bloc.state.answers,
            questionId: bloc.state.questionId,
            myReplies: bloc.state.myReplies,
          ),
        ),
      );
    }
  }

  @override
  AnswerState _nextState(AnswerBloc bloc) => AnswersLoadedState(
        questionId: bloc.state.questionId,
        myReplies: bloc.state.myReplies,
        answers: bloc.state.answers,
      );
}

///Abstract class which allows Answers to be marked as (not) helpful.
abstract class HelpfulEvent extends AnswerEvent {
  HelpfulEvent(int answerId) : super(answerId: answerId);
}

///Event which marks an answer as Helpful
class MarkAnswerHelpfulEvent extends HelpfulEvent {
  MarkAnswerHelpfulEvent(int answerId) : super(answerId);

  @override
  Future<void> _performAction(AnswerBloc bloc) async {
    try {
      await HttpRepository().post(api.helpfulUrl(answerId));
      bloc.add(LoadMyAnswersEvent());
    } on Exception catch (e) {
      debugPrint(e.toString());
    }
  }

  @override
  AnswerState _nextState(AnswerBloc bloc) {
    var updatedAnswers = bloc.state._markHelpfulById(answerId);
    return HelpfulState(
      answers: updatedAnswers,
      myReplies: bloc.state.myReplies,
      questionId: bloc.state.questionId,
    );
  }
}

///Event which removes the Helpful Marker from an answer
class MarkAnswerNotHelpfulEvent extends HelpfulEvent {
  MarkAnswerNotHelpfulEvent(int answerId) : super(answerId);

  @override
  Future<void> _performAction(AnswerBloc bloc) async {
    try {
      await HttpRepository().delete(api.helpfulUrl(answerId));
      bloc.add(LoadMyAnswersEvent());
    } on Exception catch (e) {
      debugPrint(e.toString());
    }
  }

  @override
  AnswerState _nextState(AnswerBloc bloc) {
    var updatedAnswers = bloc.state._unmarkHelpful();
    return HelpfulState(
      answers: updatedAnswers,
      myReplies: bloc.state.myReplies,
      questionId: bloc.state.questionId,
    );
  }
}

///Event which toggles the Like on an answer.
class ToggleLikeEvent extends AnswerEvent {
  final Answer answer;

  ToggleLikeEvent(this.answer);

  @override
  Future<void> _performAction(AnswerBloc bloc) async {
    try {
      if (answer.isLiked) {
        await HttpRepository().post(api.unlikeAnswerByIdUrl(answer.id));
      } else {
        await HttpRepository().post(api.likeAnswerByIdUrl(answer.id));
      }
    } on Exception catch (e) {
      debugPrint(e.toString());
    }
    bloc.add(LoadMyAnswersEvent());
  }

  @override
  AnswerState _nextState(AnswerBloc bloc) {
    var state = bloc.state.._toggleAnswer(answer);
    return AnswerToggledState(
      questionId: state.questionId,
      answers: state.answers,
      myReplies: state.myReplies,
    );
  }
}

/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

// DO NOT MODIFY!
// THIS IS A COPY OF [HydratedBloc].
// PLEASE REFER TO THEIR DOCUMENTATION.
// THIS IS ONLY INTENDED TO RESOLVE THE ISSUE OF SCOPES
// https://github.com/felangel/hydrated_bloc/issues/54

// ignore_for_file: avoid_catches_without_on_clauses, implementation_imports
// ignore_for_file: avoid_slow_async_io

import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:hive/hive.dart';
import 'package:hive/src/hive_impl.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:meta/meta.dart';
import 'package:path_provider/path_provider.dart';
import 'package:synchronized/synchronized.dart';

/// Implementation of [Storage] which uses `PathProvider` and `dart.io`
/// to persist and retrieve state changes from the local device.
class LocalUserHydStorage implements Storage {
  /// {@macro hydrated_cubit_storage}
  @visibleForTesting
  LocalUserHydStorage(this._box);

  /// Returns an instance of [HydratedStorage].
  /// [storageDirectory] can optionally be provided.
  /// By default, [getTemporaryDirectory] is used.
  ///
  /// With [encryptionCipher] you can provide custom encryption.
  /// Following snippet shows how to make default one:
  /// ```dart
  ///
  /// const password = 'hydration';
  /// final byteskey = sha256.convert(utf8.encode(password)).bytes;
  /// return HydratedAesCipher(byteskey);
  /// ```
  static Future<LocalUserHydStorage> build({
    Directory? storageDirectory,
    HydratedCipher? encryptionCipher,
  }) =>
      _lock.synchronized(() async {
        if (_instance != null) return _instance!;
        // Use HiveImpl directly to avoid conflicts with existing Hive.init
        // https://github.com/hivedb/hive/issues/336
        hive = HiveImpl();
        Box<dynamic> box;
        if (storageDirectory == HydratedStorage.webStorageDirectory) {
          box = await hive.openBox<dynamic>(
            'local_user_hydrated_box',
            encryptionCipher: encryptionCipher,
          );
        } else {
          final directory =
              storageDirectory ?? await getApplicationSupportDirectory();
          hive.init(directory.path);
          box = await hive.openBox<dynamic>(
            'local_user_hydrated_box',
            encryptionCipher: encryptionCipher,
          );
          await _migrate(directory, box);
        }

        return _instance = LocalUserHydStorage(box);
      });

  static Future _migrate(Directory directory, Box box) async {
    final file = File('${directory.path}/.local_user_hydrated_box.json');
    if (await file.exists()) {
      try {
        final dynamic storageJson = json.decode(await file.readAsString());
        final cache = (storageJson as Map).cast<String, String>();
        for (final key in cache.keys) {
          try {
            final string = cache[key];
            final dynamic object = json.decode(string ?? '');
            await box.put(key, object);
          } catch (_) {}
        }
      } catch (_) {}
      await file.delete();
    }
  }

  /// Internal instance of [HiveImpl].
  /// It should only be used for testing.
  @visibleForTesting
  static late HiveInterface hive;

  static final _lock = Lock();
  static LocalUserHydStorage? _instance;

  final Box _box;

  @override
  dynamic read(String key) => _box.isOpen ? _box.get(key) : null;

  @override
  Future<void> write(String key, dynamic value) async {
    if (_box.isOpen) {
      return _lock.synchronized(() => _box.put(key, value));
    }
  }

  @override
  Future<void> delete(String key) async {
    if (_box.isOpen) {
      return _lock.synchronized(() => _box.delete(key));
    }
  }

  @override
  Future<void> clear() async {
    if (_box.isOpen) {
      _instance = null;
      return _lock.synchronized(_box.deleteFromDisk);
    }
  }
}

/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:async';
import 'dart:convert';

import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:json_annotation/json_annotation.dart';

import '../../common/constants/api_constants.dart';
import '../../repositories/http/http_repository.dart';
import '../perm_hyd_bloc/perm_hyd_bloc.dart';
import 'model/user_model.dart';

export 'model/user_model.dart';

part 'user_bloc.g.dart';
part 'user_event.dart';
part 'user_state.dart';

// TODO: Make more actions on the user available
/// Provides data to `User`
/// also allows to modify the data via [UserEvent]s.
///
/// Persistence is handled by extending [PermHydBloc] and our backend.
/// This relays [fromJson] and [toJson] to [UserState].
class UserBloc extends PermHydBloc<UserEvent, UserState> {
  /// This is implemented as a singleton,
  /// so that other Blocs can more easily depend on this.
  ///
  /// Singleton instance to be retrieved.
  static final UserBloc _singleton = UserBloc._internal();

  /// Factory constructor to return singleton instance.
  factory UserBloc() => _singleton;

  /// Actual constructor to initiate this.
  /// Constructor that sets the [_httpRepo] from [HttpRepository]
  UserBloc._internal()
      : _httpRepo = HttpRepository(),
        super(UserLoggedOutState()) {
    if (state.errorType != UserError.none) {
      add(UserFetchEvent());
    }
  }

  final HttpRepository _httpRepo;

  /// Lets the [UserEvent.performAction] method handle the business logic
  /// and returns a new [UserState] based on the changed values.
  @override
  Stream<UserState> mapEventToState(UserEvent event) async* {
    yield event._performAction(this);
  }

  /// Deserializes the [UserState] as support for [PermHydBloc].
  ///
  /// If no state can be generated, [UserState.baseState] is used.
  @override
  UserState fromJson(Map<String, dynamic> json) {
    try {
      return UserState.fromJson(json);
    } on Exception catch (e) {
      add(UserErrorEvent.read(e.toString()));
      return UserErrorState.baseState(UserError.read, e.toString());
    }
  }

  /// Serializes the [UserState] as support for [PermHydBloc].
  ///
  /// If no json can be generated, [UserErrorState] is used.
  @override
  Map<String, dynamic> toJson(UserState state) {
    Map<String, dynamic> json;
    try {
      json = state.toJson();
    } on Exception catch (e) {
      add(UserErrorEvent.write(e.toString()));
      json = UserErrorState(state.user, UserError.write, e.toString()).toJson();
    }
    return json;
  }

  @override
  String toString() => runtimeType.toString();
}

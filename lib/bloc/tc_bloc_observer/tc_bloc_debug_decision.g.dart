// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'tc_bloc_debug_decision.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TCBlocDebugDecision _$TCBlocDebugDecisionFromJson(Map<String, dynamic> json) =>
    TCBlocDebugDecision(
      onCreate: json['onCreate'] as bool? ?? false,
      onEvent: json['onEvent'] as bool? ?? false,
      onChange: json['onChange'] as bool? ?? false,
      onTransition: json['onTransition'] as bool? ?? false,
      onPersistence: json['onPersistence'] as bool? ?? false,
      onError: json['onError'] as bool? ?? false,
      onClose: json['onClose'] as bool? ?? false,
    );

Map<String, dynamic> _$TCBlocDebugDecisionToJson(
        TCBlocDebugDecision instance) =>
    <String, dynamic>{
      'onCreate': instance.onCreate,
      'onEvent': instance.onEvent,
      'onChange': instance.onChange,
      'onTransition': instance.onTransition,
      'onPersistence': instance.onPersistence,
      'onError': instance.onError,
      'onClose': instance.onClose,
    };

/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

part of 'auth_bloc.dart';

/// Enumeration for all types of Errors we may encounter
enum AuthError {
  /// No error detected
  none,

  /// Unknown Exception detected
  unknown,

  /// No token available
  hasNoToken,

  /// Persisting the token failed
  persistTokenFailed,

  /// Resetting the used Storages failed
  resettingStorageFailed,

  /// Registering a new Account failed
  registerFailed,

  /// Tried to register an already existing User
  registerExistingUser,

  /// Authentication failed
  authenticateFailed,

  /// Credentials are incorrect
  credentialsIncorrect,

  /// Problems with upgrading an non verified User
  upgradeUser,

  /// Connectivity issues
  connectivity,

  /// Malformed massage
  messageFormat,

  /// Email not Verified
  emailNotVerified,

  /// Email not Found
  emailNotFound,

  /// Email already in use
  emailAlreadyInUse,

  /// Refresh Token expired
  refreshExpired,

  /// Unauthorized
  unauthorized,
}

/// Abstract class from which the other [AuthState]s inherit
/// [errorType] and [errorMessage] must not be null
abstract class AuthState extends Equatable {
  /// [AuthError] which describes the type of error
  final AuthError errorType;

  /// [String] which gives specific information of possible errors
  final String errorMsg;

  AuthState([this.errorType = AuthError.none, this.errorMsg = ""]);

  AuthState._fromState(AuthState state)
      : errorType = state.errorType,
        errorMsg = state.errorMsg;

  @override
  String toString() => errorType == AuthError.none
      ? runtimeType.toString()
      : '${runtimeType.toString()}: '
          '{ ${errorType.toString()}, errorMsg: $errorMsg }';

  @override
  List<Object> get props => [errorType, errorMsg];
}

/// Class which extends [AuthState] and is used to describe
/// an uninitialized state
class AuthUninitializedState extends AuthState {}

/// Class which extends [AuthState] and is used to describe
/// and authenticated state
class AuthenticatedState extends AuthState {}

/// Class which extends [AuthState] and is used to describe
/// an unauthenticated State
class UnauthenticatedState extends AuthState {
  /// Empty Constructor must be provided, for consistency reasons
  UnauthenticatedState();

  /// Creates an [UnauthenticatedState] from a previous State
  UnauthenticatedState.fromState(AuthState state) : super._fromState(state);
}

/// Class which extends [AuthState] and is used to describe
/// a loading State
class AuthLoadingState extends AuthState {
  /// Empty Constructor must be provided, for consistency reasons
  AuthLoadingState();

  /// Create an [AuthLoadingState] from a previous state
  AuthLoadingState.fromState(AuthState state) : super._fromState(state);
}

/// Class which extends [AuthState] and is used to describe
/// an error State in the authentication
class AuthErrorState extends AuthState {
  /// Creates an [AuthErrorState] with an appropriate [errorType] and [errorMsg]
  AuthErrorState(AuthError errorType, String errorMsg)
      : super(errorType, errorMsg);
}

/// Class which extends [AuthErrorState] and is used to explicitly
/// describe an offline State
class AuthOfflineState extends AuthErrorState {
  /// Creates an [AuthOfflineState] with [AuthError.connectivity] and [errorMsg]
  AuthOfflineState(AuthError errorType, String errorMsg)
      : super(errorType, errorMsg);
}

/// Emitted when email reset request was successfully received
class AuthResetEmailSentState extends AuthState {
  AuthResetEmailSentState();
}

/*
 * Copyright 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/widgets.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

/// Check current locale, try 'de' if unavailable or return placeholder
String read(Map<String, String> data, BuildContext context) {
  if (data.isEmpty) return 'kein Inhalt';

  final languageCode = FlutterI18n.currentLocale(context)?.languageCode ?? 'de';

  return data[languageCode] ?? 'nicht gefunden';
}

bool hasContent(Map<String, String> data, BuildContext context) {
  if (data.isEmpty) return false;

  final languageCode = FlutterI18n.currentLocale(context)?.languageCode ?? 'de';

  return (data[languageCode] ?? '').isNotEmpty;
}

bool hasGermanContent(Map<String, String> data) {
  if (data.isEmpty) return false;

  return data['de']?.isNotEmpty ?? false;
}

List<T> buildEntitiesFromList<T>(
  List<dynamic>? list,
  T Function(Map<String, dynamic>) constructor,
) =>
    List<Map<String, dynamic>?>.from(list ?? [])
        .whereType<Map<String, dynamic>>()
        .map(constructor)
        .toList();

List<T> buildEntitiesFromMap<T>(
  Map<String, dynamic>? map,
  T Function(MapEntry<String, Map<String, dynamic>>) constructor,
) =>
    Map<String, Map<String, dynamic>?>.from(map ?? {})
        .entries
        .where((e) => e.value is Map<String, dynamic>)
        .map((e) => MapEntry(e.key, e.value as Map<String, dynamic>))
        .map(constructor)
        .toList();

/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
part of '../tc_theme.dart';

class CurrentTheme {
  static final CurrentTheme _instance = CurrentTheme._internal();

  factory CurrentTheme() => _instance;

  CurrentTheme._internal() {
    _setInitTheme();
  }

  late TCThemes _currentTheme;

  TCThemes get theme => _currentTheme;

  ThemeData get themeData {
    switch (_currentTheme) {
      case TCThemes.light:
        return light.theme;
      case TCThemes.dark:
        return dark.theme;
    }
  }

  Color get textSoftLightBlue {
    switch (_currentTheme) {
      case TCThemes.light:
        return CorporateColors.tinyCampusTextSoft;
      case TCThemes.dark:
        return CorporateColors.tinyCampusLightBlue;
    }
  }

  TextStyle get sportTextLabel => TextStyle(fontSize: 20, color: textSoftWhite);

  TextStyle get sportTextLabelSubtitle =>
      TextStyle(fontSize: 16, color: textPassive);

  TextStyle get sportInputIndicator => TextStyle(fontSize: 16, color: tcBlue);

  Color get passiveIconColor {
    switch (_currentTheme) {
      case TCThemes.light:
        return CorporateColors.tinyCampusPassiveIconGrey;
      case TCThemes.dark:
        return CorporateColors.tinyCampusPassiveIconGreyDM;
    }
  }

  Color get textSoftWhite {
    switch (_currentTheme) {
      case TCThemes.light:
        return CorporateColors.tinyCampusTextSoft;
      case TCThemes.dark:
        return Colors.white;
    }
  }

  Color get textPassive {
    switch (_currentTheme) {
      case TCThemes.light:
        return CorporateColors.tinyCampusIconGrey;
      case TCThemes.dark:
        return Colors.white;
    }
  }

  Color get tcBlue {
    switch (_currentTheme) {
      case TCThemes.light:
        return CorporateColors.tinyCampusBlue;
      case TCThemes.dark:
        return CorporateColors.tinyCampusLightBlue;
    }
  }

  Color get tcDarkBlue {
    switch (_currentTheme) {
      case TCThemes.light:
        return CorporateColors.tinyCampusDarkBlue;
      case TCThemes.dark:
        return CorporateColors.tinyCampusLightBlue;
    }
  }

  Color get tcBlueFont {
    switch (_currentTheme) {
      case TCThemes.light:
        return CorporateColors.tinyCampusBlue;
      case TCThemes.dark:
        return Colors.white;
    }
  }

  Color get textColor {
    switch (_currentTheme) {
      case TCThemes.light:
        return Colors.black;
      case TCThemes.dark:
        return Colors.white;
    }
  }

  String get homeScreenTCLogo {
    switch (_currentTheme) {
      case TCThemes.light:
        return AssetAdapter.tinyCampusLogo();
      case TCThemes.dark:
        return AssetAdapter.tinyCampusLogoDarkMode();
    }
  }

  void setCurrentTheme(TCThemes currentTheme) {
    _currentTheme = currentTheme;
    SharedPreferences.getInstance()
        .then((value) => value.setString('theme', currentTheme.toString()));
  }

  Future<void> _setInitTheme() async {
    var temp = await SharedPreferences.getInstance()
            .then((value) => value.getString('theme')) ??
        "";
    if (temp.isNotEmpty || !kDebugMode) {
      _currentTheme = TCThemes.values.firstWhere(
          (element) => element.toString() == temp,
          orElse: () => TCThemes.light);
    } else {
      var brightness = SchedulerBinding.instance?.window.platformBrightness ??
          Brightness.light;
      setCurrentTheme(
          brightness == Brightness.light ? TCThemes.light : TCThemes.dark);
    }
  }
}

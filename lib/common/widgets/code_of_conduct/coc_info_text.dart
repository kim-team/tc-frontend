/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../constants/routing_constants.dart';
import '../../tc_theme.dart';

class CocInfoTextWidget extends StatelessWidget {
  const CocInfoTextWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => RichText(
        text: TextSpan(
          text: '${FlutterI18n.translate(
            context,
            'modules.q_and_a.code_of_conduct.part_1',
          )} ',
          style: Theme.of(context)
              .textTheme
              .bodyText1
              ?.copyWith(color: CorporateColors.tinyCampusIconGrey),
          children: <TextSpan>[
            TextSpan(
              recognizer: TapGestureRecognizer()
                ..onTap = () => Navigator.pushNamed(context, cocRoute),
              text: FlutterI18n.translate(
                context,
                'modules.q_and_a.code_of_conduct.part_2',
              ),
              style: Theme.of(context)
                  .textTheme
                  .bodyText1
                  ?.copyWith(color: CorporateColors.tinyCampusOrange),
            ),
            TextSpan(
              text: FlutterI18n.translate(
                context,
                'modules.q_and_a.code_of_conduct.part_3',
              ),
              style: Theme.of(context)
                  .textTheme
                  .bodyText1
                  ?.copyWith(color: CorporateColors.tinyCampusIconGrey),
            ),
          ],
        ),
      );
}

/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../../repositories/http/http_repository.dart';
import '../../constants/api_constants.dart';
import '../../tc_theme.dart';
import '../dialog/tc_dialog.dart';

enum ReportAction { reportPost, blockUser }

extension ReportActionTranslationKey on ReportAction {
  String get _baseKey {
    switch (this) {
      case ReportAction.reportPost:
        return 'common.report.post';
      case ReportAction.blockUser:
        return 'common.report.block_user';
      default:
        return '';
    }
  }

  String get headlineKey => '$_baseKey.headline';
  String get bodyKey => '$_baseKey.body';
  String get successKey => '$_baseKey.success';
  String get errorKey => '$_baseKey.error';
}

class PopupReportButton extends StatelessWidget {
  final String reportPostRoute;
  final int userId;
  final String blockUserRoute;

  PopupReportButton({
    Key? key,
    required this.reportPostRoute,
    required this.userId,
  })  : blockUserRoute = blockUserUrl(userId),
        super(key: key);

  String _resolveRoute(ReportAction action) {
    switch (action) {
      case ReportAction.reportPost:
        return reportPostRoute;
      case ReportAction.blockUser:
        return blockUserRoute;
      default:
        return '';
    }
  }

  @override
  Widget build(BuildContext context) => PopupMenuButton<ReportAction>(
        child: Padding(
          padding: const EdgeInsets.only(right: 8.0),
          child: Icon(
            Icons.more_vert,
            color: CorporateColors.tinyCampusIconGrey.withOpacity(0.5),
          ),
        ),
        onSelected: (action) => TCDialog.showCustomDialog(
          context: context,
          headlineText: FlutterI18n.translate(context, action.headlineKey),
          bodyText: FlutterI18n.translate(context, action.bodyKey),
          onConfirm: () async {
            try {
              final messenger = ScaffoldMessenger.of(context);
              try {
                await HttpRepository().post(_resolveRoute(action));
                messenger.showSnackBar(
                  SnackBar(content: I18nText(action.successKey)),
                );
              } on Exception catch (e) {
                debugPrint(e.toString());
                messenger.showSnackBar(
                  SnackBar(content: I18nText(action.errorKey)),
                );
              }
            } on Exception catch (e) {
              // don't rethrow for users sanity
              debugPrint(e.toString());
            }
          },
        ),
        itemBuilder: (context) => ReportAction.values
            .expand<PopupMenuEntry<ReportAction>>(
              (e) => [
                PopupMenuItem(value: e, child: I18nText(e.headlineKey)),
                if (e != ReportAction.values.last) PopupMenuDivider(),
              ],
            )
            .toList(),
      );
}

/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:math';

import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../tc_theme.dart';
import 'tc_form_field_model.dart';
import 'tc_form_field_util.dart';

/// [TCFormField] is a widget for our Design of a TextFormField.
/// It contains a [TCFormFieldModel] where you can set different settings.
class TCFormField extends StatefulWidget {
  /// [fmodel] is the specific [TCFormFieldModel] of a [TCFormField]
  final TCFormFieldModel formFieldModel;

  /// [_current] is the current value of the TextFormField
  /// inside of the [TCFormField]

  /// Constructor of [TCFormField]
  /// @param [formFieldModel] is required so the values can be used for
  /// the design of the [TCFormField]
  TCFormField({Key? key, required this.formFieldModel}) : super(key: key);

  @override
  TCFormFieldState createState() => TCFormFieldState();
}

typedef Listener = void Function();

/// [TCFormFieldState] is the state of one [TCFormField]
/// The state changes when you focus or unfocus a TCFormField, when you
/// enter text or when you click on one of the buttons
class TCFormFieldState extends State<TCFormField> {
  final FocusNode _currentFocus = FocusNode();

  /// counts the number of letters in the entered text
  late int _counter;
  late String _text;
  late Listener _listener;
  late TextEditingController _controller;

  String _errorText = "";

  bool _isFocus = false;
  bool _wasEdited = false;
  bool _wasSaved = false;
  bool _error = false;
  bool _snackBarShown = false;

  Color _borderColor = CurrentTheme().tcBlueFont.withOpacity(0.2);

  @override
  void initState() {
    super.initState();
    _text = widget.formFieldModel.initialValue;
    _counter = widget.formFieldModel.initialValue.length;
    _controller = widget.formFieldModel.controller ??
        TextEditingController(text: widget.formFieldModel.initialValue);
    _controller.addListener(() => _hasError(_controller.text));
    _listener = () {
      if (mounted) {
        setState(() {
          if (!widget.formFieldModel.onlyReadable) {
            _isFocus = !_isFocus;
            _borderColor =
                _isFocus ? _borderColor : Color.fromARGB(255, 203, 217, 220);
            if (!_isFocus) {
              if (_wasEdited) {
                if (widget.formFieldModel.isPersistent && _wasEdited) {
                  widget.formFieldModel.persistent!(_controller.text.trim());
                }
              }
              _wasSaved = true;
            }
          }
        });
      }
    };
    _currentFocus.addListener(_listener);
    _controller.addListener(() {
      if (_controller.text.length > widget.formFieldModel.maxLength) {
        _controller.text = _text;
        _controller.selection = TextSelection.fromPosition(
            TextPosition(offset: _controller.text.length));
      } else {
        _text = _controller.text;
      }
      _counter = _text.length;
    });
  }

  @override
  Widget build(BuildContext context) => WillPopScope(
        onWillPop: () {
          setState(() {
            _wasEdited = false;
          });
          if ((_wasEdited && !_wasSaved) && _isFocus) {
            Future<void>.delayed(
              Duration(seconds: 1),
              _currentFocus.unfocus,
            );
          }
          return Future.value(true);
        },
        child: Container(
          color: Theme.of(context).primaryColor,
          child: Column(
            children: <Widget>[
              Container(
                height: 8,
              ),
              Stack(
                clipBehavior: Clip.none,
                children: <Widget>[
                  AnimatedContainer(
                    duration: Duration(milliseconds: 260),
                    margin: EdgeInsets.only(left: 5, right: 10),
                    padding: EdgeInsets.only(top: 2, bottom: 2),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      border: Border.all(
                          color: widget.formFieldModel.onlyReadable
                              ? Colors.transparent
                              : _borderColor,
                          width: 1.5,
                          style: BorderStyle.solid),
                    ),
                    child: DottedBorder(
                      dashPattern: [
                        3,
                        widget.formFieldModel.onlyReadable ? 3 : 0
                      ],
                      strokeWidth: 2,
                      color: widget.formFieldModel.onlyReadable
                          ? _borderColor
                          : Colors.transparent,
                      borderType: BorderType.RRect,
                      radius: Radius.circular(12),
                      child: _buildInputFieldContent(),
                    ),
                  ),
                  Positioned(
                    left: 12,
                    top: -8,
                    child: TCFormFieldHeader(
                        widget: widget,
                        wasEdited: _wasEdited,
                        error: _error,
                        wasSaved: _wasSaved),
                  ),
                  _isFocus && widget.formFieldModel.showRemainingChars
                      ? TCFormFieldTextCounter(
                          counter: _counter, widget: widget)
                      : Container(),
                  if (_isFocus && widget.formFieldModel.showButtons)
                    TCFormFieldButtons(del: () {
                      setState(() {
                        _wasSaved = false;
                        _controller.text = '';
                        _wasEdited = _controller.text !=
                            widget.formFieldModel.initialValue.trim();
                        _counter = _controller.text.length;
                        _hasError(_controller.text);
                        _controller.selection = TextSelection.fromPosition(
                            TextPosition(offset: _controller.text.length));
                      });
                    }, save: () {
                      setState(() {
                        _wasSaved = true;
                        _currentFocus.unfocus();
                      });
                    })
                  else
                    Container(),
                ],
              ),
              Container(
                height: widget.formFieldModel.showButtons ? 15 : 5,
              ),
            ],
          ),
        ),
      );

  /// @return the widget inside of the Border
  Widget _buildInputFieldContent() => Column(children: <Widget>[
        Container(
          margin: EdgeInsets.only(left: 9, top: 2, bottom: 2),
          child: Form(
            child: TextFormField(
              controller: _controller,
              focusNode: _currentFocus,
              onTap: () {
                setState(() {
                  if (!widget.formFieldModel.onlyReadable) {
                    _hasError(_controller.text);
                  }
                });
              },
              maxLines: widget.formFieldModel.obscure ? 1 : null,
              readOnly: widget.formFieldModel.onlyReadable,
              textCapitalization: widget.formFieldModel.textCapitalization,
              keyboardType: widget.formFieldModel.textInputType,
              obscureText: widget.formFieldModel.obscure,
              inputFormatters: widget.formFieldModel.formatters,
              style: TextStyle(
                fontSize: 16,
              ),
              decoration: InputDecoration(
                  counterText: "",
                  errorStyle: TextStyle(
                    fontSize: 18,
                    color: Colors.black,
                  ),
                  hintText: _controller.text.isEmpty
                      ? widget.formFieldModel.hintText
                      : null,
                  border: InputBorder.none),
              onChanged: (value) {
                // TODO: check this out in the future:
                // https://api.flutter.dev/flutter/widgets/TextSelectionControls/handlePaste.html
                if (value.length > widget.formFieldModel.maxLength) {
                  _controller.text = value.substring(
                      0, min(widget.formFieldModel.maxLength, value.length));
                  if (!_snackBarShown) {
                    setState(() {
                      _snackBarShown = true;
                    });
                    final snackBar = SnackBar(
                      duration: Duration(seconds: 3),
                      content: Text(FlutterI18n.translate(
                          context,
                          'modules.business_cards.vcard.'
                          'form.personal.too_many_chars')),
                    );
                    ScaffoldMessenger.of(context)
                        .showSnackBar(snackBar)
                        .closed
                        .then((value) {
                      setState(() {
                        _snackBarShown = false;
                      });
                    });
                  }
                }
                setState(() {
                  _wasSaved = false;
                  _wasEdited =
                      _controller.text != widget.formFieldModel.initialValue;
                  _hasError(value);
                });
              },
            ),
          ),
        ),
        _error && widget.formFieldModel.showErrorMessage
            ? Container(
                alignment: Alignment.centerLeft,
                margin: EdgeInsets.only(
                  left: 9,
                  bottom: 12,
                  right: 16,
                ),
                child: Text(
                  _errorText,
                  style: TextStyle(
                    fontSize: 14,
                    letterSpacing: -0.4,
                    color: CorporateColors.cafeteriaCautionRed,
                  ),
                ),
              )
            : Container(),
      ]);

  /// @param gets current value inside of the TextFormField
  /// validates the current value
  void _hasError(String value) {
    if (widget.formFieldModel.valid.isEmpty) {
      _borderColor = CurrentTheme().tcBlueFont;
      _error = false;
      return;
    }
    for (var val in widget.formFieldModel.valid) {
      _errorText = val(value) ?? "";
      if (_errorText.isNotEmpty) {
        _error = true;
        _borderColor = CorporateColors.cafeteriaCautionRed;
        return;
      }
    }
    _borderColor = CorporateColors.tinyCampusBlue;
    _error = false;
  }
}

// Thanks to 768 for the idea on StackOverflow:
// https://stackoverflow.com/questions/16800540/validate-email-address-in-dart/61512807#61512807
extension EmailValidator on String {
  bool isValidEmail() => RegExp(
          r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$')
      .hasMatch(this);
}

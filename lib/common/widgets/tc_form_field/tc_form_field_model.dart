/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

typedef Validate = String? Function(String value);
typedef Persist = void Function(String value);

/// [TCFormFieldModel] is the Model of one [TCFormField]
/// It contains hte values which can be set to change the design and
/// the behavior of a [TCFormField]
class TCFormFieldModel {
  /// The [initialValue] of a [TCFormField] is the start value.
  /// When its empty then [hintText] is shown
  final String initialValue;

  /// [title] is the text which stands above of the [TCFormField].
  /// It should be a representative text of the statement inside
  final String title;

  /// [hintText] only appears, when the current value of [TCFormField] is
  /// empty or null. Like [title] it's a description of what the user
  /// should write
  final String hintText;

  /// [maxLength] is the maximal number of letters which could be used in
  /// a [TCFormField]
  ///
  /// Must be a number greater then 0.
  final int maxLength;

  /// [isPersistent] gives the information if the value of a
  /// [TCFormField] is saved or not.
  ///
  /// When true [persistent] must not be null.
  ///
  /// When true it show's a "gespeichert" or "not gespeichert"
  /// besides validator sign
  final bool isPersistent;

  /// [persistent] is the function, which saves the value of the
  /// [TCFormField].
  ///
  /// When set [isPersistent] must be true.
  final Persist? persistent;

  /// [valid] is a List of Functions which checks
  /// if the input value is valid or not.
  ///
  /// A Function gets the current value and returns the error message.
  /// If no error then it return null.
  final List<Validate> valid;

  /// [textCapitalization] shows when the keyboard should activate shift again.
  ///
  /// Every word, every sentence
  final TextCapitalization textCapitalization;

  /// [textInputType] shows what type of input you have. A number, an email or
  /// a normal text. It changes the keyboard look
  final TextInputType textInputType;

  /// [onlyReadable] is true when the value of the
  /// [TCFormField] can't be changed.
  ///
  /// The [TCFormField] gets a [DottedBorder] and the text is lighter.
  final bool onlyReadable;

  /// [obscure] is true when the current value of a [TCFormField]
  /// shouldn't be visible. Example: Password
  final bool obscure;

  /// [controller] is the [TextEditingController] used to access the form
  /// fields value
  ///
  /// This is an optional parameter
  final TextEditingController? controller;

  /// [formatters] to be used for incoming text changes.
  ///
  /// This is an optional parameter.
  final List<TextInputFormatter> formatters;

  final bool showRemainingChars;

  final bool showErrorMessage;

  final bool showButtons;

  /// Constructor of [TCFormFieldModel].
  /// Creates a new TCFormFieldModel
  TCFormFieldModel({
    this.initialValue = "",
    this.title = "Titel",
    this.hintText = "Beschreibung",
    this.maxLength = 20,
    this.isPersistent = false,
    this.persistent,
    this.valid = const [],
    this.textCapitalization = TextCapitalization.sentences,
    this.textInputType = TextInputType.text,
    this.onlyReadable = false,
    this.obscure = false,
    this.controller,
    this.formatters = const [],
    this.showRemainingChars = true,
    this.showErrorMessage = true,
    this.showButtons = true,
  })  : assert(maxLength != 0),
        assert(isPersistent ? persistent != null : persistent == null),
        assert(persistent != null ? isPersistent : !isPersistent);
}

/*
 * Copyright 2020 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import '../../common/assets_adapter.dart';

class TcCachedImage extends StatelessWidget {
  const TcCachedImage({
    Key? key,
    this.width = 20,
    this.height = 20,
    this.borderRadius = 8.0,
    this.isRounded = false,
    this.url = "",
    this.stackForeground,
    this.stackBackground,
    this.imageMargin = EdgeInsets.zero,
    this.greyedOut = false,
    this.localPath,
    this.boxFit = BoxFit.cover,
  }) : super(key: key);

  final double width;
  final double height;
  final double borderRadius;
  final bool isRounded;
  final String url;
  final Widget? stackForeground;
  final Widget? stackBackground;
  final EdgeInsets imageMargin;
  final bool greyedOut;
  final String? localPath;
  final BoxFit boxFit;

  final duration = const Duration(milliseconds: 350);

  @override
  Widget build(BuildContext context) => Container(
        width: width,
        height: height,
        clipBehavior: Clip.none,
        child: Stack(
          clipBehavior: Clip.none,
          // alignment: Alignment.center,
          children: [
            if (stackBackground != null)
              Positioned.fill(child: stackBackground!),
            Positioned.fill(
              child: Container(
                margin: imageMargin,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(
                    isRounded ? borderRadius : 0,
                  ),
                ),
                clipBehavior: Clip.antiAlias,
                child: ColorFiltered(
                  colorFilter: !greyedOut
                      ? ColorFilter.mode(
                          Colors.transparent,
                          BlendMode.plus,
                        )
                      : ColorFilter.mode(
                          Colors.white,
                          BlendMode.saturation,
                        ),
                  child: !validImgUrl(url) || localPath != null
                      ? Image.asset(
                          localPath ?? AssetAdapter.tinyCampusLogoWithPadding(),
                          fit: boxFit,
                        )
                      : CachedNetworkImage(
                          placeholderFadeInDuration: duration,
                          fadeOutDuration: duration,
                          fadeInDuration: duration,
                          imageUrl: url,
                          placeholder: (context, _) => Image.asset(
                            AssetAdapter.tinyCampusLogoWithPadding(),
                            fit: boxFit,
                          ),
                          fit: boxFit,
                        ),
                ),
              ),
            ),
            if (stackForeground != null)
              Positioned.fill(child: stackForeground!),
          ],
        ),
      );

  bool validImgUrl(String url) {
    if (url.contains(RegExp(r'\.(gif|jpe?g|tiff?|png|webp|bmp)$'))) {
      if (Uri.parse(url).isAbsolute) {
        return true;
      }
    }
    return false;
  }
}

/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_linkify/flutter_linkify.dart';
import 'package:url_launcher/url_launcher.dart';

import '../tc_theme.dart';
import 'dialog/tc_dialog.dart';

class TCLinkText extends StatelessWidget {
  const TCLinkText({
    Key? key,
    required this.text,
    this.style,
    this.linkStyle,
    this.maxLines,
    this.overflow = TextOverflow.visible,
    this.textAlign = TextAlign.start,
    this.softWrap = true,
  }) : super(key: key);

  final String text;
  final TextStyle? style;
  final TextStyle? linkStyle;
  final int? maxLines;
  final bool softWrap;
  final TextOverflow overflow;
  final TextAlign textAlign;

  @override
  Widget build(BuildContext context) => Linkify(
        text: text,
        style: style ?? Theme.of(context).textTheme.bodyText1,
        linkStyle: linkStyle ??
            Theme.of(context).textTheme.bodyText1?.copyWith(
                color: CurrentTheme().textSoftLightBlue,
                decoration: TextDecoration.underline,
                decorationColor:
                    CorporateColors.tinyCampusTextSoft.withOpacity(0.3)),
        softWrap: softWrap,
        overflow: overflow,
        maxLines: maxLines,
        textAlign: textAlign,
        options: LinkifyOptions(
          looseUrl: false,
          humanize: false,
          removeWww: false,
          defaultToHttps: false,
        ),

        /// TODO: continue working when [TCUrlLinkifier] is ready
        // linkifiers: [EmailLinkifier(), TCUrlLinkifier()],
        onOpen: (link) => TCDialog.showCustomDialog(
          context: context,
          onConfirm: () => _launchUrl(link.url, context),
          functionActionText:
              FlutterI18n.translate(context, 'common.actions.continue'),
          headlineText: FlutterI18n.translate(
              context,
              'common.external_links.'
              'dialog_headline'),
          functionActionColor: CurrentTheme().tcBlue,
          bodyText: '${link.url}\n\n${FlutterI18n.translate(
            context,
            'common.external_links.dialog_body',
          )}',
        ),
      );
}

Future<void> _launchUrl(String url, BuildContext context) async {
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(content: Text("Error")),
    );
  }
}

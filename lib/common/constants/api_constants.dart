/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/foundation.dart';

/// tinyCampus API base URL
const String baseUrl = kReleaseMode
    ? "https://api-ipv6.tinycampus.de"
    : "https://staging.tinycampus.de";

/// IPv6 proxy URLs
const String cafeteriaUrl = 'https://mensa.tinycampus.de';
const String cafeteriaImageHost = '$cafeteriaUrl/images';

// TODO Change back once MaxManager supports IPv6
/// Official cafeteria data URL
// const String cafeteriaUrl = 'https://uni-giessen.maxmanager.xyz';
// const String cafeteriaImageHost = 'https://www.maxmanager.de';

/// URL for cafeteria testing purposes
const String cafeteriaTestingUrl = "https://tinycampus.de/mensaplanbackup";

// TODO Add back 'secured: false' to HttpRepo calls once proxy becomes obsolete
const String thmUrl = "$baseUrl/proxy";

/// URL of the THM semester fee page
String thmSemesterFeePage({bool useProxy = true}) =>
    "${useProxy ? thmUrl : "https://www.thm.de"}/site/studium/sie-studieren/rueckmeldung.html";

/// URI for remote translation files
const String i18nBaseUrl = 'https://tinycampus.de/i18n/';
final Uri localizationUri = Uri.parse(i18nBaseUrl);

const String tinyCampusEmail = 'info@tinycampus.de';

const String storeAndroidUrl =
    "https://play.google.com/store/apps/details?id=de.tinycampus.thm.app";
const String storeIosUrl =
    "https://apps.apple.com/de/app/tinycampus-thm/id1531921018";

// API URLs for core features
// API constants and helpers for modules are found in the module directories

/// URL for the authentication
const String authDeleteUrl = "$baseUrl/oauth/delete";

const String authRegisterUrl = "$baseUrl/oauth/signup";

const String authRecoveryUrl = "$baseUrl/oauth/recovery";

const String authResetUrl = "$baseUrl/oauth/reset";

const String authUpgradeUrl = "$baseUrl/oauth/upgrade";

/// URL for fetching the token
const String authFetchTokenUrl = "$baseUrl/oauth/token";

/// URL for refreshing the token
const String authRefreshTokenUrl = "$baseUrl/oauth/token";

/// URL for the homescreen
const String homePageUrl = "$baseUrl/homescreen";

/// URL for the timeline
const String timelineUrl = "$baseUrl/timeline";

/// URL for the profile
const String profileUrl = "$baseUrl/profile";

/// URL for the sport exercises
const String sportExercisesUrl = "${i18nBaseUrl}sport.exercises.json";

/// URL for the
const String ppUrl = "$baseUrl/praxis";

/// URL for bouncy ball
const String bbUrl = "$baseUrl/bouncyball";

/// URL for blocking users
String blockUserUrl(int userId) => "$baseUrl/users/$userId/block";

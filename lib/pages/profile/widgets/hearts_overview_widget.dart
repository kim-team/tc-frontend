/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';

import '../../../common/tc_theme.dart';

/// Widget for showcasing the distribution of earned Hearts
class HeartsOverviewWidget extends StatelessWidget {
  /// Category of the Hearts
  final String category;

  /// Hearts earned in the given category
  final int hearts;

  /// Hearts earned across all categories
  /// This value is needed for percentage calculation
  final int totalHearts;

  /// Constructor for this
  HeartsOverviewWidget({
    Key? key,
    required this.category,
    required this.hearts,
    required this.totalHearts,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => Container(
        padding: EdgeInsets.only(top: 10.0),
        child: ListTile(
          leading: Container(
            padding: EdgeInsets.only(left: 30.0, right: 20.0),
            child: Column(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(top: 8.0),
                  child: Icon(
                    Icons.favorite,
                    color: Colors.red,
                  ),
                ),
                Text(
                  "$hearts",
                ),
              ],
            ),
          ),
          title: Text(category, style: Theme.of(context).textTheme.headline3),
          subtitle: LinearPercentIndicator(
            width: MediaQuery.of(context).size.width - 180,
            curve: Curves.easeInOut,
            padding: EdgeInsets.symmetric(horizontal: 16.0),
            animation: true,
            lineHeight: 12.0,
            animationDuration: 900,
            leading: Text(totalHearts == 0
                ? '0 %'
                : '${(hearts * 100 / totalHearts).roundToDouble()} %'),
            percent: totalHearts == 0 ? 0 : hearts / totalHearts,
            linearStrokeCap: LinearStrokeCap.roundAll,
            progressColor: CorporateColors.tinyCampusBlue,
            backgroundColor: Colors.transparent,
          ),
        ),
      );
}

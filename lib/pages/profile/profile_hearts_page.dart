/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../bloc/user/user_bloc.dart';
import 'widgets/hearts_overview_widget.dart';

/// This page displays a User's Heart distribution
class ProfileHeartsPage extends StatelessWidget {
  const ProfileHeartsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => BlocBuilder<UserBloc, UserState>(
        builder: (context, state) => Scaffold(
          backgroundColor: Theme.of(context).backgroundColor,
          appBar: AppBar(
            title: I18nText('profile.hearts.title'),
          ),
          body: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                _buildHeartSumDisplay(context, state.user.totalHearts),
                ...state.user.hearts.entries.map(
                  (entry) => HeartsOverviewWidget(
                    category: entry.key,
                    hearts: entry.value,
                    totalHearts: state.user.totalHearts,
                  ),
                ),
              ],
            ),
          ),
        ),
      );

  Container _buildHeartSumDisplay(BuildContext context, int hearts) =>
      Container(
        padding: EdgeInsets.only(top: 40.0, bottom: 15.0),
        child: Row(
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(left: 30.0, right: 20.0),
              child: FloatingActionButton(
                elevation: 0.0,
                backgroundColor: Colors.transparent,
                child: Column(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(top: 5.0),
                      child: Icon(
                        Icons.favorite,
                        color: Colors.red,
                      ),
                    ),
                    Text("$hearts",
                        style: Theme.of(context).textTheme.bodyText1),
                  ],
                ),
                onPressed: null,
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(right: 32.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    I18nPlural(
                      'profile.hearts.amount.hearts',
                      hearts,
                      child: Text(
                        '',
                        style: Theme.of(context).textTheme.headline2,
                        textAlign: TextAlign.left,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      );
}

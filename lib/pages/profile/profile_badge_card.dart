/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:math';
import 'dart:ui';

import 'package:flare_flutter/flare_actor.dart';
import 'package:flare_flutter/flare_controls.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:intl/intl.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';

import '../../bloc/user/model/badge_model.dart';
import '../../common/tc_theme.dart';
import 'controller/badge_loop_controller.dart';

class ProfileBadgeCard extends StatefulWidget {
  final Badge badge;
  final int currentConditions;
  final int neededConditions;

  ProfileBadgeCard(this.badge, {Key? key})
      : neededConditions = badge.conditions[0].needed,
        currentConditions =
            min(badge.conditions[0].current, badge.conditions[0].needed),
        super(key: key);

  @override
  ProfileBadgeCardState createState() => ProfileBadgeCardState();
}

class ProfileBadgeCardState extends State<ProfileBadgeCard> {
  final FlareControls _ctrl = BadgeLoopController();

  @override
  Widget build(BuildContext context) {
    final badge = widget.badge;
    final needed = widget.neededConditions;
    final current = widget.currentConditions;

    return GestureDetector(
      onTap: () {
        Navigator.pop(context);
      },
      child: Scaffold(
        backgroundColor: Theme.of(context).primaryColor.withOpacity(0.3),
        body: GestureDetector(
          onTap: () {},
          child: BackdropFilter(
            filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
            child: Align(
              alignment: Alignment.center,
              child: Container(
                decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                      blurRadius: 5,
                      color: Colors.grey[400]!,
                    )
                  ],
                  borderRadius: BorderRadius.circular(20),
                  color: Theme.of(context).primaryColor,
                ),
                width: 300,
                height: 375,
                child: Stack(children: [
                  Align(
                    alignment: Alignment.center,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        SizedBox(
                          width: 120,
                          height: 120,
                          child: FlareActor(
                            "assets/flare/${badge.asset}",
                            alignment: Alignment.center,
                            fit: BoxFit.scaleDown,
                            antialias: false,
                            animation: badge.animationLevel,
                            controller: _ctrl,
                            callback: _ctrl.onCompleted,
                            isPaused: false,
                          ),
                        ),
                        Text(
                          FlutterI18n.translate(
                            context,
                            'badges.content.'
                            'id_${badge.badgeId.toString()}.title',
                          ),
                          style: Theme.of(context).textTheme.headline2,
                        ),
                        SizedBox(
                          width: 240,
                          child: Text(
                              FlutterI18n.translate(
                                  context,
                                  'badges.content.'
                                  'id_${badge.badgeId.toString()}.'
                                  'description'),
                              textAlign: TextAlign.center,
                              style: Theme.of(context).textTheme.bodyText1,
                              textWidthBasis: TextWidthBasis.parent,
                              maxLines: 6),
                        ),

                        //ProgressBar
                        (badge.unlockDate != null)
                            ? Column(
                                children: [
                                  I18nText(
                                    'profile.completed_on',
                                    child: Text(
                                      "abgeschlossen am",
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodyText2
                                          ?.copyWith(
                                              color: CorporateColors
                                                  .tinyCampusIconGrey),
                                    ),
                                  ),
                                  Text(
                                    DateFormat.yMd(
                                            FlutterI18n.currentLocale(context)
                                                    ?.languageCode ??
                                                'de')
                                        .add_jm()
                                        .format(badge.unlockDate!),
                                    style: Theme.of(context)
                                        .textTheme
                                        .headline6
                                        ?.copyWith(fontSize: 17),
                                  )
                                ],
                              )
                            : Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  VerticalDivider(
                                    width: 30,
                                    color: Colors.transparent,
                                  ),
                                  Text(
                                    '$current/$needed',
                                    style: TextStyle(fontSize: 16),
                                  ),
                                  VerticalDivider(
                                    color: Colors.transparent,
                                  ),
                                  Expanded(
                                    child: Container(
                                      decoration: BoxDecoration(
                                        gradient: LinearGradient(colors: [
                                          current == 0
                                              ? Colors.white
                                              : CorporateColors.tinyCampusBlue,
                                          Colors.white
                                        ]),
                                        border: Border.all(),
                                        borderRadius:
                                            BorderRadius.circular(30.0),
                                      ),
                                      child: LinearPercentIndicator(
                                        lineHeight: 15,
                                        percent: current / needed,
                                        animationDuration: 900,
                                        curve: Curves.easeInOut,
                                        animation: true,
                                        progressColor:
                                            CorporateColors.tinyCampusBlue,
                                        linearStrokeCap:
                                            LinearStrokeCap.roundAll,
                                        backgroundColor: Colors.white,
                                      ),
                                    ),
                                  ),
                                  VerticalDivider(
                                    width: 30,
                                    color: Colors.transparent,
                                  ),
                                ],
                              ),
                      ],
                    ),
                  ),
                  Align(
                    alignment: Alignment.topRight,
                    child: SizedBox(
                      width: 75,
                      height: 75,
                      child: IconButton(
                        icon: Icon(
                          Icons.close,
                          size: 40,
                          color: CorporateColors.tinyCampusIconGrey,
                        ),
                        onPressed: () {
                          Navigator.pop(context);
                        },
                      ),
                    ),
                  ),
                ]),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:dynamic_themes/dynamic_themes.dart';
import 'package:flutter/material.dart';

import '../../../common/tc_theme.dart';

class ThemeWidget extends StatefulWidget {
  final Color backgroundColor;
  final Color primaryColor;
  final String title;
  final TCThemes name;

  ThemeWidget(
    this.backgroundColor,
    this.primaryColor,
    this.title,
    this.name, {
    Key? key,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => ThemeWidgetState();
}

class ThemeWidgetState extends State<ThemeWidget> {
  @override
  Widget build(BuildContext context) => Column(
        children: [
          InkWell(
            onTap: () {
              setState(() {
                CurrentTheme().setCurrentTheme(widget.name);
                DynamicTheme.of(context)?.setTheme(
                  CurrentTheme().theme.index,
                );
              });
            },
            child: Container(
              decoration: BoxDecoration(
                color: widget.backgroundColor,
                borderRadius: BorderRadius.circular(5),
                border: Border.all(
                  color: CorporateColors.tinyCampusLightBlue,
                  width: 3,
                ),
              ),
              height: 110,
              width: 90,
              child: Column(
                children: [
                  Container(
                    margin: EdgeInsets.only(
                      left: 10,
                      top: 10,
                      bottom: 5,
                    ),
                    decoration: BoxDecoration(
                      color: widget.primaryColor,
                      borderRadius: BorderRadius.circular(15),
                    ),
                    height: 15,
                    width: 60,
                  ),
                  Container(
                    margin: EdgeInsets.only(
                      left: 20,
                      top: 5,
                      bottom: 5,
                    ),
                    decoration: BoxDecoration(
                      color: widget.primaryColor,
                      borderRadius: BorderRadius.circular(15),
                    ),
                    height: 15,
                    width: 50,
                  ),
                  Radio(
                    activeColor: CorporateColors.tinyCampusLightBlue,
                    groupValue: CurrentTheme().theme,
                    onChanged: (value) {
                      setState(() {
                        CurrentTheme().setCurrentTheme(widget.name);
                        DynamicTheme.of(context)?.setTheme(
                          CurrentTheme().theme.index,
                        );
                      });
                    },
                    value: widget.name,
                  ),
                ],
              ),
            ),
          ),
          Text(
            widget.title,
            style: TextStyle(
              fontSize: 20,
            ),
          ),
        ],
      );
}

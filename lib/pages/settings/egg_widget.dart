/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:math';

import 'package:flutter/material.dart';

import '../../common/tinycampus_icons.dart';
import 'credits_widget.dart';

class EggWidget extends StatefulWidget {
  EggWidget({
    Key? key,
    required this.creditWidget,
  }) : super(key: key);
  final CreditWidget creditWidget;

  @override
  _EggWidgetState createState() => _EggWidgetState();
}

class _EggWidgetState extends State<EggWidget> {
  late String name;
  late String role;
  late int counter;

  @override
  void initState() {
    super.initState();
    name = widget.creditWidget.name;
    role = widget.creditWidget.role;
    counter = 0;
  }

  void countDown() {
    setState(() {
      // if (role.length >= 1) {
      //   role = role.substring(0, role.length - 1);
      // }
      role = role.substring(1, role.length) + role.substring(0, 1);
      counter++;
    });
  }

  @override
  Widget build(BuildContext context) => Material(
        color: Colors.white,
        child: InkWell(
            onTap: countDown,
            child: Column(
              children: [
                CreditWidget(
                  credits: widget.creditWidget.credits,
                  date: widget.creditWidget.date,
                  name: name,
                  role: role,
                ),
                // Preparation for EasterEgg
                // Container(
                //   width: 100,
                //   height: 20,
                //   child: Stack(
                //     overflow: Overflow.visible,
                //     children: [
                //       AnimatedStar(),
                //     ],
                //   ),
                // ),
              ],
            )),
      );
}

class AnimatedStar extends StatefulWidget {
  AnimatedStar({Key? key}) : super(key: key);

  @override
  _AnimatedStarState createState() => _AnimatedStarState();
}

class _AnimatedStarState extends State<AnimatedStar>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;
  late CurvedAnimation curve;
  late StagAnimation stagAnimation;

  @override
  void initState() {
    super.initState();
    initAnimationController();
  }

  void initAnimationController() {
    _controller = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 1200),
    )..repeat();
    initAnimation();
    startAnimation();
    // _controller.forward();
  }

  void initAnimation() {
    curve = CurvedAnimation(parent: _controller, curve: Curves.elasticIn);
    stagAnimation = StagAnimation(controller: _controller);
  }

  void startAnimation() {
    _controller.animateTo(0.0, duration: Duration(milliseconds: 0));
    stagAnimation =
        StagAnimation(controller: _controller, beginScale: 1.0, endScale: 0.0);
    _controller.animateTo(1.0, duration: Duration(milliseconds: 600));
    setState(() {});
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) => Positioned(
        top: 1,
        left: 1,
        child: AnimatedBuilder(
          builder: (context, child) => Transform.rotate(
              angle: stagAnimation.rotation.value, child: child),
          animation: _controller,
          child: SizedBox(
            width: 20,
            height: 20,
            child: Transform.scale(
              scale: stagAnimation.scale.value,
              child: Icon(
                TinyCampusIcons.star_100p,
              ),
            ),
          ),
        ),
      );
}

class StagAnimation {
  StagAnimation({
    required this.controller,
    double beginRotation = 1,
    double endRotation = 1.10,
    double beginScale = 1.0,
    double endScale = 5.90,
  })  : rotation =
            Tween<double>(begin: beginRotation, end: endRotation).animate(
          CurvedAnimation(
            parent: controller,
            curve: Interval(0.0, 1.0, curve: Curves.linear),
          ),
        ),
        scale = Tween<double>(begin: beginScale, end: endScale).animate(
          CurvedAnimation(
            parent: controller,
            curve: Interval(0.3, 0.8, curve: Curves.easeIn),
          ),
        ),
        yTranslate = Tween<double>(begin: 0.0, end: 2 * pi).animate(
          CurvedAnimation(
            parent: controller,
            curve: Interval(0.3, 0.8, curve: Curves.linear),
          ),
        );

  final AnimationController controller;
  final Animation<double> rotation;
  final Animation<double> scale;
  final Animation<double> yTranslate;
}

/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';

import '../../common/tc_theme.dart';

class CreditWidget extends StatelessWidget implements Comparable<CreditWidget> {
  final String name;
  final String role;
  final String date;
  final List<String> credits;

  const CreditWidget({
    Key? key,
    required this.name,
    this.role = '',
    this.date = '',
    this.credits = const [],
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => Container(
        color: Theme.of(context).primaryColor,
        padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Text(
              name,
              style: Theme.of(context).textTheme.headline2,
            ),
            if (role.isEmpty)
              Padding(
                padding: EdgeInsets.symmetric(vertical: 2.0),
                child: Text(
                  role,
                  style: Theme.of(context)
                      .textTheme
                      .headline3
                      ?.copyWith(color: CorporateColors.tinyCampusOrange),
                ),
              ),

            /*

          Bitte als Kommentar drinlassen. Wenn die Credit-Beschreibungen
          fertig sind, wird das unten wieder auskommentiert

          */

            // date == null
            //     ? Container()
            //     : date.length == 0
            //         ? Container()
            //         : Padding(
            //             padding: EdgeInsets.symmetric(vertical: 5.0),
            //             child: Text(
            //               date,
            //               style: TextStyle(
            //                   color: CorporateColors.tinyCampusIconGrey),
            //             ),
            //           ),

            /*

          Bitte als Kommentar drinlassen. Wenn die Credit-Beschreibungen
          fertig sind, wird das unten wieder auskommentiert

          */
            /*
             credits == null
                 ? Container()
                 : credits.length == 0
                     ? Container()
                     : Padding(
                         padding: EdgeInsets.only(top: 6.0),
                         child: Column(
                           mainAxisAlignment: MainAxisAlignment.start,
                           crossAxisAlignment: CrossAxisAlignment.start,
                           children: credits.map((word) => Text(word)).toList(),
                         ),
                       ),
             */
          ],
        ),
      );

  @override
  int compareTo(CreditWidget other) =>
      name.toLowerCase().compareTo(other.name.toLowerCase());
}

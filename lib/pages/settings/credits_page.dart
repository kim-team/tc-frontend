/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../common/constants/api_constants.dart';
import 'credits_widget.dart';
import 'egg_widget.dart';

class CreditsPage extends StatelessWidget {
  const CreditsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
      appBar: AppBar(
        title: I18nText('credits.title'),
      ),
      backgroundColor: Theme.of(context).backgroundColor,
      body: Scrollbar(
        child: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Column(
            children: [
              buildCreditsHeader(context),
              Padding(
                padding: const EdgeInsets.only(top: 42.0),
                child: ListTile(
                  title: Row(
                    children: [
                      Text(
                        FlutterI18n.translate(
                            context, 'settings.credits_page.role.lead_project'),
                        style: Theme.of(context).textTheme.headline3,
                      ),
                    ],
                  ),
                ),
              ),
              CreditWidget(
                name: "Gisa von Marcard",
              ),
              Padding(
                padding: const EdgeInsets.only(top: 42.0),
                child: ListTile(
                  title: Row(
                    children: [
                      Text(
                        "Team ",
                        style: Theme.of(context).textTheme.headline3,
                      ),
                      SizedBox(
                          height: 20,
                          child: Image.asset(
                              "assets/images/tinycampus/logofont_horizontal.png"))
                    ],
                  ),
                ),
              ),
              ..._buildMainCreditsList(context),
              Padding(
                padding: const EdgeInsets.only(top: 42.0),
                child: ListTile(
                  title: Text(
                    FlutterI18n.translate(
                        context, 'settings.credits_page.preproduction'),
                    style: Theme.of(context).textTheme.headline3,
                  ),
                ),
              ),
              ..._buildPreProductionCredits(),
              Padding(
                padding: const EdgeInsets.only(top: 42.0),
                child: ListTile(
                  title: Text(
                    FlutterI18n.translate(
                        context, 'settings.credits_page.special_thanks'),
                    style: Theme.of(context).textTheme.headline3,
                  ),
                ),
              ),
              CreditWidget(name: "Prof. Dr. Katja Specht"),
              Padding(
                padding: const EdgeInsets.only(top: 42.0),
                child: ListTile(
                  title: Text(
                    FlutterI18n.translate(
                        context, 'settings.credits_page.thanks'),
                    style: Theme.of(context).textTheme.headline3,
                  ),
                ),
              ),
              ..._buildSpecialThanksCredits(),
            ],
          ),
        ),
      ));

  // TODO: think about implementing that in the json for better adjustments
  Widget buildCreditsHeader(BuildContext context) => Container(
        padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 20.0),
        margin: EdgeInsets.only(bottom: 4.0),
        color: Theme.of(context).primaryColor,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(bottom: 12.0),
              child: I18nText(
                'settings.credits_page.project_headline',
                child: Text(
                  "",
                  style: Theme.of(context).textTheme.headline2,
                ),
              ),
            ),
            I18nText('settings.credits_page.project_description',
                child: Text(
                  "",
                  style: Theme.of(context).textTheme.bodyText1,
                )),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 8.0),
              child: Text(
                '${FlutterI18n.translate(
                  context,
                  'settings.credits_page.email',
                )}: $tinyCampusEmail',
                style: Theme.of(context).textTheme.bodyText1,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                  bottom: 18.0, top: 24.0, left: 16, right: 16),
              child: Image.asset("assets/images/kim/thm-kim-bmbf.png"),
            ),
          ],
        ),
      );

  // TODO: think about implementing that in the json for better adjustments
  List<CreditWidget> _buildSpecialThanksCredits() {
    var strings = <String>[
      "Alexander Dickmann",
      "Alexandra Williams",
      "Andreas Deublein",
      "Andreas Reinhardt",
      "Annika Schön",
      "Babak Dibaj",
      "Benjamin Stuchly",
      "Carlo Pabst",
      "Christina Paulencu",
      "Christine Niksch",
      "Dagmar Hofmann",
      "Daniel Hemmelmann",
      "Elfriede Kloos",
      "Erik Weigand",
      "Ernö Szivek",
      "Eva Mohr",
      "Fabrice Weiss",
      "Florian Schambach",
      "Harald Platen",
      "Heiko Krischker",
      "Holger Carstensen",
      "Isabell Koch",
      "Ivo Senner",
      "James Antrim",
      "Jens Ihle",
      "Johanna Vogt",
      "Julius Schönhut",
      "Kai Dammann",
      "Katja Däumer",
      "Lars Wottawa",
      "Maili Winoto",
      "Meike Feth",
      "Meike Zimmer",
      "Michael Fey",
      "Michel Schmitz",
      "Monika Gonka",
      "Prof. Dr. Olaf Berger",
      "Prof. Dr. Peter Hohmann",
      "Ralph Kampmann",
      "Robin Geretschläger",
      "Sabine Fidorski",
      "Simone Binz",
      "Prof. Dr. Sven Keller",
      "Tim Brückner",
      "Tim Mehlmann",
      "Yasmin Yenen",
    ];
    return strings.map((e) => CreditWidget(name: e)).toSet().toList();
  }

  // TODO: think about implementing that in the json for better adjustments
  List<CreditWidget> _buildPreProductionCredits() => <CreditWidget>[
        CreditWidget(
          name: "André Dommaschke",
        ),
        CreditWidget(
          name: "Lea Schmidt",
        ),
        CreditWidget(
          name: "Marco Allendorf",
        ),
        CreditWidget(
          name: "Martin Heller",
        ),
      ];

  // TODO: think about implementing that in the json for better adjustments
  List<Widget> _buildMainCreditsList(BuildContext context) => <Widget>[
        CreditWidget(
            name: "Abdullah Al Noman",
            role: FlutterI18n.translate(
                context, 'settings.credits_page.role.dev_frontend')),
        CreditWidget(
            name: "Anastasia Ruppel",
            role: FlutterI18n.translate(
                context, 'settings.credits_page.role.lead_art')),
        CreditWidget(
            name: "Anja Brüstle",
            role: FlutterI18n.translate(
                context, 'settings.credits_page.role.assistant_producer')),
        CreditWidget(
            name: "Anthony Schuster",
            role: FlutterI18n.translate(
                context, 'settings.credits_page.role.producer')),
        CreditWidget(
            name: "Björn Hansen",
            role: FlutterI18n.translate(
                context, 'settings.credits_page.role.qa_usability')),
        CreditWidget(
            name: "Chris Cron",
            role: FlutterI18n.translate(
                context, 'settings.credits_page.role.design_frontend')),
        CreditWidget(
            name: "Christian Epping",
            role: FlutterI18n.translate(
                context, 'settings.credits_page.role.dev_frontend')),
        CreditWidget(
            name: "Daniél Kerkmann",
            role: FlutterI18n.translate(
                context, 'settings.credits_page.role.dev_backend')),
        CreditWidget(
            name: "Dustin Becker-von Buch",
            role: FlutterI18n.translate(
                context, 'settings.credits_page.role.dev_frontend')),
        CreditWidget(
            name: "Fabian Rudzinski",
            role: FlutterI18n.translate(context,
                'settings.credits_page.role.creative_director_teamleader')),
        CreditWidget(
            name: "Fabio Cristiano",
            role: FlutterI18n.translate(
                context, 'settings.credits_page.role.dev_backend')),
        CreditWidget(
          name: "Felicitas Neyer",
          role: FlutterI18n.translate(context,
              'settings.credits_page.role.pr_marketing_community_management'),
        ),
        CreditWidget(
            name: "Geronimo Nassois",
            role: FlutterI18n.translate(
                context, 'settings.credits_page.role.dev_backend')),
        // CreditWidget(
        //     name: "Gisa von Marcard",
        //     role: FlutterI18n.translate(
        //         context, 'settings.credits_page.role.lead_project')),
        CreditWidget(
            name: "Heinrich Marks",
            role: FlutterI18n.translate(
                context, 'settings.credits_page.role.design_frontend')),
        CreditWidget(
            name: "Johanna Becken",
            role: FlutterI18n.translate(
                context, 'settings.credits_page.role.events')),
        CreditWidget(
            name: "Jonas Gonka",
            role: FlutterI18n.translate(
                context, 'settings.credits_page.role.dev_backend')),
        CreditWidget(
            name: "Justin Sauer",
            role: FlutterI18n.translate(
                context, 'settings.credits_page.role.dev_frontend')),
        CreditWidget(
            name: "Laura Gottschalk",
            role: FlutterI18n.translate(
                context, 'settings.credits_page.role.events_pr')),
        CreditWidget(
            name: "Laura Weber",
            role: FlutterI18n.translate(
                context, 'settings.credits_page.role.design_print')),
        CreditWidget(
            name: "Leon Kiesel",
            role: FlutterI18n.translate(
                context, 'settings.credits_page.role.dev_backend_lead_qa')),
        CreditWidget(
            name: "Luca Schneider",
            role: FlutterI18n.translate(
                context, 'settings.credits_page.role.qa_usability')),
        CreditWidget(
            name: "Lukas Gholam",
            role: FlutterI18n.translate(context,
                'settings.credits_page.role.assistant_producer_copywriter')),
        CreditWidget(
            name: "Lukas Walter",
            role: FlutterI18n.translate(context,
                'settings.credits_page.role.assistant_producer_scrummaster')),
        CreditWidget(
            name: "Maraike Rosanski",
            role: FlutterI18n.translate(
                context, 'settings.credits_page.role.design_frontend')),
        CreditWidget(
            name: "Marcel Mehlmann",
            role: FlutterI18n.translate(
                context, 'settings.credits_page.role.td_admin_devops')),
        CreditWidget(
            name: "Maria Lorenz",
            role: FlutterI18n.translate(
                context, 'settings.credits_page.role.art_animation')),
        EggWidget(
          creditWidget: CreditWidget(
              name: "Marwin Lebensky",
              role: FlutterI18n.translate(
                  context, 'settings.credits_page.role.lead_design')),
        ),
        CreditWidget(
            name: "Mateusz Czajkowski",
            role: FlutterI18n.translate(
                context, 'settings.credits_page.role.dev_frontend')),
        CreditWidget(
            name: "Maurice Blattmann",
            role: FlutterI18n.translate(
                context, 'settings.credits_page.role.dev_frontend')),
        CreditWidget(
            name: "Maurice Kontz",
            role: FlutterI18n.translate(
                context, 'settings.credits_page.role.assistant_producer_pr')),
        CreditWidget(
            name: "Pia Georgiew",
            role: FlutterI18n.translate(
                context, 'settings.credits_page.role.dev_frontend')),
        CreditWidget(
            name: "Robert Palm",
            role: FlutterI18n.translate(
                context, 'settings.credits_page.role.lead_programming')),
        CreditWidget(
            name: "Robert Zedlitz",
            role: FlutterI18n.translate(
                context, 'settings.credits_page.role.pr')),
        CreditWidget(
            name: "Steffen Bock",
            role: FlutterI18n.translate(
                context, 'settings.credits_page.role.dev_backend')),
        CreditWidget(
            name: "Tymoteusz Mucha",
            role: FlutterI18n.translate(
                context, 'settings.credits_page.role.dev_frontend')),
        CreditWidget(
            name: "Wenke Ingwersen",
            role: FlutterI18n.translate(
                context, 'settings.credits_page.role.events')),
      ];
}

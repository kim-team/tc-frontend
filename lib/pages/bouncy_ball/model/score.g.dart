// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'score.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Score _$ScoreFromJson(Map json) => Score(
      name: json['name'] as String? ?? "",
      score: json['score'] as int? ?? -1,
      isSelf: json['isSelf'] as bool? ?? false,
    );

Map<String, dynamic> _$ScoreToJson(Score instance) => <String, dynamic>{
      'name': instance.name,
      'score': instance.score,
      'isSelf': instance.isSelf,
    };

/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../../common/constants/routing_constants.dart';
import '../../../common/tc_theme.dart';
import '../bloc/entries/timeline_bloc.dart';
import '../model/timeline_entry.dart';
import 'common.dart';
import 'page/timeline_error_reload_button.dart';
import 'page/timeline_page_fave_banner.dart';
import 'page/timeline_slide_widget.dart';

part 'page/timeline_page_bottom_loader.dart';
part 'page/timeline_page_list_builder.dart';
part 'page/timeline_page_tl_image.dart';

/// Main page for Timeline
class TimelinePage extends StatelessWidget {
  /// Creates the main view of the timeline
  TimelinePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var _timelineBloc = BlocProvider.of<TimelineBloc>(context);
    return Scaffold(
      appBar: AppBar(
        title: I18nText('timeline.title'),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.more_vert,
              color: Theme.of(context).iconTheme.color,
            ),
            onPressed: () => Navigator.of(context).pushNamed(
              timelineSettingsRoute,
              arguments: (showFaves) {
                _timelineBloc
                    .add(showFaves ? TEventFetchFaved() : TEventFetchAll());
                return true;
              },
            ),
          ),
        ],
      ),
      backgroundColor: Theme.of(context).backgroundColor,
      body: RefreshIndicator(
        color: Theme.of(context).textTheme.bodyText1?.color,
        backgroundColor: Theme.of(context).primaryColor,
        //key: Key('timelineRefreshIndicator'),
        onRefresh: () => Future<void>.sync(
          () => _timelineBloc.add(
            (_timelineBloc.state as TStateLoaded).favoredOnly
                ? TEventFetchFaved()
                : TEventReset(),
          ),
        ),

        child: BlocBuilder<TimelineBloc, TState>(
          //key: Key('timelineBlocBuilder'),
          bloc: _timelineBloc,

          builder: (context, state) {
            if (state is TStateUninitialized) {
              _timelineBloc.add(TEventFetchAll());
              //_showFavoriteEvent(_timelineBloc);
              return Center(
                child: CircularProgressIndicator(
                  backgroundColor: CorporateColors.tinyCampusDarkBlue,
                  valueColor: AlwaysStoppedAnimation<Color>(
                    CorporateColors.tinyCampusOrange,
                  ),
                ),
              );
            }
            if (state is TStateError) {
              return Center(
                child: TLErrorReloadButton(timelineBloc: _timelineBloc),
              );
            }
            if (state is TStateLoaded) {
              return Stack(
                children: [
                  _TLListBuilder(
                    //key: Key('timelineStateBuilder'),
                    state: state,
                    blocCallback: (event) => _timelineBloc.add(event),
                  ),
                  if (state.favoredOnly)
                    Positioned(
                        top: 0, left: 0, right: 0, child: TLFaveBanner()),
                ],
              );
            }
            return Center(child: Text(state.toString()));
          },
        ),
      ),
    );
  }
}

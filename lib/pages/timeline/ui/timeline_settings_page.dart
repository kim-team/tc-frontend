/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../../common/tc_theme.dart';
import '../../../repositories/http/http_repository.dart';
import '../bloc/entries/timeline_bloc.dart';
import '../bloc/sources/sources_bloc.dart';
import '../helper/http_helper.dart';
import '../model/timeline_source.dart';

part 'settings/timeline_settings_entry_widget.dart';

typedef FaveCallback = bool Function(dynamic showFaves);

/// Widget for the Timeline settings page
class TimelineSettingsPage extends StatefulWidget {
  final FaveCallback callback;

  /// Creates a new timeline settings page
  TimelineSettingsPage({Key? key, required this.callback}) : super(key: key);

  @override
  _TimelineSettingsPageState createState() => _TimelineSettingsPageState();
}

class _TimelineSettingsPageState extends State<TimelineSettingsPage> {
  late SourceBloc _sourceBloc;
  late TimelineBloc _timelineBloc;

  bool restored = false;
  bool tryToRestore = false;
  bool showFaves = false;

  @override
  void initState() {
    super.initState();
    _timelineBloc = BlocProvider.of<TimelineBloc>(context);

    showFaves = _timelineBloc.state.favoredOnly;

    _sourceBloc = SourceBloc(
      bhh: BlocHttpHelper(
        httpRepo: RepositoryProvider.of<HttpRepository>(context),
      ),
    );
    _sourceBloc.add(SourceEventGetSubscriptions());
  }

  @override
  Widget build(BuildContext context) => WillPopScope(
        onWillPop: () => Future<bool>(() => widget.callback(showFaves)),
        child: Scaffold(
          backgroundColor: Theme.of(context).backgroundColor,
          appBar: AppBar(
            title: I18nText('timeline.settings.title'),
          ),
          body: BlocConsumer<SourceBloc, SourceState>(
            bloc: _sourceBloc,
            buildWhen: (previous, current) =>
                current is SourceStateListResponse,
            listener: (context, state) {
              if (state is SourceStateHiddenEntriesRestored) {
                setState(() {
                  restored = true;
                });
              }
            },
            builder: (context, state) {
              if (state is SourceStateUninitialized) {
                return Center(
                  child: CircularProgressIndicator(
                    backgroundColor: CorporateColors.tinyCampusDarkBlue,
                    valueColor: AlwaysStoppedAnimation<Color>(
                        CorporateColors.tinyCampusOrange),
                  ),
                );
              }
              if (state is SourceStateError) {
                return Center(
                  child: I18nText('timeline.errors.failed_fetch_sources'),
                );
              }
              return ListView.builder(
                physics: BouncingScrollPhysics(),
                itemBuilder: (context, index) {
                  if (index == 0) {
                    return Material(
                      color: Theme.of(context).primaryColor,
                      child: Container(
                        padding: EdgeInsets.symmetric(vertical: 12.0),
                        margin: EdgeInsets.only(top: 8.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 16.0, vertical: 8.0),
                              child: I18nText(
                                'timeline.settings.general',
                                child: Text(
                                  '',
                                  style: Theme.of(context).textTheme.headline2,
                                ),
                              ),
                            ),
                            SwitchListTile(
                              title: I18nText('timeline.settings.only_faves'),
                              value: showFaves,
                              onChanged: (value) {
                                // Used to reload page state.
                                // DO NOT DELETE!
                                setState(() => showFaves = value);
                              },
                            ),
                            ListTile(
                              title: TweenAnimationBuilder<Color?>(
                                duration: Duration(milliseconds: 260),
                                tween: ColorTween(
                                  begin: CurrentTheme().textSoftLightBlue,
                                  end: restored
                                      ? Theme.of(context).iconTheme.color
                                      : CurrentTheme().textSoftLightBlue,
                                ),
                                builder: (context, colorValue, child) => Text(
                                  FlutterI18n.translate(
                                    context,
                                    restored
                                        ? 'timeline.settings'
                                            '.restored_hidden_entries'
                                        : 'timeline.settings'
                                            '.restore_hidden_entries',
                                  ),
                                  style: TextStyle(color: colorValue),
                                ),
                              ),
                              trailing: TweenAnimationBuilder<double>(
                                duration: Duration(milliseconds: 900),
                                curve: Curves.easeInOut,
                                tween: Tween<double>(
                                    begin: 0.0, end: tryToRestore ? 2.0 : 0.0),
                                builder: (context, value, child) => Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Transform.rotate(
                                    angle: value * -3.14159,
                                    child: Icon(
                                      restored
                                          ? Icons.check_circle
                                          : Icons.settings_backup_restore,
                                      color: restored
                                          ? CorporateColors.cafeteriaVeganGreen
                                          : CorporateColors.tinyCampusBlue,
                                      size: 24,
                                    ),
                                  ),
                                ),
                              ),
                              onTap: !restored
                                  ? () {
                                      setState(() => tryToRestore = true);
                                      _sourceBloc.add(
                                          SourceEventRestoreHiddenEntries());
                                    }
                                  : null,
                            ),
                            if (kDebugMode)
                              Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 16.0, vertical: 8.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    I18nText('timeline.settings.subscriptions',
                                        child: Text('',
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 16.0))),
                                    I18nText(
                                      'timeline.settings.subscriptions_error',
                                      child: Text('',
                                          softWrap: true,
                                          style: TextStyle(fontSize: 16.0)),
                                    ),
                                  ],
                                ),
                              ),
                          ],
                        ),
                      ),
                    );
                  }
                  return kDebugMode
                      ? Material(
                          color: Theme.of(context).primaryColor,
                          child: Container(
                            padding: EdgeInsets.only(bottom: 8.0),
                            child: BlocProvider<SourceBloc>(
                              create: (context) => _sourceBloc,
                              child: _SourceEntryWidget(
                                  source: (state as SourceStateListResponse)
                                      .sourceList[index - 1]),
                            ),
                          ),
                        )
                      : Container();
                },
                itemCount:
                    (state as SourceStateListResponse).sourceList.length + 1,
              );
            },
          ),
        ),
      );
}

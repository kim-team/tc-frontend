/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

part of 'timeline_slide_widget.dart';

/// Widget for a single timeline entry
class TLEntryWidget extends StatelessWidget {
  /// Entry to be shown
  final TLEntry tlEntry;

  /// Callback for toggling the visibility of the widget
  final BlocCallback blocCallback;

  /// Widget constructor
  const TLEntryWidget({
    Key? key,
    required this.tlEntry,
    required this.blocCallback,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => ReportedBlurWrapper(
        shouldBlur: tlEntry.isReported,
        child: Container(
          color: Theme.of(context).backgroundColor,
          child: Container(
            color: Theme.of(context).primaryColor,
            child: Padding(
              padding: EdgeInsets.only(
                  left: 16.0, right: 8.0, top: 6.0, bottom: 6.0),
              child: Column(
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      if (tlEntry.routeExists && tlEntry.route != '/home') {
                        Navigator.pushNamed(
                          context,
                          tlEntry.route,
                          arguments: int.tryParse(tlEntry.routeArgs),
                        );
                      }
                    },
                    child: Column(
                      children: [
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: ListTile(
                                trailing: __TLEntryIconButton(
                                  tlEntry: tlEntry,
                                  blocCallback: blocCallback,
                                ),
                                dense: true,
                                contentPadding: EdgeInsets.all(0.0),
                                leading: CircleAvatar(
                                  backgroundColor: Colors.grey[100],
                                  child: Container(
                                    decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      image: DecorationImage(
                                        fit: BoxFit.cover,
                                        image: tlEntry.avatar,
                                      ),
                                    ),
                                  ),
                                ),
                                title: I18nText(tlEntry.author,
                                    child: Text(
                                      tlEntry.author,
                                      style: TextStyle(
                                          fontWeight: FontWeight.normal,
                                          fontSize: 16),
                                    )),
                                subtitle: I18nText(tlEntry.parentSource,
                                    child: Text(
                                      tlEntry.parentSource,
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodyText2
                                          ?.copyWith(
                                              color: CorporateColors
                                                  .tinyCampusIconGrey),
                                    )),
                              ),
                            ),
                          ],
                        ),
                        Container(
                          padding: EdgeInsets.all(0.0),
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                child: TCLinkText(
                                  text: tlEntry.text.trim(),
                                  maxLines: 6,
                                  style: TextStyle(
                                      fontSize: 16.0,
                                      fontWeight: FontWeight.w400),
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: Padding(
                          padding: EdgeInsets.only(top: 2.0, bottom: 2.0),
                          child: Text(
                            timeago.format(
                              tlEntry.date,
                              locale: FlutterI18n.currentLocale(context)
                                      ?.languageCode ??
                                  'de',
                            ),
                            style: TextStyle(fontSize: 14, color: Colors.grey),
                          ),
                        ),
                      ),
                      Container(height: 36),
                      //TODO: When timeline-hearts are ready, use this again
                      // tlEntry.heart != null
                      //     ? LegacyHeartWidget(heartData: tlEntry.heart)
                      //     : Row()
                    ],
                    // ),
                  ),
                ],
              ),
            ),
          ),
        ),
      );
}

class __TLEntryIconButton extends StatefulWidget {
  final TLEntry tlEntry;
  final BlocCallback blocCallback;

  const __TLEntryIconButton({
    Key? key,
    required this.tlEntry,
    required this.blocCallback,
  }) : super(key: key);

  @override
  __TLEntryIconButtonState createState() => __TLEntryIconButtonState();
}

class __TLEntryIconButtonState extends State<__TLEntryIconButton> {
  @override
  Widget build(BuildContext context) => widget.tlEntry.pinned
      ? IconButton(
          padding: EdgeInsets.symmetric(vertical: 8, horizontal: 4),
          key: Key('PinBtn${widget.tlEntry.id}'),
          iconSize: 20.0,
          icon: Icon(
            TinyCampusIcons.pin,
            color: CorporateColors.tinyCampusOrange,
          ),
          onPressed: () {
            //_tlBloc.add(TPinToggle(entry: widget.tlEntry));
            setState(() {
              //widget.tlEntry = widget.tlEntry.copyWith(pinned: false);
              widget.blocCallback(TPinToggle(entry: widget.tlEntry));
            });
          })
      : SizedBox.shrink();
}

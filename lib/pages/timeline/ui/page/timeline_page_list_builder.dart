/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

part of '../timeline_page.dart';

class _TLListBuilder extends StatelessWidget {
  static const _duration = Duration(milliseconds: 160);

  final TStateLoaded state;
  final BlocCallback blocCallback;

  _TLListBuilder({
    Key? key,
    required this.state,
    required this.blocCallback,
  }) : super(key: key);

  List<TLEntry> get _entries => state.entries.data;

  List<TLEntry> get _datedEntries => state.datedEntries.data;

  List<TLEntry> get _pinnedEntries => state.pinnedEntries.data;

  List<TLEntry> get _recentEntries => state.recentEntries.data;

  @override
  Widget build(BuildContext context) {
    var timelineBloc = BlocProvider.of<TimelineBloc>(context);
    var list = <Widget>[];
    if (state.isEmpty) {
      return Center(
        child: TLErrorReloadButton(timelineBloc: timelineBloc, isEmpty: true),
      );
    }

    if (state.favoredOnly) {
      // Pseudo padding
      list.add(
        Container(
          height: 48.0,
        ),
      );
      list.addAll(_entries
          .map<Widget>(
            (e) => SlideWrapper(
              key: ValueKey(e.id),
              entry: e,
              listUpdateCallback: blocCallback,
            ),
          )
          .toList());

      list.add(_TLMessageWrapper('timeline.entries.end'));

      return ListView.builder(
        physics: BouncingScrollPhysics(
          parent: AlwaysScrollableScrollPhysics(),
        ),
        addAutomaticKeepAlives: false,
        itemCount: list.length,
        itemBuilder: (context, index) => list[index],
      );
    }

    list = _generateListWithDelimiters(state);

    var scrollController = ScrollController();
    var lastPosition = 0.0;
    void scrollListener() {
      if (scrollController.offset >=
              scrollController.position.maxScrollExtent &&
          !scrollController.position.outOfRange &&
          !state.hasReachedMax &&
          scrollController.offset != lastPosition) {
        timelineBloc.add(TEventFetchAll());
        lastPosition = scrollController.offset;
      }
    }

    scrollController.addListener(scrollListener);

    return ListView.builder(
      physics: BouncingScrollPhysics(
        parent: AlwaysScrollableScrollPhysics(),
      ),
      controller: scrollController,
      itemCount: list.length + 1,
      itemBuilder: (context, index) {
        if (index > list.length) return Container();
        if (index == list.length) {
          if (state.hasReachedMax) {
            return _TLMessageWrapper('timeline.entries.end');
          } else {
            return _BottomLoader();
          }
        }
        return list[index];
      },
    );
  }

  List<Widget> _generateListWithDelimiters(TStateLoaded state) {
    var widgets = <Widget>[];

    if (_pinnedEntries.isNotEmpty) {
      widgets.add(
        BlocBuilder<TimelineBloc, TState>(
          builder: (context, state) {
            if (state is TStateLoaded) {
              return buildWrapper(
                key: Key('pinned_delimiter'),
                image: _pinnedImage,
                isVisible: state.pinnedEntries.length > 0,
                child: I18nPlural(
                  'timeline.pinned.times',
                  state.pinnedEntries.length,
                  child: Text(
                    '',
                    style: TextStyle(color: CorporateColors.tinyCampusOrange),
                  ),
                ),
              );
            } else {
              return Container();
            }
          },
        ),
      );

      widgets.addAll(
        _pinnedEntries.map(
          (e) => SlideWrapper(
            key: ValueKey(e.id),
            listUpdateCallback: blocCallback,
            entry: e,
          ),
        ),
      );
    }
    if (_recentEntries.isNotEmpty) {
      widgets.add(
        buildWrapper(
          key: Key('recent_delimiter'),
          image: _recentImage,
          isVisible: _recentEntries.isNotEmpty,
          child: I18nText(
            'timeline.entries.new',
            child: Text(
              '',
              style: TextStyle(
                color: CorporateColors.tinyCampusOrange,
              ),
            ),
          ),
        ),
      );

      widgets.addAll(
        _recentEntries.map(
          (e) => SlideWrapper(
            key: ValueKey(e.id),
            entry: e,
            listUpdateCallback: blocCallback,
          ),
        ),
      );
    }
    if (_datedEntries.isNotEmpty) {
      widgets.add(
        buildWrapper(
          key: Key('dated_delimiter'),
          image: _datedImage,
          isVisible: _datedEntries.isNotEmpty,
          child: I18nText(
            'timeline.entries.read',
            child: Text(
              'Gelesene Einträge',
              style: TextStyle(
                color: CorporateColors.tinyCampusIconGrey,
              ),
            ),
          ),
        ),
      );
      widgets.addAll(
        _datedEntries.map<Widget>(
          (e) => SlideWrapper(
            key: ValueKey(e.id),
            entry: e,
            listUpdateCallback: blocCallback,
          ),
        ),
      );
    }

    return widgets;
  }

  Widget buildWrapper({
    required Image image,
    required bool isVisible,
    required Widget child,
    required Key key,
  }) =>
      AnimatedCrossFade(
        key: key,
        sizeCurve: Curves.easeOutCubic,
        duration: _duration,
        crossFadeState:
            isVisible ? CrossFadeState.showFirst : CrossFadeState.showSecond,
        firstChild: Container(
          margin: EdgeInsets.all(20.0),
          child: Column(
            children: [
              Center(
                child: Padding(
                    padding: const EdgeInsets.all(
                      4.0,
                    ),
                    child: image),
              ),
              child,
            ],
          ),
        ),
        secondChild: Container(),
      );
}

class _TLMessageWrapper extends StatelessWidget {
  final String text;

  const _TLMessageWrapper(this.text, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Center(
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 16.0),
          child: I18nText(text),
        ),
      );
}

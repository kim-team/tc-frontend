/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../../../common/tc_theme.dart';
import '../../bloc/entries/timeline_bloc.dart';

class TLErrorReloadButton extends StatelessWidget {
  final bool isEmpty;
  final TimelineBloc timelineBloc;

  const TLErrorReloadButton({
    Key? key,
    required this.timelineBloc,
    this.isEmpty = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => Material(
        color: Colors.white,
        elevation: 4,
        borderRadius: BorderRadius.all(Radius.circular(12)),
        child: InkWell(
          onTap: () => timelineBloc.add(
            TEventReset(),
          ),
          borderRadius: BorderRadius.all(Radius.circular(12)),
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 16, horizontal: 24),
            width: 210,
            height: 160,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  FlutterI18n.translate(
                      context,
                      isEmpty
                          ? 'timeline.entries.no_entries_provided'
                          : 'timeline.errors.failed_fetch_entries'),
                  textAlign: TextAlign.center,
                  style: CurrentTheme().themeData.textTheme.bodyText1,
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: Text(
                    FlutterI18n.translate(context, 'common.actions.reload')
                        .toUpperCase(),
                    textAlign: TextAlign.center,
                    style: CurrentTheme().themeData.textTheme.headline3,
                  ),
                ),
              ],
            ),
          ),
        ),
      );
}

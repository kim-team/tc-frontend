/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:convert';

import '../../../common/tc_utility_functions.dart';
import '../../../repositories/http/http_repository.dart';
import '../model/timeline_entry.dart';
import '../model/timeline_source.dart';

class BlocHttpHelper {
  final HttpRepository _httpRepo;

  BlocHttpHelper({required HttpRepository httpRepo}) : _httpRepo = httpRepo;

  Future<TLEntryList> fetchPinnedEntries({required String request}) async =>
      TLEntryList.pinned(
        data: buildEntitiesFromList(
          jsonDecode(await _httpRepo.read(request)),
          TLEntry.fromJson,
        ),
      );

  Future<TLEntryList> fetchEntries({required String request}) async {
    final data = Map<String, dynamic>.from(
      jsonDecode(await _httpRepo.read(request)) ?? {},
    );
    return TLEntryList(
      data: buildEntitiesFromList(
        data["content"],
        TLEntry.fromJson,
      ),
      last: data['last'] ?? false,
    );
  }

  Future<bool> sendPutRequest({required String request}) async {
    await _httpRepo.put(request);
    return true;
  }

  Future<List<TLSource>> getSources({required String request}) async =>
      buildEntitiesFromList(
        jsonDecode(await _httpRepo.read(request)),
        TLSource.fromJson,
      );

  Future<bool> sendDeleteRequest({required String request}) async {
    await _httpRepo.delete(request);
    return true;
  }

  Future<bool> sendPostRequest({required String request}) async {
    await _httpRepo.post(request);
    return true;
  }
}

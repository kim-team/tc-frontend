/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:rxdart/rxdart.dart';

import '../../helper/helper.dart';
import '../../model/timeline_source.dart';

part 'sources_event.dart';
part 'sources_state.dart';

/// A Bloc that handles the server communication concerning [TLSource]
class SourceBloc extends Bloc<SourceEvent, SourceState> {
  /// Wrapper for [HttpRepository]. Contains a few qol-methods
  /// to parse the server response.
  final BlocHttpHelper bhh;

  /// [SourceBloc] requires [bhh], which is a wrapper for the [HttpRepository]
  SourceBloc({required this.bhh}) : super(SourceStateUninitialized());

  @override
  Stream<Transition<SourceEvent, SourceState>> transformEvents(
    Stream<SourceEvent> events,
    Stream<Transition<SourceEvent, SourceState>> Function(SourceEvent event)
        transitionFn,
  ) =>
      super.transformEvents(
        events.debounce(
          (event) => Stream.periodic(Duration(milliseconds: 500)),
        ),
        transitionFn,
      );

  @override
  Stream<SourceState> mapEventToState(SourceEvent event) async* {
    if (event is SourceEventGetSubscriptions) {
      try {
        final subscribedSources = await bhh.getSources(
          request: getTLSources(),
        );
        yield SourceStateListResponse(sourceList: subscribedSources);
      } on Exception catch (e) {
        yield SourceStateError(error: e.toString());
      }
    }
    if (event is SourceEventSubscribe) {
      try {
        var bufferSource = event.source;
        final serverAnswer = await bhh.sendPutRequest(
          request: toggleTLSourceSubscription(
            sourceID: bufferSource.id,
            subscriptionType: 'SUBSCRIBED',
          ),
        );
        if (serverAnswer) {
          bufferSource = bufferSource.copyWith(subscribed: true);
        }
        yield serverAnswer
            ? SourceStateSubscribed(source: bufferSource)
            : SourceStateError(error: 'Internal server error?');
      } on Exception catch (e) {
        yield SourceStateError(error: e.toString());
      }
    }
    if (event is SourceEventUnsubscribe) {
      try {
        var bufferSource = event.source;
        final serverAnswer = await bhh.sendPutRequest(
          request: toggleTLSourceSubscription(
            sourceID: bufferSource.id,
            subscriptionType: 'UNSUBSCRIBED',
          ),
        );
        if (serverAnswer) {
          bufferSource = bufferSource.copyWith(subscribed: false);
        }
        yield serverAnswer
            ? SourceStateUnsubscribed(source: bufferSource)
            : SourceStateError(error: 'Internal server error?');
      } on Exception catch (e) {
        yield SourceStateError(error: e.toString());
      }
    }
    if (event is SourceEventRestoreHiddenEntries) {
      try {
        var response =
            await bhh.sendDeleteRequest(request: deleteAllHiddenStates());
        if (response == false) {
          yield SourceStateError(
            error: 'connection timeout or bad server response',
          );
        }
        yield SourceStateHiddenEntriesRestored();
      } on Exception catch (e) {
        yield SourceStateError(error: e.toString());
      }
    }
  }
}

/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

part of 'sources_bloc.dart';

/// Abstract class for SourceStates that serves as
/// an archetype for other SourceStates.
@Immutable()
abstract class SourceState extends Equatable {
  @override
  List<Object> get props => [];

  @override
  bool get stringify => true;
}

/// Marks that [SourceBloc] is uninitialized.
class SourceStateUninitialized extends SourceState {}

/// Indicates that the user has subscribed to a certain source.
class SourceStateSubscribed extends SourceState {
  /// Target source that the user is subscribed to.
  final TLSource source;

  /// [SourceStateSubscribed] constructor
  SourceStateSubscribed({required this.source});

  @override
  List<Object> get props => [source];
}

/// Indicates that the user has unsubscribed from a certain source.
class SourceStateUnsubscribed extends SourceState {
  /// Target source that the user is unsubscribed from.
  final TLSource source;

  /// [SourceStateUnsubscribed] constructor
  SourceStateUnsubscribed({required this.source});

  @override
  List<Object> get props => [source];
}

/// Contains the list of sources that the user is subscribed to.
class SourceStateListResponse extends SourceState {
  /// List of subscriptions.
  final List<TLSource> sourceList;

  /// [SourceStateListResponse] constructor.
  /// Requires a [sourceList]
  SourceStateListResponse({required this.sourceList});

  @override
  List<Object> get props => [sourceList];
}

/// Class for catching errors that may have occurred during
/// server communication or data manipulation.
class SourceStateError extends SourceState {
  /// Error message or exception thrown during runtime
  final String error;

  SourceStateError({required this.error});

  @override
  List<Object> get props => [error];
}

/// All entries hidden by the user are now visible again
class SourceStateHiddenEntriesRestored extends SourceState {}

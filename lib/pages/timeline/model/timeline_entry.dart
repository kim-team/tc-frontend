/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:copy_with_extension/copy_with_extension.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:meta/meta.dart';

import '../../../common/assets_adapter.dart';

part 'entry_list.dart';
part 'timeline_entry.g.dart';

///Klasse für Timeline-Einträge
@CopyWith()
@Immutable()
@JsonSerializable(anyMap: true, explicitToJson: true)
class TLEntry extends Equatable {
  ///Einzigartige ID des Eintrags
  final int id;

  ///Autor/Source des Eintrags
  final String author;

  ///Übergeordnete Source des Eintrags
  final String parentSource;

  ///Bild für das Kartenobjekt des Eintrags
  @JsonKey(fromJson: _avatarFromJson, toJson: _avatarToJson)
  final AssetImage avatar;

  ///Erstellungsdatum des Eintrags
  final DateTime date;

  ///Navigationsroute des Eintrags
  final String route;

  /// Routen-Argumente
  final String routeArgs;

  ///Inhalt des Eintrags
  final String text;

  ///Speichert, ob der Nutzer den Eintrag favorisiert hat
  @JsonKey(name: 'faved')
  final bool favored;

  ///Speichert, ob der Nutzer den Eintrag gepinnt hat
  final bool pinned;

  ///Whether the [TLEntry] is reported by the [User] or not
  @JsonKey(name: 'reported')
  final bool isReported;

  ///Konstruktor für einen Timeline-Eintrag
  TLEntry({
    required this.id,
    required this.author,
    required this.parentSource,
    required this.date,
    this.text = 'ERROR: No Content',
    required this.avatar,
    required this.route,
    required this.routeArgs,
    this.favored = false,
    this.pinned = false,
    this.isReported = false,
  });

  bool get isEmpty => id < 0;

  bool get routeExists => route.isNotEmpty;

  ///Factory um ein [TLEntry]-Objekt aus JSON zu erzeugen
  factory TLEntry.fromJson(Map<String, dynamic> json) =>
      _$TLEntryFromJson(json);

  /// Methode um aus einem [TLEntry] wieder ein Json zu erzeugen
  Map<String, dynamic> toJson() => _$TLEntryToJson(this);

  @override
  bool get stringify => true;

  @override
  List<Object> get props => [id, isReported];

  static AssetImage _avatarFromJson(String path) => path.isEmpty
      ? AssetImage(AssetAdapter.tinyCampusLogoWithPadding())
      : AssetImage('assets/images/$path');

  static String _avatarToJson(AssetImage image) =>
      image.assetName.substring(14);
}

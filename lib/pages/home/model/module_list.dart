/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/foundation.dart';

import '../../../common/assets_adapter.dart';
import '../../../common/constants/routing_constants.dart';
import 'module.dart';

// TODO: Find a way to import these from different
//  packages to really be modular.

// TODO: Find a way to update this list on version changes!

// WARNING: ID MAY NOT BE 0!!!
// OTHERWISE THE SERVER CAN'T REPRESENT THE DISABLED STATE.
// TODO: Find a better way to enforce this ^!

/// A [List] of [Module]s available in the app.
///
/// Used as default configuration for `ModuleBlocState`.
final List<Module> moduleList = [
  Module(
    id: 1,
    name: 'modules.q_and_a.title',
    description: 'modules.q_and_a.description',
    pathToImage: AssetAdapter.module(ModuleModel.qAndA),
    onTapPath: qaModuleRoute,
  ),
  Module(
    id: 5,
    name: 'modules.cafeteria.title',
    description: 'modules.cafeteria.description',
    pathToImage: AssetAdapter.module(ModuleModel.cafeteria),
    onTapPath: cafeteriaModuleRoute,
  ),
  Module(
    id: 4,
    name: 'modules.organizer.title',
    description: 'modules.organizer.description',
    pathToImage: AssetAdapter.module(ModuleModel.organizer),
    onTapPath: orgaModuleRoute,
  ),
  Module(
    id: 8,
    name: 'modules.semester_fee.title',
    description: 'modules.semester_fee.description',
    pathToImage: AssetAdapter.module(ModuleModel.semesterFee),
    onTapPath: semesterFeeRoute,
  ),
  Module(
    id: 6,
    name: 'modules.coming_soon.title',
    description: 'modules.coming_soon.description',
    pathToImage: AssetAdapter.module(ModuleModel.comingNext),
    onTapPath: comingSoonRoute,
  ),

  // Below should be removed for beta
  Module(
    id: 2,
    name: 'modules.business_cards.title',
    description: 'modules.business_cards.description',
    pathToImage: AssetAdapter.module(ModuleModel.businessCard),
    onTapPath: vCardModuleRoute,
  ),
  // Removed ThinkBig module used to have ID 3
  Module(
    id: 7,
    name: 'modules.bulletin_board.title',
    description: 'modules.bulletin_board.description',
    pathToImage: AssetAdapter.module(ModuleModel.comingNext),
    onTapPath: bulletinBoardRoute,
  ),
  Module(
    id: 9,
    name: 'modules.events.title',
    description: 'modules.events.description',
    pathToImage: AssetAdapter.module(ModuleModel.comingNext),
    onTapPath: eventsRoute,
  ),
  Module(
    id: 10,
    name: 'modules.feedback.title',
    description: 'modules.feedback.description',
    pathToImage: AssetAdapter.module(ModuleModel.comingNext),
    onTapPath: feedbackRoute,
  ),
  Module(
    id: 11,
    name: 'modules.important_links.title',
    description: 'modules.important_links.description',
    pathToImage: AssetAdapter.module(ModuleModel.importantLinks),
    onTapPath: importantLinksRoute,
  ),
  Module(
    id: 12,
    name: 'modules.zs.title',
    description: 'modules.zs.description',
    pathToImage: AssetAdapter.module(ModuleModel.zs),
    onTapPath: zsRoute,
  ),
  Module(
    id: 13,
    name: 'modules.practice_phase.title',
    description: 'modules.practice_phase.description',
    pathToImage: AssetAdapter.module(ModuleModel.practicePhase),
    onTapPath: practicePhaseRoute,
  ),
  Module(
    id: 14,
    name: 'modules.sport.title',
    description: 'modules.sport.description',
    pathToImage: AssetAdapter.module(ModuleModel.sport),
    onTapPath: sportRoute,
  ),
  Module(
    id: 15,
    name: 'modules.bouncy_ball.title',
    description: 'modules.bouncy_ball.description',
    pathToImage: AssetAdapter.module(ModuleModel.bouncyBall),
    onTapPath: offlineGame,
  ),
]..retainWhere(
    (e) => kDebugMode || [1, 5, 4, 8, 11, 12, 13, 14, 15].contains(e.id));

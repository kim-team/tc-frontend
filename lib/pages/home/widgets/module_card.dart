/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import '../../../common/constants/routing_constants.dart';
import '../../../common/tc_theme.dart';
import '../../../modules/cafeteria/bloc/cafeteria_bloc.dart';
import '../../../modules/cafeteria/bloc/cafeteria_settings/cafeteria_settings_bloc.dart';
import '../model/module.dart';

/// A [Card] used in `HomePage`.
class ModuleCard extends StatelessWidget {
  /// [Module] to be displayed.
  final Module module;

  const ModuleCard({
    Key? key,
    required this.module,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => FractionallySizedBox(
        widthFactor: .5,
        child: AspectRatio(
          aspectRatio: 1,
          child: Card(
            margin: EdgeInsets.all(7.5),
            clipBehavior: Clip.antiAlias,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(7.5),
            ),
            elevation: 4,
            child: InkWell(
              onTap: module.onTapPath == cafeteriaModuleRoute
                  ? () {
                      BlocProvider.of<CafeteriaSettingsBloc>(context).add(
                        ModulePressedEvent(
                          cafeteriaNames: [
                            for (var cafeteria
                                in BlocProvider.of<CafeteriaBloc>(context)
                                    .state
                                    .cafeteriaList)
                              cafeteria.building.name,
                          ],
                        ),
                      );
                      BlocProvider.of<CafeteriaBloc>(context)
                          .add(LoadingEvent(loadTest: false));
                      Navigator.pushNamed(
                        context,
                        module.onTapPath,
                        arguments: module,
                      );
                    }
                  : () => Navigator.pushNamed(
                        context,
                        module.onTapPath,
                        arguments: module,
                      ),
              child: Column(
                children: <Widget>[
                  Expanded(
                    flex: 2,
                    child: Container(
                      height: 100,
                      constraints: BoxConstraints.expand(),
                      child: Image.asset(module.pathToImage, fit: BoxFit.cover),
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Container(
                      color: Theme.of(context).primaryColor,
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: Padding(
                              padding: EdgeInsets.symmetric(horizontal: 8.0),
                              child: I18nText(
                                module.name,
                                child: Text(
                                  '',
                                  textAlign: TextAlign.center,
                                  overflow: TextOverflow.fade,
                                  style: Theme.of(context)
                                      .textTheme
                                      .headline4
                                      ?.copyWith(
                                          color:
                                              CurrentTheme().textSoftLightBlue),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      );
}

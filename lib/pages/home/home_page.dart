/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:reorderables/reorderables.dart';

import '../../common/constants/routing_constants.dart';
import '../../common/tc_theme.dart';
import 'bloc/home_bloc.dart';
import 'widgets/home_footer.dart';
import 'widgets/home_header.dart';
import 'widgets/module_card.dart';

/// A main page of the TC-App showing available Modules
/// and allowing those to be reordered.
///
/// Uses [HomeBloc] for business logic
/// and updates when [HomeState] changes.
class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
        backgroundColor: Theme.of(context).backgroundColor,
        appBar: AppBar(
          title: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Expanded(
                child: Image.asset(
                  CurrentTheme().homeScreenTCLogo,
                  alignment: Alignment.centerLeft,
                  fit: BoxFit.contain,
                  height: 32,
                ),
              ),
            ],
          ),
          automaticallyImplyLeading: kDebugMode,
          actions: [
            IconButton(
              icon: Icon(Icons.more_vert),
              color: CorporateColors.tinyCampusIconGrey,
              onPressed: () => Navigator.pushNamed(context, homeSettingsRoute),
            ),
          ],
        ),
        body: BlocBuilder<HomeBloc, HomeState>(
          builder: (context, state) => SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: ReorderableWrap(
              header: [HomeHeader()],
              children: state.modules
                  .where((m) => m.enabled)
                  .map((m) => ModuleCard(module: m))
                  .toList(),
              footer: HomeFooter(),
              buildDraggableFeedback: (_, constraint, child) => ConstrainedBox(
                constraints: constraint.copyWith(
                  maxWidth: constraint.constrainWidth() * 2,
                ),
                child: child,
              ),
              onReorder: (oldIndex, newIndex) =>
                  BlocProvider.of<HomeBloc>(context).add(
                HomeReorderEvent(oldIndex, newIndex),
              ),
            ),
          ),
        ),
      );
}

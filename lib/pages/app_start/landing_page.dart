/*
 * Copyright 2020, 2021 Gisa von Marcard
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

import 'dart:async';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../bloc/authentication/auth_bloc.dart';
import '../../bloc/user/user_bloc.dart';
import '../../common/constants/routing_constants.dart';
import '../../common/styled_print.dart';
import '../../common/tc_markdown_stylesheet.dart';
import '../../common/tc_theme.dart';
import '../../common/tinycampus_icons.dart';
import '../../common/widgets/tc_button.dart';
import '../../common/widgets/tc_form_field/tc_form_field.dart';
import '../../common/widgets/tc_form_field/tc_form_field_model.dart';
import '../../repositories/token/token_repository.dart';
import 'landing_page_flare_controller.dart';

class LandingPageArguments {
  LandingPageArguments({this.tasks = AvailableTasks.all});

  AvailableTasks tasks;
}

enum AvailableTasks {
  all,
  upgradeOnly,
}

class LandingPage extends StatefulWidget {
  LandingPage({Key? key, this.tasks = AvailableTasks.all}) : super(key: key);

  @override
  _LandingPageState createState() => _LandingPageState();
  final AvailableTasks tasks;
}

enum ScreenState {
  landing,
  login,
  loginRecovery,
  registerGuestPlus,
  registerUpgrade,
  registeredRecoveryInfo,
  successfullyLoggedIn,
  offline,
  resetPassword,
  registeredVerifyEmailInfo,
}

class _LandingPageState extends State<LandingPage>
    with SingleTickerProviderStateMixin {
  late AuthBloc _bloc;
  ValueNotifier<bool> checkConditions = ValueNotifier<bool>(false);
  ValueNotifier<bool> checkGuest = ValueNotifier<bool>(false);
  ValueNotifier<bool> listenAuthenticated = ValueNotifier<bool>(false);
  ValueNotifier<bool> listenAuthLoggedIn = ValueNotifier<bool>(false);
  ValueNotifier<bool> listenUserLoggedIn = ValueNotifier<bool>(false);
  ValueNotifier<bool> waitingForGuestPlusData = ValueNotifier<bool>(false);
  ValueNotifier<bool> navigating = ValueNotifier<bool>(false);
  ValueNotifier<bool> loading = ValueNotifier<bool>(false);
  ValueNotifier<bool> emailSent = ValueNotifier<bool>(false);
  ValueNotifier<bool> showDebug = ValueNotifier<bool>(false);
  ValueNotifier<bool> copiedRecovery = ValueNotifier<bool>(false);
  ValueNotifier<bool> checkTextFields = ValueNotifier<bool>(true);
  ValueNotifier<int> emailCooldownSeconds = ValueNotifier<int>(10);
  ValueNotifier<bool> guestPlusChosen = ValueNotifier<bool>(false);
  ValueNotifier<bool> fullRegisterChosen = ValueNotifier<bool>(false);
  ValueNotifier<bool> loginRequest = ValueNotifier<bool>(false);
  ValueNotifier<int> passwordNumberCheck = ValueNotifier<int>(0);
  ValueNotifier<int> passwordLetterCheck = ValueNotifier<int>(0);
  ValueNotifier<int> passwordLengthCheck = ValueNotifier<int>(0);
  ValueNotifier<int> passwordRepeatCheck = ValueNotifier<int>(0);

  Timer? emailCooldownTimer;

  final textDisplayNameController = TextEditingController();
  final textEmailController = TextEditingController();
  final textPassword = TextEditingController();
  final textPasswordConfirm = TextEditingController();
  final textRecovery = TextEditingController();
  ScreenState state = ScreenState.landing;
  ScreenState lastState = ScreenState.landing;
  ValueNotifier<ConnectivityResult> connection =
      ValueNotifier<ConnectivityResult>(ConnectivityResult.mobile);
  StreamSubscription<ConnectivityResult>? subscription;
  late LandingPageFlareController _ctrl;

  @override
  void initState() {
    super.initState();
    _bloc = BlocProvider.of<AuthBloc>(context);
    _ctrl = LandingPageFlareController(
      mixSeconds: 4.0,
      initCallback: () {
        if (mounted) {
          _ctrl.forceAdvance();
          _ctrl.mixSeconds = 1;
          if (connection.value == ConnectivityResult.none) {
            // _ctrl.play("static_end");
            _ctrl.play("offline");
            changeView(ScreenState.offline);
          } else {
            playDefaultAnimation();
          }
          Future.delayed(Duration(milliseconds: 900), () {
            if (mounted) {
              _ctrl.mixSeconds = 4;
            }
          });
        }
      },
    );
    initValues();
    initTextController();

    if (widget.tasks == AvailableTasks.upgradeOnly) {
      changeView(ScreenState.registerUpgrade);
    } else if (widget.tasks == AvailableTasks.all) {
      // Deny this behaviour if navigation origin is settings menu
      initConnectivity();
      initConnectivitySubscription();
    }
  }

  Future<void> initConnectivity() async {
    connection.value = await Connectivity().checkConnectivity();
    if (connection.value == ConnectivityResult.none) {
      _ctrl.play("offline");
      if (state != ScreenState.offline) {
        lastState = ScreenState.landing;
      }
      changeView(ScreenState.offline);
    }
  }

  void startLoadingAnimation() {
    FocusScope.of(context).unfocus();
    loading.value = true;
    _ctrl.play("loading_ease");
  }

  void stopLoadingAnimation() {
    loading.value = false;
    switch (CurrentTheme().theme) {
      case TCThemes.light:
        if (guestPlusChosen.value || loginRequest.value) {
          _ctrl.play("success_login", overrideSameAnimation: false);
        } else {
          _ctrl.play("success_register_mail", overrideSameAnimation: false);
        }
        break;
      case TCThemes.dark:
        if (guestPlusChosen.value || loginRequest.value) {
          _ctrl.play("success_login_DM", overrideSameAnimation: false);
        } else {
          _ctrl.play("success_register_mail_DM", overrideSameAnimation: false);
        }
        break;
      default:
        if (guestPlusChosen.value || loginRequest.value) {
          _ctrl.play("success_login_DM", overrideSameAnimation: false);
        } else {
          _ctrl.play("success_register_mail_DM", overrideSameAnimation: false);
        }
    }
  }

  void errorAnimation() {
    loading.value = false;
    switch (CurrentTheme().theme) {
      case TCThemes.light:
        _ctrl.play("error_wrong_pw");
        break;
      case TCThemes.dark:
        _ctrl.play("error_wrong_pw_DM");
        break;
      default:
        _ctrl.play("error_wrong_pw");
    }
    loginRequest.value = false;
    fullRegisterChosen.value = false;
    guestPlusChosen.value = false;
  }

  void initValues() {
    checkConditions.value = false;
    checkGuest.value = false;
    listenAuthenticated.value = false;
    listenAuthLoggedIn.value = false;
    listenUserLoggedIn.value = false;
    waitingForGuestPlusData.value = false;
    navigating.value = false;
    loading.value = false;
    emailSent.value = false;
    playDefaultAnimation();
    changeView(ScreenState.landing);
  }

  void initTextController() {
    textDisplayNameController.text = "";
    textDisplayNameController.addListener(() {
      checkTextFields.value = checkDisplay();
    });
    textEmailController.text = "";
    textEmailController.addListener(() {
      checkTextFields.value = checkEPP();
    });
    textPassword.text = "";
    textPassword.addListener(() {
      checkTextFields.value = checkEPP();
    });
    textPassword.addListener(() {
      passwordNumberCheck.value = checkPasswordNumber();
    });
    textPassword.addListener(() {
      passwordLetterCheck.value = checkPasswordLetter();
    });
    textPassword.addListener(() {
      passwordLengthCheck.value = checkPasswordLength();
    });
    textPasswordConfirm.text = "";
    textPasswordConfirm.addListener(() {
      checkTextFields.value = checkEPP();
    });
    textPasswordConfirm.addListener(() {
      passwordRepeatCheck.value = checkPasswordRepeat();
    });
    textRecovery.text = "";
    textRecovery.addListener(() {
      checkTextFields.value = checkRecovery();
    });
  }

  @override
  void dispose() {
    subscription?.cancel();
    super.dispose();
  }

  void initConnectivitySubscription() {
    subscription =
        Connectivity().onConnectivityChanged.listen(onConnectivityChange);
  }

  void onConnectivityChange(ConnectivityResult result) {
    final oldValue = connection.value;
    connection.value = result;
    if (connection.value == ConnectivityResult.none) {
      _ctrl.play("offline");
      if (state != ScreenState.offline) {
        lastState = state;
      }
      changeView(ScreenState.offline);
    } else {
      if (oldValue == ConnectivityResult.none) {
        if (lastState == ScreenState.offline) {
          changeView(ScreenState.landing);
        } else {
          changeView(lastState);
        }
        playDefaultAnimation();
      }
    }
  }

  void playDefaultAnimation() {
    switch (CurrentTheme().theme) {
      case TCThemes.light:
        _ctrl.play("transparent_to_end_wired");
        break;
      case TCThemes.dark:
        _ctrl.play("transparent_to_end_wired_DM");
        break;
      default:
        _ctrl.play("transparent_to_end_wired");
    }
  }

  @override
  Widget build(BuildContext context) => WillPopScope(
        onWillPop: () async {
          switch (state) {
            case ScreenState.landing:
              return false;
            case ScreenState.login:
              changeView(ScreenState.landing);
              break;
            case ScreenState.loginRecovery:
              changeView(ScreenState.login);
              break;
            case ScreenState.registerGuestPlus:
              changeView(ScreenState.landing);
              break;
            case ScreenState.registerUpgrade:
              if (widget.tasks == AvailableTasks.upgradeOnly) {
                Navigator.pop(context);
              }
              changeView(ScreenState.registerGuestPlus);
              break;
            case ScreenState.offline:
              break;
            case ScreenState.resetPassword:
              changeView(ScreenState.login);
              playDefaultAnimation();
              break;
            case ScreenState.registeredRecoveryInfo:
              if (!kReleaseMode) {
                playDefaultAnimation();
                changeView(lastState);
              }
              break;
            case ScreenState.registeredVerifyEmailInfo:
              if (!kReleaseMode) {
                playDefaultAnimation();
                changeView(lastState);
              }
              break;
            case ScreenState.successfullyLoggedIn:
              if (!kReleaseMode) {
                playDefaultAnimation();
                changeView(lastState);
              }
              break;
            default:
              break;
          }
          if (!kReleaseMode) {
            navigating.value = false;
          }
          return false;
        },
        child: Scaffold(
          resizeToAvoidBottomInset: true,
          body: BlocListener<UserBloc, UserState>(
            listener: (context, state) {
              // TODO: implement listener
            },
            child: MultiBlocListener(
              listeners: [
                BlocListener<AuthBloc, AuthState>(listener: (a, b) {
                  sprint(b.runtimeType.toString(),
                      prefix: "[AUTH]", style: PrintStyle.attention);
                  if (b is AuthenticatedState) {
                    sprint("Authenticated");
                    listenAuthLoggedIn.value = true;

                    checkIfSuccessfullyLoggedIn();
                  }
                  if (b is AuthResetEmailSentState) {
                    sprint("EMAIL SENT");
                    stopLoadingAnimation();
                    emailSent.value = true;
                    // loading.value = false;
                  }
                  if (b is UnauthenticatedState) {
                    // playDefaultAnimation();
                  }
                  if (b is AuthErrorState) {
                    eprint(b.errorType.toString());
                    guestPlusChosen.value = false;
                    errorAnimation();
                    var snackBarContentKey = "";
                    if (b.errorType == AuthError.unknown) {}
                    if (b.errorType == AuthError.emailNotVerified) {
                      snackBarContentKey = "login.errors.email_not_verified";
                    }
                    if (b.errorType == AuthError.upgradeUser) {
                      snackBarContentKey = "login.errors.unknown";
                    }
                    if (b.errorType == AuthError.credentialsIncorrect) {
                      snackBarContentKey = "login.errors.credentials_incorrect";
                      checkTextFields.value = false;
                      textPassword.clear();
                      textPasswordConfirm.clear();

                      /// TODO: [Validators should be red!]
                      setState(() {});
                    }

                    if (b.errorType == AuthError.emailAlreadyInUse) {
                      snackBarContentKey = "login.errors.email_already_in_use";
                    }
                    if (snackBarContentKey.isEmpty) {
                      snackBarContentKey = "login.errors.unknown";
                    }
                    if (widget.tasks == AvailableTasks.all) {
                      /// Because if you upgrade from settings menu,
                      /// user do not want to be logged out if an error occurs
                      _bloc.add(AuthLogoutEvent());
                    }
                    eprint(b.toString(), caller: LandingPage);
                    if (b.errorType == AuthError.unauthorized) {
                      sprint(
                          "Because it is unauthorized, do not build snackbar");
                      return;
                    }
                    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                      content: Text(
                          FlutterI18n.translate(context, snackBarContentKey)),
                      behavior: SnackBarBehavior.fixed,
                      duration: Duration(milliseconds: 1600),
                      action: SnackBarAction(
                        label: "OK",
                        textColor: CorporateColors.tinyCampusOrange,
                        onPressed: () {},
                      ),
                    ));
                  }
                }),
                BlocListener<UserBloc, UserState>(
                    listener: (context, userState) {
                  if (userState is UserLoggedInState) {
                    listenUserLoggedIn.value = true;
                    checkIfSuccessfullyLoggedIn();
                  }
                }),
              ],
              child: SafeArea(bottom: true, child: appStartContent()),
            ),
          ),
        ),
      );

  Future<void> checkIfSuccessfullyLoggedIn() async {
    if (listenAuthLoggedIn.value && listenUserLoggedIn.value) {
      if (!navigating.value) {
        navigating.value = true;
        stopLoadingAnimation();
        if (guestPlusChosen.value) {
          changeView(ScreenState.registeredRecoveryInfo);
          guestPlusChosen.value = false;
        }
        if (fullRegisterChosen.value) {
          changeView(ScreenState.registeredVerifyEmailInfo);
        }
        if (loginRequest.value) {
          changeView(ScreenState.successfullyLoggedIn);
        }
        sprint("SUCCESS! it would navigate now (only gets triggered once)");
        loginRequest.value = false;
        fullRegisterChosen.value = false;
        guestPlusChosen.value = false;
        // return;
        final recoveryToken = await TokenRepository().recoveryCode ?? "";
        if (recoveryToken.isEmpty) {
          navigateToGreeterAndHome();
        }
      }
    }
  }

  bool logoShouldExpand() =>
      state == ScreenState.landing ||
      state == ScreenState.registeredRecoveryInfo ||
      state == ScreenState.registeredVerifyEmailInfo ||
      state == ScreenState.successfullyLoggedIn;

  Column appStartContent() => Column(
        children: [
          Expanded(
            child: SingleChildScrollView(
              physics: const BouncingScrollPhysics(),
              child: Column(
                children: [
                  AnimatedContainer(
                    duration: Duration(milliseconds: 280),
                    curve: Curves.easeOut,
                    margin: EdgeInsets.only(
                        top: logoShouldExpand() ? 60 : 0,
                        bottom: logoShouldExpand() ? 20 : 0),
                    height: logoShouldExpand() ? 240 : 192,
                    padding: EdgeInsets.symmetric(vertical: 16),
                    child: Row(
                      children: [
                        Expanded(
                          flex: 2,
                          child: widget.tasks == AvailableTasks.upgradeOnly &&
                                  state == ScreenState.registerUpgrade
                              ? Align(
                                  alignment: Alignment.topCenter,
                                  child: BackButton())
                              : kReleaseMode
                                  ? Container()
                                  : SingleChildScrollView(
                                      child: debugOptions()),
                        ),
                        Expanded(
                          flex: 5,
                          child: GestureDetector(
                            onTap: kReleaseMode
                                ? null
                                : () {
                                    _ctrl.replayLastAnimation();
                                    showDebug.value = !showDebug.value;
                                  },
                            child: FlareActor(
                              "assets/flare/tc_logo_compound.flr",
                              isPaused: false,
                              controller: _ctrl,
                              fit: BoxFit.contain,
                              shouldClip: false,
                            ),
                          ),
                        ),
                        Expanded(
                            flex: 2,
                            child: kReleaseMode
                                ? Container()
                                : blocDisplayColumn()),
                      ],
                    ),
                  ),
                  Container(height: 16),
                  AnimatedSize(
                      alignment: Alignment.topCenter,
                      curve: Curves.easeOut,
                      duration: Duration(milliseconds: 260),
                      child: Container(
                        margin: const EdgeInsets.symmetric(vertical: 12),
                        child: Align(
                          alignment: Alignment.topCenter,
                          child: AnimatedSwitcher(
                              duration: Duration(milliseconds: 260),
                              layoutBuilder: (currentChild, previousChildren) {
                                var children = previousChildren;
                                if (currentChild != null) {
                                  children = children.toList()
                                    ..add(currentChild);
                                }
                                return Stack(
                                    children: children,
                                    alignment: Alignment.topCenter);
                              },
                              transitionBuilder: (child, animation) =>
                                  FadeTransition(
                                      child: Container(child: child),
                                      opacity:
                                          Tween<double>(begin: 0.0, end: 1.0)
                                              .animate(
                                        CurvedAnimation(
                                          parent: animation,
                                          curve: Interval(0.5, 1.0),
                                        ),
                                      )),
                              child: determineContent()),
                        ),
                      )),
                ],
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.all(12),
            child: ValueListenableBuilder(
                valueListenable: loading,
                builder: (context, value, child) => AnimatedSwitcher(
                    duration: Duration(milliseconds: 260),
                    layoutBuilder: (currentChild, previousChildren) {
                      var children = previousChildren;
                      if (currentChild != null) {
                        children = children.toList()..add(currentChild);
                      }
                      return Stack(
                          children: children,
                          alignment: Alignment.bottomCenter);
                    },
                    transitionBuilder: (child, animation) => FadeTransition(
                        child: Container(child: child),
                        opacity: Tween<double>(begin: 0.0, end: 1.0).animate(
                          CurvedAnimation(
                            parent: animation,
                            curve: Interval(0.5, 1.0),
                          ),
                        )),
                    child: Container(
                        key: Key(state.toString()),
                        child: determineButtons()))),
          ),
        ],
      );

  Widget blocDisplayColumn() => ValueListenableBuilder<bool>(
        valueListenable: showDebug,
        builder: (context, value, child) => !value
            ? Container()
            : SingleChildScrollView(
                child: Column(
                  children: [
                    Text(state.toString()),
                    if (!kReleaseMode)
                      BlocBuilder<UserBloc, UserState>(
                        builder: (context, state) =>
                            Text(state.user.displayName),
                      ),
                    BlocBuilder<UserBloc, UserState>(
                      builder: (context, state) => Column(children: [
                        Text(
                          state.runtimeType.toString(),
                          style: TextStyle(color: Colors.blue),
                        )
                      ]),
                    ),
                    BlocBuilder<UserBloc, UserState>(
                      builder: (context, state) => Column(children: [
                        Text(
                          state.user.verified.toString(),
                          style: TextStyle(color: Colors.blue[900]),
                        )
                      ]),
                    ),
                    BlocBuilder<AuthBloc, AuthState>(
                      builder: (context, state) => Column(children: [
                        Text(
                          state.runtimeType.toString(),
                          style: TextStyle(color: Colors.purple),
                        ),
                        Text(
                          state.errorMsg.toString(),
                          style: TextStyle(color: Colors.purpleAccent),
                        ),
                        Text(
                          state.errorType.toString(),
                          style: TextStyle(color: Colors.purpleAccent),
                        ),
                      ]),
                    ),
                  ],
                ),
              ),
      );

  Widget determineButtons() {
    switch (state) {
      case ScreenState.landing:
        return landingButtons(context);

      case ScreenState.login:
        return loginButtons(context);

      case ScreenState.loginRecovery:
        return loginRecoveryButtons(context);

      case ScreenState.registerGuestPlus:
        return registerGuestPlusButtons();

      case ScreenState.registerUpgrade:
        return registerUpgradeButtons();

      case ScreenState.registeredRecoveryInfo:
        return registeredRecoveryInfoButtons();

      case ScreenState.registeredVerifyEmailInfo:
        return registeredVerifyEmailInfoButtons();

      case ScreenState.offline:
        return offlineButtons();

      case ScreenState.resetPassword:
        return resetPasswordButtons(context);

      case ScreenState.successfullyLoggedIn:
        return successfullyLoggedInButtons();

      default:
        return registerUpgrade(context);
    }
  }

  void changeView(ScreenState? newState) {
    lastState = state;
    setState(() {
      state = newState ?? ScreenState.landing;
    });
  }

  Widget loginButtons(BuildContext context) => Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: KeyboardVisibilityBuilder(
              builder: (context, isKeyboardVisible) => TCButton(
                useAutoSizeText: true,
                onPressedCallback: () {
                  if (isKeyboardVisible) {
                    FocusScope.of(context).unfocus();
                  } else {
                    textEmailController.clear();
                    textPassword.clear();
                    setState(() {
                      state = ScreenState.landing;
                    });
                  }
                },
                buttonStyle: TCButtonStyle.outline,
                customPadding:
                    EdgeInsets.symmetric(horizontal: 12.0, vertical: 4),
                customHeight: 64,
                buttonLabel: FlutterI18n.translate(
                  context,
                  isKeyboardVisible
                      ? "login.form.hide_keyboard"
                      : "common.actions.back",
                ),
              ),
            ),
          ),
          Container(
            width: 12.0,
          ),
          Expanded(
            child: ValueListenableBuilder(
              valueListenable: checkTextFields,
              builder: (context, checkGuestVal, child) => TCButton(
                onPressedCallback: checkTextFields.value || loading.value
                    ? null
                    : () {
                        FocusScope.of(context).unfocus();
                        startLoadingAnimation();
                        loginRequest.value = true;
                        _bloc.add(AuthLogoutEvent());
                        _bloc.add(AuthLoginEvent(
                            textEmailController.text.trim().toLowerCase(),
                            textPassword.text.trim()));
                      },
                customPadding:
                    EdgeInsets.symmetric(horizontal: 12.0, vertical: 4),
                customHeight: 64,
                buttonLabel: FlutterI18n.translate(context, "login.login"),
              ),
            ),
          ),
        ],
      );

  Widget resetPasswordButtons(BuildContext context) => Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: KeyboardVisibilityBuilder(
              builder: (context, isKeyboardVisible) => TCButton(
                useAutoSizeText: true,
                onPressedCallback: () {
                  if (isKeyboardVisible) {
                    FocusScope.of(context).unfocus();
                  } else {
                    setState(() {
                      state = ScreenState.login;
                      if (!_ctrl.lastPlayedAnimation
                          .contains("transparent_to_end_wired")) {
                        playDefaultAnimation();
                      }
                      checkTextFields.value = checkEPP();
                      emailSent.value = false;
                    });
                    checkTextFields.value = checkEPP();
                  }
                },
                buttonStyle: TCButtonStyle.outline,
                customPadding:
                    EdgeInsets.symmetric(horizontal: 12.0, vertical: 4),
                customHeight: 64,
                buttonLabel: FlutterI18n.translate(
                  context,
                  isKeyboardVisible
                      ? "login.form.hide_keyboard"
                      : "common.actions.back",
                ),
              ),
            ),
          ),
          Container(
            width: 12.0,
          ),
          Expanded(
            child: ValueListenableBuilder(
              valueListenable: checkTextFields,
              builder: (context, checkGuestVal, child) =>
                  ValueListenableBuilder<int>(
                valueListenable: emailCooldownSeconds,
                builder: (context, emailCooldownSecondsValue, child) =>
                    TCButton(
                  onPressedCallback: checkTextFields.value ||
                          loading.value ||
                          (emailCooldownTimer?.isActive ?? false)
                      ? null
                      : () {
                          FocusScope.of(context).unfocus();
                          loading.value = true;
                          _bloc.add(AuthLogoutEvent());
                          Future.delayed(
                              Duration(milliseconds: kReleaseMode ? 1 : 260),
                              () {
                            const oneSec = Duration(milliseconds: 1000);
                            emailCooldownSeconds.value = 10;
                            emailCooldownTimer = Timer.periodic(oneSec, (t) {
                              if (emailCooldownSeconds.value-- > 0) {
                              } else {
                                if (mounted) {
                                  emailCooldownTimer?.cancel();
                                  emailSent.value = false;
                                }
                              }
                            });
                            _bloc.add(AuthResetPasswordEvent(
                              textEmailController.text.trim().toLowerCase(),
                            ));
                          });
                        },
                  customPadding:
                      EdgeInsets.symmetric(horizontal: 12.0, vertical: 4),
                  customHeight: 64,
                  buttonLabel: (emailCooldownTimer?.isActive ?? false)
                      ? emailCooldownSecondsValue.toString()
                      : FlutterI18n.translate(context, "login.send"),
                ),
              ),
            ),
          ),
        ],
      );

  Widget loginRecoveryButtons(BuildContext context) => Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: KeyboardVisibilityBuilder(
              builder: (context, isKeyboardVisible) => TCButton(
                useAutoSizeText: true,
                onPressedCallback: () {
                  if (isKeyboardVisible) {
                    FocusScope.of(context).unfocus();
                  } else {
                    setState(() {
                      state = ScreenState.login;
                    });
                    checkTextFields.value = checkEPP();
                  }
                },
                buttonStyle: TCButtonStyle.outline,
                customPadding:
                    EdgeInsets.symmetric(horizontal: 12.0, vertical: 4),
                customHeight: 64,
                buttonLabel: FlutterI18n.translate(
                  context,
                  isKeyboardVisible
                      ? "login.form.hide_keyboard"
                      : "common.actions.back",
                ),
              ),
            ),
          ),
          Container(
            width: 12.0,
          ),
          Expanded(
            child: ValueListenableBuilder(
              valueListenable: checkTextFields,
              builder: (context, checkGuestVal, child) => TCButton(
                onPressedCallback: checkTextFields.value || loading.value
                    ? null
                    : () {
                        FocusScope.of(context).unfocus();
                        startLoadingAnimation();
                        loginRequest.value = true;
                        _bloc.add(AuthRecoverEvent(textRecovery.text));
                      },
                customPadding:
                    EdgeInsets.symmetric(horizontal: 12.0, vertical: 4),
                customHeight: 64,
                buttonLabel: FlutterI18n.translate(context, "login.login"),
              ),
            ),
          ),
        ],
      );

  Widget landingButtons(BuildContext context) => Padding(
        padding: const EdgeInsets.symmetric(horizontal: 12.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            TCButton(
              onPressedCallback: () {
                changeView(ScreenState.login);
                checkTextFields.value = checkEPP();
              },
              buttonLabel: FlutterI18n.translate(context, "login.login"),
            ),
            Container(
              height: 20.0,
            ),
            TCButton(
              onPressedCallback: () {
                changeView(ScreenState.registerGuestPlus);
                checkTextFields.value = checkDisplay();
              },
              buttonStyle: TCButtonStyle.outline,
              buttonLabel: FlutterI18n.translate(context, "login.register"),
            ),
            Container(height: 32),
            Wrap(
              crossAxisAlignment: WrapCrossAlignment.center,
              alignment: WrapAlignment.center,
              children: [
                TextButton(
                  child: Text(
                      FlutterI18n.translate(context, "common.privacy_policy")),
                  // FlutterI18n.translate(context, 'login.privacy')),
                  onPressed: () =>
                      _launchUrl("https://tinycampus.de/datenschutz"),
                ),
                TextButton(
                  child: Text(
                      FlutterI18n.translate(context, 'settings.legal_notice')),
                  onPressed: () =>
                      _launchUrl("https://tinycampus.de/impressum"),
                ),
                TextButton(
                  child: Text(
                    FlutterI18n.translate(context, "common.coc.title"),
                    textAlign: TextAlign.center,
                  ),
                  onPressed: () =>
                      _launchUrl("https://tinycampus.de/verhaltenskodex"),
                ),
              ],
            ),
          ],
        ),
      );

  Row registerGuestPlusButtons() => Row(
        children: [
          Expanded(
            child: KeyboardVisibilityBuilder(
              builder: (context, isKeyboardVisible) => TCButton(
                useAutoSizeText: true,
                onPressedCallback: () {
                  if (isKeyboardVisible) {
                    FocusScope.of(context).unfocus();
                  } else {
                    setState(() {
                      state = ScreenState.landing;
                    });
                    checkTextFields.value = checkDisplay();
                  }
                },
                buttonStyle: TCButtonStyle.outline,
                customPadding:
                    EdgeInsets.symmetric(horizontal: 12.0, vertical: 4),
                customHeight: 64,
                buttonLabel: FlutterI18n.translate(
                  context,
                  isKeyboardVisible
                      ? "login.form.hide_keyboard"
                      : "common.actions.back",
                ),
              ),
            ),
          ),
          Container(
            width: 12.0,
          ),
          Expanded(
            child: ValueListenableBuilder(
              valueListenable: checkTextFields,
              builder: (context, checkGuestVal, child) => TCButton(
                useAutoSizeText: true,
                customPadding:
                    EdgeInsets.symmetric(horizontal: 12.0, vertical: 4),
                customHeight: 64,
                buttonLabel:
                    FlutterI18n.translate(context, "common.actions.continue"),
                onPressedCallback:
                    checkTextFields.value || !checkConditions.value
                        ? null
                        : () async {
                            setState(() {
                              state = ScreenState.registerUpgrade;
                            });
                            checkTextFields.value = checkEPP();
                          },
              ),
            ),
          ),
        ],
      );

  void navigateToGreeterAndHome() {
    sprint("navitating now");
    final nav = Navigator.of(context)
      ..pushNamedAndRemoveUntil(loginGreeterRoute, (route) => false);
    Future.delayed(
      Duration(milliseconds: 1600),
      () => nav.pushReplacementNamed(homeTabRoute),
    );
  }

  Widget offlineButtons() => Column(
        children: [
          Container(height: 12),
          Row(
            children: [
              Expanded(
                child: ValueListenableBuilder(
                  valueListenable: checkGuest,
                  builder: (context, checkGuestVal, child) => TCButton(
                    useAutoSizeText: true,
                    customPadding:
                        EdgeInsets.symmetric(horizontal: 12.0, vertical: 4),
                    customHeight: 64,
                    buttonLabel:
                        FlutterI18n.translate(context, "login.bouncy_ball"),
                    noAnimationDuration: true,
                    onPressedCallback: () {
                      Navigator.of(context).pushNamed(offlineGame);
                    },
                  ),
                ),
              ),
            ],
          ),
        ],
      );

  Widget successfullyLoggedInButtons() => Column(
        children: [
          Container(height: 12),
          Row(
            children: [
              Expanded(
                child: ValueListenableBuilder(
                  valueListenable: checkGuest,
                  builder: (context, checkGuestVal, child) => TCButton(
                    useAutoSizeText: true,
                    customPadding:
                        EdgeInsets.symmetric(horizontal: 12.0, vertical: 4),
                    customHeight: 64,
                    buttonLabel: FlutterI18n.translate(
                        context, "common.actions.continue"),
                    noAnimationDuration: true,
                    onPressedCallback: navigateToGreeterAndHome,
                  ),
                ),
              ),
            ],
          ),
        ],
      );

  Widget registeredRecoveryInfoButtons() => Column(
        children: [
          registeredRecoveryInfo(context),
          Container(height: 12),
          Row(
            children: [
              Expanded(
                child: ValueListenableBuilder(
                  valueListenable: checkGuest,
                  builder: (context, checkGuestVal, child) => TCButton(
                    // key: Key("goToHomescreenButton"),
                    useAutoSizeText: true,
                    customPadding:
                        EdgeInsets.symmetric(horizontal: 12.0, vertical: 4),
                    customHeight: 64,
                    buttonLabel: FlutterI18n.translate(
                        context, "common.actions.continue"),
                    noAnimationDuration: true,
                    onPressedCallback: navigateToGreeterAndHome,
                  ),
                ),
              ),
            ],
          ),
        ],
      );

  Widget registeredVerifyEmailInfoButtons() =>
      BlocConsumer<UserBloc, UserState>(
          listener: (context, state) {
            if (state is UserLoggedInState) {
              if (state.user.verified) {
                loginRequest.value = true;
                stopLoadingAnimation();
                loginRequest.value = false;
              }
            }
          },
          builder: (context, state) => Column(
                children: [
                  Text(
                    state.user.verified
                        ? FlutterI18n.translate(
                            context, "login.verification.verified")
                        : FlutterI18n.translate(
                            context, "login.verification.check_emails"),
                    textAlign: TextAlign.left,
                    style: CurrentTheme()
                        .themeData
                        .textTheme
                        .headline2
                        ?.copyWith(color: CurrentTheme().textSoftWhite),
                  ),
                  if (!state.user.verified)
                    Text(
                      FlutterI18n.translate(
                          context, "login.verification.verify_later"),
                      textAlign: TextAlign.left,
                      style: CurrentTheme()
                          .themeData
                          .textTheme
                          .headline4
                          ?.copyWith(color: CurrentTheme().textPassive),
                    ),
                  Container(height: 36),
                  Row(
                    children: [
                      Expanded(
                        child: ValueListenableBuilder(
                          valueListenable: checkGuest,
                          builder: (context, checkGuestVal, child) => TCButton(
                            useAutoSizeText: true,
                            customPadding: EdgeInsets.symmetric(
                                horizontal: 12.0, vertical: 4),
                            customHeight: 64,
                            buttonLabel: FlutterI18n.translate(
                                context,
                                state.user.verified
                                    ? "common.actions.continue"
                                    : "common.actions.skip"),
                            noAnimationDuration: true,
                            onPressedCallback: navigateToGreeterAndHome,
                            buttonStyle: state.user.verified
                                ? TCButtonStyle.filled
                                : TCButtonStyle.outline,
                          ),
                        ),
                      ),
                    ],
                  ),
                  if (!state.user.verified) Container(height: 12),
                  if (!state.user.verified)
                    Row(
                      children: [
                        Expanded(
                          child: ValueListenableBuilder(
                            valueListenable: checkGuest,
                            builder: (context, checkGuestVal, child) =>
                                Column(children: [
                              TCButton(
                                useAutoSizeText: true,
                                customPadding: EdgeInsets.symmetric(
                                    horizontal: 12.0, vertical: 4),
                                customHeight: 64,
                                buttonLabel: FlutterI18n.translate(
                                    context,
                                    state.user.verified
                                        ? "login.verification.verified"
                                        : "login.verification.check_status"),
                                noAnimationDuration: true,
                                buttonStyle: state.user.verified
                                    ? TCButtonStyle.outline
                                    : TCButtonStyle.filled,
                                onPressedCallback: (state is UserLoadingState)
                                    ? state.user.verified
                                        ? () {}
                                        : null
                                    : () {
                                        BlocProvider.of<UserBloc>(context)
                                            .add(UserFetchEvent());
                                        // navigateToGreeterAndHome();
                                      },
                              )
                            ]),
                          ),
                        ),
                      ],
                    ),
                ],
              ));

  Row registerUpgradeButtons() => Row(
        children: [
          if (widget.tasks == AvailableTasks.all)
            Expanded(
                child: KeyboardVisibilityBuilder(
              builder: (context, isKeyboardVisible) => TCButton(
                useAutoSizeText: true,
                onPressedCallback: () {
                  if (isKeyboardVisible) {
                    FocusScope.of(context).unfocus();
                  } else {
                    textEmailController.clear();
                    textPassword.clear();
                    textPasswordConfirm.clear();
                    setState(() {
                      state = ScreenState.registerGuestPlus;
                    });
                    passwordNumberCheck.value = 0;
                    passwordLengthCheck.value = 0;
                    passwordLetterCheck.value = 0;
                    passwordRepeatCheck.value = 0;
                    checkTextFields.value = checkDisplay();
                  }
                },
                buttonStyle: TCButtonStyle.outline,
                customPadding:
                    EdgeInsets.symmetric(horizontal: 12.0, vertical: 4),
                customHeight: 64,
                buttonLabel: FlutterI18n.translate(
                  context,
                  isKeyboardVisible
                      ? "login.form.hide_keyboard"
                      : "common.actions.back",
                ),
              ),
            )),
          if (widget.tasks == AvailableTasks.all)
            Container(
              width: 12.0,
            ),
          Expanded(
            child: ValueListenableBuilder<bool>(
              valueListenable: checkGuest,
              builder: (context, checkGuestVal, child) => checkGuestVal
                  ? TCButton(
                      useAutoSizeText: true,
                      customPadding:
                          EdgeInsets.symmetric(horizontal: 12.0, vertical: 4),
                      customHeight: 64,
                      buttonLabel: FlutterI18n.translate(context, "login.skip"),
                      onPressedCallback: loading.value
                          ? null
                          : () {
                              startLoadingAnimation();
                              guestPlusChosen.value = true;
                              _bloc.add(AuthLogoutEvent());
                              _bloc.add(AuthRegisterEvent(
                                  textDisplayNameController.text.trim(),
                                  shouldUpgrade: true));
                              waitingForGuestPlusData.value = true;
                            },
                    )
                  : ValueListenableBuilder(
                      valueListenable: checkTextFields,
                      builder: (context, checkGuestVal, child) => TCButton(
                        useAutoSizeText: true,
                        customPadding:
                            EdgeInsets.symmetric(horizontal: 12.0, vertical: 4),
                        customHeight: 64,
                        buttonLabel:
                            FlutterI18n.translate(context, "login.register"),
                        onPressedCallback:
                            checkTextFields.value || loading.value
                                ? null
                                : () {
                                    startLoadingAnimation();
                                    if (widget.tasks == AvailableTasks.all) {
                                      fullRegisterChosen.value = true;
                                      _bloc.add(AuthLogoutEvent());
                                      _bloc.add(AuthRegisterEvent(
                                        textDisplayNameController.text.trim(),
                                        shouldUpgrade: true,
                                        username: textEmailController.text
                                            .trim()
                                            .toLowerCase(),
                                        password: textPassword.text.trim(),
                                      ));
                                    }

                                    if (widget.tasks ==
                                        AvailableTasks.upgradeOnly) {
                                      fullRegisterChosen.value = true;
                                      _bloc.add(AuthUpgradeEvent(
                                        textEmailController.text
                                            .trim()
                                            .toLowerCase(),
                                        textPassword.text.trim(),
                                      ));
                                    }
                                  },
                      ),
                    ),
            ),
          ),
        ],
      );

  Widget determineContent() {
    switch (state) {
      case ScreenState.landing:
        return Material(
          color: CurrentTheme().themeData.primaryColor,
          key: Key("landing"),
          child: landing(context),
        );
      case ScreenState.login:
        return Material(key: Key("login"), child: login(context));
      case ScreenState.loginRecovery:
        return Material(
          color: CurrentTheme().themeData.primaryColor,
          key: Key("loginRecovery"),
          child: loginRecovery(context),
        );
      case ScreenState.registerGuestPlus:
        return Material(
          color: CurrentTheme().themeData.primaryColor,
          key: Key("registerGuestPlus"),
          child: registerGuestPlus(context),
        );
      case ScreenState.registerUpgrade:
        return Material(
          color: CurrentTheme().themeData.primaryColor,
          key: Key("registerUpgrade"),
          child: registerUpgrade(context),
        );
      case ScreenState.registeredRecoveryInfo:
        return Container(
            key: Key("registeredRecoveryInfo"), child: Container());
      case ScreenState.registeredVerifyEmailInfo:
        return Container(
            key: Key("registeredVerifyEmailInfo"), child: Container());
      case ScreenState.offline:
        return Container(
          key: Key("offline"),
          child: offline(),
        );
      case ScreenState.resetPassword:
        return Container(
          key: Key("registerUpgrade"),
          child: resetPassword(context),
        );
      case ScreenState.successfullyLoggedIn:
        return Container(
          key: Key("successfullyLoggedIn"),
          child: successfullyLoggedIn(context),
        );
      default:
        return registerUpgrade(context);
    }
  }

  Widget offline() => Padding(
        padding: const EdgeInsets.all(32.0),
        child: Text(
            FlutterI18n.translate(context, 'login.offline_notification'),
            style: CurrentTheme().themeData.textTheme.headline3),
      );

  Widget successfullyLoggedIn(BuildContext context) => Column(
        children: [Container()],
      );

  Widget landing(BuildContext context) => Column(
        children: [Container()],
      );

  Widget registerGuestPlus(BuildContext context) => Column(
        children: [
          Column(
            children: [
              CheckboxListTile(
                value: checkConditions.value,
                onChanged: (value) {
                  setState(() {
                    checkConditions.value = value ?? false;
                  });
                },
                contentPadding:
                    EdgeInsets.symmetric(vertical: 12, horizontal: 16),
                title: Semantics(
                  label: FlutterI18n.translate(context, 'login.approval'),
                  child: ExcludeSemantics(
                    child: Markdown(
                      padding: EdgeInsets.zero,
                      shrinkWrap: true,
                      data: FlutterI18n.translate(
                          context, 'login.approval_markdown'),
                      onTapLink: (_, url, __) => _launchUrl(url ?? ""),
                      styleSheet: TinyCampusMarkdownStylesheet(context),
                      physics: BouncingScrollPhysics(),
                    ),
                  ),
                ),
              ),
              ValueListenableBuilder<bool>(
                valueListenable: checkConditions,
                builder: (context, checkConditionsValue, child) =>
                    !checkConditionsValue
                        ? Container()
                        : Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 10, vertical: 6),
                            child: TCFormField(
                              formFieldModel: TCFormFieldModel(
                                title: FlutterI18n.translate(
                                    context, "login.form.display_name.name"),
                                hintText: FlutterI18n.translate(
                                    context, "login.form.display_name.hint"),
                                maxLength: 70,
                                controller: textDisplayNameController,
                                valid: [
                                  (text) => text.isNotEmpty
                                      ? null
                                      : FlutterI18n.translate(context,
                                          "login.form.display_name.empty"),
                                ],
                              ),
                            ),
                          ),
              ),
            ],
          ),
        ],
      );

  Widget registeredRecoveryInfo(BuildContext context) => Column(
        children: [
          FutureBuilder(
            future: TokenRepository().recoveryCode,
            builder: (context, snapshot) {
              switch (snapshot.connectionState) {
                case ConnectionState.waiting:
                  return CircularProgressIndicator();
                case ConnectionState.done:
                  return Material(
                    elevation: 4,
                    clipBehavior: Clip.antiAlias,
                    color: CurrentTheme().themeData.primaryColor,
                    borderRadius: BorderRadius.all(Radius.circular(12.0)),
                    child: InkWell(
                      onTap: () {
                        Clipboard.setData(
                            ClipboardData(text: snapshot.data.toString()));
                        if (!copiedRecovery.value) {
                          copiedRecovery.value = true;
                          Future.delayed(Duration(milliseconds: 1200), () {
                            if (mounted) {
                              copiedRecovery.value = false;
                            }
                          });
                        }
                      },
                      child: Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Column(
                          children: [
                            Row(
                              children: [
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(FlutterI18n.translate(
                                          context, "login.recovery.title")),
                                      Row(
                                        children: [
                                          Expanded(
                                            child: Text(
                                              snapshot.data.toString(),
                                              style: CurrentTheme()
                                                  .themeData
                                                  .textTheme
                                                  .headline2,
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                              right: 16,
                                              left: 24,
                                            ),
                                            child: ValueListenableBuilder(
                                              valueListenable: copiedRecovery,
                                              builder:
                                                  (context, value, child) =>
                                                      AnimatedSwitcher(
                                                duration:
                                                    Duration(milliseconds: 160),
                                                child: copiedRecovery.value
                                                    ? Icon(
                                                        Icons.check,
                                                        key: Key(
                                                          "copiedRecoveryKey",
                                                        ),
                                                        size: 32,
                                                      )
                                                    : Icon(
                                                        Icons.copy,
                                                        key:
                                                            Key("notCopiedKey"),
                                                        size: 32,
                                                      ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      Container(height: 24),
                                      Text(
                                          FlutterI18n.translate(
                                              context, "login.recovery.copy"),
                                          style: CurrentTheme()
                                              .themeData
                                              .textTheme
                                              .bodyText1
                                              ?.copyWith(
                                                  color: CurrentTheme()
                                                      .tcBlueFont)),
                                      Text(
                                          FlutterI18n.translate(context,
                                              "login.recovery.managa_recovery"),
                                          style: CurrentTheme()
                                              .themeData
                                              .textTheme
                                              .bodyText1),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                default:
                  return CircularProgressIndicator();
              }
            },
          ),
        ],
      );

  Widget login(BuildContext context) => Column(
        children: [
          Material(
            color: CurrentTheme().themeData.primaryColor,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10.0),
              child: Column(
                children: [
                  Container(height: 12),
                  TCFormField(
                    formFieldModel: TCFormFieldModel(
                      title: FlutterI18n.translate(
                          context, "login.form.email.name"),
                      hintText: FlutterI18n.translate(
                          context, "login.form.email.hint"),
                      maxLength: 128,
                      controller: textEmailController,
                      valid: [
                        (text) => text.trim().isNotEmpty
                            ? null
                            : FlutterI18n.translate(
                                context, "login.form.email.empty"),
                        (text) => text.trim().isValidEmail()
                            ? null
                            : FlutterI18n.translate(
                                context, "login.form.email.not_email"),
                      ],
                      showRemainingChars: false,
                    ),
                  ),
                  TCFormField(
                    formFieldModel: TCFormFieldModel(
                      title: FlutterI18n.translate(
                          context, "login.form.passwords.name"),
                      hintText: FlutterI18n.translate(
                          context, "login.form.passwords.hint"),
                      maxLength: 128,
                      controller: textPassword,
                      obscure: true,
                      valid: [
                        (text) => text.trim().isNotEmpty
                            ? null
                            : FlutterI18n.translate(
                                context, "login.form.passwords.empty"),
                        (text) => RegExp(r"^(?=.*\d)(?=.*[a-z]).{8,64}$")
                                .hasMatch(text.trim())
                            ? null
                            : FlutterI18n.translate(
                                context, "login.form.passwords.password_error"),
                      ],
                      showRemainingChars: false,
                    ),
                  ),
                ],
              ),
            ),
          ),
          TextButton(
            child:
                Text(FlutterI18n.translate(context, "login.use_recovery_code")),
            onPressed: () {
              changeView(ScreenState.loginRecovery);
              checkTextFields.value = checkRecovery();
            },
          ),
          TextButton(
            child:
                Text(FlutterI18n.translate(context, "login.forgot_password")),
            onPressed: () {
              changeView(ScreenState.resetPassword);
              checkTextFields.value = checkEPP();
            },
          ),
        ],
      );

  Widget loginRecovery(BuildContext context) => Column(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10.0),
            child: Column(
              children: [
                Container(height: 12),
                TCFormField(
                  formFieldModel: TCFormFieldModel(
                    title: FlutterI18n.translate(
                        context, "login.form.recovery_code.name"),
                    hintText: FlutterI18n.translate(
                        context, "login.form.recovery_code.hint"),
                    maxLength: 128,
                    controller: textRecovery,
                    valid: [
                      (text) => text.isNotEmpty
                          ? null
                          : FlutterI18n.translate(
                              context, "login.form.recovery_code.empty"),
                    ],
                    showRemainingChars: false,
                  ),
                ),
              ],
            ),
          ),
        ],
      );

  Widget registerUpgrade(BuildContext context) => Column(
        children: [
          if (widget.tasks == AvailableTasks.all)
            CheckboxListTile(
              value: checkGuest.value,
              onChanged: (value) {
                setState(() {
                  checkGuest.value = value ?? false;
                });
              },
              title:
                  Text(FlutterI18n.translate(context, "login.guest_mode.try")),
              subtitle: Text(FlutterI18n.translate(
                  context, "login.guest_mode.verify_anytime")),
            ),
          ValueListenableBuilder<bool>(
            valueListenable: checkGuest,
            builder: (context, value, child) => Column(
              children: [
                AnimatedSwitcher(
                  duration: Duration(milliseconds: 510),
                  child: !value
                      ? optionInstantUpgrade()
                      : optionGuestPlus(context),
                  layoutBuilder: (currentChild, previousChildren) {
                    var children = previousChildren;
                    if (currentChild != null) {
                      children = children.toList()..add(currentChild);
                    }
                    return Stack(
                        children: children, alignment: Alignment.topCenter);
                  },
                  transitionBuilder: (child, animation) => FadeTransition(
                      child: Container(child: child),
                      opacity: Tween<double>(begin: 0.0, end: 1.0).animate(
                        CurvedAnimation(
                          parent: animation,
                          curve: Interval(0.7, 1.0),
                        ),
                      )),
                ),
              ],
            ),
          ),
        ],
      );

  Widget resetPassword(BuildContext context) => Column(
        children: [
          Material(
            color: CurrentTheme().themeData.primaryColor,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10.0),
              child: Column(
                children: [
                  Container(height: 12),
                  TCFormField(
                    formFieldModel: TCFormFieldModel(
                      title: FlutterI18n.translate(
                          context, "login.form.email.name_reset"),
                      hintText: FlutterI18n.translate(
                          context, "login.form.email.hint_reset"),
                      maxLength: 128,
                      controller: textEmailController,
                      valid: [
                        (text) => text.trim().isNotEmpty
                            ? null
                            : FlutterI18n.translate(
                                context, "login.form.email.empty"),
                        (text) => text.trim().isValidEmail()
                            ? null
                            : FlutterI18n.translate(
                                context, "login.form.email.not_email"),
                      ],
                      showRemainingChars: false,
                    ),
                  ),
                  ValueListenableBuilder<bool>(
                      valueListenable: emailSent,
                      builder: (context, emailSentValue, child) =>
                          AnimatedSwitcher(
                              duration: Duration(milliseconds: 260),
                              child: emailSentValue
                                  ? Container(
                                      padding:
                                          EdgeInsets.symmetric(vertical: 24),
                                      child: Text(
                                        FlutterI18n.translate(
                                            context, "login.email_send"),
                                        style: CurrentTheme()
                                            .themeData
                                            .textTheme
                                            .headline3,
                                      ),
                                    )
                                  : Container())),
                ],
              ),
            ),
          ),
        ],
      );

  Widget debugOptions() => ValueListenableBuilder<bool>(
        valueListenable: showDebug,
        builder: (context, value, child) => !value
            ? Container()
            : Column(
                children: [
                  if (!kReleaseMode)
                    TextButton(
                      child: Text(
                        "OfflineMode",
                      ),
                      onPressed: () {
                        changeView(ScreenState.offline);
                      },
                    ),
                  if (!kReleaseMode)
                    TextButton(
                      child: Text(
                        "Logout",
                      ),
                      onPressed: () {
                        RepositoryProvider.of<AuthBloc>(context)
                            .add(AuthLogoutEvent());
                      },
                    ),
                  if (!kReleaseMode)
                    TextButton(
                      child: Text(
                        "Upgrade",
                      ),
                      onPressed: () {
                        _bloc.add(AuthUpgradeEvent(
                            textEmailController.text.trim().toLowerCase(),
                            textPassword.text));
                      },
                    ),
                  if (!kReleaseMode)
                    TextButton(
                      child: Text(
                        "Go To Home",
                      ),
                      onPressed: () {
                        final nav = Navigator.of(context)
                          ..pushNamedAndRemoveUntil(
                              loginGreeterRoute, (route) => false);
                        Future.delayed(
                          Duration(milliseconds: 1600),
                          () => nav.pushReplacementNamed(homeTabRoute),
                        );
                      },
                    ),
                  if (!kReleaseMode)
                    TextButton(
                      child: Text(
                        "Init Values",
                      ),
                      onPressed: initValues,
                    ),
                  if (!kReleaseMode)
                    TextButton(
                      child: Text(
                        "Splash Page",
                      ),
                      onPressed: () {
                        Navigator.of(context).pushNamedAndRemoveUntil(
                          mainRoute,
                          (route) => false,
                        );
                      },
                    )
                ],
              ),
      );

  Column optionGuestPlus(BuildContext context) => Column(
        children: [
          Divider(indent: 12, endIndent: 12),
          ListTile(
            leading: Icon(
              TinyCampusIcons.timeRunning,
              color: CorporateColors.cafeteriaCautionRed,
            ),
            title: Text(
              FlutterI18n.translate(context, "login.guest_mode.deletion"),
              style: CurrentTheme()
                  .themeData
                  .textTheme
                  .bodyText1
                  ?.copyWith(color: CorporateColors.cafeteriaCautionRed),
            ),
          ),
          Divider(indent: 12, endIndent: 12),
          ListTile(
            leading: Icon(Icons.settings_backup_restore_rounded),
            title: Text(
              FlutterI18n.translate(context, "login.guest_mode.get_recovery"),
              style: CurrentTheme().themeData.textTheme.bodyText1,
            ),
          ),
          Divider(indent: 12, endIndent: 12),
          ListTile(
            leading: Icon(Icons.check),
            title: Text(
              FlutterI18n.translate(context, "login.guest_mode.verify_in_app"),
              style: CurrentTheme().themeData.textTheme.bodyText1,
            ),
          ),
          Container(height: 12),
        ],
      );

  Padding optionInstantUpgrade() => Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10.0),
        child: Column(
          children: [
            TCFormField(
              formFieldModel: TCFormFieldModel(
                title: FlutterI18n.translate(context, "login.form.email.name"),
                hintText:
                    FlutterI18n.translate(context, "login.form.email.hint"),
                maxLength: 128,
                controller: textEmailController,
                valid: [
                  (text) => text.trim().isNotEmpty
                      ? null
                      : FlutterI18n.translate(
                          context, "login.form.email.empty"),
                  (text) => text.trim().isValidEmail()
                      ? null
                      : FlutterI18n.translate(
                          context, "login.form.email.not_email"),
                ],
                showRemainingChars: false,
              ),
            ),
            TCFormField(
              formFieldModel: TCFormFieldModel(
                title:
                    FlutterI18n.translate(context, "login.form.passwords.name"),
                hintText:
                    FlutterI18n.translate(context, "login.form.passwords.hint"),
                maxLength: 128,
                obscure: true,
                controller: textPassword,
                valid: [
                  (text) => text.trim().isNotEmpty
                      ? null
                      : FlutterI18n.translate(
                          context, "login.form.passwords.empty"),
                  (text) => RegExp(r"^(?=.*\d)(?=.*[a-z]).{8,64}$")
                          .hasMatch(text.trim())
                      ? null
                      : FlutterI18n.translate(
                          context, "login.form.passwords.password_error"),
                ],
                showRemainingChars: false,
                showErrorMessage: false,
                showButtons: false,
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 10, bottom: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      ValueListenableBuilder(
                        valueListenable: passwordNumberCheck,
                        builder: (context, value, child) => Text(
                          FlutterI18n.translate(
                              context, "login.form.passwords.one_number"),
                          style: Theme.of(context).textTheme.caption?.copyWith(
                                color: passwordNumberCheck.value == 0
                                    ? CurrentTheme().textPassive
                                    : passwordNumberCheck.value == 1
                                        ? CorporateColors.ppAnswerCorrectGreen
                                        : CorporateColors.ppAnswerWrongRed,
                                fontSize: 14,
                              ),
                        ),
                      ),
                      ValueListenableBuilder(
                        valueListenable: passwordLetterCheck,
                        builder: (context, value, child) => Text(
                          FlutterI18n.translate(
                              context, "login.form.passwords.one_letter"),
                          style: Theme.of(context).textTheme.caption?.copyWith(
                                color: passwordLetterCheck.value == 0
                                    ? CurrentTheme().textPassive
                                    : passwordLetterCheck.value == 1
                                        ? CorporateColors.ppAnswerCorrectGreen
                                        : CorporateColors.ppAnswerWrongRed,
                                fontSize: 14,
                              ),
                        ),
                      ),
                      ValueListenableBuilder(
                        valueListenable: passwordLengthCheck,
                        builder: (context, value, child) => Text(
                          FlutterI18n.translate(context,
                              "login.form.passwords.number_of_characters"),
                          style: Theme.of(context).textTheme.caption?.copyWith(
                                color: passwordLengthCheck.value == 0
                                    ? CurrentTheme().textPassive
                                    : passwordLengthCheck.value == 1
                                        ? CorporateColors.ppAnswerCorrectGreen
                                        : CorporateColors.ppAnswerWrongRed,
                                fontSize: 14,
                              ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            TCFormField(
              formFieldModel: TCFormFieldModel(
                title: FlutterI18n.translate(
                    context, "login.form.passwords.name_repeat"),
                hintText: FlutterI18n.translate(
                    context, "login.form.passwords.hint_repeat"),
                maxLength: 128,
                obscure: true,
                controller: textPasswordConfirm,
                valid: [
                  (text) => text.trim().isNotEmpty
                      ? null
                      : FlutterI18n.translate(
                          context, "login.form.passwords.empty"),
                  (text) => text.trim() == textPassword.text.trim()
                      ? null
                      : FlutterI18n.translate(
                          context, "login.form.passwords.error_repeat"),
                ],
                showRemainingChars: false,
                showErrorMessage: false,
                showButtons: false,
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 10, bottom: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  ValueListenableBuilder(
                    valueListenable: passwordRepeatCheck,
                    builder: (context, value, child) => Text(
                      FlutterI18n.translate(
                          context, "login.form.passwords.repetition_match"),
                      style: Theme.of(context).textTheme.caption?.copyWith(
                            color: passwordRepeatCheck.value == 0
                                ? CurrentTheme().textPassive
                                : passwordRepeatCheck.value == 1
                                    ? CorporateColors.ppAnswerCorrectGreen
                                    : CorporateColors.ppAnswerWrongRed,
                            fontSize: 14,
                          ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      );

  Future<void> _launchUrl(String url) async {
    if (await canLaunch(url)) {
      await launch(
        url,
      );
    } else {
      throw 'Could not launch $url';
    }
  }

  bool checkDisplay() => textDisplayNameController.text.trim().isEmpty;

  bool checkRecovery() => textRecovery.text.trim().isEmpty;

  int checkPasswordNumber() {
    if (state == ScreenState.registerUpgrade) {
      if (RegExp(r"^(?=.*\d).{0,64}$").hasMatch(textPassword.text.trim())) {
        return 1;
      } else {
        return -1;
      }
    }
    return 0;
  }

  int checkPasswordLetter() {
    if (state == ScreenState.registerUpgrade) {
      if (RegExp(r"^(?=.*[a-z]).{0,64}$").hasMatch(textPassword.text.trim())) {
        return 1;
      } else {
        return -1;
      }
    }
    return 0;
  }

  int checkPasswordLength() {
    if (state == ScreenState.registerUpgrade) {
      if (textPassword.text.trim().length >= 8) {
        return 1;
      } else {
        return -1;
      }
    }
    return 0;
  }

  int checkPasswordRepeat() {
    if (state == ScreenState.registerUpgrade) {
      if (textPasswordConfirm.text.trim() == textPassword.text.trim()) {
        return 1;
      } else {
        return -1;
      }
    }
    return 0;
  }

  bool checkEPP() {
    var email = textEmailController.text.trim().toLowerCase();
    if (email.isEmpty || !email.isValidEmail()) {
      return true;
    }

    var pw = textPassword.text.trim();
    if ((pw.isEmpty || !RegExp(r"^(?=.*\d)(?=.*[a-z]).{8,64}$").hasMatch(pw)) &&
        state != ScreenState.resetPassword) {
      return true;
    }

    var pww = textPasswordConfirm.text.trim();
    if ((pww.isEmpty || pww != pw) &&
        state != ScreenState.login &&
        state != ScreenState.resetPassword) {
      return true;
    }

    return false;
  }
}
